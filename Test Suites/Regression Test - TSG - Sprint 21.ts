<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - TSG - Sprint 21</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>de2deba4-6d63-4bc4-b9bb-e1a38256e8e4</testSuiteGuid>
   <testCaseLink>
      <guid>b1e991bb-11d9-4107-826c-2bc2d19e4f6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - AM not available</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e66b42c-4a4d-4ac8-891c-b5925922096e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - Covered (IP - Reimbursement)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c0b6c2d-b10e-400b-8e2f-6d3d1748fd64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - Covered (IP Cashless)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efff6309-278f-4a11-83b4-ab4daaf2f7e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - Covered (MA)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6178a7c2-1ac4-4f71-993c-8475e3da2d39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - Covered (ODC)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5eadd795-8c14-4870-872b-9ba07a42f368</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - Covered (ODS)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5248e9f7-72d5-4411-94d8-25163835b0d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - Covered (OP - Cashless)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0eb7bcd0-9783-4ef6-a4c5-2118452cede6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - Covered (OP - Reimbursement)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>447c4911-4e91-4a0f-a7df-b4b6d7b3187e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - Document Received</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0cb9e722-e112-4f9e-b0bd-e7ce193c28a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - Guaranteed but Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8dace623-8908-4522-b5e2-d71f88709af2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - Hold</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b416e6e6-9094-4ff1-9465-851efa96f3cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - AM - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ef59956-b911-4c16-b496-06c4c7431242</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - CCO - Covered (IP - Cashless)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4221ffc1-b146-443b-abd2-67092e79b79b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - CCO - Covered (IP - Reimbursement)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe3f9e97-dffe-40c0-ada6-6b23471a44b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - CCO - Covered (MA)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99ffb3ad-67cb-4894-84f4-f262a7639802</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - CCO - Covered (ODC)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d048bb26-c713-48fe-9f18-da65d5dc1481</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - CCO - Covered (ODS)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f911cdc-c57d-4ae6-a3b3-ca28509d9613</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - CCO - Covered (OP - Cashless)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83eaaef0-2e8c-4442-9660-1336af20df34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - CCO - Covered (OP - Reimbursement)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00f213de-196d-4ec4-b9e8-294821c9bee9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Doctor - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>066db519-c8b5-4c05-a36b-f6d65c8889b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Doctor - Doctor not available</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc85abc6-0355-44d5-9f52-c94693ade93d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Doctor - Hold</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3d34f9d-c7bc-4099-9f4c-45b4b8a49578</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Doctor - Need Confirmation Letter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0dc55e4-9b6d-4b66-99d5-5fb57dfa490a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Doctor - Need Others Documents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6960b840-b276-479a-9980-d47933ea68b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Doctor - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cad94186-38b5-47cc-9dbf-93306141e9f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Head CCO - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc45770d-b3f2-4f5d-9898-e6ee5f07bed7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Head CCO - Head CCO Not Available</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87f7e039-05af-4213-961f-44b2c3d4be1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Head CCO - Hold</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9d14b47-7d0b-4eb6-8f7f-b223a8d8d49f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Provider - Document Received</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cf6890c-6396-4a59-821f-4b6c08c382a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Provider - Hold</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e52ffebd-093f-449a-a079-3df3693a0db3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/FU - Diagnosis - Provider - Provider Not Available</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
