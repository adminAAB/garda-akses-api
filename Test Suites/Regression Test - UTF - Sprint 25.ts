<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - UTF - Sprint 25</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>99a8381d-2323-4a2b-83ce-782588aae057</testSuiteGuid>
   <testCaseLink>
      <guid>297c0eed-5117-4c14-8bc5-cb5f4113866c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/UTF/GA/Obat - 066 - MT Uncovered vs MTC Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5353ec4e-4cb8-46ea-bb43-296429d56c9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/UTF/GA/Obat - 067 - MT Uncovered vs MTC Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>489a3fd5-5d99-4a3c-83b9-f5fb4ec6deb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/UTF/GA/Obat - 068 - MT Covered vs Multi MTC Last Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06618c14-7a61-4722-9b36-38e7b694ea56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/UTF/GA/Obat - 069 - MT Covered vs Multi MTC Last Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d838b5ef-639e-4396-a491-56d636ec37fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/UTF/GA/Obat - 070 - MT Confirm vs Multi MTC Last Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>deae34dd-abc1-4878-9f42-7a28c5bedd56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/UTF/GA/Obat - 071 - MT Confirm vs Multi MTC Last Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cfbed47-84ee-40af-b470-aad0ebf231df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/UTF/GA/Obat - 072 - MT Uncovered vs Multi MTC Last Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29362da5-e8ec-4b86-839d-6fee5dccec6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/UTF/GA/Obat - 073 - MT Uncovered vs Multi MTC Last Need FU No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
