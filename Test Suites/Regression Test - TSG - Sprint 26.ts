<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - TSG - Sprint 26</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0baef7d2-ec92-4185-aa53-fd151ca76366</testSuiteGuid>
   <testCaseLink>
      <guid>8061e4c9-3595-404c-8b1e-46e157fa87fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/CCO-GA_ClaimFinalValidation - 073 - Obat Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1a869cb-97e3-4320-a0a7-02746e5dbeae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/CCO-GA_ClaimFinalValidation - 074 - Obat Covered - Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d17362de-03ab-45d0-bf6e-bf5b3e7aa1a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/CCO-GA_ClaimFinalValidation - 075 - Obat Covered - Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f08e42ab-e900-42f7-854d-2dd05b90c9fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 001 - Obat - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dec3ef11-4917-4bc8-9224-b3ad040fadb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 002 - Obat - Covered - Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>826234f0-97e7-4b80-9e43-599988df5f9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 003 - Obat - Covered - Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>834bc8fc-3a4f-4f7d-bb36-85ed2685966f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 004 - Obat - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1344b9fe-1a5f-46e5-af05-4fd9943690da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 005 - Obat - Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>199c6897-df0b-4bf7-8440-e8013982c2f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 006 - Obat - True - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8dca4bd-1882-4fda-b5cc-27328546e00e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 007 - Obat - True - Covered - Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c2ed603-3557-4737-a0ed-a5c8a7083468</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 008 - Obat - True - Covered - Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>138cb26f-4191-48e6-877d-d4152f52ef36</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 009 - Obat - True - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9b95f50-8571-43ab-9e3b-a49055e519aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 010 - Obat - True - Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25211b2b-d13f-45be-958e-b56dec0ba738</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 011 - Obat - False - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8fbf279-4ddb-4e92-99d9-4a2862a79bcb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 012 - Obat - False - Covered - Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0222b82-6d64-4c3b-a759-467cfedc0062</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 013 - Obat - False - Covered - Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0c30dc1-4375-4199-b36d-7eef2d6b7b1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 014 - Obat - False - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>865dae01-376a-482b-a795-37ffebeb574e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 015 - Obat - False - Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e21041a1-e153-4416-a7fb-1edd3cfb9441</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 016 - Obat - NoResult - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee25ab63-3590-4d7e-a802-2949afdd5732</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 017 - Obat - NoResult - Covered - Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9cec34bb-8f83-426f-89da-4e6be21a3206</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 018 - Obat - NoResult - Covered - Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed4e43a5-cab4-4b0f-921e-ff1df8c27731</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 019 - Obat - NoResult - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba8d459d-b5ec-4f95-a383-c9e4df904304</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 020 - Obat - NoResult - Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d92553b4-251f-41e5-bac5-9c4781c374ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 023 - Obat - Covered - Before Eff Date - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>432ab1b1-5219-4551-bf2b-1d4ba47099e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 024 - Obat - Covered - After Eff Date - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ec9d080-e476-478e-b5a5-beb8136461ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 025 - Obat - Covered - Client Tidak Sesuai - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>070e3375-fd5c-403e-9318-7be2cf620f9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 026 - Obat - Covered - Client Sesuai - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd882645-1e93-4203-9f0d-249f270359b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 027 - Obat - Covered - Class Tidak Sesuai - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbe7ca27-a7a0-4c04-b375-fd068b357c53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 028 - Obat - Covered - Class Sesuai - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5060c9f3-107c-4e83-ad89-347d170982bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 029 - Obat - Covered - Treatment Tidak Sesuai - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9c23bea-39a4-4850-96f3-3cf5aa94196e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 030 - Obat - Covered - Treatment Sesuai - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9123523a-5221-4675-9bb2-7de0aa13ac12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 031 - Obat - Covered - MAT001 - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c495e0e1-a88e-4e4e-bfb8-9190294558c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 032 - Obat - Covered - MAT002 - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cef021bc-64cd-4a5a-a6b1-d833c510a09a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 033 - Obat - Covered - MAT003 - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3eac4970-0548-4f77-ad2e-37cfd3d8904f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 034 - Obat - Covered - MAT004 - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ef30150-2cc7-44ee-8e9c-0616122cfa15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 035 - Obat - Covered - MAT005 - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9b53f0a-04c9-4087-8675-cf248e0cb30e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 039 - Obat - Covered - ODS - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6db70de-2d46-45b4-811e-e769c03e7962</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 040 - Obat - Covered - ODC - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b56e0be8-561f-4a44-87a0-17daa68a6202</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 041 - Obat - Billed Lebih Kecil LimitCC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a4501ed-bc88-4b45-970c-188f4f1d8ad3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 042 - Obat - Billed Sama Dengan LimitCC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fed09a9a-c516-44d0-9fa6-0d88c324fc12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 043 - Obat - Billed Lebih Besar LimitCC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08982984-fec4-44eb-9991-bfe6e13e805f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 045 - Obat - Billed Lebih Besar 1 Juta - Unknown Patient Condition</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c4c37b8-88dd-46a6-ab80-f50300b65fff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 047 - Obat - Billed Lebih Besar 1 Juta - Patient Condition terisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5fe44d4-06f0-43ee-995d-3c0dda0d50fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 048 - Obat - Covered - Conf AM Hold Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5acc16f7-f153-4fd7-bcde-fe3deed03a19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 054 - Obat - Protap Covid MT 1 vs Provider 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>17d73f80-e2bd-4d16-bc59-f8ff525ccfa4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 055 - Obat - Protap Covid MT 1 vs Provider 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>724f35be-90e0-4e1d-ae72-21c628877291</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 056 - Obat - Protap Covid MT 0 vs Provider 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4d45159-62b3-44cd-be4c-4db5cc2c1c88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 057 - Obat - Protap Covid MT 0 vs Provider 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0a101b6-dff3-48ab-8326-97aa15c5f4f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 058 - Obat - Covered - Conf Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>456cce74-5409-4d84-a994-db0cb3d31ac6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 059 - Obat - Confirm - Conf Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be1563f4-74d9-4c7e-93fb-f9487168d8b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 060 - Obat - Uncovered - Conf Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75e30f3f-6bee-4d44-b1b2-d54ab5ef00b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 061 - Obat - Covered - Conf AM Hold Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6e23e49-7e36-4e1c-a3e4-9a301b611c7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 062 - Obat - Covered - Conf AM Hold Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69507531-eaf7-4f32-9065-d5b21da86d15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 063 - Obat - Confirm - Conf AM Hold Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6dce10c-19b8-4b0a-a1a9-d155d572ee66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 064 - Obat - Confirm - Conf AM Hold Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bee68e1b-dbc5-4e03-8336-39b02776a15a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 065 - Obat - Uncovered - Conf AM Hold Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7bc2b89d-e4e6-46aa-b798-cf5ce03a04c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 066 - Obat - Uncovered - Conf AM Hold Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1e7e3fd-6d27-47b1-a7b1-9d00ea0a09cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 067 - Obat - Covered - Last Conf AM Hold Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f73fd99c-f13b-4b10-ad18-a8100e398eb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 068 - Obat - Covered - Last Conf AM Hold Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb8c87fb-0dcf-4eb2-bccb-0b8e9370d3ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 069 - Obat - Confirm - Last Conf AM Hold Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74c43b86-21f8-47f8-af48-b18b978507d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 070 - Obat - Confirm - Last Conf AM Hold Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36956a45-084e-404d-8a23-15ae8d5a6ace</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 071 - Obat - Uncovered - Last Conf AM Hold Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40edc61b-1891-4862-b69a-63448dbaa70e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/FU APTTO/ProcessGL - 072 - Obat - Uncovered - Last Conf AM Hold Need FU No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
