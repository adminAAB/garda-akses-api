<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - TSG - Sprint 23</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>34cf9bcd-0003-4bea-8f07-1574f9d61e75</testSuiteGuid>
   <testCaseLink>
      <guid>fbb685e3-8422-47d8-a159-4b867c4258ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 01 - DC Uncovered - IsNeedConf 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3368cfbd-697e-47d2-af2d-792cb06e59ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 02 - DC Uncovered - IsNeedConf 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70967c37-f29d-4411-9eb6-fe6f4ee02e23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 03 - DC Uncovered - IsNeedConf Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccd5acd1-6dd8-4602-aebc-e5041db833e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 04 - DC Guar but Uncov - IsNeedConf 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c9ae6ba-0946-4712-91bc-e134d32bc366</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 05 - DC Guar but Uncov - IsNeedConf 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>543e8ead-1878-47c1-a6fe-1255a569593c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 06 - DC Covered - IsNeedConf 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe265969-6a79-4a92-8d07-7cc2d29486d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 07 - DC Covered - IsNeedConf Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed3d9255-eb72-430c-82ec-502b360e73cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 08 - DC Covered - IsNeedConf Null - DG Addt Info Not Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75cdb609-c64d-430d-ad61-69f6e6c592c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 09 - DC Covered - IsNeedConf Null - DG Addt Doc Not Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e9b30f8-3a18-4381-b4a3-a1460df1570a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 10 - DC Covered - IsNeedConf Null - DG Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a983381-f10d-484d-af9f-870112ee978b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 11 - DC Covered - IsNeedConf Null - DG Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fabb889-5ab4-446d-93e0-93afcb9b7edc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 12 - PDE Covered 0 - DC Guar but Uncov 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d92d8211-f562-4939-ae9c-494c09645da1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 13 - PDE Covered 1 - DC Covered 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44ad955e-4c18-401a-9727-0810093bb38e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 14 - PDE Guar but Uncov 0 - DC Uncovered 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99a90a49-8413-48e2-a032-ae17964206de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 15 - PDE Guar but Uncov 1 - DC Guar but Uncov 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d58e9866-fd7c-404a-9fb8-e5553190166f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 16 - PDE Uncovered 0 - DC Covered 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bdf4b4a8-34b0-4001-92ef-1896d5606752</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 17 - PDE Uncovered 1 - DC Uncovered 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b6d31a4-fed3-4269-afe3-a484403155f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 18 - DC Covered 0 - DQ True Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>029bc1d0-dceb-4978-b83a-2d06203ed809</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 19 - DC Covered 0 - DQ True Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>199551d6-6492-4113-ad2c-5c28ad53c1c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 20 - DC Covered 0 - DQ True Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>950ed206-8de5-4816-882b-a468b3c4d3c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 21 - DC Covered 0 - DQ True Covered Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8470a0a-c159-40eb-b67a-22d2e21f658d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 22 - DC Covered 0 - DQ True Covered Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16c9cbb4-57e2-47f7-bf16-b23335894b78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 23 - DC Covered 0 - DQ False Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20b29aba-467e-47f5-99cc-d489effbb287</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 24 - DC Covered 0 - DQ False Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae7485e9-423c-4e33-8395-32c85c3142b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 25 - DC Covered 0 - DQ False Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fbbc9b1-a42b-498b-b34f-75ecef33b29a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 26 - DC Covered 0 - DQ False Covered Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90d7185f-b5e3-466f-aeef-5b46541727b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 27 - DC Covered 0 - DQ False Covered Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>995e07dc-bac2-422b-84f2-eadca83261eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 28 - DC Covered 0 - DQ No Result Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebeaf712-8345-45fd-bdc2-66ae1b3b1ab6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 29 - DC Covered 0 - DQ No Result Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e804cb1-27da-4798-b86e-8a0d4073af7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 30 - DC Covered 0 - DQ No Result Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1aa3e39b-01e3-4c3c-b970-91339a63555b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 31 - DC Covered 0 - DQ No Result Covered Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>253d62b1-601c-4b4c-b8d5-60e235accde1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 32 - DC Covered 0 - DQ No Result Covered Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbde72ca-fb6c-4447-ae3f-b6f03b29a2ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 33 - PDE Null - DC Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29281b95-e16c-4926-9722-fa7b3f79a84f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GA_ClaimFinalValidation_New - 34 - DC Uncovered 1 vs Uncovered 0</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
