<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - NBH - Sprint 24</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d13cb385-ef6f-486e-b92b-10dd001ff0da</testSuiteGuid>
   <testCaseLink>
      <guid>90e05382-57c2-4c6b-8493-98839b0e300b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b891df55-e140-4d57-8104-9a2cd358e0f7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>135febf3-789c-4b98-9979-d73ae8f8ae21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3da41452-fee6-4fd6-894b-2383ce68ae75</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3b359673-3527-4048-8114-c77a4aa9ed32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>46a31b00-8c39-4b2f-a3ea-fff68df490f6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f16c151b-6d14-4db9-9b02-30a15a861f7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7c70a9b8-acb9-410d-9696-f1d78f843cc5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>88cd2d37-ab9a-4fb5-b0ee-275a5bdc3628</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6d75378d-0433-4e96-8a0f-3062d7f4a64d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3176e5a8-00b2-4f68-a216-05fa9c499a06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>43a9b459-e72f-4963-89b7-65a5a7e3a1f3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c1e19ec4-7b04-49a0-9438-64be690a4388</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e28d8ea0-76d9-4277-8bcb-0efb4cb5cac8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>461eed01-5e9e-4693-be54-fff3bfdc2d6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cd79eb53-a77e-4239-b003-e0ed5591d782</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b1b9383c-ecb3-451c-924e-16570f40e60e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a29a2add-c973-432a-b8a5-e3dec57db994</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>275cfb70-a086-4cef-b58b-a4c54575be5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-10</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c9f42a18-4bb2-433a-a0d8-acf08e3186a6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e80efef8-0c52-4ac3-925e-87668ae2e568</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-11</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cfe28cdd-bacb-4b06-b5a9-a79acf0af9db</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>bcad1912-8eaa-4789-9474-b536481b47ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-12</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9e49d6c6-79bf-4b47-b2af-c0d3714b096b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a29f7a58-234f-490b-bdf0-87d1c8e5bcdd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-13</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>06f5dad1-a031-4942-9703-0a7aec623cd3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cd19d740-b8fc-4da8-94dc-7b4a1a66ace2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-14</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ed8f09d7-1e23-48f1-bb8e-6ddcf5e8ce8a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ae7a6ab8-64a7-49d2-8461-b65fb8b1116b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-15</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b6f0d0ad-f6eb-40fc-92e2-c6cce282ab8c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>49628939-2cce-4237-9234-586faa16c2a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-16</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8e1e9979-b2c4-49e4-9396-3f1d13bf66f9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e5baed6d-42d7-428a-8c67-5b403d43b729</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-17</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a3f8ab1c-2998-4518-b375-079d39b2691d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7c4a5df4-7a1d-4bc9-9f09-6f76da160396</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-18</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>345fb27d-7e14-4f00-b81c-32456bc6e840</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>829f2790-d8a1-43d3-a94a-4146c9c57257</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 24/NBH - TS-0042-21-19</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5dd6fd15-bbf6-4de9-8591-a5b20e74fa0e</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
