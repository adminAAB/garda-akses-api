<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - RRU - Sprint 19</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8ff4a860-962c-4b28-8289-d5304701dab7</testSuiteGuid>
   <testCaseLink>
      <guid>16386e21-589c-4a03-8a15-931aa4938524</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/GMA/root/Signin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e296572-9e01-4544-b76c-60b26c8ed87b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/GA_ClaimFinalValidation - Validasi Product IP - Client EGL - Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1640404-97d7-4d8d-af13-c708d6cc0701</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/GA_ClaimFinalValidation - Validasi Product IP - Client EGL - Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c17f162a-93ea-43d4-9c22-146b0caa4899</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/GA_ClaimFinalValidation - Validasi Product IP - Client Non EGL - Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e57a28fb-0d11-48b8-8edd-a46ee4c794ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/GA_ClaimFinalValidation - Validasi Product IP - Client Non EGL - Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>202d0870-8bb5-4f5a-80eb-d99d13f194c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/GA_ClaimFinalValidation - Validasi Product MA - Client EGL - Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24e0ee8c-e3ba-43ba-a90b-f12a99c3a9f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/GA_ClaimFinalValidation - Validasi Product MA - Client EGL - Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d4c0699-d1ec-472c-b124-c2163f5d407b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/GA_ClaimFinalValidation - Validasi Product MA - Client Non EGL - Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1bde255-c348-4430-9303-9d448b43ea17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/GA_ClaimFinalValidation - Validasi Product MA - Client Non EGL - Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef89530f-84cc-45e8-bb57-95855f9bd1fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/SaveTempGL - Validasi Product</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
