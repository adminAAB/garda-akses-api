<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - RRU - Sprint 23</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ae36f1e7-ee3a-4a91-ba2e-17ab66a34003</testSuiteGuid>
   <testCaseLink>
      <guid>a07e9903-fb11-4360-ae36-adb97885d9fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 01 - OP - Passed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12362b3e-0ced-4a78-bfd5-2bbfec420682</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 02 - OP - Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62248ab9-71b3-4f26-afa3-42f2fefdb3e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 03 - DT - Passed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6cadade-675c-4944-875b-91274c2df179</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 04 - DT - Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3239678-be79-493d-a3f0-75763895a901</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 05 - MCU - Passed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b43b3f30-2db7-4613-89cf-ed8a44487513</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 06 - MCU - Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02667856-8adb-4d41-9bc0-ed66cbc6efa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 07 - IM - Passed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ffa730a0-0b24-485a-a580-673c24b94bad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 08 - IM - Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01255475-6a0a-4877-939b-b242e6dc6e93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 09 - FP - Passed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff561ea3-7ef5-47ad-b41d-d1dfcb285539</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 10 - FP - Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4cfe513-b0c1-4aa3-8471-46c192941a7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 11 - GL - Passed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c26b9773-acf0-4e3c-8f07-f40664578e30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 12 - GL - Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6485d4bf-56fa-4208-8acd-e42cda414122</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 13 - DISP - Passed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d101189b-3b8a-40a8-a826-1ec813360666</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - EligibilityDischarge - CheckBenefitMember - 14 - DISP - Failed</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
