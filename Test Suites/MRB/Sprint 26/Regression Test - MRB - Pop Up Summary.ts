<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - MRB - Pop Up Summary</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0df9ef6e-fa04-4a90-b905-6e66598f5d1f</testSuiteGuid>
   <testCaseLink>
      <guid>716f6504-a3ef-484d-a2f4-93f45061c904</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GA/Pop Up Summary - Dokter Garda Medika</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28241610-5bd2-4fa3-ba21-db44725f358a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GA/Pop Up Summary - Dokumen Tambahan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8dcab0ab-e2fe-44af-96dc-3933c4b42c51</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GA/Pop Up Summary - Kelengkapan Informasi APTTO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3aea965-f60e-4b61-ad71-24c49a49addc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GA/Pop Up Summary - Konfirmasi Billing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5fabe6a4-2762-476e-ac61-033a75369f86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GA/Pop Up Summary - Konfirmasi Garda Medika</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1736ce7-3069-4e26-af22-41670febaaa7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GA/Pop Up Summary - Protap Covid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>763a0384-024f-41ea-bf63-f44922180cbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GA/Pop Up Summary - Tindakan ODS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42f2e11c-c645-41ca-9318-c33611d0179c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GA/Pop Up Summary - Variasi 1 - Protap - Billing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d31f051-ad73-44fa-845e-7cdd1b891570</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GA/Pop Up Summary - Variasi 2 - Kelengkapan APTTO - Billing - Dokumen Tambahan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4c859e7-20bc-4fa6-8015-b269aa133e93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GA/Pop Up Summary - Variasi 3 - ODS - Dokumen Tambahan - Protap Covid - Konfirmasi Dokter - APTTO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc1357f4-f8e3-4804-b6c8-742596f1f41f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GA/Pop Up Summary - Variasi 4 - Show All</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ea86193-7190-4b3c-a0e8-2cc94dc3a5df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GMA/Pop Up Summary - Dokter Garda Medika</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db413697-e739-4f1b-97ed-ed092a81217d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GMA/Pop Up Summary - Dokumen Tambahan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>264aafa9-57eb-419e-9760-fdd602c0abcb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GMA/Pop Up Summary - Kelengkapan Informasi APTTO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b76bfba-3d92-4db5-916f-62a827de3583</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GMA/Pop Up Summary - Konfirmasi Billing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc0ceb2e-4feb-40de-8458-c2b690c93510</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GMA/Pop Up Summary - Konfirmasi Garda Medika</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f59a10c5-87a4-418b-97ac-6faa7ad9fa2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GMA/Pop Up Summary - Protap Covid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e21b83f-8467-4799-be72-b4116a451a10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GMA/Pop Up Summary - Tindakan ODS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94237b86-c3b6-4346-999e-8152824aeea9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GMA/Pop Up Summary - Variasi 1 - Protap - Billing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b88408f9-5fdf-4ffc-8301-6e81d786b74c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GMA/Pop Up Summary - Variasi 2 - Kelengkapan APTTO - Billing - Dokumen Tambahan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9041590d-9a27-4c7a-96a7-9c5810937a86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GMA/Pop Up Summary - Variasi 3 - ODS - Dokumen Tambahan - Protap Covid - Konfirmasi Dokter - APTTO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a38b3db-1d96-4dbc-b611-4ee8fb9d6231</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Pop Up Summary/GMA/Pop Up Summary - Variasi 4 - Show All</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
