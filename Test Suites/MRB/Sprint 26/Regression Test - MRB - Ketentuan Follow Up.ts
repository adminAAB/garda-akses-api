<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - MRB - Ketentuan Follow Up</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>753e6127-4791-4290-b987-dcb26cb7ec05</testSuiteGuid>
   <testCaseLink>
      <guid>26b507b3-2a89-4f30-87d7-373ed305035f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/APTTO 0 - GA - Akhir</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed111a36-ba38-4772-afd7-7051600e4278</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/APTTO 0 - GA - Awal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4cf459f4-b632-4a1d-b872-aa167f6e0fb5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/APTTO 0 - GA - Revisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67426539-01ab-481e-81d8-765b801e809a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/APTTO 0 - GMA - Akhir</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d5e2a5d-dc72-4a34-964f-aff5a47168c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/APTTO 0 - GMA - Awal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eefe8ee5-d7bc-4d00-87c9-358cf73f6309</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/APTTO 0 - GMA - Revisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0cd3949-cfa0-4862-a200-37cff2af4501</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/APTTO 1 - GA - Awal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5dd2e23c-4e6a-4aaf-8ffe-e25e6318a29c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/APTTO 1 - GA - Revisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2dfa03ac-1575-4c46-94b3-951a8ba55207</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/APTTO 1 - GMA - Awal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4cf728c0-b29f-44ef-8237-66c31e7b23be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/APTTO 1 - GMA - Revisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33ece2da-7c73-468c-b494-90ce72a23931</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Account Manager - AM Not Available</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc73f728-8123-4231-96b2-94b6e200a212</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Account Manager - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e8829a9-f3ce-486e-8033-eb0856333e70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Account Manager - Document Received</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8875981f-091d-460a-bd5e-8655f8177452</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Account Manager - Guaranted But Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>013afda6-bf3c-4f28-9936-692510eb6324</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Account Manager - Hold</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8446fcb6-6bc0-4cd2-9787-538e4e64b3a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Account Manager - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fcd1dce-aaf8-4810-902e-5f86ddec60c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Contact Center - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e202e01a-7a14-4334-9b58-2cc29c62ceef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Doctor - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e598f964-0c1f-4f3c-a016-9cf7114095ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Doctor - Doctor not available</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f51ddd4-188c-448a-97dd-5a4f375f2850</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Doctor - Hold</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8683b5e-00af-4582-8263-f0431068de76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Doctor - Need Confirmation Letter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb0db0c1-11ec-4f63-a47f-397388d6707e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Doctor - Need Others Documents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cc5a2f6-39d8-4412-871c-8a260db48f0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Doctor - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd0189a2-0ced-4400-a309-4882c4bbbf0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - HeadCCO - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3f5f7ce-0c9f-4a47-96c9-4cec4e3bfcf7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - HeadCCO - Head CCO Not Available</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c929df1-4c93-4cc2-8f52-914daa405f94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - HeadCCO - Hold</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0780001-e70b-466e-913e-e7e362b2dfa3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Provider - Document Received</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6420b2c4-ff79-494c-ac93-0790cc9e48f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Provider - Hold</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57197a26-9994-4c33-afd0-7cef040e2347</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Ketentuan Follow Up/Matriks - Provider - Provider Not Available</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
