<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - MRB - Validasi Medical Treatment Tidakan Obat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>df0c37b9-37d2-4c26-961e-d23c0fea7b87</testSuiteGuid>
   <testCaseLink>
      <guid>89eeda5a-1f19-4645-8469-8f60d3efe304</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/001 APTTO - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e636456-ca7b-47c5-82c1-822ffaa0e815</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/002 APTTO - Covered - AddInfo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18c7318b-6a84-434a-8ed2-caad4fdcae19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/003 APTTO - Covered - AddDoc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>232b6824-7f9d-4475-bf31-f99c3db1a1f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/004 APTTO - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa76d6f4-071a-437d-9285-dfa52a9bf12c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/005 APTTO - Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73d68b6e-1d3d-4990-95d1-fbb040050698</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/006 APTTO - True Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b120fe2b-4523-441b-849d-23c958ff61b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/007 APTTO - True Covered - AddInfo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bedca67b-e7fc-46e5-af37-1ed34968a93d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/008 APTTO - True Covered - AddDoc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e633fb0d-0630-4b31-9539-8e281491e820</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/009 APTTO - True Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>223993f4-88bb-4076-bac9-b8d7a0b6bfb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/010 APTTO - True Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>647572e3-2389-47f1-b7a5-e35f14bd585e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/011 APTTO - False Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e69d626-f765-496d-9eb1-b07ac7f2b192</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/012 APTTO - False Covered - AddInfo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1dcde4fb-15cd-48ba-857c-b49f5a921694</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/013 APTTO - False Covered - AddDoc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>486ee111-e283-41b3-ae6d-1ffec6d94ab4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/014 APTTO - False Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdfcbf85-f4fb-45d1-bfc6-5a97eaf7cf4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/015 APTTO - False Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56dcfb6c-a490-46fb-b865-87a247e939c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/016 APTTO - Unknwon Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efb6330e-0c6b-4084-9981-b0851be0a294</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/017 APTTO - Unknwon Covered - AddInfo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44e4a62a-6566-4ba9-8d35-0f549d235d7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/018 APTTO - Unknwon Covered - AddDoc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2534a9e-6aa2-4ccb-b520-207efef30f44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/019 APTTO - Unknown Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6160d232-940b-4936-a9c4-fc4bddf80f64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/020 APTTO - Unknown Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ed08dea-9192-4561-bbad-afbe649d91f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/021 APTTO - Effective Date - Sesudah</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0024807-9aa9-4056-b6c1-10ab391c298d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/022 APTTO - Effective Date - Sebelum</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a52e790-4af6-4b6a-8393-62440af14f63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/023 APTTO - Client ID - Sesuai</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9127dad1-a7ce-4a94-af5b-29ad5bf05ef2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/024 APTTO - Client ID - Tidak Sesuai</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9013612c-230f-48ce-98a2-5fb2705d4602</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/025 APTTO - Client Class - Sesuai</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29d40f45-8515-405a-a35d-0a3c5dc33731</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/026 APTTO - Client Class - Tidak Sesuai</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>727901c2-061f-45d4-b275-e811499872bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/027 APTTO - Category - All</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e475558f-fb4e-4717-8d3a-b3bdc81729f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/028 APTTO - MAT001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5522edb5-6e49-4bc3-bb61-b3d663cb7e0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/029 APTTO - MAT002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39bc9774-8861-4060-8243-309a72bf7553</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/030 APTTO - MAT003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88010a33-08c7-4404-ad60-7ce74587cf9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/031 APTTO - MAT004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20901314-24fd-4a29-a8ae-9c9f30305d9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/032 APTTO - MAT005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18e36b85-d2e1-4796-91f5-ba57c2b7ef23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/033 APTTO - ODS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8044d0c-ce28-4d78-b5f0-37dd33253cea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/034 APTTO - ODC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab4c16e5-19fe-4943-aa47-29f5e6d2d702</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/035 APTTO - Unregistered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e9eb1a3-a334-477b-85b6-95c63b7acb9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/036 APTTO - Total Billed Kurang Limit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a32aa50-94ff-46c1-a178-f003e4f381f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/037 APTTO - Total Billed Sama dengan Limit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4fbdbb4-a308-4b26-ad8d-191494ce51a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/038 APTTO - Total Billed Lebih Limit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78095fa7-d98d-4bac-ad8e-7fc768cf8cdb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/039 APTTO - Total Billed Unknown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>afd440c1-f463-40c4-99d0-0ad69512e330</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/040 APTTO - Patient Condition</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c0ff24a-7e4d-461b-b2a4-40a650d91df5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/041 APTTO - Patient Condition Unknown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6cde3315-b293-4d62-acf5-d1ba7d6bb24e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/042 APTTO - ProtapCovid MT 1 PV 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cced4dde-d7d1-47ea-b4b4-3319d68c06de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/043 APTTO - ProtapCovid MT 1 PV 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf560be4-7958-4e27-ad29-d4e53ea6e4e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/044 APTTO - ProtapCovid MT 0 PV 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e4794aa-648b-40be-8e13-1ab129db299b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/045 APTTO - ProtapCovid MT 0 PV 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1dbcd786-9a16-49ec-840b-65bb70dbea95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/046 APTTO - MT Covered - MTC Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c1c8616-3268-42c5-9333-7b4a9796b251</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/047 APTTO - MT Confirm - MTC Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ced760bb-39b2-44cb-b911-69306a8ca1c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/048 APTTO - MT Uncovered - MTC Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b78a9f7c-9704-4687-bd33-e7f61717e1d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/049 APTTO - MT Covered - MTC Need FU - Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a71e21e1-f107-4b88-b31f-7b5c8b30241e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/050 APTTO - MT Covered - MTC Need FU - No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33263303-1df5-47e0-b897-9c2221f6be0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/051 APTTO - MT Confirm - MTC Need FU - Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37288305-909f-45e3-a79e-57c366d65883</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/052 APTTO - MT Confirm - MTC Need FU - No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77afe0f8-f401-4fd0-beb2-100ba0486f69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/053 APTTO - MT Uncovered - MTC Need FU - Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f863d7b5-f8fe-40e2-b640-4042f7cc3767</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/054 APTTO - MT Uncovered - MTC Need FU - No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>766d845c-7d5d-4980-b39e-6364da32ae96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/055 APTTO - Multiple MT Covered - MTC Need FU - Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68b17cfc-7908-4d05-b37c-1ee5bef1ea6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/056 APTTO - Multiple MT Covered - MTC Need FU - No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42616631-1517-4c4e-8abf-b936a1ec351b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/057 APTTO - Multiple MT Confirm - MTC Need FU - Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03d279e5-70d8-4728-8dff-f6374f438c90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/058 APTTO - Multiple MT Confirm - MTC Need FU - No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9d6b5de-eeed-4056-8af7-5708acca9e94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/059 APTTO - Multiple MT Uncovered - MTC Need FU - Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>484553d6-6589-4151-9fae-4e9ed4708ddb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/060 APTTO - Multiple MT Uncovered - MTC Need FU - No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a6a3b6c-5963-4648-a066-5e54eafb3272</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/061 APTTO - Need Doc Mandatory - NeedFU Yes - Belum Upload - Latest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc555e50-f9e7-49f2-a2f2-0edf0f29c0dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/062 APTTO - Need Doc Mandatory - NeedFU Yes - Belum Upload - Not Latest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb80736e-3618-436e-b9cc-daa0c02bcdbb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/063 APTTO - Need Doc Mandatory - NeedFU Yes - Sudah Upload - Latest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3eb3310-edb2-4b4e-9972-204d0ba34613</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/064 APTTO - Need Doc Mandatory - NeedFU Yes - Sudah Upload - Not Latest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a440a45-3084-40ae-8c3f-ca4bdd280198</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/065 APTTO - Need Doc Mandatory - NeedFU No - Belum Upload - Latest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d10ae40-7848-4395-ab33-cc6e6c77bc5e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/066 APTTO - Need Doc Mandatory - NeedFU No - Belum Upload - Not Latest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e344639-9a11-4a00-b0b6-fc1ab8c17e5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/067 APTTO - Need Doc Mandatory - NeedFU No - Sudah Upload - Latest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71ddab96-68f6-4831-98fd-fd8fe90482ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 26/MRB/Validasi Medical Treatment Tidakan Obat/068 APTTO - Need Doc Mandatory - NeedFU No - Sudah Upload - Not Latest</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
