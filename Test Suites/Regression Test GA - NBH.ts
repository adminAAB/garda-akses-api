<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test GA - NBH</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>abhaskoro@asuransiastra.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a01a4da4-916d-4ee1-aa64-9f04089cb6d7</testSuiteGuid>
   <testCaseLink>
      <guid>85a5686d-b1b3-4aa9-9cda-a019c369d927</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/Login - Berhasil</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>968ea1a4-d37d-4383-8738-35ab685bd764</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/Login - Gagal Username Salah</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3567dd1c-5aac-4546-99c2-73fd36cee010</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 19/Login - Gagal Password Salah</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37abeeb2-7d37-41aa-8a42-45119bf76ebd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 20/Create Ticket - Provider Health Claim</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
