<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>GMA - EligibilityDischarge</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3def9504-ba36-47a8-ac4e-046625c0c78a</testSuiteGuid>
   <testCaseLink>
      <guid>37cab440-532e-4ece-bcf3-bfb856cc9ebd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 22/GMA - Eligibility - Discharge - OP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fbd22a4-1aac-4be7-918c-3c326b542531</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - Eligibility - Discharge - DT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83fd92d5-f884-4a58-9029-1a76644aaf90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - Eligibility - Discharge - MCU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da8b3b0d-3962-4dde-a201-2d9b9143ac34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - Eligibility - Discharge - IM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a48f18ff-0d38-4413-9abc-81eb960128dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - Eligibility - Discharge - FP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c3dcef7-d291-4cf2-9c94-1d1d56ce165a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - Eligibility - Discharge - GL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5133623b-09bb-401b-9c9e-0ffc7e6ad483</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 23/GMA - Eligibility - Discharge - DISP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e1afa88-f76c-4278-aa6d-ae9f8d88d1af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 22/GMA - Eligibility - Void - OP</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
