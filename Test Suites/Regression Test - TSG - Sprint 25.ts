<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Test - TSG - Sprint 25</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d7ab1b60-a48f-4756-872c-eb4b76b7fc09</testSuiteGuid>
   <testCaseLink>
      <guid>1f569540-f28a-48f4-80ab-3108a2e019cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 001 - Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>effd935e-039c-4fc0-aaa3-b1d92707bbe2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 002 - Covered - Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28ace5d8-a1bb-4696-bca6-3dafde5691c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 003 - Covered - Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c824fe9c-a6ad-48d5-89e5-4ffdb726a11a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 004 - Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>047421ec-0bcc-4448-8061-ed17071e6e10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 005 - Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ca98900-dd84-4382-bfbf-ccf2a15405a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 006 - Question True Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6c084dd-2e34-45da-91e6-614963bff1e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 007 - Question True Covered - Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>245bba4c-9356-439f-8ed0-87735b111cae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 008 - Question True Covered - Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1595298a-aee3-4265-9a63-ca00cdb2289b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 009 - Question True Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58de15ed-22d3-450a-b212-6bf19bdbffb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 010 - Question True Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>161c7243-f0af-4b62-a718-a577c29d7200</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 011 - Question False Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8602b154-1ad0-41ce-9a28-433d5af551e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 012 - Question False Covered - Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>518aa7b9-e1ed-4457-b2e0-bb7f195fb84f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 013 - Question False Covered - Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa535795-379e-4231-b2df-604f1a0f7f4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 014 - Question False Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cbe5af04-549b-4a68-8694-a595abcbff5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 015 - Question False Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd867317-c1ff-4a10-96c6-26db924f859a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 016 - Question NoResult Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42b40ac0-bcdd-4920-83c7-9a2794c4f986</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 017 - Question NoResult Covered - Addt Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67dff246-52c1-4770-ad26-944e15f8059d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 018 - Question NoResult Covered - Addt Doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62246120-4c30-4332-9f8b-7380ccc5071a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 019 - Question NoResult Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d90b5fb-59b5-4f3b-aa88-22d1f273cb11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 020 - Question NoResult Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6afcc1cf-ffb1-47c9-a111-4a48f7c8423b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 021 - Unregistered Medical Treatment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b927408b-991e-4cfc-ae6a-9bce7cf10492</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 022 - Non Mapping All Covered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33562b3d-5ab6-4d8b-b966-4f9f07bf9bfa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 023 - MT Covered - CT Uncovered Before Eff Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96268ccb-8a06-452a-9573-6ba3f3431d0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 024 - MT Covered - CT Uncovered After Eff Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>138d3c14-4293-4007-aff5-55b326238680</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 025 - MT Covered - CT Uncovered Client Tidak Sesuai</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77e0a4a4-11fb-4f97-a2e6-c4e21197bbb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 026 - MT Covered - CT Uncovered Client Sesuai</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b797f4fc-2b1a-432c-a2b6-7fe373e5c17e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 027 - MT Covered - CT Uncovered Class Tidak Sesuai</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50578dfe-ed11-4796-8eb2-2c87329529ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 028 - MT Covered - CT Uncovered Class Sesuai</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b3c4ded-4e54-4699-9180-8ebb904dc7e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 029 - MT Covered - CT Uncovered Treatment Tidak Sesuai</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93be7c42-bafd-431e-9b78-73f71baa81c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 030 - MT Covered - CT Uncovered Treatment Sesuai</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f87746a-6267-41d0-bf1a-63cbb95badba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 031 - MA MAT001 MT Covered - CT Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbd2443c-92f4-47df-99bb-79b0f617971a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 032 - MA MAT002 MT Covered - CT Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0829e28c-dd2e-4d52-816b-99d1ff133c12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 033 - MA MAT003 MT Covered - CT Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92f70b83-e291-42c8-aaa9-43d270a996d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 034 - MA MAT004 MT Covered - CT Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a90c7d6-7b2c-4495-86ed-7eebc66496b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 035 - MA MAT005 MT Covered - CT Uncovered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa69f035-aa64-4786-b9df-faafd6490fe0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 041 - Billed Lebih Kecil ApprovalLimitCC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>048b0fa1-48f8-4d35-a2ca-576a26b6db2c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 042 - Billed Sama Dengan ApprovalLimitCC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01e4a932-3829-4483-ae52-25c1e3744234</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 043 - Billed Lebih Besar ApprovalLimitCC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ab82b4b-70b2-43d3-ab0c-e62e554fa24b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 044 - Unknown Billed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>783d3bf3-a8ff-4223-94be-70baf0a6f339</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 045 - Unknown Patient Condition</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f870da76-b67f-41b6-9870-1a8f2ee9a871</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 046 - Patient Condition</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4782aff4-f255-402a-8d4c-402ca711d255</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 047 - Patient Condition Terisi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f591506-faea-489a-a461-12d61f4a6138</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 048 - Covered - AM Hold - Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6159d98-39f8-4c3c-80c3-43e2cb30a81d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 049 - Covered - Doctor Need Oth Doc Yes - Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87e1aae8-934c-4e5e-ae3d-c70e926b0f74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 054 - MT Covid 1 - Provider 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa1cbc14-44cc-4154-b6e7-cc4adb028ab4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 055 - MT Covid 1 - Provider 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee917af4-21f3-4702-9396-b7a614369666</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 056 - MT Covid 0 - Provider 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58dfbb2a-fec3-447f-8965-9795e6887df9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 057 - MT Covid 0 - Provider 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7366848-af84-444f-a458-5cba5d4304fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 058 - MT Covered - Conf Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7033e72b-0dc5-4cbf-acaa-1f32d76cc476</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 059 - MT Confirm - Conf Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd46c770-0497-4556-8d23-df1e0718668a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 060 - MT Uncovered - Conf Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a99a4f5c-9c18-4a36-a5f6-47a85c961cfd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 061 - MT Covered - Conf Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e7c4965-e3aa-4c43-b57c-50d0230caac8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 062 - MT Covered - Conf Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>127e00ec-4455-4f34-a06c-a834c66a8d6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 063 - MT Confirm - Conf Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f7451c0-b132-465b-8381-90142bf8145a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 064 - MT Confirm - Conf Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e9a61f8-36be-4bbb-bb59-b3ab016399d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 065 - MT Uncovered - Conf Need FU Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9dfb61c-a08e-4c86-952a-6b7a8a29f0be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 066 - MT Uncovered - Conf Need FU No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1fa34252-3d58-46a8-86b3-2ea372dc1a03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 067 - MT Covered - Conf 1 No - Conf 2 Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebdc8e86-400d-4a97-b9d9-a780d10e2290</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 068 - MT Covered - Conf 1 Yes - Conf 2 No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6aed0a79-fdf4-40ad-a48e-5f9b0a8acec5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 069 - MT Confirm - Conf 1 No - Conf 2 Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb18f870-6527-4263-81ed-28dea96083ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 070 - MT Confirm - Conf 1 Yes - Conf 2 No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>206ee727-e853-4f77-b6f9-a9c37e7a0c8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 071 - MT Uncovered - Conf 1 No - Conf 2 Yes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c01f8e4-2489-4316-bdbb-8000c8cfe735</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Sprint 25/APTTO/GA/GA_ClaimFinalValidationAPTTOInTreatment - 072 - MT Uncovered - Conf 1 Yes - Conf 2 No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
