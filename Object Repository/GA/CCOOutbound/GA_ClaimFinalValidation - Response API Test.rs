<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>RRU - Sprint 24</description>
   <name>GA_ClaimFinalValidation - Response API Test</name>
   <tag></tag>
   <elementGuidId>f193319a-b2ab-40d9-abbd-a33951559a47</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;O/00005308&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousTrID&quot;,
      &quot;value&quot;: &quot;2824837&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentDate&quot;,
      &quot;value&quot;: &quot;2021-03-10&quot;
    },
    {
      &quot;name&quot;: &quot;ProductType&quot;,
      &quot;value&quot;: &quot;IP&quot;
    },
    {
      &quot;name&quot;: &quot;DiagnosisCode&quot;,
      &quot;value&quot;: &quot;W01&quot;
    },
    {
      &quot;name&quot;: &quot;DiagnosisAdditionalInfo&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderID&quot;,
      &quot;value&quot;: &quot;TJKRH00013&quot;
    },
    {
      &quot;name&quot;: &quot;PatientPhone&quot;,
      &quot;value&quot;: &quot;0987654321&quot;
    },
    {
      &quot;name&quot;: &quot;CallerName&quot;,
      &quot;value&quot;: &quot;-&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoom&quot;,
      &quot;value&quot;: &quot;KELAS III&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomAmount&quot;,
      &quot;value&quot;: &quot;203000&quot;
    },
    {
      &quot;name&quot;: &quot;DoctorName&quot;,
      &quot;value&quot;: &quot;Dele&quot;
    },
    {
      &quot;name&quot;: &quot;RoomOption&quot;,
      &quot;value&quot;: &quot;On Plan&quot;
    },
    {
      &quot;name&quot;: &quot;RoomAvailability&quot;,
      &quot;value&quot;: &quot;NONE&quot;
    },
    {
      &quot;name&quot;: &quot;UpgradeClass&quot;,
      &quot;value&quot;: &quot;NONE&quot;
    },
    {
      &quot;name&quot;: &quot;BenefitAmount&quot;,
      &quot;value&quot;: &quot;450000&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderEmail&quot;,
      &quot;value&quot;: &quot;ehu@beyond.asuransi.astra.co.id&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderFax&quot;,
      &quot;value&quot;: &quot;0218560601&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderPhone&quot;,
      &quot;value&quot;: &quot;081806221986&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderExt&quot;,
      &quot;value&quot;: &quot;121&quot;
    },
    {
      &quot;name&quot;: &quot;IsTiri&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;DefaultProviderID&quot;,
      &quot;value&quot;: &quot;TJKRH00013&quot;
    },
    {
      &quot;name&quot;: &quot;Remarks&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;ServiceTypeID&quot;,
      &quot;value&quot;: &quot;PRI&quot;
    },
    {
      &quot;name&quot;: &quot;Guid&quot;,
      &quot;value&quot;: &quot;adc2d999873a423483481261e3a6e3f0549fa7d2741bb2f58dd1069e3aa72896&quot;
    },
    {
      &quot;name&quot;: &quot;TicketNo&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;GLType&quot;,
      &quot;value&quot;: &quot;GL Awal&quot;
    },
    {
      &quot;name&quot;: &quot;AccountManager&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;TotalBilled&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;ClientID&quot;,
      &quot;value&quot;: &quot;CJKPA11&quot;
    },
    {
      &quot;name&quot;: &quot;ClassNo&quot;,
      &quot;value&quot;: &quot;2&quot;
    },
    {
      &quot;name&quot;: &quot;MembershipType&quot;,
      &quot;value&quot;: &quot;2. SPO&quot;
    },
    {
      &quot;name&quot;: &quot;AllDiagnosis&quot;,
      &quot;value&quot;: &quot;[[1,\&quot;W01\&quot;,\&quot;Fall on same level from slipping, tripping and stumbling\&quot;,\&quot;Initial Primary\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Uncovered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,0,\&quot;1\&quot;,\&quot;0\&quot;],[2,\&quot;W05\&quot;,\&quot;Fall involving wheelchair\&quot;,\&quot;Initial Primary\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Uncovered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,0,\&quot;1\&quot;,\&quot;0\&quot;]]&quot;
    },
    {
      &quot;name&quot;: &quot;AllDiagnosisAdditionalInfo&quot;,
      &quot;value&quot;: &quot;[[],[]]&quot;
    },
    {
      &quot;name&quot;: &quot;AllDoctors&quot;,
      &quot;value&quot;: &quot;Dele&quot;
    },
    {
      &quot;name&quot;: &quot;Gender&quot;,
      &quot;value&quot;: &quot;F&quot;
    },
    {
      &quot;name&quot;: &quot;DOB&quot;,
      &quot;value&quot;: &quot;1990-07-19&quot;
    },
    {
      &quot;name&quot;: &quot;NewMemberName&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousGuid&quot;,
      &quot;value&quot;: &quot;bd4bf2e05d7c4e8984fb2f083fe970a52c23e0d5342d1a3245dcbf9cf8e43a53&quot;
    },
    {
      &quot;name&quot;: &quot;GLStatus&quot;,
      &quot;value&quot;: &quot;Not Produce - Need Follow Up&quot;
    },
    {
      &quot;name&quot;: &quot;EmpMemberNo&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;NMEmpID&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;FollowUpTaskID&quot;,
      &quot;value&quot;: &quot;bd4bf2e05d7c4e8984fb2f083fe970a52c23e0d5342d1a3245dcbf9cf8e43a53&quot;
    },
    {
      &quot;name&quot;: &quot;IsClient&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;CallStatusID&quot;,
      &quot;value&quot;: &quot;Not Need Follow Up&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClass&quot;,
      &quot;value&quot;: &quot;KELAS III&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRate&quot;,
      &quot;value&quot;: &quot;203000&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentEnd&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClassChoosen&quot;,
      &quot;value&quot;: &quot;KELAS III&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRateChoosen&quot;,
      &quot;value&quot;: &quot;203000&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomChoosen&quot;,
      &quot;value&quot;: &quot;KELAS III&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomAmountChoosen&quot;,
      &quot;value&quot;: &quot;203000&quot;
    },
    {
      &quot;name&quot;: &quot;IsReferral&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;IsSpecialCondition&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;ReferralReasonCode&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;CallInStart&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;SecondaryDiagnosisCode&quot;,
      &quot;value&quot;: &quot;[\&quot;W05\&quot;]&quot;
    },
    {
      &quot;name&quot;: &quot;ClientClassNo&quot;,
      &quot;value&quot;: &quot;2&quot;
    },
    {
      &quot;name&quot;: &quot;UserPosition&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;PopUpMsg&quot;,
      &quot;value&quot;: &quot;\u003cbr\u003e\u003cb\u003eValidasi Medical Treatment\u003c/b\u003e\u003cbr\u003ePIC : CCO \u003cbr\u003e&quot;
    },
    {
      &quot;name&quot;: &quot;FUStatus&quot;,
      &quot;value&quot;: &quot;Not Need Follow Up&quot;
    },
    {
      &quot;name&quot;: &quot;OPNO&quot;,
      &quot;value&quot;: &quot;91164&quot;
    },
    {
      &quot;name&quot;: &quot;TrIDCallOut&quot;,
      &quot;value&quot;: &quot;2824838&quot;
    },
    {
      &quot;name&quot;: &quot;BenefitCoverage&quot;,
      &quot;value&quot;: &quot;BC001&quot;
    },
    {
      &quot;name&quot;: &quot;IsDiagnosisNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsAnnualLimitNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsNewMemberNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsExcessNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsDocumentNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityMedicalTreatment&quot;,
      &quot;value&quot;: &quot;[[\&quot;W10\&quot;,\&quot;\&quot;,1,\&quot;Dele W10\&quot;,\&quot;1000\&quot;,\&quot;\&quot;,\&quot;Fall on and from stairs and steps\&quot;,\&quot;\&quot;,0,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,1,\&quot;1\&quot;]]&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityFamilyPlanningItem&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentCode&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;IsEligibilityNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsMedicalTreatmentNeedNextValidation&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;IsFamilyPlanningNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsProductNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;Source&quot;,
      &quot;value&quot;: &quot;HMY&quot;
    },
    {
      &quot;name&quot;: &quot;IsExcessCalculation&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;IsFromProcessButton&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;NonMedicalItem&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;IsDiagnosisQuestionNotRegistered&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;MedicalTreatmentAdditionalQuestion&quot;,
      &quot;value&quot;: &quot;[[]]&quot;
    },
    {
      &quot;name&quot;: &quot;IsGLProvider&quot;,
      &quot;value&quot;: &quot;False&quot;
    },
    {
      &quot;name&quot;: &quot;IsODS&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;IsODC&quot;,
      &quot;value&quot;: &quot;0&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer WGLMLPQvhoQN8gyQL7wRG1eRLb/UBibfHhYTdmnkpNkngNzDIoO4GMB9S50aXEH5vA9ZjfBGgkLhxumpPe7n04gE9wtIPFjwI0Q2CkAu9ff5pkDwHFTpPTyXc3iFcYg0tGnGkvqBkYuiizy6N8OZ6K8WwaPkBFcEoQYVOhu8RAnZwV7gighGW7A4g/G15FKCrLbD3Gm8RQr83JaS0QxdnUymn7hbPcBwdqI/MTywPr52vGUfx0kiVhWBuA8FZ3mG</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gen5-qc.asuransiastra.com/GardaAkses/API/CCOOutbound/GA_ClaimFinalValidation</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
