<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>TSG</description>
   <name>FollowUpTicketClaimValue</name>
   <tag></tag>
   <elementGuidId>3251ace9-9e23-4263-8e9f-fb06c9483913</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;ServiceType&quot;,
      &quot;value&quot;: &quot;PRI&quot;
    },
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;B/00017507&quot;
    },
    {
      &quot;name&quot;: &quot;MemberName&quot;,
      &quot;value&quot;: &quot;BUNGA ASRI KENCANA&quot;
    },
    {
      &quot;name&quot;: &quot;ClientID&quot;,
      &quot;value&quot;: &quot;CJKPP0009&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderID&quot;,
      &quot;value&quot;: &quot;TJKRP0146&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousTrID&quot;,
      &quot;value&quot;: &quot;2744046&quot;
    },
    {
      &quot;name&quot;: &quot;GLType&quot;,
      &quot;value&quot;: &quot;GL Awal&quot;
    },
    {
      &quot;name&quot;: &quot;StartFollowUp&quot;,
      &quot;value&quot;: &quot;2020-11-17 15:56:47&quot;
    },
    {
      &quot;name&quot;: &quot;Guid&quot;,
      &quot;value&quot;: &quot;5edfafca1a324db0bdb8a833d63481110595b500e630f3ea2b359de4b3edae92&quot;
    },
    {
      &quot;name&quot;: &quot;ConfirmationItem&quot;,
      &quot;value&quot;: &quot;[{\&quot;ConfirmationType\&quot;:\&quot;AM\&quot;,\&quot;PICDesc\&quot;:\&quot;Account Manager\&quot;,\&quot;PICName\&quot;:\&quot;AIB\&quot;,\&quot;NameDesc\&quot;:\&quot;Arrio Probo Wibowo\&quot;,\&quot;ConfirmationDate\&quot;:\&quot;17/11/2020 15:58\&quot;,\&quot;Confirmation\&quot;:\&quot;COV\&quot;,\&quot;ConfirmationDesc\&quot;:\&quot;Covered\&quot;,\&quot;Remarks\&quot;:\&quot;\&quot;,\&quot;FollowUpTime\&quot;:\&quot;0\&quot;,\&quot;Channel\&quot;:\&quot;CALL\&quot;,\&quot;PreviousLimit\&quot;:null,\&quot;CurrentLimit\&quot;:null,\&quot;BenefitCoverage\&quot;:\&quot;BC001\&quot;,\&quot;Reason\&quot;:\&quot;\&quot;,\&quot;ConfirmationCategory\&quot;:\&quot;Diagnosa\&quot;,\&quot;DateTimeConfirmation\&quot;:null,\&quot;DocumentGLNeeded\&quot;:null,\&quot;fileCLByte\&quot;:null,\&quot;fileNames\&quot;:null,\&quot;isPrevious\&quot;:0,\&quot;FollowUpTicketDocuments\&quot;:[],\&quot;FollowUpTicketDocumentsNeeded\&quot;:[],\&quot;Payer\&quot;:\&quot;\&quot;,\&quot;ClientHR\&quot;:\&quot;\&quot;,\&quot;IsFromTicketBefore\&quot;:0}]&quot;
    },
    {
      &quot;name&quot;: &quot;diagnosisConfirmationItem&quot;,
      &quot;value&quot;: &quot;[{\&quot;DiagnosisID\&quot;:\&quot;B20\&quot;,\&quot;Diagnosis\&quot;:\&quot;HIV DISEASE RESULTING IN INFECTIOUS AND PARASITIC DISEASE\&quot;,\&quot;DiagnosisPayer\&quot;:\&quot;Employee\&quot;,\&quot;DiagnosisCategory\&quot;:\&quot;HIV, AIDS, ARDS\&quot;,\&quot;DiagnosisCoverage\&quot;:\&quot;Covered\&quot;}]&quot;
    },
    {
      &quot;name&quot;: &quot;FamilyPlanningItemConfirmation&quot;,
      &quot;value&quot;: &quot;[]&quot;
    },
    {
      &quot;name&quot;: &quot;email&quot;,
      &quot;value&quot;: &quot;ehu@beyond.asuransi.astra.co.id&quot;
    },
    {
      &quot;name&quot;: &quot;hp&quot;,
      &quot;value&quot;: &quot;081808623854&quot;
    },
    {
      &quot;name&quot;: &quot;providerconfirmationitem&quot;,
      &quot;value&quot;: &quot;[]&quot;
    },
    {
      &quot;name&quot;: &quot;IsFUWaved&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;WavedReasonCode&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;ProductType&quot;,
      &quot;value&quot;: &quot;IP&quot;
    },
    {
      &quot;name&quot;: &quot;NonMedicalItem&quot;,
      &quot;value&quot;: &quot;[]&quot;
    },
    {
      &quot;name&quot;: &quot;claimdetails&quot;,
      &quot;value&quot;: &quot;[]&quot;
    },
    {
      &quot;name&quot;: &quot;ReferenceClaimLimitRecovery&quot;,
      &quot;value&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ePlc9SPCqnedMnfoCDwnHotUDipSv0FJWo55rYtz20FpiVPnHu7xYiVOBt4m/+ma6CPmezqYElxVckMLx4vWRpezN03VRPbtfzEykoAj3PJVgQNEkNP9n24TZIOiS7ilmrJ7z1ptOBdQz5eoRN5w4a2Zh7VuJQyJWFhlgIVayKNyK88+yc4fXp1clcyAm7zPiysKP9bnPAm4R+hQYBkrHO5nP6GNfm1TQ74a4Q9Toca5VF3ez6CLzNXflKUFth3a</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gen5-qc.asuransiastra.com/GardaAkses/API/CCOOutbound/FollowUpTicketClaim</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
