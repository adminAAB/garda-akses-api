<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>RRU - Sprint 24</description>
   <name>GA_ClaimFinalValidation - DQ</name>
   <tag></tag>
   <elementGuidId>0a1cfa43-3311-471d-aaec-c0fd48439322</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;R/00057587&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousTrID&quot;,
      &quot;value&quot;: &quot;2824870&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentDate&quot;,
      &quot;value&quot;: &quot;2021-03-11&quot;
    },
    {
      &quot;name&quot;: &quot;ProductType&quot;,
      &quot;value&quot;: &quot;MA&quot;
    },
    {
      &quot;name&quot;: &quot;DiagnosisCode&quot;,
      &quot;value&quot;: &quot;O80&quot;
    },
    {
      &quot;name&quot;: &quot;DiagnosisAdditionalInfo&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderID&quot;,
      &quot;value&quot;: &quot;TJKRH00013&quot;
    },
    {
      &quot;name&quot;: &quot;PatientPhone&quot;,
      &quot;value&quot;: &quot;0987654321&quot;
    },
    {
      &quot;name&quot;: &quot;CallerName&quot;,
      &quot;value&quot;: &quot;-&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoom&quot;,
      &quot;value&quot;: &quot;ODS (One Day Surgery)&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomAmount&quot;,
      &quot;value&quot;: &quot;203000&quot;
    },
    {
      &quot;name&quot;: &quot;DoctorName&quot;,
      &quot;value&quot;: &quot;Dele&quot;
    },
    {
      &quot;name&quot;: &quot;RoomOption&quot;,
      &quot;value&quot;: &quot;On Plan&quot;
    },
    {
      &quot;name&quot;: &quot;RoomAvailability&quot;,
      &quot;value&quot;: &quot;NONE&quot;
    },
    {
      &quot;name&quot;: &quot;UpgradeClass&quot;,
      &quot;value&quot;: &quot;NONE&quot;
    },
    {
      &quot;name&quot;: &quot;BenefitAmount&quot;,
      &quot;value&quot;: &quot;400000&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderEmail&quot;,
      &quot;value&quot;: &quot;ehu@beyond.asuransi.astra.co.id&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderFax&quot;,
      &quot;value&quot;: &quot;0218560601&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderPhone&quot;,
      &quot;value&quot;: &quot;081806221986&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderExt&quot;,
      &quot;value&quot;: &quot;121&quot;
    },
    {
      &quot;name&quot;: &quot;IsTiri&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;DefaultProviderID&quot;,
      &quot;value&quot;: &quot;TJKRH00013&quot;
    },
    {
      &quot;name&quot;: &quot;Remarks&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;ServiceTypeID&quot;,
      &quot;value&quot;: &quot;PRI&quot;
    },
    {
      &quot;name&quot;: &quot;Guid&quot;,
      &quot;value&quot;: &quot;4b52e80f3b234a18b3a09521b2453d3c3ee8774f78f3a8b85ff9ee6a08a083d6&quot;
    },
    {
      &quot;name&quot;: &quot;TicketNo&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;GLType&quot;,
      &quot;value&quot;: &quot;GL Awal&quot;
    },
    {
      &quot;name&quot;: &quot;AccountManager&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;TotalBilled&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;ClientID&quot;,
      &quot;value&quot;: &quot;CJKPA11&quot;
    },
    {
      &quot;name&quot;: &quot;ClassNo&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;MembershipType&quot;,
      &quot;value&quot;: &quot;2. SPO&quot;
    },
    {
      &quot;name&quot;: &quot;AllDiagnosis&quot;,
      &quot;value&quot;: &quot;[[1,\&quot;O80\&quot;,\&quot;SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY\&quot;,\&quot;Initial Primary\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;1\&quot;,\&quot;1\&quot;,\&quot;1\&quot;,\&quot;1\&quot;,\&quot;\&quot;,\&quot;Covered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Covered\&quot;,0,\&quot;1\&quot;,\&quot;1\&quot;],[3,\&quot;W19\&quot;,\&quot;Unspecified fall\&quot;,\&quot;Initial Secondary\&quot;,\&quot;Covered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Guaranteed but Uncovered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,null,\&quot;Question\&quot;,0,\&quot;1\&quot;,\&quot;1\&quot;]]&quot;
    },
    {
      &quot;name&quot;: &quot;AllDiagnosisAdditionalInfo&quot;,
      &quot;value&quot;: &quot;[[],[{\&quot;QuestionID\&quot;:\&quot;1017\&quot;,\&quot;QuestionDescription\&quot;:\&quot;RRU Question NoResult Covered Adt Doc\&quot;,\&quot;AnswerValue\&quot;:\&quot;2\&quot;,\&quot;Remarks\&quot;:\&quot;\&quot;,\&quot;AdditionalInfo\&quot;:\&quot;\&quot;,\&quot;AdditionalInfoProvider\&quot;:\&quot;\&quot;,\&quot;AdditionalDocument\&quot;:\&quot;KKELF\&quot;,\&quot;AdditionalDocDesc\&quot;:\&quot;\&quot;}]]&quot;
    },
    {
      &quot;name&quot;: &quot;AllDoctors&quot;,
      &quot;value&quot;: &quot;Dele&quot;
    },
    {
      &quot;name&quot;: &quot;Gender&quot;,
      &quot;value&quot;: &quot;F&quot;
    },
    {
      &quot;name&quot;: &quot;DOB&quot;,
      &quot;value&quot;: &quot;1989-08-06&quot;
    },
    {
      &quot;name&quot;: &quot;NewMemberName&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousGuid&quot;,
      &quot;value&quot;: &quot;fc6caf27dcb843acb2950dc7a617af9ea3c5d7faa12a970f23d91ea3b5ef4fab&quot;
    },
    {
      &quot;name&quot;: &quot;GLStatus&quot;,
      &quot;value&quot;: &quot;Not Produce - Need Follow Up&quot;
    },
    {
      &quot;name&quot;: &quot;EmpMemberNo&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;NMEmpID&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;FollowUpTaskID&quot;,
      &quot;value&quot;: &quot;fc6caf27dcb843acb2950dc7a617af9ea3c5d7faa12a970f23d91ea3b5ef4fab&quot;
    },
    {
      &quot;name&quot;: &quot;IsClient&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;CallStatusID&quot;,
      &quot;value&quot;: &quot;Not Need Follow Up&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClass&quot;,
      &quot;value&quot;: &quot;ODS (One Day Surgery)&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRate&quot;,
      &quot;value&quot;: &quot;203000&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentEnd&quot;,
      &quot;value&quot;: &quot;2021-03-11&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClassChoosen&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRateChoosen&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomChoosen&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomAmountChoosen&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;IsReferral&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;IsSpecialCondition&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;ReferralReasonCode&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;CallInStart&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;SecondaryDiagnosisCode&quot;,
      &quot;value&quot;: &quot;[\&quot;W19\&quot;]&quot;
    },
    {
      &quot;name&quot;: &quot;ClientClassNo&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;UserPosition&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;PopUpMsg&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;FUStatus&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;OPNO&quot;,
      &quot;value&quot;: &quot;91164&quot;
    },
    {
      &quot;name&quot;: &quot;TrIDCallOut&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;BenefitCoverage&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;IsDiagnosisNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsAnnualLimitNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsNewMemberNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsExcessNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsDocumentNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityMedicalTreatment&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityFamilyPlanningItem&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentCode&quot;,
      &quot;value&quot;: &quot;MAT001&quot;
    },
    {
      &quot;name&quot;: &quot;IsEligibilityNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsMedicalTreatmentNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsFamilyPlanningNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsProductNeedNextValidation&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;Source&quot;,
      &quot;value&quot;: &quot;HMY&quot;
    },
    {
      &quot;name&quot;: &quot;IsExcessCalculation&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;IsFromProcessButton&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;NonMedicalItem&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;IsDiagnosisQuestionNotRegistered&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;MedicalTreatmentAdditionalQuestion&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;IsGLProvider&quot;,
      &quot;value&quot;: &quot;False&quot;
    },
    {
      &quot;name&quot;: &quot;IsODS&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;IsODC&quot;,
      &quot;value&quot;: &quot;0&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer iNia8yTqMLVVxAQeE0rUl9WzNHdcgiTPLyviEPE8l/FyW0sfzJh8jXjKT4UW2gYsmSbSR6OC1YNlx0joT6bQMKEpTefMXZWiP7eax18ISjCxF7X1ixaEuXX1p/BRgcOUxtEh0XP+gjFEylud6MzAXUbrf3Jim+26YPcLVA5lk1dTQnUhoyFpZtf5T1a2KAHraG+PGuebhrZB2WYE894DJGnkAU8QABMGSBddU/xd7ZgWPFK5kPTIUMLSZ9p6eOzZ</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gen5-qc.asuransiastra.com/GardaAkses/API/CCOOutbound/GA_ClaimFinalValidation</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
