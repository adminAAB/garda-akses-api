<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>TSG</description>
   <name>FollowUpTicketClaim</name>
   <tag></tag>
   <elementGuidId>41ab3692-f7e2-47c9-9c23-af51e0f9d828</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;ServiceType&quot;,
      &quot;value&quot;: &quot;${ServiceType}&quot;
    },
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;${MemberNo}&quot;
    },
    {
      &quot;name&quot;: &quot;MemberName&quot;,
      &quot;value&quot;: &quot;${MemberName}&quot;
    },
    {
      &quot;name&quot;: &quot;ClientID&quot;,
      &quot;value&quot;: &quot;${ClientID}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderID&quot;,
      &quot;value&quot;: &quot;${ProviderID}&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousTrID&quot;,
      &quot;value&quot;: &quot;${PreviousTrID}&quot;
    },
    {
      &quot;name&quot;: &quot;GLType&quot;,
      &quot;value&quot;: &quot;${GLType}&quot;
    },
    {
      &quot;name&quot;: &quot;StartFollowUp&quot;,
      &quot;value&quot;: &quot;${StartFollowUp}&quot;
    },
    {
      &quot;name&quot;: &quot;Guid&quot;,
      &quot;value&quot;: &quot;${Guid}&quot;
    },
    {
      &quot;name&quot;: &quot;ConfirmationItem&quot;,
      &quot;value&quot;: &quot;${ConfirmationItem}&quot;
    },
    {
      &quot;name&quot;: &quot;diagnosisConfirmationItem&quot;,
      &quot;value&quot;: &quot;${diagnosisConfirmationItem}&quot;
    },
    {
      &quot;name&quot;: &quot;FamilyPlanningItemConfirmation&quot;,
      &quot;value&quot;: &quot;${FamilyPlanningItemConfirmation}&quot;
    },
    {
      &quot;name&quot;: &quot;email&quot;,
      &quot;value&quot;: &quot;${email}&quot;
    },
    {
      &quot;name&quot;: &quot;hp&quot;,
      &quot;value&quot;: &quot;${hp}&quot;
    },
    {
      &quot;name&quot;: &quot;providerconfirmationitem&quot;,
      &quot;value&quot;: &quot;${providerconfirmationitem}&quot;
    },
    {
      &quot;name&quot;: &quot;IsFUWaved&quot;,
      &quot;value&quot;: &quot;${IsFUWaved}&quot;
    },
    {
      &quot;name&quot;: &quot;WavedReasonCode&quot;,
      &quot;value&quot;: &quot;${WavedReasonCode}&quot;
    },
    {
      &quot;name&quot;: &quot;ProductType&quot;,
      &quot;value&quot;: &quot;${ProductType}&quot;
    },
    {
      &quot;name&quot;: &quot;NonMedicalItem&quot;,
      &quot;value&quot;: &quot;${NonMedicalItem}&quot;
    },
    {
      &quot;name&quot;: &quot;claimdetails&quot;,
      &quot;value&quot;: &quot;${claimdetails}&quot;
    },
    {
      &quot;name&quot;: &quot;ReferenceClaimLimitRecovery&quot;,
      &quot;value&quot;: &quot;${ReferenceClaimLimitRecovery}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.authorization}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gen5-qc.asuransiastra.com/GardaAkses/API/CCOOutbound/FollowUpTicketClaim</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
