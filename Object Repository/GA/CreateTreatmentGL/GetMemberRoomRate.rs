<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>TSG</description>
   <name>GetMemberRoomRate</name>
   <tag></tag>
   <elementGuidId>a02e2634-613e-4568-997b-c851f2cb38c2</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;${MemberNo}&quot;
    },
    {
      &quot;name&quot;: &quot;ProductType&quot;,
      &quot;value&quot;: &quot;${ProductType}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentDate&quot;,
      &quot;value&quot;: &quot;${TreatmentDate}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderID&quot;,
      &quot;value&quot;: &quot;${ProviderID}&quot;
    },
    {
      &quot;name&quot;: &quot;ClientID&quot;,
      &quot;value&quot;: &quot;${ClientID}&quot;
    },
    {
      &quot;name&quot;: &quot;ClientClassNo&quot;,
      &quot;value&quot;: &quot;${ClientClassNo}&quot;
    },
    {
      &quot;name&quot;: &quot;MembershipType&quot;,
      &quot;value&quot;: &quot;${MembershipType}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.authorization}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.urlGardaAkses}/API/CreateTreatmentGL/GetMemberRoomRate</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
