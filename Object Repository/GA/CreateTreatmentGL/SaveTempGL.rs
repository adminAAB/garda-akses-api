<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>SaveTempGL</name>
   <tag></tag>
   <elementGuidId>7fd121b3-d40c-4933-adb8-e2035ffe4bc9</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;pIsGLProvider&quot;,
      &quot;value&quot;: &quot;${pIsGLProvider}&quot;
    },
    {
      &quot;name&quot;: &quot;Guid&quot;,
      &quot;value&quot;: &quot;${Guid}&quot;
    },
    {
      &quot;name&quot;: &quot;TicketNo&quot;,
      &quot;value&quot;: &quot;${TicketNo}&quot;
    },
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;${MemberNo}&quot;
    },
    {
      &quot;name&quot;: &quot;MemberName&quot;,
      &quot;value&quot;: &quot;${MemberName}&quot;
    },
    {
      &quot;name&quot;: &quot;Membership&quot;,
      &quot;value&quot;: &quot;${Membership}&quot;
    },
    {
      &quot;name&quot;: &quot;ClassNo&quot;,
      &quot;value&quot;: &quot;${ClassNo}&quot;
    },
    {
      &quot;name&quot;: &quot;ClientID&quot;,
      &quot;value&quot;: &quot;${ClientID}&quot;
    },
    {
      &quot;name&quot;: &quot;ClientName&quot;,
      &quot;value&quot;: &quot;${ClientName}&quot;
    },
    {
      &quot;name&quot;: &quot;BenefitInfo&quot;,
      &quot;value&quot;: &quot;${BenefitInfo}&quot;
    },
    {
      &quot;name&quot;: &quot;EmpID&quot;,
      &quot;value&quot;: &quot;${EmpID}&quot;
    },
    {
      &quot;name&quot;: &quot;GLType&quot;,
      &quot;value&quot;: &quot;${GLType}&quot;
    },
    {
      &quot;name&quot;: &quot;DOB&quot;,
      &quot;value&quot;: &quot;${DOB}&quot;
    },
    {
      &quot;name&quot;: &quot;ProductType&quot;,
      &quot;value&quot;: &quot;${ProductType}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentStart&quot;,
      &quot;value&quot;: &quot;${TreatmentStart}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentEnd&quot;,
      &quot;value&quot;: &quot;${TreatmentEnd}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderID&quot;,
      &quot;value&quot;: &quot;${ProviderID}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderName&quot;,
      &quot;value&quot;: &quot;${ProviderName}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderEmail&quot;,
      &quot;value&quot;: &quot;${ProviderEmail}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderFax&quot;,
      &quot;value&quot;: &quot;${ProviderFax}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderPhoneNo&quot;,
      &quot;value&quot;: &quot;${ProviderPhoneNo}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderExt&quot;,
      &quot;value&quot;: &quot;${ProviderExt}&quot;
    },
    {
      &quot;name&quot;: &quot;IsPassedAway&quot;,
      &quot;value&quot;: &quot;${IsPassedAway}&quot;
    },
    {
      &quot;name&quot;: &quot;Diagnosis&quot;,
      &quot;value&quot;: &quot;${Diagnosis}&quot;
    },
    {
      &quot;name&quot;: &quot;AccountManager&quot;,
      &quot;value&quot;: &quot;${AccountManager}&quot;
    },
    {
      &quot;name&quot;: &quot;Doctor&quot;,
      &quot;value&quot;: &quot;${Doctor}&quot;
    },
    {
      &quot;name&quot;: &quot;Remarks&quot;,
      &quot;value&quot;: &quot;${Remarks}&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClass&quot;,
      &quot;value&quot;: &quot;${AppropriateRBClass}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRBClass&quot;,
      &quot;value&quot;: &quot;${TreatmentRBClass}&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRate&quot;,
      &quot;value&quot;: &quot;${AppropriateRBRate}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRBRate&quot;,
      &quot;value&quot;: &quot;${TreatmentRBRate}&quot;
    },
    {
      &quot;name&quot;: &quot;RoomOption&quot;,
      &quot;value&quot;: &quot;${RoomOption}&quot;
    },
    {
      &quot;name&quot;: &quot;RemainingLimit&quot;,
      &quot;value&quot;: &quot;${RemainingLimit}&quot;
    },
    {
      &quot;name&quot;: &quot;RemainingLimitType&quot;,
      &quot;value&quot;: &quot;${RemainingLimitType}&quot;
    },
    {
      &quot;name&quot;: &quot;RBScheme&quot;,
      &quot;value&quot;: &quot;${RBScheme}&quot;
    },
    {
      &quot;name&quot;: &quot;ICULimit&quot;,
      &quot;value&quot;: &quot;${ICULimit}&quot;
    },
    {
      &quot;name&quot;: &quot;IsolationLimit&quot;,
      &quot;value&quot;: &quot;${IsolationLimit}&quot;
    },
    {
      &quot;name&quot;: &quot;TotalBilled&quot;,
      &quot;value&quot;: &quot;${TotalBilled}&quot;
    },
    {
      &quot;name&quot;: &quot;AdditionalDiagnosisInfo&quot;,
      &quot;value&quot;: &quot;${AdditionalDiagnosisInfo}&quot;
    },
    {
      &quot;name&quot;: &quot;FamilyPhone&quot;,
      &quot;value&quot;: &quot;${FamilyPhone}&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousGuid&quot;,
      &quot;value&quot;: &quot;${PreviousGuid}&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousTrID&quot;,
      &quot;value&quot;: &quot;${PreviousTrID}&quot;
    },
    {
      &quot;name&quot;: &quot;GLStatus&quot;,
      &quot;value&quot;: &quot;${GLStatus}&quot;
    },
    {
      &quot;name&quot;: &quot;CallerName&quot;,
      &quot;value&quot;: &quot;${CallerName}&quot;
    },
    {
      &quot;name&quot;: &quot;IsPreAdmission&quot;,
      &quot;value&quot;: &quot;${IsPreAdmission}&quot;
    },
    {
      &quot;name&quot;: &quot;IsInteruptedCalls&quot;,
      &quot;value&quot;: &quot;${IsInteruptedCalls}&quot;
    },
    {
      &quot;name&quot;: &quot;IsReject&quot;,
      &quot;value&quot;: &quot;${IsReject}&quot;
    },
    {
      &quot;name&quot;: &quot;NMMemberType&quot;,
      &quot;value&quot;: &quot;${NMMemberType}&quot;
    },
    {
      &quot;name&quot;: &quot;NMMemberName&quot;,
      &quot;value&quot;: &quot;${NMMemberName}&quot;
    },
    {
      &quot;name&quot;: &quot;NMEmpID&quot;,
      &quot;value&quot;: &quot;${NMEmpID}&quot;
    },
    {
      &quot;name&quot;: &quot;NMEmployee&quot;,
      &quot;value&quot;: &quot;${NMEmployee}&quot;
    },
    {
      &quot;name&quot;: &quot;NMClassification&quot;,
      &quot;value&quot;: &quot;${NMClassification}&quot;
    },
    {
      &quot;name&quot;: &quot;NMClientId&quot;,
      &quot;value&quot;: &quot;${NMClientId}&quot;
    },
    {
      &quot;name&quot;: &quot;NMDOB&quot;,
      &quot;value&quot;: &quot;${NMDOB}&quot;
    },
    {
      &quot;name&quot;: &quot;NMGender&quot;,
      &quot;value&quot;: &quot;${NMGender}&quot;
    },
    {
      &quot;name&quot;: &quot;NMIsNewBorn&quot;,
      &quot;value&quot;: &quot;${NMIsNewBorn}&quot;
    },
    {
      &quot;name&quot;: &quot;NMIsTwin&quot;,
      &quot;value&quot;: &quot;${NMIsTwin}&quot;
    },
    {
      &quot;name&quot;: &quot;IsNeedFollowUpF&quot;,
      &quot;value&quot;: &quot;${IsNeedFollowUpF}&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClassActual&quot;,
      &quot;value&quot;: &quot;${AppropriateRBClassActual}&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRateActual&quot;,
      &quot;value&quot;: &quot;${AppropriateRBRateActual}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRBClassActual&quot;,
      &quot;value&quot;: &quot;${TreatmentRBClassActual}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRBRateActual&quot;,
      &quot;value&quot;: &quot;${TreatmentRBRateActual}&quot;
    },
    {
      &quot;name&quot;: &quot;IsReferral&quot;,
      &quot;value&quot;: &quot;${IsReferral}&quot;
    },
    {
      &quot;name&quot;: &quot;IsSpecialCondition&quot;,
      &quot;value&quot;: &quot;${IsSpecialCondition}&quot;
    },
    {
      &quot;name&quot;: &quot;ReferralReasonCode&quot;,
      &quot;value&quot;: &quot;${ReferralReasonCode}&quot;
    },
    {
      &quot;name&quot;: &quot;SpecialConditionReason&quot;,
      &quot;value&quot;: &quot;${SpecialConditionReason}&quot;
    },
    {
      &quot;name&quot;: &quot;Actor&quot;,
      &quot;value&quot;: &quot;${Actor}&quot;
    },
    {
      &quot;name&quot;: &quot;isFromCreateTicket&quot;,
      &quot;value&quot;: &quot;${isFromCreateTicket}&quot;
    },
    {
      &quot;name&quot;: &quot;inboundCallGuid&quot;,
      &quot;value&quot;: &quot;${inboundCallGuid}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentTT&quot;,
      &quot;value&quot;: &quot;${TreatmentTT}&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateTT&quot;,
      &quot;value&quot;: &quot;${AppropriateTT}&quot;
    },
    {
      &quot;name&quot;: &quot;ClientClassNo&quot;,
      &quot;value&quot;: &quot;${ClientClassNo}&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityScheme&quot;,
      &quot;value&quot;: &quot;${MaternityScheme}&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityPackagePrice&quot;,
      &quot;value&quot;: &quot;${MaternityPackagePrice}&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityFamilyPlanningItem&quot;,
      &quot;value&quot;: &quot;${MaternityFamilyPlanningItem}&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityMedicalTreatment&quot;,
      &quot;value&quot;: &quot;${MaternityMedicalTreatment}&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityTreatmentCode&quot;,
      &quot;value&quot;: &quot;${MaternityTreatmentCode}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderAdminName&quot;,
      &quot;value&quot;: &quot;${ProviderAdminName}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentDuration&quot;,
      &quot;value&quot;: &quot;${TreatmentDuration}&quot;
    },
    {
      &quot;name&quot;: &quot;PassedAwayDate&quot;,
      &quot;value&quot;: &quot;${PassedAwayDate}&quot;
    },
    {
      &quot;name&quot;: &quot;DoctorOnCall&quot;,
      &quot;value&quot;: &quot;${DoctorOnCall}&quot;
    },
    {
      &quot;name&quot;: &quot;LastCondition&quot;,
      &quot;value&quot;: &quot;${LastCondition}&quot;
    },
    {
      &quot;name&quot;: &quot;LastTreatment&quot;,
      &quot;value&quot;: &quot;${LastTreatment}&quot;
    },
    {
      &quot;name&quot;: &quot;ResusitasiF&quot;,
      &quot;value&quot;: &quot;${ResusitasiF}&quot;
    },
    {
      &quot;name&quot;: &quot;NonMedicalItem&quot;,
      &quot;value&quot;: &quot;${NonMedicalItem}&quot;
    },
    {
      &quot;name&quot;: &quot;IsDiagnosisQuestionNotRegistered&quot;,
      &quot;value&quot;: &quot;${IsDiagnosisQuestionNotRegistered}&quot;
    },
    {
      &quot;name&quot;: &quot;AdditionalMedicalTreatment&quot;,
      &quot;value&quot;: &quot;${AdditionalMedicalTreatment}&quot;
    },
    {
      &quot;name&quot;: &quot;IsODS&quot;,
      &quot;value&quot;: &quot;${IsODS}&quot;
    },
    {
      &quot;name&quot;: &quot;IsODC&quot;,
      &quot;value&quot;: &quot;${IsODC}&quot;
    },
    {
      &quot;name&quot;: &quot;DoctorID&quot;,
      &quot;value&quot;: &quot;${DoctorID}&quot;
    },
    {
      &quot;name&quot;: &quot;SchemeType&quot;,
      &quot;value&quot;: &quot;${SchemeType}&quot;
    },
    {
      &quot;name&quot;: &quot;GMRBClass&quot;,
      &quot;value&quot;: &quot;${GMRBClass}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderRBClass&quot;,
      &quot;value&quot;: &quot;${ProviderRBClass}&quot;
    },
    {
      &quot;name&quot;: &quot;AmountLimit&quot;,
      &quot;value&quot;: &quot;${AmountLimit}&quot;
    },
    {
      &quot;name&quot;: &quot;RBAmount&quot;,
      &quot;value&quot;: &quot;${RBAmount}&quot;
    },
    {
      &quot;name&quot;: &quot;IsCovid&quot;,
      &quot;value&quot;: &quot;${IsCovid}&quot;
    },
    {
      &quot;name&quot;: &quot;IsAPTTO&quot;,
      &quot;value&quot;: &quot;${IsAPTTO}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.authorization}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.urlGardaAkses}/API/CreateTreatmentGL/SaveTempGL</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
