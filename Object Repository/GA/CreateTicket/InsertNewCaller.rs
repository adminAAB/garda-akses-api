<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>InsertNewCaller</name>
   <tag></tag>
   <elementGuidId>1c38965a-f24e-44ed-b208-66b5575f64e2</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;pGUID&quot;,
      &quot;value&quot;: &quot;${GlobalVariable.guid}&quot;
    },
    {
      &quot;name&quot;: &quot;CallerName&quot;,
      &quot;value&quot;: &quot;${contactName}&quot;
    },
    {
      &quot;name&quot;: &quot;PhoneNumber&quot;,
      &quot;value&quot;: &quot;${phoneNumber}&quot;
    },
    {
      &quot;name&quot;: &quot;Gender&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;CallDate&quot;,
      &quot;value&quot;: &quot;${callDate}&quot;
    },
    {
      &quot;name&quot;: &quot;Email&quot;,
      &quot;value&quot;: &quot;${email}&quot;
    },
    {
      &quot;name&quot;: &quot;Relationship&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;Actor&quot;,
      &quot;value&quot;: &quot;${username}&quot;
    },
    {
      &quot;name&quot;: &quot;pCountryCode&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;pAreaCode&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;ContactLine&quot;,
      &quot;value&quot;: &quot;2&quot;
    },
    {
      &quot;name&quot;: &quot;ChannelType&quot;,
      &quot;value&quot;: &quot;${channelType}&quot;
    },
    {
      &quot;name&quot;: &quot;ContactName&quot;,
      &quot;value&quot;: &quot;${contactName}&quot;
    },
    {
      &quot;name&quot;: &quot;ContactType&quot;,
      &quot;value&quot;: &quot;${contactType}&quot;
    },
    {
      &quot;name&quot;: &quot;ServiceType&quot;,
      &quot;value&quot;: &quot;${serviceType}&quot;
    },
    {
      &quot;name&quot;: &quot;InterruptedContact&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderName&quot;,
      &quot;value&quot;: &quot;${providerName}&quot;
    },
    {
      &quot;name&quot;: &quot;JobFunction&quot;,
      &quot;value&quot;: &quot;Inbound Provider;Outbound Provider;Inbound Provider Non Health;Inbound Provider Health;Outbound Provider Non Health;Outbound Provider Health;Inbound Customer Non-Health;Outbound Customer Non-Health;Outbound Customer Health;Inbound Customer Health;&quot;
    },
    {
      &quot;name&quot;: &quot;ticketno&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;socialmediaaccount&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;providerHealthPhone&quot;,
      &quot;value&quot;: &quot;${phoneNumber}&quot;
    },
    {
      &quot;name&quot;: &quot;providerHealthPhoneExt&quot;,
      &quot;value&quot;: &quot;${ext}&quot;
    },
    {
      &quot;name&quot;: &quot;providerHealthEmail&quot;,
      &quot;value&quot;: &quot;${email}&quot;
    },
    {
      &quot;name&quot;: &quot;providerHealthFax&quot;,
      &quot;value&quot;: &quot;${fax}&quot;
    },
    {
      &quot;name&quot;: &quot;productType&quot;,
      &quot;value&quot;: &quot;${product}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.authorization}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.urlGardaAkses}/API/CreateTicket/InsertNewCaller</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
