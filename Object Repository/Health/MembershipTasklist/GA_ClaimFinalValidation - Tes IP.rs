<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>RRU - Sprint 24</description>
   <name>GA_ClaimFinalValidation - Tes IP</name>
   <tag></tag>
   <elementGuidId>3332055a-5518-40d3-9b70-27679cf55f07</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;O/00005308&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousTrID&quot;,
      &quot;value&quot;: &quot;2824925&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentDate&quot;,
      &quot;value&quot;: &quot;2021-03-12&quot;
    },
    {
      &quot;name&quot;: &quot;ProductType&quot;,
      &quot;value&quot;: &quot;IP&quot;
    },
    {
      &quot;name&quot;: &quot;DiagnosisCode&quot;,
      &quot;value&quot;: &quot;W01&quot;
    },
    {
      &quot;name&quot;: &quot;DiagnosisAdditionalInfo&quot;,
      &quot;value&quot;: &quot;[]&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderID&quot;,
      &quot;value&quot;: &quot;TJKRH00013&quot;
    },
    {
      &quot;name&quot;: &quot;PatientPhone&quot;,
      &quot;value&quot;: &quot;0987654321&quot;
    },
    {
      &quot;name&quot;: &quot;CallerName&quot;,
      &quot;value&quot;: &quot;Dele - Dokter&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoom&quot;,
      &quot;value&quot;: &quot;KELAS III&quot;
    },
    {
      &quot;name&quot;: &quot;DoctorName&quot;,
      &quot;value&quot;: &quot;Dele&quot;
    },
    {
      &quot;name&quot;: &quot;RoomOption&quot;,
      &quot;value&quot;: &quot;On Plan&quot;
    },
    {
      &quot;name&quot;: &quot;RoomAvailability&quot;,
      &quot;value&quot;: &quot;NONE&quot;
    },
    {
      &quot;name&quot;: &quot;UpgradeClass&quot;,
      &quot;value&quot;: &quot;NONE&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomAmount&quot;,
      &quot;value&quot;: &quot;203000&quot;
    },
    {
      &quot;name&quot;: &quot;BenefitAmount&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderEmail&quot;,
      &quot;value&quot;: &quot;ehu@beyond.asuransi.astra.co.id&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderFax&quot;,
      &quot;value&quot;: &quot;0218560601&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderPhone&quot;,
      &quot;value&quot;: &quot;081806221986&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderExt&quot;,
      &quot;value&quot;: &quot;121&quot;
    },
    {
      &quot;name&quot;: &quot;IsTiri&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;DefaultProviderID&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;Remarks&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousGLNo&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;Guid&quot;,
      &quot;value&quot;: &quot;7a9bd1be35d2455e9e98eaa7daccfe8da1ee0528da190ef1424d1ab69b51ff42&quot;
    },
    {
      &quot;name&quot;: &quot;TicketNo&quot;,
      &quot;value&quot;: &quot;CL194283&quot;
    },
    {
      &quot;name&quot;: &quot;GLType&quot;,
      &quot;value&quot;: &quot;GL Awal&quot;
    },
    {
      &quot;name&quot;: &quot;AccountManager&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;TotalBilled&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;ClientID&quot;,
      &quot;value&quot;: &quot;CJKPA11&quot;
    },
    {
      &quot;name&quot;: &quot;ClassNo&quot;,
      &quot;value&quot;: &quot;2&quot;
    },
    {
      &quot;name&quot;: &quot;MembershipType&quot;,
      &quot;value&quot;: &quot;2. SPO&quot;
    },
    {
      &quot;name&quot;: &quot;AllDiagnosis&quot;,
      &quot;value&quot;: &quot;[[1,\&quot;W01\&quot;,\&quot;Fall on same level from slipping, tripping and stumbling\&quot;,\&quot;Initial Primary\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Uncovered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,0,\&quot;1\&quot;,\&quot;0\&quot;],[2,\&quot;W06\&quot;,\&quot;Fall involving bed\&quot;,\&quot;Initial Secondary\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Uncovered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,0,\&quot;1\&quot;,\&quot;0\&quot;]]&quot;
    },
    {
      &quot;name&quot;: &quot;AllDiagnosisAdditionalInfo&quot;,
      &quot;value&quot;: &quot;[[],[]]&quot;
    },
    {
      &quot;name&quot;: &quot;AllDoctors&quot;,
      &quot;value&quot;: &quot;Dele&quot;
    },
    {
      &quot;name&quot;: &quot;Gender&quot;,
      &quot;value&quot;: &quot;F&quot;
    },
    {
      &quot;name&quot;: &quot;DOB&quot;,
      &quot;value&quot;: &quot;1990-07-19&quot;
    },
    {
      &quot;name&quot;: &quot;IsClient&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentEnd&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;IsReferral&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;ReferralReasonCode&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClass&quot;,
      &quot;value&quot;: &quot;KELAS III&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRate&quot;,
      &quot;value&quot;: &quot;203000&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClassChoosen&quot;,
      &quot;value&quot;: &quot;KELAS III&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRateChoosen&quot;,
      &quot;value&quot;: &quot;203000&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomChoosen&quot;,
      &quot;value&quot;: &quot;KELAS III&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomAmountChoosen&quot;,
      &quot;value&quot;: &quot;203000&quot;
    },
    {
      &quot;name&quot;: &quot;IsSpecialCondition&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;ClientClassNo&quot;,
      &quot;value&quot;: &quot;2&quot;
    },
    {
      &quot;name&quot;: &quot;SecondaryDiagnosisCode&quot;,
      &quot;value&quot;: &quot;[[1,\&quot;W01\&quot;,\&quot;Fall on same level from slipping, tripping and stumbling\&quot;,\&quot;Initial Primary\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Uncovered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,0,\&quot;1\&quot;,\&quot;0\&quot;],[2,\&quot;W06\&quot;,\&quot;Fall involving bed\&quot;,\&quot;Initial Secondary\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Uncovered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,0,\&quot;1\&quot;,\&quot;0\&quot;]]&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityMedicalTreatment&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityFamilyPlanningItem&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentCode&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentDuration&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;IsODS&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;IsODC&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;CallStatusID&quot;,
      &quot;value&quot;: &quot;Not Need Follow Up&quot;
    },
    {
      &quot;name&quot;: &quot;EmpMemberNo&quot;,
      &quot;value&quot;: &quot;O/00005308&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer /r8ctJL7IuPKOmKPqIWm4hOaSrp/zmW4WuuMzH+G6I0rRiFNzLP6lXS3gkC0Rsb3En+3e++slpz93/hHxVCYRXuZm9+wT+qCTBaXWexDnZjLZJI6j7jUj6gPoP+t3a+PC5hxXy2rCmIVuzGVSUr3zDCaea4KbD5GsmvifbVcQRjQitL5qJv2u0l9T9GIkH4OWzjdcgQRKXbkK/X8O9fWuCQQW/ZBM9a+luyvHlR7OuobSUqiNdcCO/Uez+I5l+vO</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gen5-qc.asuransiastra.com/health/API/MembershipTaskList/GA_ClaimFinalValidation</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
