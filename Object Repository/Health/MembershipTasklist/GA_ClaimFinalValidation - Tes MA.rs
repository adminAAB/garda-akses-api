<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>RRU - Sprint 24</description>
   <name>GA_ClaimFinalValidation - Tes MA</name>
   <tag></tag>
   <elementGuidId>96d10071-059c-4f6f-989a-3938cdc91679</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;P/00016483&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousTrID&quot;,
      &quot;value&quot;: &quot;2824930&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentDate&quot;,
      &quot;value&quot;: &quot;2021-03-13&quot;
    },
    {
      &quot;name&quot;: &quot;ProductType&quot;,
      &quot;value&quot;: &quot;MA&quot;
    },
    {
      &quot;name&quot;: &quot;DiagnosisCode&quot;,
      &quot;value&quot;: &quot;O80&quot;
    },
    {
      &quot;name&quot;: &quot;DiagnosisAdditionalInfo&quot;,
      &quot;value&quot;: &quot;[]&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderID&quot;,
      &quot;value&quot;: &quot;TJKRH00013&quot;
    },
    {
      &quot;name&quot;: &quot;PatientPhone&quot;,
      &quot;value&quot;: &quot;0987654321&quot;
    },
    {
      &quot;name&quot;: &quot;CallerName&quot;,
      &quot;value&quot;: &quot;Dele - Dokter&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoom&quot;,
      &quot;value&quot;: &quot;KELAS II&quot;
    },
    {
      &quot;name&quot;: &quot;DoctorName&quot;,
      &quot;value&quot;: &quot;Dele&quot;
    },
    {
      &quot;name&quot;: &quot;RoomOption&quot;,
      &quot;value&quot;: &quot;On Plan&quot;
    },
    {
      &quot;name&quot;: &quot;RoomAvailability&quot;,
      &quot;value&quot;: &quot;NONE&quot;
    },
    {
      &quot;name&quot;: &quot;UpgradeClass&quot;,
      &quot;value&quot;: &quot;NONE&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomAmount&quot;,
      &quot;value&quot;: &quot;487000&quot;
    },
    {
      &quot;name&quot;: &quot;BenefitAmount&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderEmail&quot;,
      &quot;value&quot;: &quot;ehu@beyond.asuransi.astra.co.id&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderFax&quot;,
      &quot;value&quot;: &quot;0218560601&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderPhone&quot;,
      &quot;value&quot;: &quot;081806221986&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderExt&quot;,
      &quot;value&quot;: &quot;121&quot;
    },
    {
      &quot;name&quot;: &quot;IsTiri&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;DefaultProviderID&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;Remarks&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousGLNo&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;Guid&quot;,
      &quot;value&quot;: &quot;21c5cca072b740f2a42b379ebc89d6562fda2e3596959ffd09641c28f4b30434&quot;
    },
    {
      &quot;name&quot;: &quot;TicketNo&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;GLType&quot;,
      &quot;value&quot;: &quot;GL Akhir&quot;
    },
    {
      &quot;name&quot;: &quot;AccountManager&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;TotalBilled&quot;,
      &quot;value&quot;: &quot;123456&quot;
    },
    {
      &quot;name&quot;: &quot;ClientID&quot;,
      &quot;value&quot;: &quot;CJKPA18&quot;
    },
    {
      &quot;name&quot;: &quot;ClassNo&quot;,
      &quot;value&quot;: &quot;2&quot;
    },
    {
      &quot;name&quot;: &quot;MembershipType&quot;,
      &quot;value&quot;: &quot;2. SPO&quot;
    },
    {
      &quot;name&quot;: &quot;AllDiagnosis&quot;,
      &quot;value&quot;: &quot;[[1,\&quot;O80\&quot;,\&quot;SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY\&quot;,\&quot;Initial Primary\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;1\&quot;,\&quot;1\&quot;,\&quot;1\&quot;,\&quot;1\&quot;,\&quot;\&quot;,\&quot;Covered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Covered\&quot;,0,\&quot;0\&quot;,\&quot;0\&quot;],[2,\&quot;W01\&quot;,\&quot;Fall on same level from slipping, tripping and stumbling\&quot;,\&quot;Initial Secondary\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Uncovered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,0,\&quot;0\&quot;,\&quot;0\&quot;]]&quot;
    },
    {
      &quot;name&quot;: &quot;AllDiagnosisAdditionalInfo&quot;,
      &quot;value&quot;: &quot;[[],[]]&quot;
    },
    {
      &quot;name&quot;: &quot;AllDoctors&quot;,
      &quot;value&quot;: &quot;Dele&quot;
    },
    {
      &quot;name&quot;: &quot;Gender&quot;,
      &quot;value&quot;: &quot;F&quot;
    },
    {
      &quot;name&quot;: &quot;DOB&quot;,
      &quot;value&quot;: &quot;1990-09-10&quot;
    },
    {
      &quot;name&quot;: &quot;IsClient&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentEnd&quot;,
      &quot;value&quot;: &quot;2021-03-15&quot;
    },
    {
      &quot;name&quot;: &quot;IsReferral&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;ReferralReasonCode&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClass&quot;,
      &quot;value&quot;: &quot;KELAS II&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRate&quot;,
      &quot;value&quot;: &quot;487000&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClassChoosen&quot;,
      &quot;value&quot;: &quot;KELAS II&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRateChoosen&quot;,
      &quot;value&quot;: &quot;487000&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomChoosen&quot;,
      &quot;value&quot;: &quot;KELAS II&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomAmountChoosen&quot;,
      &quot;value&quot;: &quot;487000&quot;
    },
    {
      &quot;name&quot;: &quot;IsSpecialCondition&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;ClientClassNo&quot;,
      &quot;value&quot;: &quot;1&quot;
    },
    {
      &quot;name&quot;: &quot;SecondaryDiagnosisCode&quot;,
      &quot;value&quot;: &quot;[[1,\&quot;O80\&quot;,\&quot;SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY\&quot;,\&quot;Initial Primary\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;1\&quot;,\&quot;1\&quot;,\&quot;1\&quot;,\&quot;1\&quot;,\&quot;\&quot;,\&quot;Covered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Covered\&quot;,0,\&quot;0\&quot;,\&quot;0\&quot;],[2,\&quot;W01\&quot;,\&quot;Fall on same level from slipping, tripping and stumbling\&quot;,\&quot;Initial Secondary\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;Uncovered\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,\&quot;\&quot;,0,\&quot;0\&quot;,\&quot;0\&quot;]]&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityMedicalTreatment&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityFamilyPlanningItem&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentCode&quot;,
      &quot;value&quot;: &quot;MAT001&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentDuration&quot;,
      &quot;value&quot;: &quot;3&quot;
    },
    {
      &quot;name&quot;: &quot;IsODS&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;IsODC&quot;,
      &quot;value&quot;: &quot;0&quot;
    },
    {
      &quot;name&quot;: &quot;CallStatusID&quot;,
      &quot;value&quot;: &quot;Not Need Follow Up&quot;
    },
    {
      &quot;name&quot;: &quot;EmpMemberNo&quot;,
      &quot;value&quot;: &quot;P/00016483&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer 3xW0REekxYX25TbXCesEVk2N1D0EVfORlhdFSye21cUgzxFH796f6S98fWAiL0O3Ydby8z9fpjmjiyQcvmAoKtlorlLq/NsxYxsVea97RK3f/CfeRP7rn5usXZU3kQEINZshfyCulCMWzdk94MkNodoToTkGl5yYS1jD5xxywu7ZWv4MOj6kv4u0Wm0imSSkKGOAID2Gs0mUg0Cc9U2Gr26eQ4GHe8oSjYlfT3UdQSAmNQ42tHCYUFiSu6fixeFc</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gen5-qc.asuransiastra.com/health/API/MembershipTaskList/GA_ClaimFinalValidation</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
