<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GetBenefitInfoDetail</name>
   <tag></tag>
   <elementGuidId>416dd26c-7994-451a-b2c2-f991b5ef348f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;pPageNo&quot;,
      &quot;value&quot;: &quot;${pageNo}&quot;
    },
    {
      &quot;name&quot;: &quot;pSortOption&quot;,
      &quot;value&quot;: &quot;${sortOption}&quot;
    },
    {
      &quot;name&quot;: &quot;pSortDirection&quot;,
      &quot;value&quot;: &quot;${sortDirection}&quot;
    },
    {
      &quot;name&quot;: &quot;pPageAmount&quot;,
      &quot;value&quot;: &quot;${pageAmount}&quot;
    },
    {
      &quot;name&quot;: &quot;pMemberNo&quot;,
      &quot;value&quot;: &quot;${memberNo}&quot;
    },
    {
      &quot;name&quot;: &quot;pIsActive&quot;,
      &quot;value&quot;: &quot;${isActive}&quot;
    },
    {
      &quot;name&quot;: &quot;pClientID&quot;,
      &quot;value&quot;: &quot;${clientID}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.authorization}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.urlGMA}/API/CreateTreatmentGL/GetBenefitInfoDetail</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
