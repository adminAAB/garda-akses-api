<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>InquiryGLData</name>
   <tag></tag>
   <elementGuidId>ca658771-523f-4d5f-ba30-0e1b7d4f21d8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;pPageNo&quot;,
      &quot;value&quot;: &quot;${pPageNo}&quot;
    },
    {
      &quot;name&quot;: &quot;pSortOption&quot;,
      &quot;value&quot;: &quot;${pSortOption}&quot;
    },
    {
      &quot;name&quot;: &quot;pSortDirection&quot;,
      &quot;value&quot;: &quot;${pSortDirection}&quot;
    },
    {
      &quot;name&quot;: &quot;pTicketNo&quot;,
      &quot;value&quot;: &quot;${pTicketNo}&quot;
    },
    {
      &quot;name&quot;: &quot;pMemberNo&quot;,
      &quot;value&quot;: &quot;${pMemberNo}&quot;
    },
    {
      &quot;name&quot;: &quot;pMemberName&quot;,
      &quot;value&quot;: &quot;${pMemberName}&quot;
    },
    {
      &quot;name&quot;: &quot;pClientName&quot;,
      &quot;value&quot;: &quot;${pClientName}&quot;
    },
    {
      &quot;name&quot;: &quot;pEmployeeID&quot;,
      &quot;value&quot;: &quot;${pEmployeeID}&quot;
    },
    {
      &quot;name&quot;: &quot;pTreatmentDate&quot;,
      &quot;value&quot;: &quot;${pTreatmentDate}&quot;
    },
    {
      &quot;name&quot;: &quot;pProviderID&quot;,
      &quot;value&quot;: &quot;${pProviderID}&quot;
    },
    {
      &quot;name&quot;: &quot;isGLProvider&quot;,
      &quot;value&quot;: &quot;${isGLProvider}&quot;
    },
    {
      &quot;name&quot;: &quot;pStatus&quot;,
      &quot;value&quot;: &quot;${pStatus}&quot;
    },
    {
      &quot;name&quot;: &quot;pDate&quot;,
      &quot;value&quot;: &quot;${pDate}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.authorization}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <katalonVersion>7.9.1</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gardamedikaakses-qc1.asuransiastra.com/GardaAkses/API/CreateTreatmentGL/InquiryGLData</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
