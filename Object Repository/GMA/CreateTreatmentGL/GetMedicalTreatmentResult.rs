<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GetMedicalTreatmentResult</name>
   <tag></tag>
   <elementGuidId>db87e0fb-b8fa-4d5f-8f8b-d7dbd30c7add</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;IsGLProvider&quot;,
      &quot;value&quot;: &quot;${IsGLProvider}&quot;
    },
    {
      &quot;name&quot;: &quot;IsODS&quot;,
      &quot;value&quot;: &quot;${IsODS}&quot;
    },
    {
      &quot;name&quot;: &quot;IsODC&quot;,
      &quot;value&quot;: &quot;${IsODC}&quot;
    },
    {
      &quot;name&quot;: &quot;IsProtapCovid&quot;,
      &quot;value&quot;: &quot;${IsProtapCovid}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentDate&quot;,
      &quot;value&quot;: &quot;${TreatmentDate}&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityTreatmentCode&quot;,
      &quot;value&quot;: &quot;${MaternityTreatmentCode}&quot;
    },
    {
      &quot;name&quot;: &quot;ClientClassNo&quot;,
      &quot;value&quot;: &quot;${ClientClassNo}&quot;
    },
    {
      &quot;name&quot;: &quot;MTAdditionalQuestion&quot;,
      &quot;value&quot;: &quot;${MTAdditionalQuestion}&quot;
    },
    {
      &quot;name&quot;: &quot;IsUnregistered&quot;,
      &quot;value&quot;: &quot;${IsUnregistered}&quot;
    },
    {
      &quot;name&quot;: &quot;DiagnosisID&quot;,
      &quot;value&quot;: &quot;${DiagnosisID}&quot;
    },
    {
      &quot;name&quot;: &quot;MedicalTreatmentID&quot;,
      &quot;value&quot;: &quot;${MedicalTreatmentID}&quot;
    },
    {
      &quot;name&quot;: &quot;Billed&quot;,
      &quot;value&quot;: &quot;${Billed}&quot;
    },
    {
      &quot;name&quot;: &quot;Category&quot;,
      &quot;value&quot;: &quot;${Category}&quot;
    },
    {
      &quot;name&quot;: &quot;UnknownBilled&quot;,
      &quot;value&quot;: &quot;${UnknownBilled}&quot;
    },
    {
      &quot;name&quot;: &quot;UnknownPatientCondition&quot;,
      &quot;value&quot;: &quot;${UnknownPatientCondition}&quot;
    },
    {
      &quot;name&quot;: &quot;UnregisteredMedicalTreatment&quot;,
      &quot;value&quot;: &quot;${UnregisteredMedicalTreatment}&quot;
    },
    {
      &quot;name&quot;: &quot;MedicalTreatmentDescription&quot;,
      &quot;value&quot;: &quot;${MedicalTreatmentDescription}&quot;
    },
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;${MemberNo}&quot;
    },
    {
      &quot;name&quot;: &quot;ClientID&quot;,
      &quot;value&quot;: &quot;${ClientID}&quot;
    },
    {
      &quot;name&quot;: &quot;UnknownTotalBilled&quot;,
      &quot;value&quot;: &quot;${UnknownTotalBilled}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.authorization}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.urlGMA}/API/CreateTreatmentGL/GetMedicalTreatmentResult</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
