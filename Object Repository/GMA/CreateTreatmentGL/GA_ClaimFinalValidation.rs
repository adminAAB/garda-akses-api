<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GA_ClaimFinalValidation</name>
   <tag></tag>
   <elementGuidId>d3f453c4-f18e-4b1f-94b4-317ae4166806</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;AdminName&quot;,
      &quot;value&quot;: &quot;${AdminName}&quot;
    },
    {
      &quot;name&quot;: &quot;IsGLProvider&quot;,
      &quot;value&quot;: &quot;${IsGLProvider}&quot;
    },
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;${MemberNo}&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousTrID&quot;,
      &quot;value&quot;: &quot;${PreviousTrID}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentDate&quot;,
      &quot;value&quot;: &quot;${TreatmentDate}&quot;
    },
    {
      &quot;name&quot;: &quot;ProductType&quot;,
      &quot;value&quot;: &quot;${ProductType}&quot;
    },
    {
      &quot;name&quot;: &quot;DiagnosisCode&quot;,
      &quot;value&quot;: &quot;${DiagnosisCode}&quot;
    },
    {
      &quot;name&quot;: &quot;DiagnosisAdditionalInfo&quot;,
      &quot;value&quot;: &quot;${DiagnosisAdditionalInfo}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderID&quot;,
      &quot;value&quot;: &quot;${ProviderID}&quot;
    },
    {
      &quot;name&quot;: &quot;PatientPhone&quot;,
      &quot;value&quot;: &quot;${PatientPhone}&quot;
    },
    {
      &quot;name&quot;: &quot;CallerName&quot;,
      &quot;value&quot;: &quot;${CallerName}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoom&quot;,
      &quot;value&quot;: &quot;${TreatmentRoom}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomAmount&quot;,
      &quot;value&quot;: &quot;${TreatmentRoomAmount}&quot;
    },
    {
      &quot;name&quot;: &quot;DoctorName&quot;,
      &quot;value&quot;: &quot;${DoctorName}&quot;
    },
    {
      &quot;name&quot;: &quot;RoomOption&quot;,
      &quot;value&quot;: &quot;${RoomOption}&quot;
    },
    {
      &quot;name&quot;: &quot;RoomAvailability&quot;,
      &quot;value&quot;: &quot;${RoomAvailability}&quot;
    },
    {
      &quot;name&quot;: &quot;UpgradeClass&quot;,
      &quot;value&quot;: &quot;${UpgradeClass}&quot;
    },
    {
      &quot;name&quot;: &quot;BenefitAmount&quot;,
      &quot;value&quot;: &quot;${BenefitAmount}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderEmail&quot;,
      &quot;value&quot;: &quot;${ProviderEmail}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderFax&quot;,
      &quot;value&quot;: &quot;${ProviderFax}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderPhone&quot;,
      &quot;value&quot;: &quot;${ProviderPhone}&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderExt&quot;,
      &quot;value&quot;: &quot;${ProviderExt}&quot;
    },
    {
      &quot;name&quot;: &quot;IsTiri&quot;,
      &quot;value&quot;: &quot;${IsTiri}&quot;
    },
    {
      &quot;name&quot;: &quot;DefaultProviderID&quot;,
      &quot;value&quot;: &quot;${DefaultProviderID}&quot;
    },
    {
      &quot;name&quot;: &quot;Remarks&quot;,
      &quot;value&quot;: &quot;${Remarks}&quot;
    },
    {
      &quot;name&quot;: &quot;Guid&quot;,
      &quot;value&quot;: &quot;${Guid}&quot;
    },
    {
      &quot;name&quot;: &quot;TicketNo&quot;,
      &quot;value&quot;: &quot;${TicketNo}&quot;
    },
    {
      &quot;name&quot;: &quot;GLType&quot;,
      &quot;value&quot;: &quot;${GLType}&quot;
    },
    {
      &quot;name&quot;: &quot;AccountManager&quot;,
      &quot;value&quot;: &quot;${AccountManager}&quot;
    },
    {
      &quot;name&quot;: &quot;TotalBilled&quot;,
      &quot;value&quot;: &quot;${TotalBilled}&quot;
    },
    {
      &quot;name&quot;: &quot;ClientID&quot;,
      &quot;value&quot;: &quot;${ClientID}&quot;
    },
    {
      &quot;name&quot;: &quot;ClassNo&quot;,
      &quot;value&quot;: &quot;${ClassNo}&quot;
    },
    {
      &quot;name&quot;: &quot;MembershipType&quot;,
      &quot;value&quot;: &quot;${MembershipType}&quot;
    },
    {
      &quot;name&quot;: &quot;AllDiagnosis&quot;,
      &quot;value&quot;: &quot;${AllDiagnosis}&quot;
    },
    {
      &quot;name&quot;: &quot;AllDiagnosisAdditionalInfo&quot;,
      &quot;value&quot;: &quot;${AllDiagnosisAdditionalInfo}&quot;
    },
    {
      &quot;name&quot;: &quot;AllDoctors&quot;,
      &quot;value&quot;: &quot;${AllDoctors}&quot;
    },
    {
      &quot;name&quot;: &quot;Gender&quot;,
      &quot;value&quot;: &quot;${Gender}&quot;
    },
    {
      &quot;name&quot;: &quot;DOB&quot;,
      &quot;value&quot;: &quot;${DOB}&quot;
    },
    {
      &quot;name&quot;: &quot;NewMemberName&quot;,
      &quot;value&quot;: &quot;${NewMemberName}&quot;
    },
    {
      &quot;name&quot;: &quot;PreviousGuid&quot;,
      &quot;value&quot;: &quot;${PreviousGuid}&quot;
    },
    {
      &quot;name&quot;: &quot;GLStatus&quot;,
      &quot;value&quot;: &quot;${GLStatus}&quot;
    },
    {
      &quot;name&quot;: &quot;EmpMemberNo&quot;,
      &quot;value&quot;: &quot;${EmpMemberNo}&quot;
    },
    {
      &quot;name&quot;: &quot;NMEmpID&quot;,
      &quot;value&quot;: &quot;${NMEmpID}&quot;
    },
    {
      &quot;name&quot;: &quot;FollowUpTaskID&quot;,
      &quot;value&quot;: &quot;${FollowUpTaskID}&quot;
    },
    {
      &quot;name&quot;: &quot;IsClient&quot;,
      &quot;value&quot;: &quot;${IsClient}&quot;
    },
    {
      &quot;name&quot;: &quot;CallStatusID&quot;,
      &quot;value&quot;: &quot;${CallStatusID}&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClass&quot;,
      &quot;value&quot;: &quot;${AppropriateRBClass}&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRate&quot;,
      &quot;value&quot;: &quot;${AppropriateRBRate}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentEnd&quot;,
      &quot;value&quot;: &quot;${TreatmentEnd}&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBClassChoosen&quot;,
      &quot;value&quot;: &quot;${AppropriateRBClassChoosen}&quot;
    },
    {
      &quot;name&quot;: &quot;AppropriateRBRateChoosen&quot;,
      &quot;value&quot;: &quot;${AppropriateRBRateChoosen}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomChoosen&quot;,
      &quot;value&quot;: &quot;${TreatmentRoomChoosen}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentRoomAmountChoosen&quot;,
      &quot;value&quot;: &quot;${TreatmentRoomAmountChoosen}&quot;
    },
    {
      &quot;name&quot;: &quot;IsReferral&quot;,
      &quot;value&quot;: &quot;${IsReferral}&quot;
    },
    {
      &quot;name&quot;: &quot;IsSpecialCondition&quot;,
      &quot;value&quot;: &quot;${IsSpecialCondition}&quot;
    },
    {
      &quot;name&quot;: &quot;ReferralReasonCode&quot;,
      &quot;value&quot;: &quot;${ReferralReasonCode}&quot;
    },
    {
      &quot;name&quot;: &quot;CallInStart&quot;,
      &quot;value&quot;: &quot;${CallInStart}&quot;
    },
    {
      &quot;name&quot;: &quot;SecondaryDiagnosisCode&quot;,
      &quot;value&quot;: &quot;${SecondaryDiagnosisCode}&quot;
    },
    {
      &quot;name&quot;: &quot;ClientClassNo&quot;,
      &quot;value&quot;: &quot;${ClientClassNo}&quot;
    },
    {
      &quot;name&quot;: &quot;UserPosition&quot;,
      &quot;value&quot;: &quot;${UserPosition}&quot;
    },
    {
      &quot;name&quot;: &quot;OPNO&quot;,
      &quot;value&quot;: &quot;${OPNO}&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityMedicalTreatment&quot;,
      &quot;value&quot;: &quot;${MaternityMedicalTreatment}&quot;
    },
    {
      &quot;name&quot;: &quot;MaternityFamilyPlanningItem&quot;,
      &quot;value&quot;: &quot;${MaternityFamilyPlanningItem}&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentCode&quot;,
      &quot;value&quot;: &quot;${TreatmentCode}&quot;
    },
    {
      &quot;name&quot;: &quot;IsFromProcessButton&quot;,
      &quot;value&quot;: &quot;${IsFromProcessButton}&quot;
    },
    {
      &quot;name&quot;: &quot;NonMedicalItem&quot;,
      &quot;value&quot;: &quot;${NonMedicalItem}&quot;
    },
    {
      &quot;name&quot;: &quot;IsDiagnosisQuestionNotRegistered&quot;,
      &quot;value&quot;: &quot;${IsDiagnosisQuestionNotRegistered}&quot;
    },
    {
      &quot;name&quot;: &quot;MedicalTreatmentAdditionalQuestion&quot;,
      &quot;value&quot;: &quot;${MedicalTreatmentAdditionalQuestion}&quot;
    },
    {
      &quot;name&quot;: &quot;IsODS&quot;,
      &quot;value&quot;: &quot;${IsODS}&quot;
    },
    {
      &quot;name&quot;: &quot;IsODC&quot;,
      &quot;value&quot;: &quot;${IsODC}&quot;
    },
    {
      &quot;name&quot;: &quot;IsProducttypeChange&quot;,
      &quot;value&quot;: &quot;${IsProducttypeChange}&quot;
    },
    {
      &quot;name&quot;: &quot;IsTreatmentPeriodChange&quot;,
      &quot;value&quot;: &quot;${IsTreatmentPeriodChange}&quot;
    },
    {
      &quot;name&quot;: &quot;IsMaternityTreatmentChange&quot;,
      &quot;value&quot;: &quot;${IsMaternityTreatmentChange}&quot;
    },
    {
      &quot;name&quot;: &quot;IsRoomOptionChange&quot;,
      &quot;value&quot;: &quot;${IsRoomOptionChange}&quot;
    },
    {
      &quot;name&quot;: &quot;IsTreatmentRBClassChange&quot;,
      &quot;value&quot;: &quot;${IsTreatmentRBClassChange}&quot;
    },
    {
      &quot;name&quot;: &quot;IsODSODCChange&quot;,
      &quot;value&quot;: &quot;${IsODSODCChange}&quot;
    },
    {
      &quot;name&quot;: &quot;IsDocValidityChange&quot;,
      &quot;value&quot;: &quot;${IsDocValidityChange}&quot;
    },
    {
      &quot;name&quot;: &quot;IsDocTypeChange&quot;,
      &quot;value&quot;: &quot;${IsDocTypeChange}&quot;
    },
    {
      &quot;name&quot;: &quot;suspectDouble&quot;,
      &quot;value&quot;: &quot;${suspectDouble}&quot;
    },
    {
      &quot;name&quot;: &quot;MedicalTreatmentConfirmation&quot;,
      &quot;value&quot;: &quot;${MedicalTreatmentConfirmation}&quot;
    },
    {
      &quot;name&quot;: &quot;IsProtapCovid&quot;,
      &quot;value&quot;: &quot;${IsProtapCovid}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.authorization}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.urlGMA}/API/CreateTreatmentGL/GA_ClaimFinalValidation</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
