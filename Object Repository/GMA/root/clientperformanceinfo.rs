<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>clientperformanceinfo</name>
   <tag></tag>
   <elementGuidId>93671982-6986-4416-afe8-b0e29187246a</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;menu&quot;,
      &quot;value&quot;: &quot;Health/Create E-GL&quot;
    },
    {
      &quot;name&quot;: &quot;loaded&quot;,
      &quot;value&quot;: &quot;8036&quot;
    },
    {
      &quot;name&quot;: &quot;ready&quot;,
      &quot;value&quot;: &quot;8472&quot;
    },
    {
      &quot;name&quot;: &quot;Complete&quot;,
      &quot;value&quot;: &quot;8525&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer hbCwAI+7f8OD3f8JdHEXznVcWy2sPRzTI8KVWJ6NQTs0AJbDb11d6+NkXyVOqmmN9j7UR6++khO05ZdlBF+xwlUPeMs3SV2RWvVui2EO9sy8sbEXUswXy37KFn6NWEF4qfTQgjSST/CpDjuY9aBXYz6romfmdR7SHDBC+F84LHvpcWEU4fI9Uso0ehO5wlmMcP2r3IyDaYe6/G3fRS1yDTxYYRna0hL+oS8e3+vo/XI+n/7hCiB2vzBE7ecZpizN/9fNiKsq+ODVEc9EVgsrzax7HRIBQ6PE8yFNJW/MPHI=</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.urlGMA}/API/root/clientperformanceinfo</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
