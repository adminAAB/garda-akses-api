<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>RRU</description>
   <name>CheckPrePostAllowed - Tes</name>
   <tag></tag>
   <elementGuidId>0e5e997b-4853-46a6-bfb7-269d25e4541b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;V/00005160&quot;
    },
    {
      &quot;name&quot;: &quot;TreatmentType&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;ClaimNo&quot;,
      &quot;value&quot;: &quot;C/03/2100000832&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer jbGyYIqF2cxGMMoquQMzQ/N2KXpyfyHswcNDUOp5Kwe/HcABN7HRj/efBKOBVTXFtVldZJ7WJJhiXaZhx9FGGVASE9HEYS077KBTBzDhiqgtpvX7sKtlsnryqoj271Eg7hzKtCAjp+hOgHzUONRZJVQNhV8NfE7AzUlOBP/eP4VHI50bk2esccXEdf5nDgMChwt8GVD6i+HjSTLuQZa4lZk3PbhKtozDTyL12YnUK9QsZ66nXcwlAncox7WWw2lW79IStxMaU5obhro103r7/W7xznMqk34uVgA0+y/66gg=</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.urlGMA}/API/EligibilityDischarge/CheckPrePostAllowed</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
