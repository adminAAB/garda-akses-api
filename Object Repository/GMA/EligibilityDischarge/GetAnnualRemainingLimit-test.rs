<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GetAnnualRemainingLimit-test</name>
   <tag></tag>
   <elementGuidId>eb484f18-0764-4763-a4d4-fa04dcb4d938</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;data&quot;,
      &quot;value&quot;: &quot;{\&quot;MemberNo\&quot;:\&quot;D/00327\&quot;,\&quot;ProviderID\&quot;:\&quot;\&quot;}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer 2NqxjIy48qQDxZkOiKQvjB+ykY6JKsAtMsjaGIciEreBkyEZv1OxglB3ao9xqhkHA612sk9E0aRGGqmgOtstKLjHXheZawrswOy+KDt5GTXcV0DggnYoG+qi7Bs2oIVv+2Kccom8dCywLFMUUTq88XWO5K38+LKByKsKy7iRJIxey/d4yNkD3f0OCcog3emjpj1gxKxKV4rtao4n58r+IBO+UYs/grKTsL1xTvFWNvm4Dkkg9GRrU5J78MzbxnM8stng6KNNMk+dlQcwOQwDRrniM5BiazHlDQoiZR1s9jUVOGJ20IDjtZVWX97IcInO</value>
   </httpHeaderProperties>
   <katalonVersion>7.9.1</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gardamedikaakses-qc1.asuransiastra.com/GardaAkses/API/EligibilityDischarge/GetAnnualAndRemainingLimit</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
