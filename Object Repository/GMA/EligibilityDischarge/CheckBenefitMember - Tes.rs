<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>NBH</description>
   <name>CheckBenefitMember - Tes</name>
   <tag></tag>
   <elementGuidId>19a4da56-e051-49b5-b9ac-dd408513191d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;MemberNo&quot;,
      &quot;value&quot;: &quot;D/00049754&quot;
    },
    {
      &quot;name&quot;: &quot;ProviderID&quot;,
      &quot;value&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer Do0gpoPoxdaOwrwEtKF5J8KUiaqOLCQMbGpvkj8FyzmM8/S8LCxxEuBn6yb5DziFI5ZO0K3Q/uKSf2WfSgDiafZuVK4EDE78O58KRmLkRejnBY6KXTeIJoVVp61yeM6RI9jSxqth84bbq2vH6D2a728s4Tcy5vHxURIzAJ+/7fA4HagQ91ZpcqKdnAMy/SOVM9MSnU3Mt206f6IfhpSWqdQd0bjEpr47CiavE+T/POR9lEvtoJA8G5gp1359n6etkywfnjcsCn5MZ3s89hDjqFoTXdpCQKoc4gceTfwjZEA=</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Connection</name>
      <type>Main</type>
      <value>keep-alive</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.urlGMA}/API/EligibilityDischarge/CheckBenefitMember</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
