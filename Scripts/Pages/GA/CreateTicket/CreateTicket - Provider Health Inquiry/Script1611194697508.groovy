import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

//API getGreeting
def getGreeting = WS.sendRequest(findTestObject('GA/CreateTicket/getGreeting'))
if (getGreeting.getStatusCode() == 200) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + getGreeting.getStatusCode())
}

//API GetMenuIDByName
def GetMenuIDByName = WS.sendRequest(findTestObject('GA/CreateTicket/GetMenuIDByName'))
if (API.getResponseData(GetMenuIDByName).Data.MenuID == 1000081) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
}

//API GetJobFunction
def GetJobFunction = WS.sendRequest(findTestObject('GA/CreateTicket/GetJobFunction', [('username') : username]))
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//API GetChannelType
def GetChannelType = WS.sendRequest(findTestObject('GA/CreateTicket/GetChannelType', [('contactLine') : contactLine]))
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//API GetContactType
def GetContactType = WS.sendRequest(findTestObject('GA/CreateTicket/GetContactType', [('contactLine') : contactLine]))
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//API GetCauseOfComplaint
def GetCauseOfComplaint = WS.sendRequest(findTestObject('GA/CreateTicket/GetCauseOfComplaint'))
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//API GetDocStatus
def GetDocStatus = WS.sendRequest(findTestObject('GA/CreateTicket/GetDocStatus'))
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//API BindComboboxPopUpConStatus
def BindComboboxPopUpConStatus = WS.sendRequest(findTestObject('GA/CreateTicket/BindComboboxPopUpConStatus'))
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//API GetMasterServiceType
def GetMasterServiceType = WS.sendRequest(findTestObject('GA/CreateTicket/GetMasterServiceType'))
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//API GetBenefitInfoDetailCD
def GetBenefitInfoDetailCD = WS.sendRequest(findTestObject('GA/CreateTicket/GetBenefitInfoDetailCD'))
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//API GetServiceType
def GetServiceType = WS.sendRequest(findTestObject('GA/CreateTicket/GetServiceType', [('contactLine') : contactLine, ('product') : product]))
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//API GetProviderInfoFullText
def GetProviderInfoFullText = WS.sendRequest(findTestObject('GA/CreateTicket/GetProviderInfoFullText', [('providerName') : providerName]))
GlobalVariable.providerID = API.getResponseData(GetProviderInfoFullText).DataID
GlobalVariable.providerDataType = API.getResponseData(GetProviderInfoFullText).DataType
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//API GetDetailInfoByID
def GetDetailInfoByID = WS.sendRequest(findTestObject('GA/CreateTicket/GetDetailInfoByID'))
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//API GetIntterupted
def GetIntterupted = WS.sendRequest(findTestObject('GA/CreateTicket/GetIntterupted', [('contactLine') : contactLine, ('contactName') : contactName]))
//if (API.getResponseData(signin).errorCode == 0) {
//	KeywordUtil.markPassed('Sesuai Skenario ' + API.getResponseData(GetIntterupted).errorCode)
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API ! " + API.getResponseData(GetIntterupted).errorCode)
//}

//API GenerateGUID
def GenerateGUID = WS.sendRequest(findTestObject('GA/CreateTicket/GenerateGUID'))
GlobalVariable.guid = API.getResponseData(GenerateGUID).guid
//if (API.getResponseData(getGreeting).status == 'true') {
//	KeywordUtil.markPassed('API OK')
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(getGreeting).ErrorMessage)
//}

//Get current date and time
Date today = new Date()
String callDate = today.format('dd/MM/yyyy HH:mm')
WebUI.comment(callDate)

//API InsertNewCaller
def InsertNewCaller = WS.sendRequest(findTestObject('GA/CreateTicket/InsertNewCaller', [('channelType') : channelType, ('contactName') : contactName, ('phoneNumber') : phoneNumber
            , ('callDate') : callDate, ('email') : email, ('username') : username, ('contactType') : contactType, ('serviceType') : serviceType, ('providerName') : providerName
            , ('ext') : ext, ('fax') : fax, ('product') : product]))
//if (API.getResponseData(signin).errorCode == 0) {
//	KeywordUtil.markPassed('Sesuai Skenario ' + API.getResponseData(GetIntterupted).errorCode)
//} else {
//	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API ! " + API.getResponseData(GetIntterupted).errorCode)
//}