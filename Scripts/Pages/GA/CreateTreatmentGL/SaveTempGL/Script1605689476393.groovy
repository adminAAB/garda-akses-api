import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

def SaveTempGL = WS.sendRequest(findTestObject('GA/CreateTreatmentGL/SaveTempGL',
	[ ('pIsGLProvider') : pIsGLProvider
	, ('guid') : guid
	, ('ticketNo') : ticketNo
	, ('memberNo') : memberNo
	, ('memberName') : memberName
	, ('membership') : membership
	, ('classNo') : classNo
	, ('clientID') : clientID
	, ('clientName') : clientName
	, ('benefitInfo') : benefitInfo
	, ('empID') : empID
	, ('gLType') : gLType
	, ('dOB') : dOB
	, ('productType') : productType
	, ('treatmentStart') : treatmentStart
	, ('treatmentEnd') : treatmentEnd
	, ('providerID') : providerID
	, ('providerName') : providerName
	, ('providerEmail') : providerEmail
	, ('providerFax') : providerFax
	, ('providerPhone') : providerPhone
	, ('providerExt') : providerExt
	, ('isPassedAway') : isPassedAway
	, ('diagnosis') : diagnosis
	, ('accountManager') : accountManager
	, ('doctor') : doctor
	, ('remarks') : remarks
	, ('appropriateRBClass') : appropriateRBClass
	, ('treatmentRBClass') : treatmentRBClass
	, ('appropriateRBRate') : appropriateRBRate
	, ('treatmentRBRate') : treatmentRBRate
	, ('roomOption') : roomOption
	, ('remainingLimit') : remainingLimit
	, ('remainingLimitType') : remainingLimitType
	, ('rBScheme') : rBScheme
	, ('iCULimit') : iCULimit
	, ('isolationLimit') : isolationLimit
	, ('totalBilled') : totalBilled
	, ('additionalDiagnosisInfo') : additionalDiagnosisInfo
	, ('familyPhone') : familyPhone
	, ('previousGuid') : previousGuid
	, ('previousTrID') : previousTrID
	, ('gLStatus') : gLStatus
	, ('callerName') : callerName
	, ('isPreAdmission') : isPreAdmission
	, ('isInteruptedCalls') : isInteruptedCalls
	, ('isReject') : isReject
	, ('nMMemberType') : nMMemberType
	, ('nMMemberName') : nMMemberName
	, ('nMEmpID') : nMEmpID
	, ('nMEmployee') : nMEmployee
	, ('nMClassification') : nMClassification
	, ('nMClientId') : nMClientId
	, ('nMDOB') : nMDOB
	, ('nMGender') : nMGender
	, ('nMIsNewBorn') : nMIsNewBorn
	, ('nMIsTwin') : nMIsTwin
	, ('isNeedFollowUpF') : isNeedFollowUpF
	, ('appropriateRBClassActual') : appropriateRBClassActual
	, ('appropriateRBRateActual') : appropriateRBRateActual
	, ('treatmentRBClassActual') : treatmentRBClassActual
	, ('treatmentRBRateActual') : treatmentRBRateActual
	, ('isReferral') : isReferral
	, ('isSpecialCondition') : isSpecialCondition
	, ('referralReasonCode') : referralReasonCode
	, ('specialConditionReason') : specialConditionReason
	, ('actor') : actor
	, ('isFromCreateTicket') : isFromCreateTicket
	, ('inboundCallGuid') : inboundCallGuid
	, ('treatmentTT') : treatmentTT
	, ('appropriateTT') : appropriateTT
	, ('clientClassNo') : clientClassNo
	, ('maternityScheme') : maternityScheme
	, ('coveredQty') : coveredQty
	, ('alwaysGuaranted') : alwaysGuaranted
	, ('maternityType') : maternityType
	, ('coverageType') : coverageType
	, ('maternityPackagePrice') : maternityPackagePrice
	, ('maternityFamilyPlanningItem') : maternityFamilyPlanningItem
	, ('maternityMedicalTreatment') : maternityMedicalTreatment
	, ('maternityTreatmentCode') : maternityTreatmentCode
	, ('providerAdminName') : providerAdminName
	, ('treatmentDuration') : treatmentDuration
	, ('passedAwayDate') : passedAwayDate
	, ('doctorOnCall') : doctorOnCall
	, ('lastCondition') : lastCondition
	, ('lastTreatment') : lastTreatment
	, ('resusitasiF') : resusitasiF
	, ('nonMedicalItem') : nonMedicalItem
	, ('isDiagnosisQuestionNotRegistered') : isDiagnosisQuestionNotRegistered
	, ('isGLProvider') : isGLProvider
	, ('isODS') : isODS
	, ('isODC') : isODC
	, ('doctorID') : doctorID
	, ('schemeType') : schemeType
	, ('gMRBClass') : gMRBClass
	, ('providerRBClass') : providerRBClass
	, ('amountLimit') : amountLimit
	, ('rBAmount') : rBAmount
	, ('isCovid') : isCovid]))

if (SaveTempGL.getStatusCode() == 200) {
	if (API.getResponseData(SaveTempGL).Status == true && API.getResponseData(SaveTempGL).Data == 'Save Temp GL - OK') {
		API.Note('SaveTempGL Success Untuk Skenario Tidak Punya Produk IP')
	} else {
	API.Note('SaveTempGL Error Skenario !')
	}
} else {
API.Note('Terjadi kesalahan pada website ! ' + SaveTempGL.getStatusCode())
}