import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

def var = WS.sendRequest(findTestObject('Object Repository/CCOOutbound/GA_ClaimFinalValidation',
	[('authorization') : GlobalVariable.authorization
	, ('MemberNo') : MemberNo
	, ('PreviousTrID') : PreviousTrID
	, ('TreatmentDate') : TreatmentDate
	, ('ProductType') : ProductType
	, ('DiagnosisCode') : DiagnosisCode
	, ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
	, ('ProviderID') : ProviderID
	, ('PatientPhone') : PatientPhone
	, ('CallerName') : CallerName
	, ('TreatmentRoom') : TreatmentRoom
	, ('TreatmentRoomAmount') : TreatmentRoomAmount
	, ('DoctorName') : DoctorName
	, ('RoomOption') : RoomOption
	, ('RoomAvailability') : RoomAvailability
	, ('UpgradeClass') : UpgradeClass
	, ('BenefitAmount') : BenefitAmount
	, ('ProviderEmail') : ProviderEmail
	, ('ProviderFax') : ProviderFax
	, ('ProviderPhone') : ProviderPhone
	, ('ProviderExt') : ProviderExt
	, ('IsTiri') : IsTiri
	, ('DefaultProviderID') : DefaultProviderID
	, ('Remarks') : Remarks
	, ('ServiceTypeID') : ServiceTypeID
	, ('Guid') : Guid
	, ('TicketNo') : TicketNo
	, ('GLType') : GLType
	, ('AccountManager') : AccountManager
	, ('TotalBilled') : TotalBilled
	, ('ClientID') : ClientID
	, ('ClassNo') : ClassNo
	, ('MembershipType') : MembershipType
	, ('AllDiagnosis') : AllDiagnosis
	, ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo
	, ('AllDoctors') : AllDoctors
	, ('Gender') : Gender
	, ('DOB') : DOB
	, ('NewMemberName') : NewMemberName
	, ('PreviousGuid') : PreviousGuid
	, ('GLStatus') : GLStatus
	, ('EmpMemberNo') : EmpMemberNo
	, ('NMEmpID') : NMEmpID
	, ('FollowUpTaskID') : FollowUpTaskID
	, ('IsClient') : IsClient
	, ('CallStatusID') : CallStatusID
	, ('AppropriateRBClass') : AppropriateRBClass
	, ('AppropriateRBRate') : AppropriateRBRate
	, ('TreatmentEnd') : TreatmentEnd
	, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen
	, ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen
	, ('TreatmentRoomChoosen') : TreatmentRoomChoosen
	, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
	, ('IsReferral') : IsReferral
	, ('IsSpecialCondition') : IsSpecialCondition
	, ('ReferralReasonCode') : ReferralReasonCode
	, ('CallInStart') : CallInStart
	, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode
	, ('ClientClassNo') : ClientClassNo
	, ('UserPosition') : UserPosition
	, ('PopUpMsg') : PopUpMsg
	, ('FUStatus') : FUStatus
	, ('OPNO') : OPNO
	, ('TrIDCallOut') : TrIDCallOut
	, ('BenefitCoverage') : BenefitCoverage
	, ('IsDiagnosisNeedNextValidation') : IsDiagnosisNeedNextValidation
	, ('IsAnnualLimitNeedNextValidation') : IsAnnualLimitNeedNextValidation
	, ('IsNewMemberNeedNextValidation') : IsNewMemberNeedNextValidation
	, ('IsExcessNeedNextValidation') : IsExcessNeedNextValidation
	, ('IsDocumentNeedNextValidation') : IsDocumentNeedNextValidation
	, ('MaternityMedicalTreatment') : MaternityMedicalTreatment
	, ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem
	, ('TreatmentCode') : TreatmentCode
	, ('IsEligibilityNeedNextValidation') : IsEligibilityNeedNextValidation
	, ('IsMedicalTreatmentNeedNextValidation') : IsMedicalTreatmentNeedNextValidation
	, ('IsFamilyPlanningNeedNextValidation') : IsFamilyPlanningNeedNextValidation
	, ('IsProductNeedNextValidation') : IsProductNeedNextValidation
	, ('Source') : Source
	, ('IsExcessCalculation') : IsExcessCalculation
	, ('IsFromProcessButton') : IsFromProcessButton
	, ('NonMedicalItem') : NonMedicalItem
	, ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered
	, ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion
	, ('IsGLProvider') : IsGLProvider
	, ('IsODS') : IsODS
	, ('IsODC') : IsODC ]))

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

API.Note(API.getResponseData(var).Status)
//API.Note(API.getResponseData(var).Data)
API.Note(API.getResponseData(var).Data.ClaimValidationResult)
API.Note(API.getResponseData(var).Data.ClaimCallInStatus)
API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)


if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}