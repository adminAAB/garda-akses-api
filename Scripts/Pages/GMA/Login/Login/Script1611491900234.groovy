import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.keyword.UI as UI

def var = WS.sendRequest(findTestObject('GMA/root/signin'))

if (var.getStatusCode() == 200) {
	if (API.getResponseData(var).status == true && API.getResponseData(var).errorCode == 0) {
		API.Note('Login Sukses')
		println API.getResponseData(var).token
		GlobalVariable.authorization = API.getResponseData(var).token
	} else if (API.getResponseData(var).status == false && API.getResponseData(var).errorCode == 100006) {
		KeywordUtil.markFailedAndStop('Login gagal')
	} else {
		KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! ")
	}
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! ")
}