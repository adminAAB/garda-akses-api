import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

String MemberNo = findTestData('MemberGMAOP').getValue(1, 1)

String TreatmentType = 'OP'

String data = '''{"CNO":0,"MemberNo":"'''+ MemberNo +'''","ProductType":"'''+ TreatmentType +'''","RoomOption":"","ClaimNo":"","DocumentNo":0,"ReferenceClaimNo":"","ReferenceDocumentNo":0,"RefLimitRecoveryClaimNo":"","RefLimitRecoveryDocumentNo":0}'''

WebUI.callTestCase(findTestCase('Pages/GMA/root/Signin'),[:])

def GetClaimDetails = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetClaimDetails',
	[('data') : data]))

if (API.getResponseData(GetClaimDetails).Status) {
	API.Note((API.getResponseData(GetClaimDetails).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetClaimDetails).Status))
	API.Note((API.getResponseData(GetClaimDetails).Message))
	API.Note((API.getResponseData(GetClaimDetails).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

