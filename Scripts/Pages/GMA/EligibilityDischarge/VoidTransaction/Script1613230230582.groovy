import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

String MemberNo = findTestData('MemberGMAOP').getValue(1, 1)

String TreatmentType = 'OP'

WebUI.callTestCase(findTestCase('Pages/GMA/root/signin - Sukses'), [:])

def ProcessRegistrationEligibility = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/ProcessRegistrationEligibility',
	[('MemberNo') : MemberNo, ('TreatmentType') : TreatmentType]))

API.Note(ProcessRegistrationEligibility.getResponseBodyContent())

if (API.getResponseData(ProcessRegistrationEligibility).Status) {
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Status))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Message))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def VoidTransaction = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/VoidTransaction',
	[('MemberNo') : MemberNo]))

API.Note(VoidTransaction.getResponseBodyContent())

if (API.getResponseData(VoidTransaction).Status) {
	API.Note((API.getResponseData(VoidTransaction).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(VoidTransaction).Status))
	API.Note((API.getResponseData(VoidTransaction).Message))
	API.Note((API.getResponseData(VoidTransaction).Data))
	KeywordUtil.markFailedAndStop("Status False")
}