import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

String MemberNo = findTestData('MemberGMAOP').getValue(1, 1)

String TreatmentType = 'OP'

String dataGetClaimDetails = '''{"CNO":0,"MemberNo":"'''+ MemberNo +'''","ProductType":"'''+ TreatmentType +'''","RoomOption":"","ClaimNo":"","DocumentNo":0,"ReferenceClaimNo":"","ReferenceDocumentNo":0,"RefLimitRecoveryClaimNo":"","RefLimitRecoveryDocumentNo":0}'''

String pDiagnosisID = 'A36'

String PrimaryDiagnosis = 'A36'

String SecondDiagnosis = ''

WebUI.callTestCase(findTestCase('Pages/GMA/root/Signin'),[:])

//========================================================================================================================
def ProcessRegistrationEligibility = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/ProcessRegistrationEligibility',
	[('MemberNo') : MemberNo, ('TreatmentType') : TreatmentType]))

API.Note(ProcessRegistrationEligibility.getResponseBodyContent())

if (API.getResponseData(ProcessRegistrationEligibility).Status) {
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Status))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Message))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Data))
	KeywordUtil.markFailedAndStop("Status False")
}
//========================================================================================================================
def GetClaimDetails = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetClaimDetails',
	[('data') : dataGetClaimDetails]))

API.Note(GetClaimDetails.getResponseBodyContent())

GlobalVariable.dataClaimDetails = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0]

GlobalVariable.POPNO = API.getResponseData(GetClaimDetails).Data.OPNO

API.Note(GlobalVariable.dataClaimDetails)

if (API.getResponseData(GetClaimDetails).Status) {
	API.Note((API.getResponseData(GetClaimDetails).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetClaimDetails).Status))
	API.Note((API.getResponseData(GetClaimDetails).Message))
	API.Note((API.getResponseData(GetClaimDetails).Data))
	KeywordUtil.markFailedAndStop("Status False")
}
//========================================================================================================================
def GetDiagnosisInfo = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetDiagnosisInfo',
	[('pDiagnosisID') : pDiagnosisID]))

API.Note(GetDiagnosisInfo.getResponseBodyContent())

GlobalVariable.dataGetDiagnosisInfo = API.getResponseData(GetDiagnosisInfo).Data

if (API.getResponseData(GetDiagnosisInfo).Status) {
	API.Note((API.getResponseData(GetDiagnosisInfo).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetDiagnosisInfo).Status))
	API.Note((API.getResponseData(GetDiagnosisInfo).Message))
	API.Note((API.getResponseData(GetDiagnosisInfo).Data))
	KeywordUtil.markFailedAndStop("Status False")
}
//========================================================================================================================
def GetMasterDoctor = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetMasterDoctor'))

API.Note(GetMasterDoctor.getResponseBodyContent())

GlobalVariable.DoctorID = API.getResponseData(GetMasterDoctor).Data.Items[0].DoctorID

GlobalVariable.DoctorName = API.getResponseData(GetMasterDoctor).Data.Items[0].DoctorName

GlobalVariable.DoctorSpecialty = API.getResponseData(GetMasterDoctor).Data.Items[0].DoctorSpecialty

if (API.getResponseData(GetMasterDoctor).Status) {
	API.Note((API.getResponseData(GetMasterDoctor).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetMasterDoctor).Status))
	API.Note((API.getResponseData(GetMasterDoctor).Message))
	API.Note((API.getResponseData(GetMasterDoctor).Data))
	KeywordUtil.markFailedAndStop("Status False")
}
//========================================================================================================================
def SubmitDischargeTransaction = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/SubmitDischargeTransaction',
	[('TreatmentType') : TreatmentType
		,('MemberNo') : MemberNo
		,('PrimaryDiagnosis') : PrimaryDiagnosis
		,('SecondDiagnosis') : SecondDiagnosis
		,('ListBenefit') : GlobalVariable.dataClaimDetails
		,('AllDiagnosisInfo') : GlobalVariable.dataGetDiagnosisInfo]))

API.Note(SubmitDischargeTransaction.getResponseBodyContent())

if (API.getResponseData(SubmitDischargeTransaction).Status) {
	API.Note((API.getResponseData(SubmitDischargeTransaction).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(SubmitDischargeTransaction).Status))
	API.Note((API.getResponseData(SubmitDischargeTransaction).Message))
	API.Note((API.getResponseData(SubmitDischargeTransaction).Data))
	KeywordUtil.markFailedAndStop("Status False")
}
//========================================================================================================================