import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

def username = GlobalVariable.usernameGMAOP

def password = GlobalVariable.passwordGMAOP

def biztype = 12

def signin = WS.sendRequest(findTestObject('Object Repository/GMA/root/signin', 
	[('username') : username
		, ('password') : password
		, ('biztype') : biztype]))

if (signin.getStatusCode() == 200) {
	API.Note('Login Sukses')
	API.Note(API.getResponseData(signin).token)
	GlobalVariable.authorization = API.getResponseData(signin).token
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! ")
}

