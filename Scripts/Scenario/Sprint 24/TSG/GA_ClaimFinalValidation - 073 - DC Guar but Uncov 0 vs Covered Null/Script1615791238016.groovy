import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String AdminName = ''
String IsGLProvider = 'False'
String MemberNo = 'S/00385'
String PreviousTrID = ''
String TreatmentDate = '2021-03-10'
String ProductType = 'IP'
String DiagnosisCode = 'S02.4'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'TJKRP0146'
String PatientPhone = '081808623854'
String CallerName = 'maste'
String TreatmentRoom = 'KELAS III'
String TreatmentRoomAmount = '190000'
String DoctorName = 'test'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '830000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = '197'
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String Guid = '175559460d2a4143bd211172d766c0653fd40d8e8888ce5ad6ecaa963a53f5c3'
String TicketNo = 'CL194253'
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'CJKPA0064'
String ClassNo = '29'
String MembershipType = '1. EMP'
String AllDiagnosis = '[[2,"S02.4","Bitten or struck by other mammals","Initial Primary","","","","","","","","","Covered","","","","","",0,"1","0"]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String AllDoctors = 'test'
String Gender = 'F'
String DOB = '1996-03-08'
String NewMemberName = ''
String PreviousGuid = ''
String GLStatus = ''
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '175559460d2a4143bd211172d766c0653fd40d8e8888ce5ad6ecaa963a53f5c3'
String IsClient = '1'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'KELAS III'
String AppropriateRBRate = '190000'
String TreatmentEnd = ''
String AppropriateRBClassChoosen = 'KELAS III'
String AppropriateRBRateChoosen = '190000'
String TreatmentRoomChoosen = 'KELAS III'
String TreatmentRoomAmountChoosen = '190000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = '2021-03-10 13:49:00'
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '0'
String UserPosition = 'Customer Service'
String OPNO = '89571'
String MaternityMedicalTreatment = ''
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = ''
String IsODS = '0'
String IsODC = '0'
String IsProducttypeChange = '1'
String IsTreatmentPeriodChange = '1'
String IsMaternityTreatmentChange = '1'
String IsRoomOptionChange = '1'
String IsTreatmentRBClassChange = '1'
String IsODSODCChange = '1'
String IsDocValidityChange = '1'
String IsDocTypeChange = '1'
String suspectDouble = '0'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/GA_ClaimFinalValidation_New', [('authorization') : GlobalVariable.authorization     	      
 , ('AdminName') : AdminName , ('IsGLProvider') : IsGLProvider , ('MemberNo') : MemberNo , ('PreviousTrID') : PreviousTrID
 , ('TreatmentDate') : TreatmentDate , ('ProductType') : ProductType , ('DiagnosisCode') : DiagnosisCode , ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
 , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom
 , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability
 , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax
 , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('DefaultProviderID') : DefaultProviderID
 , ('Remarks') : Remarks , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType
 , ('AccountManager') : AccountManager , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo
 , ('MembershipType') : MembershipType , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender
 , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('PreviousGuid') : PreviousGuid , ('GLStatus') : GLStatus
 , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('IsClient') : IsClient
 , ('CallStatusID') : CallStatusID , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd
 , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
 , ('IsReferral') : IsReferral , ('IsSpecialCondition') : IsSpecialCondition , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart
 , ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('ClientClassNo') : ClientClassNo , ('UserPosition') : UserPosition , ('OPNO') : OPNO
 , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('TreatmentCode') : TreatmentCode , ('IsFromProcessButton') : IsFromProcessButton
 , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('IsODS') : IsODS
 , ('IsODC') : IsODC , ('IsProducttypeChange') : IsProducttypeChange , ('IsTreatmentPeriodChange') : IsTreatmentPeriodChange , ('IsMaternityTreatmentChange') : IsMaternityTreatmentChange
 , ('IsRoomOptionChange') : IsRoomOptionChange , ('IsTreatmentRBClassChange') : IsTreatmentRBClassChange , ('IsODSODCChange') : IsODSODCChange , ('IsDocValidityChange') : IsDocValidityChange
 , ('IsDocTypeChange') : IsDocTypeChange , ('suspectDouble') : suspectDouble , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo ]))


API.Note(API.getResponseData(var).Status)
//API.Note(API.getResponseData(var).Data)
//API.Note(API.getResponseData(var).Data.ClaimValidationMessage)
//API.Note(API.getResponseData(var).Data.ClaimValidationNonCoverReason)
//API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
//API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)


//Cek Hasil Validasi Diagnosa

def AllValidation=API.getResponseData(var).Data.ClaimValidationNonCoverReason
def AllDiagnosisFU=API.getResponseData(var).Data.ClaimValidationDiagnosis

if (AllValidation==null){
	KeywordUtil.markPassed('Validasi Diagnosis Cover Sesuai')
	if (AllDiagnosisFU==''){
		KeywordUtil.markPassed('Diagnosis Cover Sesuai')
	} else {
		KeywordUtil.markFailed('Diagnosis Cover Tidak Sesuai')
	} 
} else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')
}
/*
	} else {
		KeywordUtil.markFailed('NON COVER FAILED')
	}
} else {
	KeywordUtil.markFailed('NON COVER IS NULL')
}
*/

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}