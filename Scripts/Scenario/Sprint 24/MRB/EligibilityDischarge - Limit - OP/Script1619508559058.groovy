import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.keyword.API as API

String MemberNo = 'D/00327'

String TreatmentType = 'OP'

String TreatmentTypeIcon = 'OP'

String PrimaryDiagnosis = '0101.1'

String SecondDiagnosis = ''

String Billed = '5000'

String ListBenefit = '[{"Name":"DANIA ADIBA SHAHAB","ExcessEmpF":1,"BenefitID":"GM-OPB-01","BenefitName":"Biaya Konsultasi dan tindakan Dokter Umum","BenefitLimit":100000,"BenefitLimitHighPlan":100000,"InitialBenefitLimit":100000,"BenefitType":"Occurrence","SDate":null,"EDate":null,"Qty":null,"Billed":'+Billed+',"Verified":500,"Accepted":'+Billed+',"UnPaid":0,"ExcessTotal":0,"ExcessCompany":0,"ExcessEmployee":0,"Remarks":"","BenefitIndo":"benefit biaya konsultasi dan tindakan dokter umum","Appropriate":0,"IsRoomAndBoard":false,"IsNotAsCharged":true,"ClaimFormula":"","AppropriateRBAI":"","TreatmentRBAI":"","TempAppropriateRBAI":"","TempTreatmentRBAI":"","IsIncludeRB":false,"IsPre":false,"IsPost":false,"IsApplyRemainingLimitToAllBenefitMA":false,"IsPoolfund":false,"IsIncludeNonMedicalItem":false,"IsIncludeFPItem":false,"PreDays":0,"PreLimit":0,"PostDays":0,"PostLimit":0,"PrePostLimit":999999999,"SurgeryType":"","ParentID":"","OccuranceFreqLimit":999999999,"ConfinementFreqLimit":999999999,"YearlyFreqLimit":999999999,"OccuranceFreqRemainingLimit":999999999,"ConfinementFreqRemainingLimit":999999999,"YearlyFreqRemainingLimit":999999999,"LimitRecoverySameDiagosis":0,"LimitRecoveryDiffDiagosis":0,"ClaimDetailItem":[],"ClaimDetailReference":[],"BenefitGMA":"Biaya Dokter Umum","IsMoveToUnpaid":0,"IsDiagnosisF":0,"IsQtyF":0,"IsDevF":0,"SeparationOfExcess":true,"PolicyProductType":"OP"}]'

String DoctorName = 'Rifa'

String DoctorID = ''

String DoctorSpecialty = ''

String AllDiagnosisInfo = '[{"DiagnosisCode":"0101.1","DiagnosisDescription":"PARATYPHOID FEVER A","Coverage":"Covered","ProductType":null,"IsNeedConfirmation":null,"IsMoveToUnpaid":0,"Payer":null,"IsNeedConfirmationUncovered":0,"IsNeedConfirmationInt":0}]'

String OPNO = '106186'

String ClaimNo = ''

String ProviderID = ''

String GuidGA = ''

String Reason = 'Input Ulang'

WebUI.callTestCase(findTestCase('Pages/GMA/Login/Login'), [:], FailureHandling.STOP_ON_FAILURE)

def regis = WS.sendRequest(findTestObject('GMA/EligibilityDischarge/ProcessRegistrationEligibility', [('MemberNo') : MemberNo
            , ('TreatmentType') : TreatmentType, ('TreatmentTypeIcon') : TreatmentTypeIcon]))

def result = API.getResponseData(regis).Data.ResultRemark

def getGMAID = WS.sendRequest(findTestObject('GMA/EligibilityDischarge/GetRegisteredEligibleMember', [('MemberNo') : MemberNo]))

	String GMAID = API.getResponseData(getGMAID).Data[0].EligibleID

if (result != 'Peserta asuransi ini sudah terdaftar perhari ini') {
   
    def getLimitAwal = WS.sendRequest(findTestObject('GMA/EligibilityDischarge/GetAnnualAndRemainingLimit', [('MemberNo') : MemberNo
                , ('authorization') : GlobalVariable.authorization]))

    String limitAwal = API.getResponseData(getLimitAwal).Data[2].RemainingLimit

    def discharge = WS.sendRequest(findTestObject('GMA/EligibilityDischarge/SubmitDischargeTransaction', [('TreatmentType') : TreatmentType
                , ('MemberNo') : MemberNo, ('PrimaryDiagnosis') : PrimaryDiagnosis, ('SecondDiagnosis') : SecondDiagnosis
                , ('ListBenefit') : ListBenefit, ('DoctorName') : DoctorName, ('DoctorID') : DoctorID, ('DoctorSpecialty') : DoctorSpecialty
                , ('AllDiagnosisInfo') : AllDiagnosisInfo, ('OPNO') : OPNO, ('ClaimNo') : ClaimNo, ('ProviderID') : ProviderID
                , ('GuidGA') : GuidGA, ('GMAID') : GMAID, ('authorization') : GlobalVariable.authorization]))

	WS.sendRequest(findTestObject('GMA/EligibilityDischarge/ProcessRegistrationEligibility', [('MemberNo') : MemberNo
		, ('TreatmentType') : TreatmentType, ('TreatmentTypeIcon') : TreatmentTypeIcon]))
	
	def getLimitAkhir = WS.sendRequest(findTestObject('GMA/EligibilityDischarge/GetAnnualAndRemainingLimit', [('MemberNo') : MemberNo
		, ('authorization') : GlobalVariable.authorization]))
	
	String limitAkhir = API.getResponseData(getLimitAkhir).Data[2].RemainingLimit

    WS.sendRequest(findTestObject('GMA/EligibilityDischarge/VoidTransaction', [('MemberNo') : MemberNo, ('Reason') : Reason
                , ('GMAID') : GMAID]))

	KeywordUtil.markPassed('Limit Awal  = '+ limitAwal)
	KeywordUtil.markPassed('Billed 		= '+ Billed)
	KeywordUtil.markPassed('Limit Akhir = '+ limitAkhir)
		
} else {
    KeywordUtil.markFailed(result)


    def voidTrx = WS.sendRequest(findTestObject('GMA/EligibilityDischarge/VoidTransaction', [('MemberNo') : MemberNo, ('Reason') : Reason
                , ('GMAID') : GMAID]))

    def voidResult = API.getResponseData(voidTrx).Message

    if (voidResult == 'Success') {
        KeywordUtil.markPassed('Berhasil di Void silahkan jalankan ulang')
    }
}

