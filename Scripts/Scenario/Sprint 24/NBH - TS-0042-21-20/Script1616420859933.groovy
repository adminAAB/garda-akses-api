import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

//API Login
String username = GlobalVariable.usernameGMAOP

String password = GlobalVariable.passwordGMAOP

String biztype = '12'

//API Pages
String MemberNo = 'S/00085145'

String TreatmentType = 'OP'

String ClaimNo = ''

String TreatmentTypeIcon = 'OP'

String GuidGA = ''

String ProviderID = ''

//Script
//====================================//
//=======API Login To Get Token=======//
//====================================//
def signin = WS.sendRequest(findTestObject('Object Repository/GMA/root/signin',
	[('username') : username
		, ('password') : password
		, ('biztype') : biztype]))

API.Note(signin.getResponseBodyContent())

if (signin.getStatusCode() == 200) {
	GlobalVariable.authorization = API.getResponseData(signin).token
	API.Note(GlobalVariable.authorization)
	KeywordUtil.markPassed("API signin Passed")
} else {
	API.Note((API.getResponseData(signin).Status))
	API.Note((API.getResponseData(signin).Message))
	API.Note((API.getResponseData(signin).Data))
	KeywordUtil.markFailedAndStop("API signin Failed")
}

//=================================================//
//=======Make Sure member not in Transaction=======//
//=================================================//
String GMAID = API.getValueDatabase('172.16.94.70', 'GMA', GlobalVariable.QueryGetGMAID.replace("_MemberNo_", MemberNo), 'ID')

API.Note(GMAID)

API.Note(GMAID.getClass().getSimpleName())

if (GMAID != 'null') {
	API.updateValueDatabase('172.16.94.70', 'GMA', GlobalVariable.QueryVoidMember.replace("_GMAID_", GMAID))
}

//=========================================//
//=======Get Coverage Tab Eligiblity=======//
//=========================================//
def ProcessRegistrationEligibility = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/ProcessRegistrationEligibility',
	[('MemberNo') : MemberNo,
		('TreatmentType') : TreatmentType,
		('ClaimNo') : ClaimNo,
		('TreatmentTypeIcon') : TreatmentTypeIcon,
		('GuidGA') : GuidGA,
		('ProviderID') : ProviderID]))

API.Note(ProcessRegistrationEligibility.getResponseBodyContent())

if (API.getResponseData(ProcessRegistrationEligibility).Status) {
	KeywordUtil.markPassed("API ProcessRegistrationEligibility Passed")
}  else {
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Status))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Message))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

//=========================//
//=======Verify Data=======//
//=========================//
if (API.getResponseData(ProcessRegistrationEligibility).Data.ResultRemark == 'Anda tidak dapat melanjutkan transaksi klaim secara cashless, dikarenakan limit telah habis') {
	KeywordUtil.markPassed("Member Tidak Memiliki Sisa Limit")
}  else {
	KeywordUtil.markFailedAndStop("Status False")
}