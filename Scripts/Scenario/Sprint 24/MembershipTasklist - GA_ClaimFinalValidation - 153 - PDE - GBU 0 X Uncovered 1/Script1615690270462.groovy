import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'O/00005308'
String PreviousTrID = '2824925'
String TreatmentDate = '2021-03-12'
String ProductType = 'IP'
String DiagnosisCode = 'W04'
String DiagnosisAdditionalInfo = '[]'
String ProviderID = 'TJKRH00013'
String PatientPhone = '0987654321'
String CallerName = 'Dele - Dokter'
String TreatmentRoom = 'KELAS III'
String DoctorName = 'Dele'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String TreatmentRoomAmount = '203000'
String BenefitAmount = '0'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '0218560601'
String ProviderPhone = '081806221986'
String ProviderExt = '121'
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String PreviousGLNo = ''
String Guid = '7a9bd1be35d2455e9e98eaa7daccfe8da1ee0528da190ef1424d1ab69b51ff42'
String TicketNo = 'CL194283'
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'CJKPA11'
String ClassNo = '2'
String MembershipType = '2. SPO'
String AllDiagnosis = '[[1,"W04","Fall while being carried or supported by other persons","Initial Primary","","","","","","","","","Uncovered","","","","","",0,"1","0"],[2,"W01","Fall on same level from slipping, tripping and stumbling","Initial Secondary","","","","","","","","","Uncovered","","","","","",0,"1","0"]]'
String AllDiagnosisAdditionalInfo = '[[],[]]'
String AllDoctors = 'Dele'
String Gender = 'F'
String DOB = '1990-07-19'
String IsClient = '1'
String TreatmentEnd = ''
String IsReferral = '0'
String ReferralReasonCode = ''
String AppropriateRBClass = 'KELAS III'
String AppropriateRBRate = '203000'
String AppropriateRBClassChoosen = 'KELAS III'
String AppropriateRBRateChoosen = '203000'
String TreatmentRoomChoosen = 'KELAS III'
String TreatmentRoomAmountChoosen = '203000'
String IsSpecialCondition = '1'
String ClientClassNo = '2'
String SecondaryDiagnosisCode = '[[1,"W04","Fall while being carried or supported by other persons","Initial Primary","","","","","","","","","Uncovered","","","","","",0,"1","0"],[2,"W01","Fall on same level from slipping, tripping and stumbling","Initial Secondary","","","","","","","","","Uncovered","","","","","",0,"1","0"]]'
String MaternityMedicalTreatment = ''
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String TreatmentDuration = '0'
String IsODS = '0'
String IsODC = '0'
String CallStatusID = 'Not Need Follow Up'
String EmpMemberNo = 'O/00005308'

WebUI.callTestCase(findTestCase('Pages/Health/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/Health/MembershipTasklist/GA_ClaimFinalValidation',
	[('authorization') : GlobalVariable.authorization
	, ('MemberNo') : MemberNo
	, ('PreviousTrID') : PreviousTrID
	, ('TreatmentDate') : TreatmentDate
	, ('ProductType') : ProductType
	, ('DiagnosisCode') : DiagnosisCode
	, ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
	, ('ProviderID') : ProviderID
	, ('PatientPhone') : PatientPhone
	, ('CallerName') : CallerName
	, ('TreatmentRoom') : TreatmentRoom
	, ('DoctorName') : DoctorName
	, ('RoomOption') : RoomOption
	, ('RoomAvailability') : RoomAvailability
	, ('UpgradeClass') : UpgradeClass
	, ('TreatmentRoomAmount') : TreatmentRoomAmount
	, ('BenefitAmount') : BenefitAmount
	, ('ProviderEmail') : ProviderEmail
	, ('ProviderFax') : ProviderFax
	, ('ProviderPhone') : ProviderPhone
	, ('ProviderExt') : ProviderExt
	, ('IsTiri') : IsTiri
	, ('DefaultProviderID') : DefaultProviderID
	, ('Remarks') : Remarks
	, ('PreviousGLNo') : PreviousGLNo
	, ('Guid') : Guid
	, ('TicketNo') : TicketNo
	, ('GLType') : GLType
	, ('AccountManager') : AccountManager
	, ('TotalBilled') : TotalBilled
	, ('ClientID') : ClientID
	, ('ClassNo') : ClassNo
	, ('MembershipType') : MembershipType
	, ('AllDiagnosis') : AllDiagnosis
	, ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo
	, ('AllDoctors') : AllDoctors
	, ('Gender') : Gender
	, ('DOB') : DOB
	, ('IsClient') : IsClient
	, ('TreatmentEnd') : TreatmentEnd
	, ('IsReferral') : IsReferral
	, ('ReferralReasonCode') : ReferralReasonCode
	, ('AppropriateRBClass') : AppropriateRBClass
	, ('AppropriateRBRate') : AppropriateRBRate
	, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen
	, ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen
	, ('TreatmentRoomChoosen') : TreatmentRoomChoosen
	, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
	, ('IsSpecialCondition') : IsSpecialCondition
	, ('ClientClassNo') : ClientClassNo
	, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode
	, ('MaternityMedicalTreatment') : MaternityMedicalTreatment
	, ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem
	, ('TreatmentCode') : TreatmentCode
	, ('TreatmentDuration') : TreatmentDuration
	, ('IsODS') : IsODS
	, ('IsODC') : IsODC
	, ('CallStatusID') : CallStatusID
	, ('EmpMemberNo') : EmpMemberNo ]))

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}


API.Note(API.getResponseData(var).Status)
//API.Note(API.getResponseData(var).Data)
API.Note(API.getResponseData(var).Data.ClaimValidationResult)
API.Note(API.getResponseData(var).Data.ClaimCallInStatus)
API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)

def ValidationResult=API.getResponseData(var).Data.ClaimValidationResult
def AllValidation=API.getResponseData(var).Data.AllClaimValidationNonCoverReason
def AdditionalRemarks=API.getResponseData(var).Data.ClaimValidationAdditionalRemarks
def FinalResult=API.getResponseData(var).Data.ClaimValidationFinalResult

if (IsSpecialCondition == '1'){
	if (ValidationResult == 'COVER' && AllValidation == null && AdditionalRemarks == null && FinalResult == null){
		KeywordUtil.markPassed('New Member (Special Condition) Tidak Terkena Validasi Diagnosis')
	}
} else if (ValidationResult == 'COVER' && AllValidation == null && AdditionalRemarks == null && FinalResult == ''){
	KeywordUtil.markPassed('New Member (Not Special Condition) Tidak Terkena Validasi Diagnosis')
} else if (AllValidation.contains('NONCOVER_DIAGNOSIS')){
	KeywordUtil.markFailed('Diagnosa New Member butuh konfirmasi dokter Garda Medika')
} else {
	KeywordUtil.markPassed('Tidak Terkena Validasi Diagnosis')
}