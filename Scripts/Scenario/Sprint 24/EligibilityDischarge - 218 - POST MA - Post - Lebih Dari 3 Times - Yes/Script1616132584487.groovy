import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String TreatmentTypeMember = 'POSTMA'

String MemberNo = 'D/00049754'
//'C/00017113' POST MA punya Claim MA Existing 
//'K/00030739' POSTMA tapi ga punya Claim MA Existing
//'R/50376' 999 Ga punya OP samsek

String ProviderID = ''

String TreatmentType = ''

String ClaimNo = 'C/03/2100000946'

WebUI.callTestCase(findTestCase('Pages/GMA/root/Signin'),[:])


//API 1 : CheckBenefitMember
def var = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/CheckBenefitMember',
	[('authorization') : GlobalVariable.authorization
	, ('MemberNo') : MemberNo
	, ('ProviderID') : ProviderID ]))

//////////////////////////////////////////////////Cek Status API 1: CheckBenefitMember
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}

API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Message)
API.Note(API.getResponseData(var).Data.Treatment.TreatmentType)
API.Note(API.getResponseData(var).Data.PrepostFlagMA)
API.Note(API.getResponseData(var).Data.PrepostDataMA)
API.Note(API.getResponseData(var).Data.ResultCode)

def TreatmentTypeGMAOP=API.getResponseData(var).Data.Treatment.TreatmentType
def FlagPostMA=API.getResponseData(var).Data.PrepostFlagMA
def ClaimNoMAExisting=API.getResponseData(var).Data.PrepostDataMA.ClaimNo
def TreatmentCodePostMA=API.getResponseData(var).Data.PrepostDataMA.TreatmentCode
def OPNotEligibleCode=API.getResponseData(var).Data.ResultCode
def OPNotEligible=API.getResponseData(var).Data.ResultRemarks
def PrepostDataMA=API.getResponseData(var).Data.PrepostDataMA

if (TreatmentTypeGMAOP.contains(TreatmentTypeMember)) {
	if (FlagPostMA == true && TreatmentCodePostMA.contains(TreatmentTypeMember)) {
		KeywordUtil.markPassed("Member memiliki Product [" + TreatmentTypeMember + "] dengan Claim No MA existing sebagai berikut: [" + ClaimNoMAExisting + "]")}
	else {
		KeywordUtil.markPassed("Member memiliki Product [" + TreatmentTypeMember + "] namun tidak memiliki claim MA existing")
	}
}
else if (OPNotEligibleCode == '999' && PrepostDataMA == []) {
	KeywordUtil.markFailed("Muncul Pop Up berikut: " + OPNotEligible)
}
else if (OPNotEligibleCode == '696' && PrepostDataMA == []) {
	KeywordUtil.markFailed("Muncul Pop Up berikut: " + OPNotEligible)
}
else {
	KeywordUtil.markFailed("Member tidak memiliki Product [" + TreatmentTypeMember + "]. Member hanya memiliki Product " + TreatmentTypeGMAOP)
}


//API 2 : CheckPrePostAllowed
def var2 = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/CheckPrePostAllowed',
	[('authorization') : GlobalVariable.authorization
	, ('MemberNo') : MemberNo
	, ('TreatmentType') : TreatmentType
	, ('ClaimNo') : ClaimNo ]))

API.Note(API.getResponseData(var2).Status)

//////////////////////////////////////////////////Cek Status API 2: CheckPrePostAllowed
if (API.getResponseData(var2).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var2).ErrorMessage)
}

API.Note(API.getResponseData(var2).Status)
API.Note(API.getResponseData(var2).Message)
API.Note(API.getResponseData(var2).Data)

def StatusVar2=API.getResponseData(var2).Status
def Message=API.getResponseData(var2).Message
def Data=API.getResponseData(var2).Data

if (StatusVar2 == true && Message == 'Success') {
	if (Data == 1) {
		KeywordUtil.markFailed("Product [" + TreatmentTypeMember + "] eligible untuk Member [" + MemberNo + "]. Claim akan dijaminkan sebagai [" + TreatmentTypeMember + "]")}
	else {
		KeywordUtil.markPassed("Product [" + TreatmentTypeMember + "] tidak eligible untuk Member [" + MemberNo + "]. Claim akan dijaminkan sebagai [ OP ]")
	}
}