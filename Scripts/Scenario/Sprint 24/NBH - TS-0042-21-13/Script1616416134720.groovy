import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

//API Login
String username = GlobalVariable.usernameGMAOP

String password = GlobalVariable.passwordGMAOP

String biztype = '12'

//API Pages
String MemberNo = 'R/00073389'

String Reason = 'Automation Test - Sprint 24'

String TreatmentType = 'DT'

String ClaimNo = ''

String TreatmentTypeIcon = 'DT'

String GuidGA = ''

String ProviderID = ''

//Script
//====================================//
//=======API Login To Get Token=======//
//====================================//
def signin = WS.sendRequest(findTestObject('Object Repository/GMA/root/signin',
	[('username') : username
		, ('password') : password
		, ('biztype') : biztype]))

API.Note(signin.getResponseBodyContent())

if (signin.getStatusCode() == 200) {
	GlobalVariable.authorization = API.getResponseData(signin).token
	API.Note(GlobalVariable.authorization)
	KeywordUtil.markPassed("API signin Passed")
} else {
	API.Note((API.getResponseData(signin).Status))
	API.Note((API.getResponseData(signin).Message))
	API.Note((API.getResponseData(signin).Data))
	KeywordUtil.markFailedAndStop("API signin Failed")
}

//=================================================//
//=======Make Sure member not in Transaction=======//
//=================================================//
String GMAID = API.getValueDatabase('172.16.94.70', 'GMA', GlobalVariable.QueryGetGMAID.replace("_MemberNo_", MemberNo), 'ID')

API.Note(GMAID)

API.Note(GMAID.getClass().getSimpleName())

if (GMAID != 'null') {
	API.updateValueDatabase('172.16.94.70', 'GMA', GlobalVariable.QueryVoidMember.replace("_GMAID_", GMAID))
}

//=========================================//
//=======Get Coverage Tab Eligiblity=======//
//=========================================//
def ProcessRegistrationEligibility = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/ProcessRegistrationEligibility',
	[('MemberNo') : MemberNo,
		('TreatmentType') : TreatmentType,
		('ClaimNo') : ClaimNo,
		('TreatmentTypeIcon') : TreatmentTypeIcon,
		('GuidGA') : GuidGA,
		('ProviderID') : ProviderID]))

API.Note(ProcessRegistrationEligibility.getResponseBodyContent())

if (API.getResponseData(ProcessRegistrationEligibility).Status) {
	KeywordUtil.markPassed("API ProcessRegistrationEligibility Passed")
}  else {
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Status))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Message))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

//========================================//
//=======Get Coverage Tab Discharge=======//
//========================================//
def GetRegisteredEligibleMember = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetRegisteredEligibleMember',
	[('MemberNo') : MemberNo,
		('ProviderID') : ProviderID]))

API.Note(API.getResponseData(GetRegisteredEligibleMember).Data)

String MemberName = API.getResponseData(GetRegisteredEligibleMember).Data[0].MemberName

String EligibleID = API.getResponseData(GetRegisteredEligibleMember).Data[0].EligibleID

if (API.getResponseData(GetRegisteredEligibleMember).Status) {
	KeywordUtil.markPassed("EligibleID adalah : " + EligibleID)
	KeywordUtil.markPassed("Membernamenya adalah : " + MemberName)
}  else {
	KeywordUtil.markFailedAndStop("Status False")
}

def SummaryDischarge = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/SummaryDischarge',
	[('MemberNo') : MemberNo,
		('TreatmentType') : TreatmentType,
		('ClaimNo') : ClaimNo,
		('TreatmentTypeIcon') : TreatmentTypeIcon,
		('ProviderID') : ProviderID,
		('GuidGA') : GuidGA,
		('MemberName') : MemberName,
		('EligibleID') : EligibleID]))

API.Note(SummaryDischarge.getResponseBodyContent())

if (API.getResponseData(SummaryDischarge).Status) {
	KeywordUtil.markPassed("API SummaryDischarge Passed")
}  else {
	API.Note((API.getResponseData(SummaryDischarge).Status))
	API.Note((API.getResponseData(SummaryDischarge).Message))
	API.Note((API.getResponseData(SummaryDischarge).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

//=============================================//
//=======Get Benefit Field Tab Discharge=======//
//=============================================//
def CheckDischargeTransaction = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/CheckDischargeTransaction',
	[('MemberNo') : MemberNo,
		('ProviderID') : ProviderID,
		('MemberName') : MemberName,
		('EligibleID') : EligibleID]))

API.Note(CheckDischargeTransaction.getResponseBodyContent())

if (API.getResponseData(CheckDischargeTransaction).Status) {
	KeywordUtil.markPassed("API CheckDischargeTransaction Passed")
}  else {
	API.Note((API.getResponseData(CheckDischargeTransaction).Status))
	API.Note((API.getResponseData(CheckDischargeTransaction).Message))
	API.Note((API.getResponseData(CheckDischargeTransaction).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

//=========================//
//=======Verify Data=======//
//=========================//
def getDataPeserta = API.getOneColumnDatabase('172.16.94.70', 'SEA', GlobalVariable.QueryCheckBenefitListGMAOP.replace("_MemberNo_", MemberNo).replace("_ProductType_", TreatmentType), 'Benefit')

def ArrayList CoverageEligibility = API.getResponseData(ProcessRegistrationEligibility).Data.DataMember.BenefitList.BenefitType

def ArrayList CoverageDischarge = API.getResponseData(SummaryDischarge).Data.DataMember.BenefitList.BenefitType

def ArrayList BenefitField = API.getResponseData(CheckDischargeTransaction).Data.FieldBenefit.BenefitType

API.Note(getDataPeserta)

API.Note(CoverageEligibility)

API.Note(CoverageDischarge)

API.Note(BenefitField)

//=======Verify getDataPeserta and getDataPeserta=======//
if ((getDataPeserta.equals(getDataPeserta))) {
	API.Note('Coverage Sesuai Antara Tab Eligibility dan Discharge')
} else {
	API.Note(getDataPeserta)
	API.Note(CoverageEligibility)
	KeywordUtil.markFailedAndStop('Coverage yang muncul Tidak Sesuai')
}

//=======Verify getDataPeserta and CoverageDischarge=======//
if ((getDataPeserta.equals(CoverageDischarge))) {
	API.Note('Coverage Sesuai Antara Tab Eligibility dan Discharge')
} else {
	API.Note(getDataPeserta)
	API.Note(CoverageDischarge)
	KeywordUtil.markFailedAndStop('Coverage yang muncul Tidak Sesuai')
}

//=======Verify getDataPeserta and BenefitField=======//
if ((getDataPeserta.equals(BenefitField))) {
	API.Note('Coverage Sesuai Antara Tab Eligibility dan Discharge')
} else {
	API.Note(getDataPeserta)
	API.Note(BenefitField)
	KeywordUtil.markFailedAndStop('Coverage yang muncul Tidak Sesuai')
}

//=======Verify CoverageEligibility and CoverageDischarge=======//
if ((CoverageEligibility.equals(CoverageDischarge))) {
	API.Note('Coverage Sesuai Antara Tab Eligibility dan Discharge')
} else {
	API.Note(CoverageEligibility)
	API.Note(CoverageDischarge)
	KeywordUtil.markFailedAndStop('Coverage yang muncul Tidak Sesuai')
}

//=======Verify CoverageEligibility and BenefitField=======//
if ((CoverageEligibility.equals(BenefitField))) {
	API.Note('Coverage Sesuai Antara Tab Eligibility dan Discharge')
} else {
	API.Note(CoverageEligibility)
	API.Note(BenefitField)
	KeywordUtil.markFailedAndStop('Coverage yang muncul Tidak Sesuai')
}

//=======Verify CoverageDischarge and BenefitField=======//
if ((CoverageDischarge.equals(BenefitField))) {
	API.Note('Coverage Sesuai Antara Tab Eligibility dan Discharge')
} else {
	API.Note(CoverageDischarge)
	API.Note(BenefitField)
	KeywordUtil.markFailedAndStop('Coverage yang muncul Tidak Sesuai')
}