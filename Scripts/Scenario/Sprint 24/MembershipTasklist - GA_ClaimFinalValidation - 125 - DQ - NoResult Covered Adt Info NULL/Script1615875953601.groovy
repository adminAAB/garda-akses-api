import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'R/00057587'
String PreviousTrID = '2828672'
String TreatmentDate = '2021-03-16'
String ProductType = 'IP'
String DiagnosisCode = 'O80'
String DiagnosisAdditionalInfo = '[]'
String ProviderID = 'TJKRH00013'
String PatientPhone = '0987654321'
String CallerName = ''
String TreatmentRoom = 'ODS (One Day Surgery)'
String DoctorName = 'Dele'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String TreatmentRoomAmount = '100000'
String BenefitAmount = '0'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '0218560601'
String ProviderPhone = '081806221986'
String ProviderExt = '121'
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String PreviousGLNo = ''
String Guid = '43f1cb20018d41e2903cdc0f26d773d879a7ae9a31b1d60b79feed0355a8a6d7'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'CJKPA11'
String ClassNo = '1'
String MembershipType = '2. SPO'
String AllDiagnosis = '[[1,"O80","SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY","Initial Primary","","","","1","1","1","1","","Covered","","","","","Covered",0,"1","1"],[2,"W19","Unspecified fall","Initial Secondary","Covered","","","","","","","","Guaranteed but Uncovered","","","",null,"Question",0,"1","1"]]'
String AllDiagnosisAdditionalInfo = '[[],[{"QuestionID":"1017","QuestionDescription":"RRU Question NoResult Covered Adt Doc","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"KKELF","AdditionalDocDesc":""}]]'
//Diagnosis Question AnswerValue: 0 = No, 1 = Yes, 2 = Unknow
String AllDoctors = 'Dele'
String Gender = 'F'
String DOB = '1989-08-06'
String IsClient = '1'
String TreatmentEnd = '2021-03-16'
String IsReferral = '0'
String ReferralReasonCode = ''
String AppropriateRBClass = 'ODS (One Day Surgery)'
String AppropriateRBRate = '100000'
String AppropriateRBClassChoosen = ''
String AppropriateRBRateChoosen = '0'
String TreatmentRoomChoosen = ''
String TreatmentRoomAmountChoosen = '0'
String IsSpecialCondition = '0'
String ClientClassNo = '1'
String SecondaryDiagnosisCode = '[[1,"O80","SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY","Initial Primary","","","","1","1","1","1","","Covered","","","","","Covered",0,"1","1"],[2,"W19","Unspecified fall","Initial Secondary","Covered","","","","","","","","Guaranteed but Uncovered","","","",null,"Question",0,"1","1"]]'
String MaternityMedicalTreatment = ''
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String TreatmentDuration = '1'
String IsODS = '1'
String IsODC = '0'
String CallStatusID = 'Not Need Follow Up'
String EmpMemberNo = 'R/00057587'

WebUI.callTestCase(findTestCase('Pages/Health/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/Health/MembershipTasklist/GA_ClaimFinalValidation',
	[('authorization') : GlobalVariable.authorization
	, ('MemberNo') : MemberNo
	, ('PreviousTrID') : PreviousTrID
	, ('TreatmentDate') : TreatmentDate
	, ('ProductType') : ProductType
	, ('DiagnosisCode') : DiagnosisCode
	, ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
	, ('ProviderID') : ProviderID
	, ('PatientPhone') : PatientPhone
	, ('CallerName') : CallerName
	, ('TreatmentRoom') : TreatmentRoom
	, ('DoctorName') : DoctorName
	, ('RoomOption') : RoomOption
	, ('RoomAvailability') : RoomAvailability
	, ('UpgradeClass') : UpgradeClass
	, ('TreatmentRoomAmount') : TreatmentRoomAmount
	, ('BenefitAmount') : BenefitAmount
	, ('ProviderEmail') : ProviderEmail
	, ('ProviderFax') : ProviderFax
	, ('ProviderPhone') : ProviderPhone
	, ('ProviderExt') : ProviderExt
	, ('IsTiri') : IsTiri
	, ('DefaultProviderID') : DefaultProviderID
	, ('Remarks') : Remarks
	, ('PreviousGLNo') : PreviousGLNo
	, ('Guid') : Guid
	, ('TicketNo') : TicketNo
	, ('GLType') : GLType
	, ('AccountManager') : AccountManager
	, ('TotalBilled') : TotalBilled
	, ('ClientID') : ClientID
	, ('ClassNo') : ClassNo
	, ('MembershipType') : MembershipType
	, ('AllDiagnosis') : AllDiagnosis
	, ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo
	, ('AllDoctors') : AllDoctors
	, ('Gender') : Gender
	, ('DOB') : DOB
	, ('IsClient') : IsClient
	, ('TreatmentEnd') : TreatmentEnd
	, ('IsReferral') : IsReferral
	, ('ReferralReasonCode') : ReferralReasonCode
	, ('AppropriateRBClass') : AppropriateRBClass
	, ('AppropriateRBRate') : AppropriateRBRate
	, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen
	, ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen
	, ('TreatmentRoomChoosen') : TreatmentRoomChoosen
	, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
	, ('IsSpecialCondition') : IsSpecialCondition
	, ('ClientClassNo') : ClientClassNo
	, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode
	, ('MaternityMedicalTreatment') : MaternityMedicalTreatment
	, ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem
	, ('TreatmentCode') : TreatmentCode
	, ('TreatmentDuration') : TreatmentDuration
	, ('IsODS') : IsODS
	, ('IsODC') : IsODC
	, ('CallStatusID') : CallStatusID
	, ('EmpMemberNo') : EmpMemberNo ]))

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}


API.Note(API.getResponseData(var).Status)
//API.Note(API.getResponseData(var).Data)
API.Note(API.getResponseData(var).Data.ClaimValidationResult)
API.Note(API.getResponseData(var).Data.ClaimCallInStatus)
API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)
API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
API.Note(API.getResponseData(var).Data.ClaimValidationFinalResult)

def ValidationResult=API.getResponseData(var).Data.ClaimValidationResult
def AllValidation=API.getResponseData(var).Data.AllClaimValidationNonCoverReason
def AdditionalRemarks=API.getResponseData(var).Data.ClaimValidationAdditionalRemarks
def FinalResult=API.getResponseData(var).Data.ClaimValidationFinalResult

if (IsSpecialCondition == '1'){
	if (ValidationResult == 'COVER' && AllValidation == null && AdditionalRemarks == null && FinalResult == null){
		KeywordUtil.markPassed('New Member (Special Condition) Tidak Terkena Validasi Diagnosis')
	}
} else if (ValidationResult == 'COVER' && AllValidation == null && AdditionalRemarks == null && FinalResult == ''){
	KeywordUtil.markPassed('New Member (Not Special Condition) Tidak Terkena Validasi Diagnosis')
} else if (AllValidation.contains('NONCOVER_DIAGNOSIS')){
	KeywordUtil.markFailed('Diagnosa New Member butuh konfirmasi dokter Garda Medika')
} else {
	KeywordUtil.markPassed('Tidak Terkena Validasi Diagnosis')
}