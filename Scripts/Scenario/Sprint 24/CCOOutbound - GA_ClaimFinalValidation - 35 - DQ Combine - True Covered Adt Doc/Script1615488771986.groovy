import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'S/00085895'
String PreviousTrID = '2824887'
String TreatmentDate = '2021-03-12'
String ProductType = 'IP'
String DiagnosisCode = 'W15'
String DiagnosisAdditionalInfo = '[{"QuestionID":"1013","QuestionDescription":"RRU Question True Covered Adt Info NULL","AnswerValue":"0","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"1014","QuestionDescription":"RRU Question True Covered Adt Doc NULL","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]'
String ProviderID = 'TJKRH00013'
String PatientPhone = '0987654321'
String CallerName = '-'
String TreatmentRoom = 'ODC (One Day Care)'
String TreatmentRoomAmount = '811000'
String DoctorName = 'Dele'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '800000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '0218560601'
String ProviderPhone = '081806221986'
String ProviderExt = '121'
String IsTiri = '0'
String DefaultProviderID = 'TJKRH00013'
String Remarks = ''
String ServiceTypeID = 'EXC'
String Guid = '8041af1e402a49228d2f4be093beb83312762ab8bafa4e4a7d3de81a3d87c49d'
String TicketNo = ''
String GLType = 'GL Akhir'
String AccountManager = ''
String TotalBilled = '123456'
String ClientID = 'CJKPA18'
String ClassNo = '8'
String MembershipType = '1. EMP'
String AllDiagnosis = '[[1,"W15","Fall from cliff","Initial Primary","Covered","","","","","","","","Guaranteed but Uncovered","","","",null,"Question",0,"1","0"],[2,"W24","Contact with lifting and transmission devices, not elsewhereclassified","Initial Secondary","Covered","","","","","","","","Covered","","","BKMA"," Bukti Perawatan Persalinan (Resume Medis, SK Lahir, SK Perawatan Persalinan)","Question",0,"1","0"]]'
String AllDiagnosisAdditionalInfo = '[[{"QuestionID":"1013","QuestionDescription":"RRU Question True Covered Adt Info NULL","AnswerValue":"0","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"1014","QuestionDescription":"RRU Question True Covered Adt Doc NULL","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}],[{"QuestionID":"1021","QuestionDescription":"RRU Question True Covered Adt Doc","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"BKMA","AdditionalDocDesc":" Bukti Perawatan Persalinan (Resume Medis, SK Lahir, SK Perawatan Persalinan)"},{"QuestionID":"1025","QuestionDescription":"RRU Question True Uncovered","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]]'
//Diagnosis Question AnswerValue: 0 = No, 1 = Yes, 2 = Unknown
String AllDoctors = 'Dele'
String Gender = 'F'
String DOB = '1975-04-15'
String NewMemberName = ''
String PreviousGuid = '187d66e054b54943bf7265e3036cab1b197b2eeb5810a169c15a37b002bb15d6'
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '187d66e054b54943bf7265e3036cab1b197b2eeb5810a169c15a37b002bb15d6'
String IsClient = '0'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'ODC (One Day Care)'
String AppropriateRBRate = '811000'
String TreatmentEnd = '2021-03-12'
String AppropriateRBClassChoosen = ''
String AppropriateRBRateChoosen = ''
String TreatmentRoomChoosen = ''
String TreatmentRoomAmountChoosen = ''
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String SecondaryDiagnosisCode = '["W24"]'
String ClientClassNo = '4'
String UserPosition = ''
String PopUpMsg = '<br><b>Validasi Excess Calculation</b><br>PIC : CCO <br> PIC Name : irwan salis <br> Date Time Confirmation : 2021-03-12 01:45:00 <br> Confirmation : Need Excess Calculation <br> <br><b>Validasi Document</b><br>PIC : AM <br> PIC Name : Reza Setiawan <br> Date Time Confirmation : 2021-03-12 01:45:00 <br> Channel : Call <br> Confirmation : Covered <br> Benefit Coverage : ODC <br> Remarks : - <br>'
String FUStatus = 'Not Need Follow Up'
String OPNO = '105900'
String TrIDCallOut = '2824888'
String BenefitCoverage = ''
String IsDiagnosisNeedNextValidation = '1'
String IsAnnualLimitNeedNextValidation = '1'
String IsNewMemberNeedNextValidation = '1'
String IsExcessNeedNextValidation = '0'
String IsDocumentNeedNextValidation = '0'
String MaternityMedicalTreatment = ''
String MaternityFamilyPlanningItem = ''
String TreatmentCode = 'MAT001'
String IsEligibilityNeedNextValidation = '1'
String IsMedicalTreatmentNeedNextValidation = '1'
String IsFamilyPlanningNeedNextValidation = '1'
String IsProductNeedNextValidation = '1'
String Source = 'HMY'
String IsExcessCalculation = '0'
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = ''
String IsGLProvider = 'False'
String IsODS = '0'
String IsODC = '1'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CCOOutbound/GA_ClaimFinalValidation', [('authorization') : GlobalVariable.authorization
 , ('MemberNo') : MemberNo , ('PreviousTrID') : PreviousTrID
 , ('TreatmentDate') : TreatmentDate , ('ProductType') : ProductType , ('DiagnosisCode') : DiagnosisCode , ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
 , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom
 , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability
 , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax
 , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('DefaultProviderID') : DefaultProviderID
 , ('Remarks') : Remarks , ('ServiceTypeID') : ServiceTypeID , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType
 , ('AccountManager') : AccountManager , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo
 , ('MembershipType') : MembershipType , ('AllDiagnosis') : AllDiagnosis , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('AllDoctors') : AllDoctors , ('Gender') : Gender
 , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('PreviousGuid') : PreviousGuid , ('GLStatus') : GLStatus
 , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('IsClient') : IsClient
 , ('CallStatusID') : CallStatusID , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd
 , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
 , ('IsReferral') : IsReferral , ('IsSpecialCondition') : IsSpecialCondition , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart
 , ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('ClientClassNo') : ClientClassNo , ('UserPosition') : UserPosition , ('PopUpMsg') : PopUpMsg , ('FUStatus') : FUStatus , ('OPNO') : OPNO
 , ('TrIDCallOut') : TrIDCallOut , ('BenefitCoverage') : BenefitCoverage , ('IsDiagnosisNeedNextValidation') : IsDiagnosisNeedNextValidation , ('IsAnnualLimitNeedNextValidation') : IsAnnualLimitNeedNextValidation
 , ('IsNewMemberNeedNextValidation') : IsNewMemberNeedNextValidation , ('IsExcessNeedNextValidation') : IsExcessNeedNextValidation , ('IsDocumentNeedNextValidation') : IsDocumentNeedNextValidation
 , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('TreatmentCode') : TreatmentCode
 , ('IsEligibilityNeedNextValidation') : IsEligibilityNeedNextValidation , ('IsMedicalTreatmentNeedNextValidation') : IsMedicalTreatmentNeedNextValidation , ('IsFamilyPlanningNeedNextValidation') : IsFamilyPlanningNeedNextValidation
 , ('IsProductNeedNextValidation') : IsProductNeedNextValidation , ('Source') : Source , ('IsExcessCalculation') : IsExcessCalculation , ('IsFromProcessButton') : IsFromProcessButton
 , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion
 , ('IsGLProvider') : IsGLProvider , ('IsODS') : IsODS , ('IsODC') : IsODC ]))

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}


API.Note(API.getResponseData(var).Status)
//API.Note(API.getResponseData(var).Data)
API.Note(API.getResponseData(var).Data.ClaimValidationResult)
API.Note(API.getResponseData(var).Data.ClaimCallInStatus)
API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)

def AllValidation=API.getResponseData(var).Data.AllClaimValidationNonCoverReason
if (AllValidation!=null){
	if (AllValidation.contains('NONCOVER_DIAGNOSIS')){
		KeywordUtil.markPassed('Diagnosa butuh konfirmasi dokter Garda Medika')
	} else {
		KeywordUtil.markFailed('Tidak terkena validasi Diagnosa')
	}
} else {
	KeywordUtil.markFailed('NONCOVER_DIAGNOSIS IS NULL')
}