import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'S/00085895'
String PreviousTrID = '2828679'
String TreatmentDate = '2021-03-16'
String ProductType = 'IP'
String DiagnosisCode = 'W14'
String DiagnosisAdditionalInfo = '[{"QuestionID":"1013","QuestionDescription":"RRU Question True Covered Adt Info NULL","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"1014","QuestionDescription":"RRU Question True Covered Adt Doc NULL","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]'
String ProviderID = 'TJKRH00013'
String PatientPhone = '0987654321'
String CallerName = ''
String TreatmentRoom = 'ODC (One Day Care)'
String DoctorName = 'Dele'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String TreatmentRoomAmount = '100000'
String BenefitAmount = '0'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '0218560601'
String ProviderPhone = '081806221986'
String ProviderExt = '121'
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String PreviousGLNo = ''
String Guid = 'a7def490bd304bb6a86f4fa5fc098a0cd255d720db984be4bbcce514606ab3d0'
String TicketNo = ''
String GLType = 'GL Akhir'
String AccountManager = ''
String TotalBilled = '123456'
String ClientID = 'CJKPA18'
String ClassNo = '8'
String MembershipType = '1. EMP'
String AllDiagnosis = '[[1,"W14","Fall from tree","Initial Primary","Covered","","","","","","","","Guaranteed but Uncovered","","","",null,"Question",0,"1","0"],[2,"W25","Contact with sharp glass","Initial Secondary","Confirm","","","","","","","","Guaranteed but Uncovered","","","CL"," Confirmation Letter","Question",0,"1","0"]]'
String AllDiagnosisAdditionalInfo = '[[{"QuestionID":"1013","QuestionDescription":"RRU Question True Covered Adt Info NULL","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"1014","QuestionDescription":"RRU Question True Covered Adt Doc NULL","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}],[{"QuestionID":"1022","QuestionDescription":"RRU Question False Covered Adt Doc","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"CL","AdditionalDocDesc":" Confirmation Letter"},{"QuestionID":"1026","QuestionDescription":"RRU Question False Confirm","AnswerValue":"0","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"1027","QuestionDescription":"RRU Question False Uncovered","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]]'
//Diagnosis Question AnswerValue: 0 = No, 1 = Yes, 2 = Unknown
String AllDoctors = 'Dele'
String Gender = 'F'
String DOB = '1975-04-15'
String IsClient = '0'
String TreatmentEnd = '2021-03-16'
String IsReferral = '0'
String ReferralReasonCode = ''
String AppropriateRBClass = 'ODC (One Day Care)'
String AppropriateRBRate = '100000'
String AppropriateRBClassChoosen = ''
String AppropriateRBRateChoosen = '0'
String TreatmentRoomChoosen = ''
String TreatmentRoomAmountChoosen = '0'
String IsSpecialCondition = '0'
String ClientClassNo = '4'
String SecondaryDiagnosisCode = '[[1,"W14","Fall from tree","Initial Primary","Covered","","","","","","","","Guaranteed but Uncovered","","","",null,"Question",0,"1","0"],[2,"W25","Contact with sharp glass","Initial Secondary","Confirm","","","","","","","","Guaranteed but Uncovered","","","CL"," Confirmation Letter","Question",0,"1","0"]]'
String MaternityMedicalTreatment = ''
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String TreatmentDuration = '1'
String IsODS = '0'
String IsODC = '1'
String CallStatusID = 'Not Need Follow Up'
String EmpMemberNo = 'S/00085895'

WebUI.callTestCase(findTestCase('Pages/Health/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/Health/MembershipTasklist/GA_ClaimFinalValidation',
	[('authorization') : GlobalVariable.authorization
	, ('MemberNo') : MemberNo
	, ('PreviousTrID') : PreviousTrID
	, ('TreatmentDate') : TreatmentDate
	, ('ProductType') : ProductType
	, ('DiagnosisCode') : DiagnosisCode
	, ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
	, ('ProviderID') : ProviderID
	, ('PatientPhone') : PatientPhone
	, ('CallerName') : CallerName
	, ('TreatmentRoom') : TreatmentRoom
	, ('DoctorName') : DoctorName
	, ('RoomOption') : RoomOption
	, ('RoomAvailability') : RoomAvailability
	, ('UpgradeClass') : UpgradeClass
	, ('TreatmentRoomAmount') : TreatmentRoomAmount
	, ('BenefitAmount') : BenefitAmount
	, ('ProviderEmail') : ProviderEmail
	, ('ProviderFax') : ProviderFax
	, ('ProviderPhone') : ProviderPhone
	, ('ProviderExt') : ProviderExt
	, ('IsTiri') : IsTiri
	, ('DefaultProviderID') : DefaultProviderID
	, ('Remarks') : Remarks
	, ('PreviousGLNo') : PreviousGLNo
	, ('Guid') : Guid
	, ('TicketNo') : TicketNo
	, ('GLType') : GLType
	, ('AccountManager') : AccountManager
	, ('TotalBilled') : TotalBilled
	, ('ClientID') : ClientID
	, ('ClassNo') : ClassNo
	, ('MembershipType') : MembershipType
	, ('AllDiagnosis') : AllDiagnosis
	, ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo
	, ('AllDoctors') : AllDoctors
	, ('Gender') : Gender
	, ('DOB') : DOB
	, ('IsClient') : IsClient
	, ('TreatmentEnd') : TreatmentEnd
	, ('IsReferral') : IsReferral
	, ('ReferralReasonCode') : ReferralReasonCode
	, ('AppropriateRBClass') : AppropriateRBClass
	, ('AppropriateRBRate') : AppropriateRBRate
	, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen
	, ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen
	, ('TreatmentRoomChoosen') : TreatmentRoomChoosen
	, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
	, ('IsSpecialCondition') : IsSpecialCondition
	, ('ClientClassNo') : ClientClassNo
	, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode
	, ('MaternityMedicalTreatment') : MaternityMedicalTreatment
	, ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem
	, ('TreatmentCode') : TreatmentCode
	, ('TreatmentDuration') : TreatmentDuration
	, ('IsODS') : IsODS
	, ('IsODC') : IsODC
	, ('CallStatusID') : CallStatusID
	, ('EmpMemberNo') : EmpMemberNo ]))

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}


API.Note(API.getResponseData(var).Status)
//API.Note(API.getResponseData(var).Data)
API.Note(API.getResponseData(var).Data.ClaimValidationResult)
API.Note(API.getResponseData(var).Data.ClaimCallInStatus)
API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)
API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
API.Note(API.getResponseData(var).Data.ClaimValidationFinalResult)

def ValidationResult=API.getResponseData(var).Data.ClaimValidationResult
def AllValidation=API.getResponseData(var).Data.AllClaimValidationNonCoverReason
def AdditionalRemarks=API.getResponseData(var).Data.ClaimValidationAdditionalRemarks
def FinalResult=API.getResponseData(var).Data.ClaimValidationFinalResult

if (IsSpecialCondition == '1'){
	if (ValidationResult == 'COVER' && AllValidation == null && AdditionalRemarks == null && FinalResult == null){
		KeywordUtil.markPassed('New Member (Special Condition) Tidak Terkena Validasi Diagnosis')
	}
} else if (ValidationResult == 'COVER' && AllValidation == null && AdditionalRemarks == null && FinalResult == ''){
	KeywordUtil.markPassed('New Member (Not Special Condition) Tidak Terkena Validasi Diagnosis')
} else if (AllValidation.contains('NONCOVER_DIAGNOSIS')){
	KeywordUtil.markFailed('Diagnosa New Member butuh konfirmasi dokter Garda Medika')
} else {
	KeywordUtil.markPassed('Tidak Terkena Validasi Diagnosis')
}