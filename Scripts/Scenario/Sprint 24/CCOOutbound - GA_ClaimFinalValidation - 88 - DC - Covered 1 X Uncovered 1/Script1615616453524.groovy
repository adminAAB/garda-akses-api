import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'P/00016483'
String PreviousTrID = '2824856'
String TreatmentDate = '2021-03-10'
String ProductType = 'MA'
String DiagnosisCode = 'O80'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'TJKRH00013'
String PatientPhone = '0987654321'
String CallerName = '-'
String TreatmentRoom = 'KELAS II'
String TreatmentRoomAmount = '487000'
String DoctorName = 'Dele'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '370000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '0218560601'
String ProviderPhone = '081806221986'
String ProviderExt = '121'
String IsTiri = '0'
String DefaultProviderID = 'TJKRH00013'
String Remarks = ''
String ServiceTypeID = 'EXC'
String Guid = '5f2f7625704e4367a7f2e04d6417e3824220d80fb6ed524df39c626ee61d2a2e'
String TicketNo = ''
String GLType = 'GL Akhir'
String AccountManager = ''
String TotalBilled = '123456'
String ClientID = 'CJKPA18'
String ClassNo = '2'
String MembershipType = '2. SPO'
String AllDiagnosis = '[[1,"O80","SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY","Initial Primary","","","","1","1","1","1","","Covered","","","","","Covered",0,"1","1"],[2,"W05","Fall involving wheelchair","Initial Secondary","","","","","","","","","Uncovered","","","","","",0,"1","0"],[3,"W01","Fall on same level from slipping, tripping and stumbling","Initial Secondary","","","","","","","","","Uncovered","","","","","",0,"1","0"]]'
String AllDiagnosisAdditionalInfo = '[[],[]]'
String AllDoctors = 'Dele'
String Gender = 'F'
String DOB = '1990-09-10'
String NewMemberName = ''
String PreviousGuid = '5a057d8e73684478bb83e7046c8929d3f36b0a369a1da4fc618e73488b885285'
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '5a057d8e73684478bb83e7046c8929d3f36b0a369a1da4fc618e73488b885285'
String IsClient = '0'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'KELAS II'
String AppropriateRBRate = '487000'
String TreatmentEnd = '2021-03-12'
String AppropriateRBClassChoosen = 'KELAS II'
String AppropriateRBRateChoosen = '487000'
String TreatmentRoomChoosen = 'KELAS II'
String TreatmentRoomAmountChoosen = '487000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String SecondaryDiagnosisCode = '["W05","W01"]'
String ClientClassNo = '1'
String UserPosition = ''
String PopUpMsg = '<br><b>Validasi Excess Calculation</b><br>PIC : CCO <br> PIC Name : irwan salis <br> Date Time Confirmation : 2021-03-10 15:01:00 <br> Confirmation : Need Excess Calculation <br>'
String FUStatus = 'Not Need Follow Up'
String OPNO = '105900'
String TrIDCallOut = '2824857'
String BenefitCoverage = ''
String IsDiagnosisNeedNextValidation = '1'
String IsAnnualLimitNeedNextValidation = '1'
String IsNewMemberNeedNextValidation = '1'
String IsExcessNeedNextValidation = '0'
String IsDocumentNeedNextValidation = '1'
String MaternityMedicalTreatment = ''
String MaternityFamilyPlanningItem = ''
String TreatmentCode = 'MAT001'
String IsEligibilityNeedNextValidation = '1'
String IsMedicalTreatmentNeedNextValidation = '1'
String IsFamilyPlanningNeedNextValidation = '1'
String IsProductNeedNextValidation = '1'
String Source = 'HMY'
String IsExcessCalculation = '0'
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = ''
String IsGLProvider = 'False'
String IsODS = '0'
String IsODC = '0'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CCOOutbound/GA_ClaimFinalValidation', [('authorization') : GlobalVariable.authorization
 , ('MemberNo') : MemberNo , ('PreviousTrID') : PreviousTrID
 , ('TreatmentDate') : TreatmentDate , ('ProductType') : ProductType , ('DiagnosisCode') : DiagnosisCode , ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
 , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom
 , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability
 , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax
 , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('DefaultProviderID') : DefaultProviderID
 , ('Remarks') : Remarks , ('ServiceTypeID') : ServiceTypeID , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType
 , ('AccountManager') : AccountManager , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo
 , ('MembershipType') : MembershipType , ('AllDiagnosis') : AllDiagnosis , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('AllDoctors') : AllDoctors , ('Gender') : Gender
 , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('PreviousGuid') : PreviousGuid , ('GLStatus') : GLStatus
 , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('IsClient') : IsClient
 , ('CallStatusID') : CallStatusID , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd
 , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
 , ('IsReferral') : IsReferral , ('IsSpecialCondition') : IsSpecialCondition , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart
 , ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('ClientClassNo') : ClientClassNo , ('UserPosition') : UserPosition , ('PopUpMsg') : PopUpMsg , ('FUStatus') : FUStatus , ('OPNO') : OPNO
 , ('TrIDCallOut') : TrIDCallOut , ('BenefitCoverage') : BenefitCoverage , ('IsDiagnosisNeedNextValidation') : IsDiagnosisNeedNextValidation , ('IsAnnualLimitNeedNextValidation') : IsAnnualLimitNeedNextValidation
 , ('IsNewMemberNeedNextValidation') : IsNewMemberNeedNextValidation , ('IsExcessNeedNextValidation') : IsExcessNeedNextValidation , ('IsDocumentNeedNextValidation') : IsDocumentNeedNextValidation
 , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('TreatmentCode') : TreatmentCode
 , ('IsEligibilityNeedNextValidation') : IsEligibilityNeedNextValidation , ('IsMedicalTreatmentNeedNextValidation') : IsMedicalTreatmentNeedNextValidation , ('IsFamilyPlanningNeedNextValidation') : IsFamilyPlanningNeedNextValidation
 , ('IsProductNeedNextValidation') : IsProductNeedNextValidation , ('Source') : Source , ('IsExcessCalculation') : IsExcessCalculation , ('IsFromProcessButton') : IsFromProcessButton
 , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion
 , ('IsGLProvider') : IsGLProvider , ('IsODS') : IsODS , ('IsODC') : IsODC ]))

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}


API.Note(API.getResponseData(var).Status)
//API.Note(API.getResponseData(var).Data)
API.Note(API.getResponseData(var).Data.ClaimValidationResult)
API.Note(API.getResponseData(var).Data.ClaimCallInStatus)
API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)

def AllValidation=API.getResponseData(var).Data.AllClaimValidationNonCoverReason
if (AllValidation!=null){
	if (AllValidation.contains('NONCOVER_DIAGNOSIS')){
		KeywordUtil.markPassed('Diagnosa butuh konfirmasi dokter Garda Medika')
	} else {
		KeywordUtil.markFailed('Tidak terkena validasi Diagnosa')
	}
} else {
	KeywordUtil.markFailed('NONCOVER_DIAGNOSIS IS NULL')
}