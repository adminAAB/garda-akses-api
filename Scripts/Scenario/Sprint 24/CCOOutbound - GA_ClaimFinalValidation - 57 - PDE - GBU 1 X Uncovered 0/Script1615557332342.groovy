import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'O/00005308'
String PreviousTrID = '2824837'
String TreatmentDate = '2021-03-10'
String ProductType = 'IP'
String DiagnosisCode = 'W03'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'TJKRH00013'
String PatientPhone = '0987654321'
String CallerName = '-'
String TreatmentRoom = 'KELAS III'
String TreatmentRoomAmount = '203000'
String DoctorName = 'Dele'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '450000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '0218560601'
String ProviderPhone = '081806221986'
String ProviderExt = '121'
String IsTiri = '0'
String DefaultProviderID = 'TJKRH00013'
String Remarks = ''
String ServiceTypeID = 'PRI'
String Guid = 'adc2d999873a423483481261e3a6e3f0549fa7d2741bb2f58dd1069e3aa72896'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'CJKPA11'
String ClassNo = '2'
String MembershipType = '2. SPO'
String AllDiagnosis = '[[1,"W03","Other fall on same level due to collision with, or pushing by,another person","Initial Primary","","","","","","","","","Uncovered","","","","","",0,"1","0"],[2,"W02","Fall involving ice-skates, skis, roller-skates or skateboards","Initial Secondary","","","","","","","","","Uncovered","","","","","",0,"1","0"]]'
String AllDiagnosisAdditionalInfo = '[[],[]]'
String AllDoctors = 'Dele'
String Gender = 'F'
String DOB = '1990-07-19'
String NewMemberName = ''
String PreviousGuid = 'bd4bf2e05d7c4e8984fb2f083fe970a52c23e0d5342d1a3245dcbf9cf8e43a53'
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = 'bd4bf2e05d7c4e8984fb2f083fe970a52c23e0d5342d1a3245dcbf9cf8e43a53'
String IsClient = '1'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'KELAS III'
String AppropriateRBRate = '203000'
String TreatmentEnd = ''
String AppropriateRBClassChoosen = 'KELAS III'
String AppropriateRBRateChoosen = '203000'
String TreatmentRoomChoosen = 'KELAS III'
String TreatmentRoomAmountChoosen = '203000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String SecondaryDiagnosisCode = '["W02"]'
String ClientClassNo = '2'
String UserPosition = ''
String PopUpMsg = '<br><b>Validasi Medical Treatment</b><br>PIC : CCO <br> PIC Name : irwan salis <br> Date Time Confirmation : 2021-03-10 10:05:00 <br> Confirmation : Covered <br> Benefit Coverage : IP - Cashless <br>'
String FUStatus = 'Not Need Follow Up'
String OPNO = '91164'
String TrIDCallOut = '2824838'
String BenefitCoverage = 'BC001'
String IsDiagnosisNeedNextValidation = '1'
String IsAnnualLimitNeedNextValidation = '1'
String IsNewMemberNeedNextValidation = '1'
String IsExcessNeedNextValidation = '1'
String IsDocumentNeedNextValidation = '1'
String MaternityMedicalTreatment = '[["W01","",1,"Dele W01","1000","","Fall on same level from slipping, tripping and stumbling","",0,"","","","",1,"1"]]'
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String IsEligibilityNeedNextValidation = '1'
String IsMedicalTreatmentNeedNextValidation = '0'
String IsFamilyPlanningNeedNextValidation = '1'
String IsProductNeedNextValidation = '1'
String Source = 'HMY'
String IsExcessCalculation = '0'
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String IsGLProvider = 'False'
String IsODS = '0'
String IsODC = '0'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CCOOutbound/GA_ClaimFinalValidation', [('authorization') : GlobalVariable.authorization
 , ('MemberNo') : MemberNo , ('PreviousTrID') : PreviousTrID
 , ('TreatmentDate') : TreatmentDate , ('ProductType') : ProductType , ('DiagnosisCode') : DiagnosisCode , ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
 , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom
 , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability
 , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax
 , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('DefaultProviderID') : DefaultProviderID
 , ('Remarks') : Remarks , ('ServiceTypeID') : ServiceTypeID , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType
 , ('AccountManager') : AccountManager , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo
 , ('MembershipType') : MembershipType , ('AllDiagnosis') : AllDiagnosis , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('AllDoctors') : AllDoctors , ('Gender') : Gender
 , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('PreviousGuid') : PreviousGuid , ('GLStatus') : GLStatus
 , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('IsClient') : IsClient
 , ('CallStatusID') : CallStatusID , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd
 , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
 , ('IsReferral') : IsReferral , ('IsSpecialCondition') : IsSpecialCondition , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart
 , ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('ClientClassNo') : ClientClassNo , ('UserPosition') : UserPosition , ('PopUpMsg') : PopUpMsg , ('FUStatus') : FUStatus , ('OPNO') : OPNO
 , ('TrIDCallOut') : TrIDCallOut , ('BenefitCoverage') : BenefitCoverage , ('IsDiagnosisNeedNextValidation') : IsDiagnosisNeedNextValidation , ('IsAnnualLimitNeedNextValidation') : IsAnnualLimitNeedNextValidation
 , ('IsNewMemberNeedNextValidation') : IsNewMemberNeedNextValidation , ('IsExcessNeedNextValidation') : IsExcessNeedNextValidation , ('IsDocumentNeedNextValidation') : IsDocumentNeedNextValidation
 , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('TreatmentCode') : TreatmentCode
 , ('IsEligibilityNeedNextValidation') : IsEligibilityNeedNextValidation , ('IsMedicalTreatmentNeedNextValidation') : IsMedicalTreatmentNeedNextValidation , ('IsFamilyPlanningNeedNextValidation') : IsFamilyPlanningNeedNextValidation
 , ('IsProductNeedNextValidation') : IsProductNeedNextValidation , ('Source') : Source , ('IsExcessCalculation') : IsExcessCalculation , ('IsFromProcessButton') : IsFromProcessButton
 , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion
 , ('IsGLProvider') : IsGLProvider , ('IsODS') : IsODS , ('IsODC') : IsODC ]))

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}


API.Note(API.getResponseData(var).Status)
//API.Note(API.getResponseData(var).Data)
API.Note(API.getResponseData(var).Data.ClaimValidationResult)
API.Note(API.getResponseData(var).Data.ClaimCallInStatus)
API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)

def AllValidation=API.getResponseData(var).Data.AllClaimValidationNonCoverReason
if (AllValidation!=null){
	if (AllValidation.contains('NONCOVER_DIAGNOSIS')){
		KeywordUtil.markPassed('Diagnosa butuh konfirmasi dokter Garda Medika')
	} else {
		KeywordUtil.markFailed('Tidak terkena validasi Diagnosa')
	}
} else {
	KeywordUtil.markFailed('NONCOVER_DIAGNOSIS IS NULL')
}