import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

//Client Class = Level 9-10A-Non MA 

String ExpectedResult = "Uncovered"

String Wording = "Medical Treatment memerlukan konfirmasi Dokter Garda Medika"

String MaternityMedicalTreatment = '[["A38",373,0,null,"0","","Scarlet fever","Covered",0,"","","","",1,"1","OBT","16/04/2021 10:32","",0,"3","30000","90000",0,null,0,"Test QC","Coverage : Dijaminkan\n\nReason : \n -\n\nDokumen yang perlu dilengkapi  : \n - \n","Obat Sakit","Obat"]]'

String AdminName = ''
String IsGLProvider = 'False'
String MemberNo = 'A/00100026'
String PreviousTrID = '2874979'
String TreatmentDate = '2021-04-16'
String ProductType = 'IP'
String DiagnosisCode = 'A38'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'OJKRI00001'
String PatientPhone = '087841632020'
String CallerName = '-'
String TreatmentRoom = 'ODS (One Day Surgery)'
String TreatmentRoomAmount = '300000'
String DoctorName = 'Ugha Anugerah'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '500000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '021 7455800'
String ProviderPhone = '081806221986'
String ProviderExt = ''
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String Guid = 'bc54777cec2341b1b2d5a9b3cb096ce4c31d2b909e461cff3656d6a93c150056'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'CJKPP0009'
String ClassNo = '16'
String MembershipType = '3. CHI'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"0","0"]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String AllDoctors = 'Ugha Anugerah'
String Gender = 'F'
String DOB = '1999-06-10'
String NewMemberName = ''
String PreviousGuid = 'bc54777cec2341b1b2d5a9b3cb096ce4c31d2b909e461cff3656d6a93c150056'
String GLStatus = 'Produce'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '95848e79a72e4235a0ef022250206b1f8451baf0f9dc4e3128169fb15660adb2'
String IsClient = '1'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'ODS (One Day Surgery)'
String AppropriateRBRate = '300000'
String TreatmentEnd = '2021-04-16'
String AppropriateRBClassChoosen = ''
String AppropriateRBRateChoosen = '0'
String TreatmentRoomChoosen = ''
String TreatmentRoomAmountChoosen = '0'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '43'
String UserPosition = 'Customer Service'
String OPNO = '105323'
//String MaternityMedicalTreatment = '[["A38",357,0,"","0","","Scarlet fever","Covered",0,"","","BILPA|BNDNG"," Billing Perawatan Asli, Billing Perbandingan",1,"1","OBT","15/04/2021 12:27","",0,"3","30000","90000",0,null,0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi Garda Medika terkait Dokumen Tambahan\n\nDokumen yang perlu dilengkapi  : \n1.  Billing Perawatan Asli, Billing Perbandingan\n","Obat Sakit","Obat"]]'
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String IsODS = '1'
String IsODC = '0'
String IsProducttypeChange = '0'
String IsTreatmentPeriodChange = '0'
String IsMaternityTreatmentChange = '0'
String IsRoomOptionChange = '0'
String IsTreatmentRBClassChange = '0'
String IsODSODCChange = '0'
String IsDocValidityChange = '0'
String IsDocTypeChange = '0'
String suspectDouble = '0'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '1'

WebUI.callTestCase(findTestCase('Pages/GMA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GMA/CreateTreatmentGL/GA_FinalValidationAPTTOInTreatment', [('authorization') : GlobalVariable.authorization
 , ('AdminName') : AdminName , ('IsGLProvider') : IsGLProvider , ('MemberNo') : MemberNo , ('PreviousTrID') : PreviousTrID
 , ('TreatmentDate') : TreatmentDate , ('ProductType') : ProductType , ('DiagnosisCode') : DiagnosisCode , ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
 , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom
 , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability
 , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax
 , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('DefaultProviderID') : DefaultProviderID
 , ('Remarks') : Remarks , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType
 , ('AccountManager') : AccountManager , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo
 , ('MembershipType') : MembershipType , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender
 , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('PreviousGuid') : PreviousGuid , ('GLStatus') : GLStatus
 , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('IsClient') : IsClient
 , ('CallStatusID') : CallStatusID , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd
 , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
 , ('IsReferral') : IsReferral , ('IsSpecialCondition') : IsSpecialCondition , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart
 , ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('ClientClassNo') : ClientClassNo , ('UserPosition') : UserPosition , ('OPNO') : OPNO
 , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('TreatmentCode') : TreatmentCode , ('IsFromProcessButton') : IsFromProcessButton
 , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('IsODS') : IsODS
 , ('IsODC') : IsODC , ('IsProducttypeChange') : IsProducttypeChange , ('IsTreatmentPeriodChange') : IsTreatmentPeriodChange , ('IsMaternityTreatmentChange') : IsMaternityTreatmentChange
 , ('IsRoomOptionChange') : IsRoomOptionChange , ('IsTreatmentRBClassChange') : IsTreatmentRBClassChange , ('IsODSODCChange') : IsODSODCChange , ('IsDocValidityChange') : IsDocValidityChange
 , ('IsDocTypeChange') : IsDocTypeChange , ('suspectDouble') : suspectDouble , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation
 , ('IsProtapCovid') : IsProtapCovid ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)
//API.Note(API.getResponseData(var).Data.ClaimValidationMessage)
//API.Note(API.getResponseData(var).Data.ClaimValidationNonCoverReason)
//API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
//API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)


//Cek Hasil Validasi Diagnosa

def AllValidation=API.getResponseData(var).Data.ClaimValidationResult
def DetailValidation = API.getResponseData(var).Data.ClaimValidationFinalResult
if (AllValidation=="COVER" && ExpectedResult == 'Covered'){
	KeywordUtil.markPassed('Validasi Medical Treatment Sesuai')
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
}else if (AllValidation=="NONCOVER" && ExpectedResult == 'Uncovered'){
	if (DetailValidation.contains(Wording)) {
		KeywordUtil.markPassed(Wording)
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationMessage)
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
	} else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationMessage)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
	}
} else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationMessage)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
}

/*
	} else {
		KeywordUtil.markFailed('NON COVER FAILED')
	}
} else {
	KeywordUtil.markFailed('NON COVER IS NULL')
}
*/

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}