import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'T/15292'
String TreatmentDate = '2021-04-01'
String DiagnosisCode = 'A50'
String ProductType = 'IP'
String AdditionalDiagnosisInfo = '[[{"QuestionID":"992","QuestionDescription":"Nilai leukosit rendah","AnswerValue":"2","Remarks":"","AdditionalInfo":"NBH Test","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"993","QuestionDescription":"Demam > 38 (+)","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"994","QuestionDescription":"Intake sulit (+)","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]]'
String ProviderID = 'TJKRP0146'
String PatientPhone = '081808623854'
String CallerName = ''
String TreatmentRoom = 'HCU'
String TreatmentRoomAmount = '850000'
String DoctorName = 'test'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '900000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = '197'
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'EXC'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKRP0146'
String Guid = '0205cdff26a6438cb7c708d38770a8302a4edefef21bc7cfe751402363661a38'
String TicketNo = ''
String GLType = 'GL Akhir'
String AccountManager = ''
String PreviousGuid = '806b1f302389442d816b1f277c8f96f32d98f86f6adb5226034c84f1c6992e42'
String PreviousTrID = '2862361'
String TotalBilled = '100000'
String ClientID = 'C01AA00001'
String ClassNo = '4'
String Membership = '1. EMP'
String AllDiagnosis = '[[1,"A50","CONGENITAL SYPHILIS","Initial Primary","Confirm","","","","","","","","Uncovered","","","",null,"Question",0,"0","0"]]'
String AllDoctors = 'test'
String Gender = 'F'
String DOB = '1984-03-27'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '0205cdff26a6438cb7c708d38770a8302a4edefef21bc7cfe751402363661a38'
String TreatmentRoomChoosen = 'HCU'
String AppropriateRBClassChoosen = 'HCU'
String AppropriateRBRateChoosen = '850000'
String TreatmentRoomAmountChoosen = '850000'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'HCU'
String AppropriateRBRate = '850000'
String TreatmentEnd = '2021-04-03'
String IsSpecialCondition = '0'
String ClientClassNo = '3'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = ''
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = ''
String Source = 'hgl'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = ''
String AllDiagnosisAdditionalInfo = '[[{"QuestionID":"992","QuestionDescription":"Nilai leukosit rendah","AnswerValue":"2","Remarks":"","AdditionalInfo":"NBH Test","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"993","QuestionDescription":"Demam > 38 (+)","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"994","QuestionDescription":"Intake sulit (+)","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]]'
String IsODS = '0'
String IsODC = '0'
String OPNO = '106186'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CCOOutbound/ProcessGL', [('authorization') : GlobalVariable.authorization     	      
, ('MemberNo') : MemberNo	, ('TreatmentDate') : TreatmentDate	, ('DiagnosisCode') : DiagnosisCode	, ('ProductType') : ProductType	, ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo
, ('ProviderID') : ProviderID	, ('PatientPhone') : PatientPhone	, ('CallerName') : CallerName	, ('TreatmentRoom') : TreatmentRoom	, ('TreatmentRoomAmount') : TreatmentRoomAmount	
, ('DoctorName') : DoctorName	, ('RoomOption') : RoomOption	, ('RoomAvailability') : RoomAvailability	, ('UpgradeClass') : UpgradeClass	, ('BenefitAmount') : BenefitAmount	
, ('ProviderEmail') : ProviderEmail	, ('ProviderFax') : ProviderFax	, ('ProviderPhone') : ProviderPhone	, ('ProviderExt') : ProviderExt	, ('IsTiri') : IsTiri	
, ('UserPosition') : UserPosition	, ('Remarks') : Remarks	, ('serviceTypeID') : serviceTypeID	, ('CallStatusID') : CallStatusID	, ('DefaultProviderID') : DefaultProviderID	
, ('Guid') : Guid	, ('TicketNo') : TicketNo	, ('GLType') : GLType	, ('AccountManager') : AccountManager	, ('PreviousGuid') : PreviousGuid	
, ('PreviousTrID') : PreviousTrID	, ('TotalBilled') : TotalBilled	, ('ClientID') : ClientID	, ('ClassNo') : ClassNo	, ('Membership') : Membership	
, ('AllDiagnosis') : AllDiagnosis	, ('AllDoctors') : AllDoctors	, ('Gender') : Gender	, ('DOB') : DOB	, ('NewMemberName') : NewMemberName	
, ('GLStatus') : GLStatus	, ('EmpMemberNo') : EmpMemberNo	, ('NMEmpID') : NMEmpID	, ('FollowUpTaskID') : FollowUpTaskID	, ('TreatmentRoomChoosen') : TreatmentRoomChoosen	
, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen	, ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen	, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen	
, ('IsReferral') : IsReferral	, ('ReferralReasonCode') : ReferralReasonCode	, ('CallInStart') : CallInStart	, ('IsClient') : IsClient	, ('AppropriateRBClass') : AppropriateRBClass	
, ('AppropriateRBRate') : AppropriateRBRate	, ('TreatmentEnd') : TreatmentEnd	, ('IsSpecialCondition') : IsSpecialCondition	, ('ClientClassNo') : ClientClassNo	
, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode	, ('TreatmentCode') : TreatmentCode	, ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem	
, ('MaternityMedicalTreatment') : MaternityMedicalTreatment	, ('Source') : Source	, ('NonMedicalItem') : NonMedicalItem	, ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered	
, ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion	, ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo	, ('IsODS') : IsODS	, ('IsODC') : IsODC	
, ('OPNO') : OPNO ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)
/*API.Note(API.getResponseData(var).Data.ClaimValidationResult)
API.Note(API.getResponseData(var).Data.ClaimCallInStatus)
API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)
*/

//Cek Hasil Validasi Diagnosa
/*
if (API.getResponseData(var).Data.ClaimValidationResult=='COVER') {
	KeywordUtil.markPassed('COVERAGE PASSED')
} else {
	KeywordUtil.markFailed('COVERAGE FAILED')
}

if (API.getResponseData(var).Data.ClaimCallInStatus=='Not Need Follow Up') {
	KeywordUtil.markPassed('CALL STATUS PASSED')
} else {
	KeywordUtil.markFailed('CALL STATUS FAILED')
}
*/

//Diagnosis Need FU
/*
def AllValidation=API.getResponseData(var).Data.DiagnosisRejectMsgValidationF
if (AllValidation!=null){
	if (AllValidation==true){
		KeywordUtil.markPassed('Diagnosis Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	} else {
		KeywordUtil.markFailed('Diagnosis Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	}
} else {
	KeywordUtil.markFailed('Diagnosis Confirmation is NULL')
}
*/

//Diagnosis Not Need FU (Produce / Reject)

def AllValidation=API.getResponseData(var).Data.DiagnosisRejectMsgValidationF
if (AllValidation!=null){
	if (AllValidation==false){
		KeywordUtil.markPassed('Diagnosis Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	} else {
		KeywordUtil.markFailed('Diagnosis Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	}
} else {
	KeywordUtil.markFailed('Diagnosis Confirmation is NULL')
}


//Validasi Lain Need FU - Excess
def AllValidationExcess=API.getResponseData(var).Data.ExcessValidationF
if (AllValidationExcess!=null){
	if (AllValidationExcess==true){
		KeywordUtil.markPassed('Excess Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.ExcessValidationF)
	} else {
		KeywordUtil.markFailed('Excess Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.ExcessValidationF)
	}
} else {
	KeywordUtil.markFailed('Excess Confirmation is NULL')
}


//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}