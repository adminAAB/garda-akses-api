import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'C/00017558'
String TreatmentDate = '2021-04-01'
String DiagnosisCode = 'Q42.3'
String ProductType = 'IP'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'TJKRP0146'
String PatientPhone = '081808623854'
String CallerName = ''
String TreatmentRoom = 'ODC (One Day Care)'
String TreatmentRoomAmount = '850000'
String DoctorName = 'test'
String RoomOption = 'APS'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '600000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = '197'
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'GLX'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKRP0146'
String Guid = '4e28caf1f5434be0b0d2e94e515da4bcc2c7dcfb588ae8d4d4d2ba26415d2e95'
String TicketNo = ''
String GLType = 'GL Lanjutan'
String AccountManager = ''
String PreviousGuid = '01bb599aee024227bf629d536fcc10e2c8956c510f82fedbea7ca2caec8738d0'
String PreviousTrID = '2862498'
String TotalBilled = '0'
String ClientID = 'C01AA00001'
String ClassNo = '2'
String Membership = '1. EMP'
String AllDiagnosis = '[[1,"Q42.3","CONGENITAL ABSENCE- ATRESIA AND STENOSIS OF ANUS WITHOUT FISTULA","Initial Primary","","","","","","","","","Uncovered","","","","","",0,"0","0"]]'
String AllDoctors = 'test'
String Gender = 'F'
String DOB = '1996-01-17'
String NewMemberName = 'EMP TSG 25 01'
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = 'EMPTSG2501'
String FollowUpTaskID = '4e28caf1f5434be0b0d2e94e515da4bcc2c7dcfb588ae8d4d4d2ba26415d2e95'
String TreatmentRoomChoosen = 'HCU'
String AppropriateRBClassChoosen = 'HCU'
String AppropriateRBRateChoosen = '850000'
String TreatmentRoomAmountChoosen = '850000'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'ODC (One Day Care)'
String AppropriateRBRate = '850000'
String TreatmentEnd = '2021-04-01'
String IsSpecialCondition = '0'
String ClientClassNo = '1'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = ''
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = ''
String Source = 'hgl'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = ''
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '1'
String OPNO = '106186'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CCOOutbound/ProcessGL', [('authorization') : GlobalVariable.authorization     	      
, ('MemberNo') : MemberNo	, ('TreatmentDate') : TreatmentDate	, ('DiagnosisCode') : DiagnosisCode	, ('ProductType') : ProductType	, ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo
, ('ProviderID') : ProviderID	, ('PatientPhone') : PatientPhone	, ('CallerName') : CallerName	, ('TreatmentRoom') : TreatmentRoom	, ('TreatmentRoomAmount') : TreatmentRoomAmount	
, ('DoctorName') : DoctorName	, ('RoomOption') : RoomOption	, ('RoomAvailability') : RoomAvailability	, ('UpgradeClass') : UpgradeClass	, ('BenefitAmount') : BenefitAmount	
, ('ProviderEmail') : ProviderEmail	, ('ProviderFax') : ProviderFax	, ('ProviderPhone') : ProviderPhone	, ('ProviderExt') : ProviderExt	, ('IsTiri') : IsTiri	
, ('UserPosition') : UserPosition	, ('Remarks') : Remarks	, ('serviceTypeID') : serviceTypeID	, ('CallStatusID') : CallStatusID	, ('DefaultProviderID') : DefaultProviderID	
, ('Guid') : Guid	, ('TicketNo') : TicketNo	, ('GLType') : GLType	, ('AccountManager') : AccountManager	, ('PreviousGuid') : PreviousGuid	
, ('PreviousTrID') : PreviousTrID	, ('TotalBilled') : TotalBilled	, ('ClientID') : ClientID	, ('ClassNo') : ClassNo	, ('Membership') : Membership	
, ('AllDiagnosis') : AllDiagnosis	, ('AllDoctors') : AllDoctors	, ('Gender') : Gender	, ('DOB') : DOB	, ('NewMemberName') : NewMemberName	
, ('GLStatus') : GLStatus	, ('EmpMemberNo') : EmpMemberNo	, ('NMEmpID') : NMEmpID	, ('FollowUpTaskID') : FollowUpTaskID	, ('TreatmentRoomChoosen') : TreatmentRoomChoosen	
, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen	, ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen	, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen	
, ('IsReferral') : IsReferral	, ('ReferralReasonCode') : ReferralReasonCode	, ('CallInStart') : CallInStart	, ('IsClient') : IsClient	, ('AppropriateRBClass') : AppropriateRBClass	
, ('AppropriateRBRate') : AppropriateRBRate	, ('TreatmentEnd') : TreatmentEnd	, ('IsSpecialCondition') : IsSpecialCondition	, ('ClientClassNo') : ClientClassNo	
, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode	, ('TreatmentCode') : TreatmentCode	, ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem	
, ('MaternityMedicalTreatment') : MaternityMedicalTreatment	, ('Source') : Source	, ('NonMedicalItem') : NonMedicalItem	, ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered	
, ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion	, ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo	, ('IsODS') : IsODS	, ('IsODC') : IsODC	
, ('OPNO') : OPNO ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)
/*API.Note(API.getResponseData(var).Data.ClaimValidationResult)
API.Note(API.getResponseData(var).Data.ClaimCallInStatus)
API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)
*/

//Cek Hasil Validasi Diagnosa
/*
if (API.getResponseData(var).Data.ClaimValidationResult=='COVER') {
	KeywordUtil.markPassed('COVERAGE PASSED')
} else {
	KeywordUtil.markFailed('COVERAGE FAILED')
}

if (API.getResponseData(var).Data.ClaimCallInStatus=='Not Need Follow Up') {
	KeywordUtil.markPassed('CALL STATUS PASSED')
} else {
	KeywordUtil.markFailed('CALL STATUS FAILED')
}
*/

//Diagnosis Need FU
/*
def AllValidation=API.getResponseData(var).Data.DiagnosisRejectMsgValidationF
if (AllValidation!=null){
	if (AllValidation==true){
		KeywordUtil.markPassed('Diagnosis Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	} else {
		KeywordUtil.markFailed('Diagnosis Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	}
} else {
	KeywordUtil.markFailed('Diagnosis Confirmation is NULL')
}
*/

//Diagnosis Not Need FU (Produce / Reject)

def AllValidation=API.getResponseData(var).Data.DiagnosisRejectMsgValidationF
if (AllValidation!=null){
	if (AllValidation==false){
		KeywordUtil.markPassed('Diagnosis Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	} else {
		KeywordUtil.markFailed('Diagnosis Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	}
} else {
	KeywordUtil.markFailed('Diagnosis Confirmation is NULL')
}


//Validasi Lain Need FU
def AllValidation2=API.getResponseData(var).Data.DocumentValidationF
if (AllValidation2!=null){
	if (AllValidation2==true){
		KeywordUtil.markPassed('Document Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.DocumentValidationF)
	} else {
		KeywordUtil.markFailed('Document Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.DocumentValidationF)
	}
} else {
	KeywordUtil.markFailed('Document Confirmation is NULL')
}

//Validasi Lain Need FU
/*
def AllValidation3=API.getResponseData(var).Data.ExcessValidationF
if (AllValidation3!=null){
	if (AllValidation3==true){
		KeywordUtil.markPassed('Excess Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.ExcessValidationF)
	} else {
		KeywordUtil.markFailed('Excess Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.ExcessValidationF)
	}
} else {
	KeywordUtil.markFailed('Excess Confirmation is NULL')
}


def AllValidation4=API.getResponseData(var).Data.MAMedicalTreatmentValidationF
if (AllValidation4!=null){
	if (AllValidation4==true){
		KeywordUtil.markPassed('Medical Treatment Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.MAMedicalTreatmentValidationF)
	} else {
		KeywordUtil.markFailed('Medical Treatment Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.MAMedicalTreatmentValidationF)
	}
} else {
	KeywordUtil.markFailed('Medical Treatment Confirmation is NULL')
}
*/
//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}