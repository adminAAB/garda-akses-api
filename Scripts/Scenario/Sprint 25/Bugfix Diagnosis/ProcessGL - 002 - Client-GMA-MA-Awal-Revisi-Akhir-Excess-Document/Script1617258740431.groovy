import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'T/00042276'
String TreatmentDate = '2021-04-01'
String DiagnosisCode = 'O80'
String ProductType = 'MA'
String AdditionalDiagnosisInfo = '[[],[{"QuestionID":"992","QuestionDescription":"Nilai leukosit rendah","AnswerValue":"2","AdditionalInfo":"NBH Test","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"993","QuestionDescription":"Demam > 38 (+)","AnswerValue":"2","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"994","QuestionDescription":"Intake sulit (+)","AnswerValue":"2","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]]'
String ProviderID = 'TJKKG0020'
String PatientPhone = '081808623854'
String CallerName = ''
String TreatmentRoom = 'KELAS 2'
String TreatmentRoomAmount = '200000'
String DoctorName = 'test'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '0'
String ProviderEmail = 'yoguh567@gmail.com'
String ProviderFax = '1234'
String ProviderPhone = '082812012121'
String ProviderExt = '1234'
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'EXC'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKKG0020'
String Guid = '9c33c4d7a0cd462a81e0b9ed7216d705656380ab21ca16a62ea3ef0e5956a0b0'
String TicketNo = ''
String GLType = 'GL Akhir'
String AccountManager = ''
String PreviousGuid = '96fa856710674be69ec0752900bf3c79568a974786f1dedc4cef20c90a41c87a'
String PreviousTrID = '2862479'
String TotalBilled = '100000'
String ClientID = 'C01AA00001'
String ClassNo = '1'
String Membership = '2. SPO'
String AllDiagnosis = '[[1,"O80","SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY","Initial Primary","","","","0","0","0","0","","Covered","","","","","Covered",0,"0","0"],[2,"Q70.4","Polysyndactyly","Initial Secondary","Covered","","","","","","","","Uncovered","","","",null,"Uncovered",0,"0","0"]]'
String AllDoctors = 'test'
String Gender = 'F'
String DOB = '1992-07-28'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '9c33c4d7a0cd462a81e0b9ed7216d705656380ab21ca16a62ea3ef0e5956a0b0'
String TreatmentRoomChoosen = 'KELAS 2'
String AppropriateRBClassChoosen = 'KELAS 2'
String AppropriateRBRateChoosen = '200000'
String TreatmentRoomAmountChoosen = '200000'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'KELAS 2'
String AppropriateRBRate = '200000'
String TreatmentEnd = '2021-04-03'
String IsSpecialCondition = '0'
String ClientClassNo = '2'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = 'MAT001'
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = ''
String Source = 'TJKKG0020'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = ''
String AllDiagnosisAdditionalInfo = '[[],[{"QuestionID":"992","QuestionDescription":"Nilai leukosit rendah","AnswerValue":"2","AdditionalInfo":"NBH Test","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"993","QuestionDescription":"Demam > 38 (+)","AnswerValue":"2","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"994","QuestionDescription":"Intake sulit (+)","AnswerValue":"2","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]]'
String IsODS = '0'
String IsODC = '0'
String OPNO = '106186'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CCOOutbound/ProcessGL', [('authorization') : GlobalVariable.authorization     	      
, ('MemberNo') : MemberNo	, ('TreatmentDate') : TreatmentDate	, ('DiagnosisCode') : DiagnosisCode	, ('ProductType') : ProductType	, ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo
, ('ProviderID') : ProviderID	, ('PatientPhone') : PatientPhone	, ('CallerName') : CallerName	, ('TreatmentRoom') : TreatmentRoom	, ('TreatmentRoomAmount') : TreatmentRoomAmount	
, ('DoctorName') : DoctorName	, ('RoomOption') : RoomOption	, ('RoomAvailability') : RoomAvailability	, ('UpgradeClass') : UpgradeClass	, ('BenefitAmount') : BenefitAmount	
, ('ProviderEmail') : ProviderEmail	, ('ProviderFax') : ProviderFax	, ('ProviderPhone') : ProviderPhone	, ('ProviderExt') : ProviderExt	, ('IsTiri') : IsTiri	
, ('UserPosition') : UserPosition	, ('Remarks') : Remarks	, ('serviceTypeID') : serviceTypeID	, ('CallStatusID') : CallStatusID	, ('DefaultProviderID') : DefaultProviderID	
, ('Guid') : Guid	, ('TicketNo') : TicketNo	, ('GLType') : GLType	, ('AccountManager') : AccountManager	, ('PreviousGuid') : PreviousGuid	
, ('PreviousTrID') : PreviousTrID	, ('TotalBilled') : TotalBilled	, ('ClientID') : ClientID	, ('ClassNo') : ClassNo	, ('Membership') : Membership	
, ('AllDiagnosis') : AllDiagnosis	, ('AllDoctors') : AllDoctors	, ('Gender') : Gender	, ('DOB') : DOB	, ('NewMemberName') : NewMemberName	
, ('GLStatus') : GLStatus	, ('EmpMemberNo') : EmpMemberNo	, ('NMEmpID') : NMEmpID	, ('FollowUpTaskID') : FollowUpTaskID	, ('TreatmentRoomChoosen') : TreatmentRoomChoosen	
, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen	, ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen	, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen	
, ('IsReferral') : IsReferral	, ('ReferralReasonCode') : ReferralReasonCode	, ('CallInStart') : CallInStart	, ('IsClient') : IsClient	, ('AppropriateRBClass') : AppropriateRBClass	
, ('AppropriateRBRate') : AppropriateRBRate	, ('TreatmentEnd') : TreatmentEnd	, ('IsSpecialCondition') : IsSpecialCondition	, ('ClientClassNo') : ClientClassNo	
, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode	, ('TreatmentCode') : TreatmentCode	, ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem	
, ('MaternityMedicalTreatment') : MaternityMedicalTreatment	, ('Source') : Source	, ('NonMedicalItem') : NonMedicalItem	, ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered	
, ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion	, ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo	, ('IsODS') : IsODS	, ('IsODC') : IsODC	
, ('OPNO') : OPNO ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)
/*API.Note(API.getResponseData(var).Data.ClaimValidationResult)
API.Note(API.getResponseData(var).Data.ClaimCallInStatus)
API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)
*/

//Cek Hasil Validasi Diagnosa
/*
if (API.getResponseData(var).Data.ClaimValidationResult=='COVER') {
	KeywordUtil.markPassed('COVERAGE PASSED')
} else {
	KeywordUtil.markFailed('COVERAGE FAILED')
}

if (API.getResponseData(var).Data.ClaimCallInStatus=='Not Need Follow Up') {
	KeywordUtil.markPassed('CALL STATUS PASSED')
} else {
	KeywordUtil.markFailed('CALL STATUS FAILED')
}
*/

//Diagnosis Need FU
/*
def AllValidation=API.getResponseData(var).Data.DiagnosisRejectMsgValidationF
if (AllValidation!=null){
	if (AllValidation==true){
		KeywordUtil.markPassed('Diagnosis Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	} else {
		KeywordUtil.markFailed('Diagnosis Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	}
} else {
	KeywordUtil.markFailed('Diagnosis Confirmation is NULL')
}
*/

//Diagnosis Not Need FU (Produce / Reject)

def AllValidation=API.getResponseData(var).Data.DiagnosisRejectMsgValidationF
if (AllValidation!=null){
	if (AllValidation==false){
		KeywordUtil.markPassed('Diagnosis Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	} else {
		KeywordUtil.markFailed('Diagnosis Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	}
} else {
	KeywordUtil.markFailed('Diagnosis Confirmation is NULL')
}


//Validasi Lain Need FU
def AllValidation2=API.getResponseData(var).Data.DocumentValidationF
if (AllValidation2!=null){
	if (AllValidation2==true){
		KeywordUtil.markPassed('Document Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.DocumentValidationF)
	} else {
		KeywordUtil.markFailed('Document Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.DocumentValidationF)
	}
} else {
	KeywordUtil.markFailed('Document Confirmation is NULL')
}

//Validasi Lain Need FU
def AllValidation3=API.getResponseData(var).Data.ExcessValidationF
if (AllValidation3!=null){
	if (AllValidation3==true){
		KeywordUtil.markPassed('Excess Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.ExcessValidationF)
	} else {
		KeywordUtil.markFailed('Excess Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.ExcessValidationF)
	}
} else {
	KeywordUtil.markFailed('Excess Confirmation is NULL')
}


//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}