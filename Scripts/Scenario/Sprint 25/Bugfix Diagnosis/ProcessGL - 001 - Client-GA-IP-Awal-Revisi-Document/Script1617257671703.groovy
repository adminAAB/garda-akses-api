import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'D/31319'
String TreatmentDate = '2021-04-01'
String DiagnosisCode = 'Q42.3'
String ProductType = 'IP'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'TJKRP0146'
String PatientPhone = '081808623854'
String CallerName = ''
String TreatmentRoom = 'HCU'
String TreatmentRoomAmount = '850000'
String DoctorName = 'test'
String RoomOption = 'APS'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '900000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = '197'
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'PRI'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKRP0146'
String Guid = '24121e4d723b4a30b9cd9b57c1f04d712f10eb94f2dcc3518011415b8c766b3f'
String TicketNo = ''
String GLType = 'GL Lanjutan'
String AccountManager = ''
String PreviousGuid = '0d71666833e24217a8af68d9c20e6abf4acafeb5174dc2129593f7b28611e795'
String PreviousTrID = '2862470'
String TotalBilled = '0'
String ClientID = 'C01AA00001'
String ClassNo = '4'
String Membership = '2. SPO'
String AllDiagnosis = '[[1,"Q42.3","CONGENITAL ABSENCE- ATRESIA AND STENOSIS OF ANUS WITHOUT FISTULA","Initial Primary","","","","","","","","","Uncovered","","","","","",0,"0","0"]]'
String AllDoctors = 'test'
String Gender = 'F'
String DOB = '1984-03-21'
String NewMemberName = ''
String GLStatus = 'Produce'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '24121e4d723b4a30b9cd9b57c1f04d712f10eb94f2dcc3518011415b8c766b3f'
String TreatmentRoomChoosen = 'HCU'
String AppropriateRBClassChoosen = 'HCU'
String AppropriateRBRateChoosen = '850000'
String TreatmentRoomAmountChoosen = '850000'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'HCU'
String AppropriateRBRate = '850000'
String TreatmentEnd = ''
String IsSpecialCondition = '0'
String ClientClassNo = '3'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = ''
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = ''
String Source = 'hgl'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = ''
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '0'
String OPNO = '106186'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CCOOutbound/ProcessGL', [('authorization') : GlobalVariable.authorization     	      
, ('MemberNo') : MemberNo	, ('TreatmentDate') : TreatmentDate	, ('DiagnosisCode') : DiagnosisCode	, ('ProductType') : ProductType	, ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo
, ('ProviderID') : ProviderID	, ('PatientPhone') : PatientPhone	, ('CallerName') : CallerName	, ('TreatmentRoom') : TreatmentRoom	, ('TreatmentRoomAmount') : TreatmentRoomAmount	
, ('DoctorName') : DoctorName	, ('RoomOption') : RoomOption	, ('RoomAvailability') : RoomAvailability	, ('UpgradeClass') : UpgradeClass	, ('BenefitAmount') : BenefitAmount	
, ('ProviderEmail') : ProviderEmail	, ('ProviderFax') : ProviderFax	, ('ProviderPhone') : ProviderPhone	, ('ProviderExt') : ProviderExt	, ('IsTiri') : IsTiri	
, ('UserPosition') : UserPosition	, ('Remarks') : Remarks	, ('serviceTypeID') : serviceTypeID	, ('CallStatusID') : CallStatusID	, ('DefaultProviderID') : DefaultProviderID	
, ('Guid') : Guid	, ('TicketNo') : TicketNo	, ('GLType') : GLType	, ('AccountManager') : AccountManager	, ('PreviousGuid') : PreviousGuid	
, ('PreviousTrID') : PreviousTrID	, ('TotalBilled') : TotalBilled	, ('ClientID') : ClientID	, ('ClassNo') : ClassNo	, ('Membership') : Membership	
, ('AllDiagnosis') : AllDiagnosis	, ('AllDoctors') : AllDoctors	, ('Gender') : Gender	, ('DOB') : DOB	, ('NewMemberName') : NewMemberName	
, ('GLStatus') : GLStatus	, ('EmpMemberNo') : EmpMemberNo	, ('NMEmpID') : NMEmpID	, ('FollowUpTaskID') : FollowUpTaskID	, ('TreatmentRoomChoosen') : TreatmentRoomChoosen	
, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen	, ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen	, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen	
, ('IsReferral') : IsReferral	, ('ReferralReasonCode') : ReferralReasonCode	, ('CallInStart') : CallInStart	, ('IsClient') : IsClient	, ('AppropriateRBClass') : AppropriateRBClass	
, ('AppropriateRBRate') : AppropriateRBRate	, ('TreatmentEnd') : TreatmentEnd	, ('IsSpecialCondition') : IsSpecialCondition	, ('ClientClassNo') : ClientClassNo	
, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode	, ('TreatmentCode') : TreatmentCode	, ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem	
, ('MaternityMedicalTreatment') : MaternityMedicalTreatment	, ('Source') : Source	, ('NonMedicalItem') : NonMedicalItem	, ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered	
, ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion	, ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo	, ('IsODS') : IsODS	, ('IsODC') : IsODC	
, ('OPNO') : OPNO ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)
/*API.Note(API.getResponseData(var).Data.ClaimValidationResult)
API.Note(API.getResponseData(var).Data.ClaimCallInStatus)
API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)
*/

//Cek Hasil Validasi Diagnosa
/*
if (API.getResponseData(var).Data.ClaimValidationResult=='COVER') {
	KeywordUtil.markPassed('COVERAGE PASSED')
} else {
	KeywordUtil.markFailed('COVERAGE FAILED')
}

if (API.getResponseData(var).Data.ClaimCallInStatus=='Not Need Follow Up') {
	KeywordUtil.markPassed('CALL STATUS PASSED')
} else {
	KeywordUtil.markFailed('CALL STATUS FAILED')
}
*/

//Diagnosis Need FU
/*
def AllValidation=API.getResponseData(var).Data.DiagnosisRejectMsgValidationF
if (AllValidation!=null){
	if (AllValidation==true){
		KeywordUtil.markPassed('Diagnosis Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	} else {
		KeywordUtil.markFailed('Diagnosis Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	}
} else {
	KeywordUtil.markFailed('Diagnosis Confirmation is NULL')
}
*/

//Diagnosis Not Need FU (Produce / Reject)

def AllValidation=API.getResponseData(var).Data.DiagnosisRejectMsgValidationF
if (AllValidation!=null){
	if (AllValidation==false){
		KeywordUtil.markPassed('Diagnosis Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	} else {
		KeywordUtil.markFailed('Diagnosis Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.DiagnosisRejectMsgValidationF)
		API.Note(API.getResponseData(var).Data.DiagnosisUncovered)
	}
} else {
	KeywordUtil.markFailed('Diagnosis Confirmation is NULL')
}


//Validasi Lain Need FU - Excess
def AllValidation2=API.getResponseData(var).Data.DocumentValidationF
if (AllValidation2!=null){
	if (AllValidation2==true){
		KeywordUtil.markPassed('Document Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.DocumentValidationF)
	} else {
		KeywordUtil.markFailed('Document Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.DocumentValidationF)
	}
} else {
	KeywordUtil.markFailed('Document Confirmation is NULL')
}


//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}