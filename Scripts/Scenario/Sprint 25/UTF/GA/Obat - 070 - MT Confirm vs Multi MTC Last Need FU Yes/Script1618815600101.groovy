import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String expectedResult = 'Uncovered'

String Wording = 'Medical Treatment memerlukan konfirmasi Garda Medika'

String MaternityMedicalTreatment = '[["A38",381,0,null,"0","","Scarlet fever","Uncovered",0,"","","","",1,"1","OBT","15/04/2021 15:36","",0,"3","30000","90000",0,null,0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi  : \n - \n","Obat Sakit","Obat"]]'
String NeedFU = 1

String MedicalTreatmentConfirmation = '[{"ConfirmationType":"AM","PICDesc":"Account Manager","PICName":"AIB","NameDesc":"Arrio Probo Wibowo","ConfirmationDate":"12/04/2021 14:53","Confirmation":"HLD","ConfirmationDesc":"Hold","Remarks":"test","FollowUpTime":"0","Channel":"CALL","BenefitCoverage":"","Reason":"FUR001","FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","NeedFU":1,"IsCito":0,"IsPrevious":1,"GuidConfirmation":"551028dd-6fb1-4920-85f4-3e781ed6184e"},{"ConfirmationType":"AM","PICDesc":"Account Manager","PICName":"AIB","NameDesc":"Arrio Probo Wibowo","ConfirmationDate":"12/04/2021 14:53","Confirmation":"HLD","ConfirmationDesc":"Hold","Remarks":"test","FollowUpTime":"0","Channel":"CALL","BenefitCoverage":"","Reason":"FUR001","FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","NeedFU":'+ NeedFU +',"IsCito":0,"IsPrevious":1,"GuidConfirmation":"551028dd-6fb1-4920-85f4-3e781ed6184e"}]'

String AdminName = ''

String IsGLProvider = 'False'

String MemberNo = 'A/00189026'

String PreviousTrID = ''

String TreatmentDate = '2021-04-19'

String ProductType = 'IP'

String DiagnosisCode = 'A38'

String DiagnosisAdditionalInfo = ''

String ProviderID = 'TJKRP0146'

String PatientPhone = '08123123123'

String CallerName = '-'

String TreatmentRoom = 'DELUXE (KELAS I A)'

String TreatmentRoomAmount = '800000'

String DoctorName = 'Alfiben'

String RoomOption = 'On Plan'

String RoomAvailability = 'NONE'

String UpgradeClass = 'NONE'

String BenefitAmount = '800000'

String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'

String ProviderFax = '021-29531922'

String ProviderPhone = '081806221986'

String ProviderExt = '197'

String IsTiri = '0'

String DefaultProviderID = ''

String Remarks = ''

String Guid = '91ffbb8013dd4976a83bde0e74888d4a40a361e3b4ccc6ce039945cfcbb3c497'

String TicketNo = ''

String GLType = 'GL Awal'

String AccountManager = ''

String TotalBilled = '0'

String ClientID = 'CJKPP0009'

String ClassNo = '14'

String MembershipType = '2. SPO'

String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"0","0"]]'

String AllDiagnosisAdditionalInfo = '[[]]'

String AllDoctors = 'Alfiben'

String Gender = 'F'

String DOB = '1989-03-31'

String NewMemberName = ''

String PreviousGuid = 'ff09d410497a41fc8a13e457c94b9cd53ee67086b938110727bf42180e80a83e'

String GLStatus = 'Produce'

String EmpMemberNo = ''

String NMEmpID = ''

String FollowUpTaskID = 'de6e53861b6548c89dff014f30a8b2d2d18bdb3476a560cbb126acaf693b0871'

String IsClient = '1'

String CallStatusID = 'Not Need Follow Up'

String AppropriateRBClass = 'DELUXE (KELAS I A)'

String AppropriateRBRate = '800000'

String TreatmentEnd = ''

String AppropriateRBClassChoosen = 'DELUXE (KELAS I A)'

String AppropriateRBRateChoosen = '800000'

String TreatmentRoomChoosen = 'DELUXE (KELAS I A)'

String TreatmentRoomAmountChoosen = '800000'

String IsReferral = '0'

String IsSpecialCondition = '0'

String ReferralReasonCode = ''

String CallInStart = ''

String SecondaryDiagnosisCode = '[]'

String ClientClassNo = '10'

String UserPosition = 'Customer Service'

String OPNO = '105323'

//String MaternityMedicalTreatment = '[["A38",322,0,null,"0","","Scarlet fever","Covered",0,"","","","",1,"1","OBT","15/04/2021 12:27","",0,"3","30000","900000",0,null,0,"Test QC","Coverage : Dijaminkan\n\nReason : \n -\n\nDokumen yang perlu dilengkapi  : \n - \n","Obat sakit","Obat"]]'
String MaternityFamilyPlanningItem = ''

String TreatmentCode = ''

String IsFromProcessButton = '0'

String NonMedicalItem = ''

String IsDiagnosisQuestionNotRegistered = '0'

String MedicalTreatmentAdditionalQuestion = '[[],[]]'

String IsODS = '0'

String IsODC = '0'

String IsProducttypeChange = '0'

String IsTreatmentPeriodChange = '0'

String IsMaternityTreatmentChange = '0'

String IsRoomOptionChange = '0'

String IsTreatmentRBClassChange = '0'

String IsODSODCChange = '0'

String IsDocValidityChange = '0'

String IsDocTypeChange = '0'

String suspectDouble = '0'

String IsProtapCovid = '1'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'), [:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/GA_FinalValidationAPTTOInTreatment', [('authorization') : GlobalVariable.authorization
            , ('AdminName') : AdminName, ('IsGLProvider') : IsGLProvider, ('MemberNo') : MemberNo, ('PreviousTrID') : PreviousTrID
            , ('TreatmentDate') : TreatmentDate, ('ProductType') : ProductType, ('DiagnosisCode') : DiagnosisCode, ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
            , ('ProviderID') : ProviderID, ('PatientPhone') : PatientPhone, ('CallerName') : CallerName, ('TreatmentRoom') : TreatmentRoom
            , ('TreatmentRoomAmount') : TreatmentRoomAmount, ('DoctorName') : DoctorName, ('RoomOption') : RoomOption, ('RoomAvailability') : RoomAvailability
            , ('UpgradeClass') : UpgradeClass, ('BenefitAmount') : BenefitAmount, ('ProviderEmail') : ProviderEmail, ('ProviderFax') : ProviderFax
            , ('ProviderPhone') : ProviderPhone, ('ProviderExt') : ProviderExt, ('IsTiri') : IsTiri, ('DefaultProviderID') : DefaultProviderID
            , ('Remarks') : Remarks, ('Guid') : Guid, ('TicketNo') : TicketNo, ('GLType') : GLType, ('AccountManager') : AccountManager
            , ('TotalBilled') : TotalBilled, ('ClientID') : ClientID, ('ClassNo') : ClassNo, ('MembershipType') : MembershipType
            , ('AllDiagnosis') : AllDiagnosis, ('AllDoctors') : AllDoctors, ('Gender') : Gender, ('DOB') : DOB, ('NewMemberName') : NewMemberName
            , ('PreviousGuid') : PreviousGuid, ('GLStatus') : GLStatus, ('EmpMemberNo') : EmpMemberNo, ('NMEmpID') : NMEmpID
            , ('FollowUpTaskID') : FollowUpTaskID, ('IsClient') : IsClient, ('CallStatusID') : CallStatusID, ('AppropriateRBClass') : AppropriateRBClass
            , ('AppropriateRBRate') : AppropriateRBRate, ('TreatmentEnd') : TreatmentEnd, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen
            , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen, ('TreatmentRoomChoosen') : TreatmentRoomChoosen, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
            , ('IsReferral') : IsReferral, ('IsSpecialCondition') : IsSpecialCondition, ('ReferralReasonCode') : ReferralReasonCode
            , ('CallInStart') : CallInStart, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode, ('ClientClassNo') : ClientClassNo
            , ('UserPosition') : UserPosition, ('OPNO') : OPNO, ('MaternityMedicalTreatment') : MaternityMedicalTreatment
            , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem, ('TreatmentCode') : TreatmentCode, ('IsFromProcessButton') : IsFromProcessButton
            , ('NonMedicalItem') : NonMedicalItem, ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered
            , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion, ('IsODS') : IsODS, ('IsODC') : IsODC
            , ('IsProducttypeChange') : IsProducttypeChange, ('IsTreatmentPeriodChange') : IsTreatmentPeriodChange, ('IsMaternityTreatmentChange') : IsMaternityTreatmentChange
            , ('IsRoomOptionChange') : IsRoomOptionChange, ('IsTreatmentRBClassChange') : IsTreatmentRBClassChange, ('IsODSODCChange') : IsODSODCChange
            , ('IsDocValidityChange') : IsDocValidityChange, ('IsDocTypeChange') : IsDocTypeChange, ('suspectDouble') : suspectDouble
            , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo, ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation
            , ('IsProtapCovid') : IsProtapCovid]))

API.Note(API.getResponseData(var).Status)

API.Note(API.getResponseData(var).Data)

//API.Note(API.getResponseData(var).Data.ClaimValidationMessage)
//API.Note(API.getResponseData(var).Data.ClaimValidationNonCoverReason)
//API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
//API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)
//Cek Hasil Validasi Diagnosa
def AllValidation = API.getResponseData(var).Data.ClaimValidationResult

def DetailValidation = API.getResponseData(var).Data.ClaimValidationFinalResult

if ((AllValidation == 'COVER') && (expectedResult == 'Covered')) {
    KeywordUtil.markPassed('Validasi Medical Treatment Sesuai')

    KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)

    KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)

    KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
} else if ((AllValidation == 'NONCOVER') && (expectedResult == 'Uncovered')) {
    if (DetailValidation.contains(Wording)) {
        KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationMessage)

        KeywordUtil.markPassed(Wording)

        KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)

        KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)

        KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
    } else {
        KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')

        KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)

        KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)

        KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
    }
} else {
    KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')

    KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)

    KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)

    KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
}

/*
	} else {
		KeywordUtil.markFailed('NON COVER FAILED')
	}
} else {
	KeywordUtil.markFailed('NON COVER IS NULL')
}
*/
//Cek Status API
if (API.getResponseData(var).Status) {
    KeywordUtil.markPassed('API OK')
} else {
    KeywordUtil.markFailedAndStop('Terjadi kesalahan pada API! - ' + API.getResponseData(var).ErrorMessage)
}

