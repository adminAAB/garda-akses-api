import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String IsGLProvider = '0'
String IsODS = '0'
String IsODC = '0'
String IsProtapCovid = '0'
String TreatmentDate = '08/04/2021 08:52'
String MaternityTreatmentCode = ''
String ClientClassNo = '0'
String MTAdditionalQuestion = ''
String IsUnregistered = '0'
String DiagnosisID = 'J35.0'
String MedicalTreatmentID = '48'
String Billed = '100000'
String Category = 'TTO'
String UnknownBilled = '0'
String UnknownPatientCondition = '0'
String UnregisteredMedicalTreatment = ''
String MedicalTreatmentDescription = 'Tonsilectomy'
String MemberNo = 'I/00057570'
String ClientID = 'CJKPA0044'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/GetMedicalTreatmentResult', [('authorization') : GlobalVariable.authorization     	      
 , ('IsGLProvider') : IsGLProvider , ('IsODS') : IsODS , ('IsODC') : IsODC
 , ('IsProtapCovid') : IsProtapCovid , ('TreatmentDate') : TreatmentDate , ('MaternityTreatmentCode') : MaternityTreatmentCode , ('ClientClassNo') : ClientClassNo
 , ('MTAdditionalQuestion') : MTAdditionalQuestion , ('IsUnregistered') : IsUnregistered , ('DiagnosisID') : DiagnosisID , ('MedicalTreatmentID') : MedicalTreatmentID
 , ('Billed') : Billed , ('Category') : Category , ('UnknownBilled') : UnknownBilled , ('UnknownPatientCondition') : UnknownPatientCondition
 , ('UnregisteredMedicalTreatment') : UnregisteredMedicalTreatment , ('MedicalTreatmentDescription') : MedicalTreatmentDescription , ('MemberNo') : MemberNo , ('ClientID') : ClientID ]))


API.Note(API.getResponseData(var).status)
//API.Note(API.getResponseData(var).Data)
//API.Note(API.getResponseData(var).Data.ClaimValidationMessage)
//API.Note(API.getResponseData(var).Data.ClaimValidationNonCoverReason)
//API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
//API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)


//Cek Hasil Validasi Diagnosa

def AllValidation=API.getResponseData(var).data.Data

if (AllValidation!=null){
	if (AllValidation.contains('Dijaminkan')){
		KeywordUtil.markPassed(API.getResponseData(var).data.Data)
		KeywordUtil.markPassed('Validasi Medical Treatment Sesuai')
	} else {
		KeywordUtil.markFailed(API.getResponseData(var).data.Data)
		KeywordUtil.markFailed('Validasi Medical Treatment Tidak Sesuai')
	}
} else {
	KeywordUtil.markFailed(API.getResponseData(var).data)
	KeywordUtil.markFailed('Validasi Medical Treatment Tidak Sesuai')
}
/*
	} else {
		KeywordUtil.markFailed('NON COVER FAILED')
	}
} else {
	KeywordUtil.markFailed('NON COVER IS NULL')
}
*/

//Cek Status API
if (API.getResponseData(var).status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}