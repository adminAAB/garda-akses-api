import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String AdminName = ''
String IsGLProvider = 'False'
String MemberNo = 'R/00101646'
String PreviousTrID = '2862779'
String TreatmentDate = '2021-04-08'
String ProductType = 'IP'
String DiagnosisCode = 'A09'
String DiagnosisAdditionalInfo = '[{"QuestionID":"3","QuestionDescription":"BAB Cair >= 3 kali dalam 24 jam","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"BILPA|BNDNG","AdditionalDocDesc":" Billing Perawatan Asli, Billing Perbandingan"},{"QuestionID":"4","QuestionDescription":"Demam (+)","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"Penyakit","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"5","QuestionDescription":"Darah/Lendir (+)","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"6","QuestionDescription":"Tanda dehidrasi (+)","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]'
String ProviderID = 'TJKRP0146'
String PatientPhone = '081808623854'
String CallerName = 'Maste'
String TreatmentRoom = 'ISOLASI'
String TreatmentRoomAmount = '750000'
String DoctorName = 'test'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '350000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = '197'
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String Guid = 'cc18aa2ed4644efeb6eaf9b19e9102b7ce8ff77906a269d4d73cac02f23299c9'
String TicketNo = 'CL204352'
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'CJKPA0045'
String ClassNo = '2'
String MembershipType = '3. CHI'
String AllDiagnosis = '[[1,"A09","DIARRHOEA AND GASTROENTERITIS OF PRESUMED INFECTIOUS ORIGIN","Initial Primary","Uncovered","","","","","","","","Covered","","","",null,"Question",0,"0","0"]]'
String AllDiagnosisAdditionalInfo = '[[{"QuestionID":"3","QuestionDescription":"BAB Cair >= 3 kali dalam 24 jam","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"BILPA|BNDNG","AdditionalDocDesc":" Billing Perawatan Asli, Billing Perbandingan"},{"QuestionID":"4","QuestionDescription":"Demam (+)","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"Penyakit","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"5","QuestionDescription":"Darah/Lendir (+)","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""},{"QuestionID":"6","QuestionDescription":"Tanda dehidrasi (+)","AnswerValue":"2","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]]'
String AllDoctors = 'test'
String Gender = 'F'
String DOB = '2017-05-23'
String NewMemberName = ''
String PreviousGuid = 'cc18aa2ed4644efeb6eaf9b19e9102b7ce8ff77906a269d4d73cac02f23299c9'
String GLStatus = 'Produce'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = 'e4f49e25d0a042b7998eb9cb511363a52171d65ee6323314defe276e4845c9e0'
String IsClient = '1'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'ISOLASI'
String AppropriateRBRate = '750000'
String TreatmentEnd = ''
String AppropriateRBClassChoosen = 'ISOLASI'
String AppropriateRBRateChoosen = '750000'
String TreatmentRoomChoosen = 'ISOLASI'
String TreatmentRoomAmountChoosen = '750000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = '2021-04-08 10:53:00'
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '2'
String UserPosition = 'Customer Service'
String OPNO = '105330'
String MaternityMedicalTreatment = '[["A09",225,0,null,"100000","Vena Section","DIARRHOEA AND GASTROENTERITIS OF PRESUMED INFECTIOUS ORIGIN","Covered",0,"","","",null,1,"1","TTO","08/04/2021 14:01","",0,null,"0","0",0,null,0,"tsg","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi  : \n",null,"Tindakan/Terapi"]] '
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[{"QuestionID":"325","QuestionDescription":"TSG Question True Covered","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocDesc":""}]]'
String IsODS = '0'
String IsODC = '0'
String IsProducttypeChange = '0'
String IsTreatmentPeriodChange = '0'
String IsMaternityTreatmentChange = '0'
String IsRoomOptionChange = '0'
String IsTreatmentRBClassChange = '0'
String IsODSODCChange = '0'
String IsDocValidityChange = '0'
String IsDocTypeChange = '0'
String suspectDouble = '0'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '0'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/GA_FinalValidationAPTTOInTreatment', [('authorization') : GlobalVariable.authorization     	      
 , ('AdminName') : AdminName , ('IsGLProvider') : IsGLProvider , ('MemberNo') : MemberNo , ('PreviousTrID') : PreviousTrID
 , ('TreatmentDate') : TreatmentDate , ('ProductType') : ProductType , ('DiagnosisCode') : DiagnosisCode , ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
 , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom
 , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability
 , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax
 , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('DefaultProviderID') : DefaultProviderID
 , ('Remarks') : Remarks , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType
 , ('AccountManager') : AccountManager , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo
 , ('MembershipType') : MembershipType , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender
 , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('PreviousGuid') : PreviousGuid , ('GLStatus') : GLStatus
 , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('IsClient') : IsClient
 , ('CallStatusID') : CallStatusID , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd
 , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
 , ('IsReferral') : IsReferral , ('IsSpecialCondition') : IsSpecialCondition , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart
 , ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('ClientClassNo') : ClientClassNo , ('UserPosition') : UserPosition , ('OPNO') : OPNO
 , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('TreatmentCode') : TreatmentCode , ('IsFromProcessButton') : IsFromProcessButton
 , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('IsODS') : IsODS
 , ('IsODC') : IsODC , ('IsProducttypeChange') : IsProducttypeChange , ('IsTreatmentPeriodChange') : IsTreatmentPeriodChange , ('IsMaternityTreatmentChange') : IsMaternityTreatmentChange
 , ('IsRoomOptionChange') : IsRoomOptionChange , ('IsTreatmentRBClassChange') : IsTreatmentRBClassChange , ('IsODSODCChange') : IsODSODCChange , ('IsDocValidityChange') : IsDocValidityChange
 , ('IsDocTypeChange') : IsDocTypeChange , ('suspectDouble') : suspectDouble , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation
 , ('IsProtapCovid') : IsProtapCovid ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)
//API.Note(API.getResponseData(var).Data.ClaimValidationMessage)
//API.Note(API.getResponseData(var).Data.ClaimValidationNonCoverReason)
//API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
//API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)


//Cek Hasil Validasi Diagnosa

def AllValidation=API.getResponseData(var).Data.ClaimValidationResult

if (AllValidation=="COVER"){
	KeywordUtil.markPassed('Validasi Medical Treatment Sesuai')
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
} else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
}


/*
	} else {
		KeywordUtil.markFailed('NON COVER FAILED')
	}
} else {
	KeywordUtil.markFailed('NON COVER IS NULL')
}
*/

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}