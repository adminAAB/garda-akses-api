import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String AdminName = 'tsg'
String IsGLProvider = 'True'
String MemberNo = 'M/00142005'
String PreviousTrID = '2892555'
String TreatmentDate = '2021-05-05'
String ProductType = 'IP'
String DiagnosisCode = 'A38'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'OJKRI00001'
String PatientPhone = '081808623854'
String CallerName = '-'
String TreatmentRoom = 'Kelas III'
String TreatmentRoomAmount = '315000'
String DoctorName = 'tsg'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '350000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '1234'
String ProviderPhone = '081806221986'
String ProviderExt = '1234'
String IsTiri = '0'
String DefaultProviderID = 'OJKRI00001'
String Remarks = ''
String Guid = 'e99a36e2483743c8b582d3377de2a0e8a6f299db410dae8f73d7de06ce6d8f21'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'CJKPA0045'
String ClassNo = '1'
String MembershipType = '3. CHI'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"0","0"]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String AllDoctors = 'tsg'
String Gender = 'F'
String DOB = '2018-02-06'
String NewMemberName = ''
String PreviousGuid = 'e99a36e2483743c8b582d3377de2a0e8a6f299db410dae8f73d7de06ce6d8f21'
String GLStatus = 'Produce'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = 'f527b8359b4648c4b96ecffc617958be2d5d9549ac7fb3bc2ffc2deb0dddae64'
String IsClient = '1'
String CallStatusID = 'Need Follow Up'
String AppropriateRBClass = 'Kelas III'
String AppropriateRBRate = '315000'
String TreatmentEnd = ''
String AppropriateRBClassChoosen = 'Kelas III'
String AppropriateRBRateChoosen = '315000'
String TreatmentRoomChoosen = 'Kelas III'
String TreatmentRoomAmountChoosen = '315000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '1'
String UserPosition = ''
String OPNO = '105330'
String MaternityMedicalTreatment = '[["A38",244,0,null,"100000","fissurectomy","Scarlet fever","Covered",0,"","","","",1,"1","TTO","05/05/2021 13:24","Minor",0,null,"0","0",0,null,0,"tsg","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi Garda Medika\n\nDokumen yang perlu dilengkapi  : \n - \n",null,"Tindakan Medis/Terapi","0"]]'
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[{"QuestionID":"326","QuestionDescription":"TSG Question True Covered","AnswerValue":"1","Remarks":"","AdditionalInfo":"TSG Additional Info 01","AdditionalInfoProvider":"","AdditionalDocDesc":""}]]'
String IsODS = '0'
String IsODC = '0'
String IsProducttypeChange = '0'
String IsTreatmentPeriodChange = '0'
String IsMaternityTreatmentChange = '0'
String IsRoomOptionChange = '0'
String IsTreatmentRBClassChange = '0'
String IsODSODCChange = '0'
String IsDocValidityChange = '0'
String IsDocTypeChange = '0'
String suspectDouble = '0'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '1'
String MedicalTreatmentResultMsg = '- Medical Treatment memerlukan konfirmasi Dokter Garda Medika<br> &ensp;Medical Treatment : fissurectomy <br> &ensp;Billed : 100.000 <br> &ensp;Additional Info : - <br> &ensp;Additional Documents : - <br> <br>'


WebUI.callTestCase(findTestCase('Pages/GMA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GMA/CreateTreatmentGL/GA_FinalValidationAPTTOInTreatment', [('authorization') : GlobalVariable.authorization     	      
 , ('AdminName') : AdminName , ('IsGLProvider') : IsGLProvider , ('MemberNo') : MemberNo , ('PreviousTrID') : PreviousTrID
 , ('TreatmentDate') : TreatmentDate , ('ProductType') : ProductType , ('DiagnosisCode') : DiagnosisCode , ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
 , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom
 , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability
 , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax
 , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('DefaultProviderID') : DefaultProviderID
 , ('Remarks') : Remarks , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType
 , ('AccountManager') : AccountManager , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo
 , ('MembershipType') : MembershipType , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender
 , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('PreviousGuid') : PreviousGuid , ('GLStatus') : GLStatus
 , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('IsClient') : IsClient
 , ('CallStatusID') : CallStatusID , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd
 , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
 , ('IsReferral') : IsReferral , ('IsSpecialCondition') : IsSpecialCondition , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart
 , ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('ClientClassNo') : ClientClassNo , ('UserPosition') : UserPosition , ('OPNO') : OPNO
 , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('TreatmentCode') : TreatmentCode , ('IsFromProcessButton') : IsFromProcessButton
 , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('IsODS') : IsODS
 , ('IsODC') : IsODC , ('IsProducttypeChange') : IsProducttypeChange , ('IsTreatmentPeriodChange') : IsTreatmentPeriodChange , ('IsMaternityTreatmentChange') : IsMaternityTreatmentChange
 , ('IsRoomOptionChange') : IsRoomOptionChange , ('IsTreatmentRBClassChange') : IsTreatmentRBClassChange , ('IsODSODCChange') : IsODSODCChange , ('IsDocValidityChange') : IsDocValidityChange
 , ('IsDocTypeChange') : IsDocTypeChange , ('suspectDouble') : suspectDouble , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation
 , ('IsProtapCovid') : IsProtapCovid, ('MedicalTreatmentResultMsg') : MedicalTreatmentResultMsg ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)
//API.Note(API.getResponseData(var).Data.ClaimValidationMessage)
//API.Note(API.getResponseData(var).Data.ClaimValidationNonCoverReason)
//API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
//API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}

//Cek Hasil Validasi Diagnosa
/*
def AllValidation=API.getResponseData(var).Data.ClaimValidationResult
def DetailValidation = API.getResponseData(var).Data.ClaimValidationFinalResult

if (AllValidation=="NONCOVER"){
	if (DetailValidation.contains('Medical Treatment memerlukan konfirmasi Dokter Garda Medika')) {
		KeywordUtil.markPassed('Medical Treatment memerlukan konfirmasi Dokter Garda Medika')
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationMessage)
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
	} else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationMessage)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
	}
} else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationMessage)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
}

*/
/*
	} else {
		KeywordUtil.markFailed('NON COVER FAILED')
	}
} else {
	KeywordUtil.markFailed('NON COVER IS NULL')
}
*/

