import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String AdminName = ''
String IsGLProvider = 'False'
String MemberNo = 'I/00161'
String PreviousTrID = '2873730'
String TreatmentDate = '2021-04-13'
String ProductType = 'IP'
String DiagnosisCode = 'A38'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'TJKRP0146'
String PatientPhone = '081808623854'
String CallerName = 'Maste'
String TreatmentRoom = 'KELAS III'
String TreatmentRoomAmount = '190000'
String DoctorName = 'test'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '0'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = '197'
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String Guid = '80bf41949ada42d6b6c296fe3d846773d8235f00617b9564a7dd61be742428cc'
String TicketNo = 'CL209141'
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = ''
String ClientID = 'C01AA00001'
String ClassNo = '1'
String MembershipType = '1. EMP'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"0","0"]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String AllDoctors = 'test'
String Gender = 'F'
String DOB = '1975-03-31'
String NewMemberName = ''
String PreviousGuid = '80bf41949ada42d6b6c296fe3d846773d8235f00617b9564a7dd61be742428cc'
String GLStatus = 'Produce'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '383e4fe13ccf4074815e348113e0b70b67173b756e2fe7e812a94f100c45a635'
String IsClient = '1'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'KELAS III'
String AppropriateRBRate = '190000'
String TreatmentEnd = ''
String AppropriateRBClassChoosen = 'KELAS III'
String AppropriateRBRateChoosen = '190000'
String TreatmentRoomChoosen = 'KELAS III'
String TreatmentRoomAmountChoosen = '190000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = '2021-04-13 13:48:00'
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '2'
String UserPosition = 'Customer Service'
String OPNO = '106186'
String MaternityMedicalTreatment = '[["A38",17,0,null,"100000","Hernioplasty","Scarlet fever","Covered",0,"","","","",1,"1","TTO","13/04/2021 14:54","",0,null,"0","0",0,null,0,"tsg","Coverage : Dijaminkan\n\nReason : \n -\n\nDokumen yang perlu dilengkapi  : \n - \n",null,"Tindakan Medis/Terapi"]]'
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String IsODS = '0'
String IsODC = '0'
String IsProducttypeChange = '0'
String IsTreatmentPeriodChange = '0'
String IsMaternityTreatmentChange = '0'
String IsRoomOptionChange = '0'
String IsTreatmentRBClassChange = '0'
String IsODSODCChange = '0'
String IsDocValidityChange = '0'
String IsDocTypeChange = '0'
String suspectDouble = '0'
String MedicalTreatmentConfirmation = '[{"ConfirmationType":"AM","PICDesc":"Account Manager","PICName":"AIB","NameDesc":"Arrio Probo Wibowo","ConfirmationDate":"13/04/2021 14:55","Confirmation":"COV","ConfirmationDesc":"Covered","Remarks":"","FollowUpTime":"60","Channel":"CALL","BenefitCoverage":"BC001","Reason":"","FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","NeedFU":0,"IsCito":0,"IsPrevious":0,"GuidConfirmation":"accdfca7-37e8-4a8d-b836-9ec7e8b40fb6"},{"ConfirmationType":"Doctor","PICDesc":"Doctor","PICName":"IEA","NameDesc":"Irna Putri Perdana","ConfirmationDate":"13/04/2021 14:56","Confirmation":"HLD","ConfirmationDesc":"Hold","Remarks":"","FollowUpTime":"60","Channel":"CALL","BenefitCoverage":"","Reason":"FUR005","FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","NeedFU":1,"IsCito":0,"IsPrevious":0,"GuidConfirmation":"62ce8265-f57d-45e8-9c5a-1cd685f9a10f"}]'
String IsProtapCovid = '0'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/GA_FinalValidationAPTTOInTreatment', [('authorization') : GlobalVariable.authorization     	      
 , ('AdminName') : AdminName , ('IsGLProvider') : IsGLProvider , ('MemberNo') : MemberNo , ('PreviousTrID') : PreviousTrID
 , ('TreatmentDate') : TreatmentDate , ('ProductType') : ProductType , ('DiagnosisCode') : DiagnosisCode , ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
 , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom
 , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability
 , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax
 , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('DefaultProviderID') : DefaultProviderID
 , ('Remarks') : Remarks , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType
 , ('AccountManager') : AccountManager , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo
 , ('MembershipType') : MembershipType , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender
 , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('PreviousGuid') : PreviousGuid , ('GLStatus') : GLStatus
 , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('IsClient') : IsClient
 , ('CallStatusID') : CallStatusID , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd
 , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
 , ('IsReferral') : IsReferral , ('IsSpecialCondition') : IsSpecialCondition , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart
 , ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('ClientClassNo') : ClientClassNo , ('UserPosition') : UserPosition , ('OPNO') : OPNO
 , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('TreatmentCode') : TreatmentCode , ('IsFromProcessButton') : IsFromProcessButton
 , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('IsODS') : IsODS
 , ('IsODC') : IsODC , ('IsProducttypeChange') : IsProducttypeChange , ('IsTreatmentPeriodChange') : IsTreatmentPeriodChange , ('IsMaternityTreatmentChange') : IsMaternityTreatmentChange
 , ('IsRoomOptionChange') : IsRoomOptionChange , ('IsTreatmentRBClassChange') : IsTreatmentRBClassChange , ('IsODSODCChange') : IsODSODCChange , ('IsDocValidityChange') : IsDocValidityChange
 , ('IsDocTypeChange') : IsDocTypeChange , ('suspectDouble') : suspectDouble , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation
 , ('IsProtapCovid') : IsProtapCovid ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)
//API.Note(API.getResponseData(var).Data.ClaimValidationMessage)
//API.Note(API.getResponseData(var).Data.ClaimValidationNonCoverReason)
//API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
//API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)


//Cek Hasil Validasi Diagnosa

def AllValidation=API.getResponseData(var).Data.ClaimValidationResult
def DetailValidation = API.getResponseData(var).Data.ClaimValidationFinalResult

if (AllValidation=="NONCOVER"){
	if (DetailValidation.contains('Medical Treatment memerlukan konfirmasi Garda Medika')) {
		KeywordUtil.markPassed('Medical Treatment memerlukan konfirmasi Garda Medika')
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
	} else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
	}
} else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
}


/*
	} else {
		KeywordUtil.markFailed('NON COVER FAILED')
	}
} else {
	KeywordUtil.markFailed('NON COVER IS NULL')
}
*/

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}