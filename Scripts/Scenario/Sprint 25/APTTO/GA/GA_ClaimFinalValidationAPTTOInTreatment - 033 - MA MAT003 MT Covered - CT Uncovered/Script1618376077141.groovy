import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String AdminName = ''
String IsGLProvider = 'False'
String MemberNo = 'L/00027859'
String PreviousTrID = '2874760'
String TreatmentDate = '2021-04-14'
String ProductType = 'MA'
String DiagnosisCode = 'O82'
String DiagnosisAdditionalInfo = '[{"QuestionID":"100","QuestionDescription":"BSC 2x","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]'
String ProviderID = 'TJKRP0146'
String PatientPhone = '081808623854'
String CallerName = 'Maste'
String TreatmentRoom = 'KELAS II'
String TreatmentRoomAmount = '400000'
String DoctorName = 'tsg'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '0'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = '197'
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String Guid = 'f9b0e5bd1cab41c6b1877c9e0c335768ce4b9956fc8745a8eed0ec2e76bc6133'
String TicketNo = 'CL209534'
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = ''
String ClientID = 'C01AA00001'
String ClassNo = '1'
String MembershipType = '2. SPO'
String AllDiagnosis = '[[1,"O82","SINGLE DELIVERY BY CAESAREAN SECTION","Initial Primary","Covered","","","0","0","0","0","","Covered","","","",null,"Question",0,"0","0"]]'
String AllDiagnosisAdditionalInfo = '[[{"QuestionID":"100","QuestionDescription":"BSC 2x","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocument":"","AdditionalDocDesc":""}]]'
String AllDoctors = 'tsg'
String Gender = 'F'
String DOB = '1996-02-16'
String NewMemberName = ''
String PreviousGuid = 'f9b0e5bd1cab41c6b1877c9e0c335768ce4b9956fc8745a8eed0ec2e76bc6133'
String GLStatus = 'Produce'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '84e879c0e32f48a18d7e31e2d2117c11bb62ced320511d6479ddf4420cc61c9c'
String IsClient = '1'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'KELAS II'
String AppropriateRBRate = '400000'
String TreatmentEnd = ''
String AppropriateRBClassChoosen = 'KELAS II'
String AppropriateRBRateChoosen = '400000'
String TreatmentRoomChoosen = 'KELAS II'
String TreatmentRoomAmountChoosen = '400000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = '2021-04-14 08:7:00'
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '2'
String UserPosition = 'Customer Service'
String OPNO = '106186'
String MaternityMedicalTreatment = '[["O82",16,0,null,"100000","Fistulectomy","SINGLE DELIVERY BY CAESAREAN SECTION","Covered",0,"","","","",1,"1","TTO","14/04/2021 14:21","",0,null,"0","0",0,null,0,"tsg","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi  : \n - \n",null,"Tindakan Medis/Terapi"]] '
String MaternityFamilyPlanningItem = ''
String TreatmentCode = 'MAT003'
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String IsODS = '0'
String IsODC = '0'
String IsProducttypeChange = '0'
String IsTreatmentPeriodChange = '0'
String IsMaternityTreatmentChange = '0'
String IsRoomOptionChange = '0'
String IsTreatmentRBClassChange = '0'
String IsODSODCChange = '0'
String IsDocValidityChange = '0'
String IsDocTypeChange = '0'
String suspectDouble = '0'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '0'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/GA_FinalValidationAPTTOInTreatment', [('authorization') : GlobalVariable.authorization     	      
 , ('AdminName') : AdminName , ('IsGLProvider') : IsGLProvider , ('MemberNo') : MemberNo , ('PreviousTrID') : PreviousTrID
 , ('TreatmentDate') : TreatmentDate , ('ProductType') : ProductType , ('DiagnosisCode') : DiagnosisCode , ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
 , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom
 , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability
 , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax
 , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('DefaultProviderID') : DefaultProviderID
 , ('Remarks') : Remarks , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType
 , ('AccountManager') : AccountManager , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo
 , ('MembershipType') : MembershipType , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender
 , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('PreviousGuid') : PreviousGuid , ('GLStatus') : GLStatus
 , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('IsClient') : IsClient
 , ('CallStatusID') : CallStatusID , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd
 , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
 , ('IsReferral') : IsReferral , ('IsSpecialCondition') : IsSpecialCondition , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart
 , ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('ClientClassNo') : ClientClassNo , ('UserPosition') : UserPosition , ('OPNO') : OPNO
 , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('TreatmentCode') : TreatmentCode , ('IsFromProcessButton') : IsFromProcessButton
 , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('IsODS') : IsODS
 , ('IsODC') : IsODC , ('IsProducttypeChange') : IsProducttypeChange , ('IsTreatmentPeriodChange') : IsTreatmentPeriodChange , ('IsMaternityTreatmentChange') : IsMaternityTreatmentChange
 , ('IsRoomOptionChange') : IsRoomOptionChange , ('IsTreatmentRBClassChange') : IsTreatmentRBClassChange , ('IsODSODCChange') : IsODSODCChange , ('IsDocValidityChange') : IsDocValidityChange
 , ('IsDocTypeChange') : IsDocTypeChange , ('suspectDouble') : suspectDouble , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation
 , ('IsProtapCovid') : IsProtapCovid ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)
//API.Note(API.getResponseData(var).Data.ClaimValidationMessage)
//API.Note(API.getResponseData(var).Data.ClaimValidationNonCoverReason)
//API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
//API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)


//Cek Hasil Validasi Diagnosa

def AllValidation=API.getResponseData(var).Data.ClaimValidationResult
def DetailValidation = API.getResponseData(var).Data.ClaimValidationFinalResult

if (AllValidation=="NONCOVER"){
	if (DetailValidation.contains('Medical Treatment memerlukan konfirmasi Dokter Garda Medika')) {
		KeywordUtil.markPassed('Medical Treatment memerlukan konfirmasi Dokter Garda Medika')
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
	} else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
	}
} else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai')
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
	KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationFinalResult)
}


/*
	} else {
		KeywordUtil.markFailed('NON COVER FAILED')
	}
} else {
	KeywordUtil.markFailed('NON COVER IS NULL')
}
*/

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}