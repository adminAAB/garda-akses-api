import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Login
String username = GlobalVariable.username

String password = GlobalVariable.password

//CreateTicket
String contactLine = 'Provider'

String product = 'Health'

String providerName = 'OJKSH00001 - SILOAM HOSPITALS KEBON JERUK'

//String contactName = 'Automation API Test 0' //Soon ambil dari Data Files
String contactName = findTestData('contactName').getValue(1, 1)

String channelType = 'CALL'

String phoneNumber = '081806221986'

String email = 'ehu@beyond.asuransi.astra.co.id'

String contactType = 'ADMK'

String serviceType = 'CLM'

String ext = ''

String fax = '0215300876'

//GL Inquiry

//Claim GL


//Script
WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'), [('username') : username, ('password') : password])

WebUI.callTestCase(findTestCase('Pages/GA/CreateTicket/CreateTicket - Provider Health Claim'), [('username') : username, ('contactLine') : contactLine
        , ('product') : product, ('providerName') : providerName, ('contactName') : contactName, ('channelType') : channelType
        , ('phoneNumber') : phoneNumber, ('email') : email, ('contactType') : contactType, ('serviceType') : serviceType
        , ('ext') : ext, ('fax') : fax])

WebUI.callTestCase(findTestCase('Pages/GA/CreateTreatmentGL/CreateTreatmentGL - GLInquiry'), [('username') : username])

CustomKeywords.'com.keyword.Query.QueryContactName'()

