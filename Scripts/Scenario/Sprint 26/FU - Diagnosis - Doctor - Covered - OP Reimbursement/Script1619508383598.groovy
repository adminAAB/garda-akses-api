import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

//Login
String username = 'DNP'

String password = 'ITG@nt1P455QC'

//
String ServiceType = 'PRI'

String MemberNo = 'B/00017507'

String MemberName = 'BUNGA ASRI KENCANA'

String ClientID = 'CJKPP0009'

String ProviderID = 'TJKRP0146'

String PreviousTrID = '2744046'

String GLType = 'GL Awal'

String StartFollowUp = '2020-11-17 15:56:47'

String Guid = '5edfafca1a324db0bdb8a833d63481110595b500e630f3ea2b359de4b3edae92'

String ConfirmationItem = '[{"ConfirmationType":"Doctor","PICDesc":"Doctor","PICName":"IEA","NameDesc":"Irna Putri Perdana","ConfirmationDate":"18/11/2020 13:47","Confirmation":"COV","ConfirmationDesc":"Covered","Remarks":"","FollowUpTime":"0","Channel":"WA","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"BC007","Reason":"","ConfirmationCategory":"Diagnosa","DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","IsFromTicketBefore":0}]'

String diagnosisConfirmationItem = '[{"DiagnosisID":"B20","Diagnosis":"HIV DISEASE RESULTING IN INFECTIOUS AND PARASITIC DISEASE","DiagnosisPayer":"Employee","DiagnosisCategory":"HIV, AIDS, ARDS","DiagnosisCoverage":"Covered"}]'

String FamilyPlanningItemConfirmation = '[]'

String email = 'ehu@beyond.asuransi.astra.co.id'

String hp = '081808623854'

String providerconfirmationitem = '[]'

String IsFUWaved = '0'

String WavedReasonCode = ''

String ProductType = 'IP'

String NonMedicalItem = '[]'

String claimdetails = '[]'

String ReferenceClaimLimitRecovery = ''

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'), [('username') : username, ('password') : password])

def var = WS.sendRequest(findTestObject('GA/CCOOutbound/FollowUpTicketClaim',[('authorization'):GlobalVariable.authorization,
	('ServiceType'):ServiceType,('MemberNo'):MemberNo,('MemberName'):MemberName,('ClientID'):ClientID,('ProviderID'):ProviderID,
	('PreviousTrID'):PreviousTrID,('GLType'):GLType,('StartFollowUp'):StartFollowUp,('Guid'):Guid,
	('ConfirmationItem'):ConfirmationItem,('diagnosisConfirmationItem'):diagnosisConfirmationItem,
	('FamilyPlanningItemConfirmation'):FamilyPlanningItemConfirmation,('email'):email,('hp'):hp,
	('providerconfirmationitem'):providerconfirmationitem,	('IsFUWaved'):IsFUWaved,('WavedReasonCode'):WavedReasonCode,
	('ProductType'):ProductType,('NonMedicalItem'):NonMedicalItem,('claimdetails'):claimdetails,
	('ReferenceClaimLimitRecovery'):ReferenceClaimLimitRecovery]))

API.Note(API.getResponseData(var).data)
//API.Note(API.getResponseData(var).data.MessagePopUp)
//API.Note(API.getResponseData(var).data.FollowUpStatus)

