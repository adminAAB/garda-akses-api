import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'R/00103576'
String TreatmentDate = '2021-05-04'
String DiagnosisCode = 'O80'
String ProductType = 'MA'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'TJKRP0146'
String PatientPhone = '081808623854'
String CallerName = ''
String TreatmentRoom = 'ISOLASI'
String TreatmentRoomAmount = '750000'
String DoctorName = 'tsg'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '350000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = '197'
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'PRI'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKRP0146'
String Guid = '661508305f804059883926f53d92e8c7603366aeb8f642732163192d4e5d0feb'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String PreviousGuid = '949fecf1b30349e98302bc8f22a59c5c2752e7c347c3da9795814ddfb00aded4'
String PreviousTrID = '2892475'
String TotalBilled = '0'
String ClientID = 'CJKPA0045'
String ClassNo = '1'
String Membership = '2. SPO'
String AllDiagnosis = '[[2,"O80","SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY","Initial Primary","","","","0","0","","0","","Covered","","","","","Covered",0,"1","1"]]'
String AllDoctors = 'tsg'
String Gender = 'F'
String DOB = '1994-10-26'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '661508305f804059883926f53d92e8c7603366aeb8f642732163192d4e5d0feb'
String TreatmentRoomChoosen = 'ISOLASI'
String AppropriateRBClassChoosen = 'ISOLASI'
String AppropriateRBRateChoosen = '750000'
String TreatmentRoomAmountChoosen = '750000'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'ISOLASI'
String AppropriateRBRate = '750000'
String TreatmentEnd = ''
String IsSpecialCondition = '0'
String ClientClassNo = '1'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = 'MAT001'
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = '[["O80",322,0,"","0","","SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY","Covered",0,"","","","",1,"1","OBT","04/05/2021 14:12","",0,"1","1","1",0,null,0,"tsg","Coverage : Dijaminkan\n\nReason : \n -\n\nDokumen yang perlu dilengkapi  : \n - \n","tsg","Obat","1"]]'
String Source = 'heo'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '0'
String OPNO = '105330'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '1'



WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CCOOutbound/ProcessGL', [('authorization') : GlobalVariable.authorization
, ('MemberNo') : MemberNo	, ('TreatmentDate') : TreatmentDate	, ('DiagnosisCode') : DiagnosisCode	, ('ProductType') : ProductType	, ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo
, ('ProviderID') : ProviderID	, ('PatientPhone') : PatientPhone	, ('CallerName') : CallerName	, ('TreatmentRoom') : TreatmentRoom	, ('TreatmentRoomAmount') : TreatmentRoomAmount
, ('DoctorName') : DoctorName	, ('RoomOption') : RoomOption	, ('RoomAvailability') : RoomAvailability	, ('UpgradeClass') : UpgradeClass	, ('BenefitAmount') : BenefitAmount
, ('ProviderEmail') : ProviderEmail	, ('ProviderFax') : ProviderFax	, ('ProviderPhone') : ProviderPhone	, ('ProviderExt') : ProviderExt	, ('IsTiri') : IsTiri
, ('UserPosition') : UserPosition	, ('Remarks') : Remarks	, ('serviceTypeID') : serviceTypeID	, ('CallStatusID') : CallStatusID	, ('DefaultProviderID') : DefaultProviderID
, ('Guid') : Guid	, ('TicketNo') : TicketNo	, ('GLType') : GLType	, ('AccountManager') : AccountManager	, ('PreviousGuid') : PreviousGuid
, ('PreviousTrID') : PreviousTrID	, ('TotalBilled') : TotalBilled	, ('ClientID') : ClientID	, ('ClassNo') : ClassNo	, ('Membership') : Membership
, ('AllDiagnosis') : AllDiagnosis	, ('AllDoctors') : AllDoctors	, ('Gender') : Gender	, ('DOB') : DOB	, ('NewMemberName') : NewMemberName
, ('GLStatus') : GLStatus	, ('EmpMemberNo') : EmpMemberNo	, ('NMEmpID') : NMEmpID	, ('FollowUpTaskID') : FollowUpTaskID	, ('TreatmentRoomChoosen') : TreatmentRoomChoosen
, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen	, ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen	, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
, ('IsReferral') : IsReferral	, ('ReferralReasonCode') : ReferralReasonCode	, ('CallInStart') : CallInStart	, ('IsClient') : IsClient	, ('AppropriateRBClass') : AppropriateRBClass
, ('AppropriateRBRate') : AppropriateRBRate	, ('TreatmentEnd') : TreatmentEnd	, ('IsSpecialCondition') : IsSpecialCondition	, ('ClientClassNo') : ClientClassNo
, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode	, ('TreatmentCode') : TreatmentCode	, ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem
, ('MaternityMedicalTreatment') : MaternityMedicalTreatment	, ('Source') : Source	, ('NonMedicalItem') : NonMedicalItem	, ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered
, ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion	, ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo	, ('IsODS') : IsODS	, ('IsODC') : IsODC
, ('OPNO') : OPNO, ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation, ('IsProtapCovid') : IsProtapCovid ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}

def AllValidation=API.getResponseData(var).Data.MAMedicalTreatmentValidationF
if (AllValidation!=null){
	if (AllValidation==false){
		KeywordUtil.markPassed('Medical Treatment Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.MAMedicalTreatmentValidationF)
	} else {
		KeywordUtil.markFailed('Medical Treatment Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.MAMedicalTreatmentValidationF)
	}
} else {
	KeywordUtil.markFailed('Confirmation is NULL')
}


