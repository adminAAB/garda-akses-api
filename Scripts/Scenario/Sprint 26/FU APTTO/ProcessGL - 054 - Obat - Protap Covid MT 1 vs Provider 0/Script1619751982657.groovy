import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'A/00213639'
String TreatmentDate = '2021-05-03'
String DiagnosisCode = 'S23'
String ProductType = 'IP'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'TJKRH00005'
String PatientPhone = '081808623854'
String CallerName = ''
String TreatmentRoom = 'KELAS I'
String TreatmentRoomAmount = '650000'
String DoctorName = 'tsg'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '550000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '0215529495'
String ProviderPhone = '081806221986'
String ProviderExt = ''
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'PRI'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKRH00005'
String Guid = '52de3004479c45dcae368d80e9a505f567a289fc85f8c020f8df7aac66618c58'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String PreviousGuid = '54183a412482491798d40f4105c2184c66e138adf74f8da5a8572087b74c7cfd'
String PreviousTrID = '2892419'
String TotalBilled = '0'
String ClientID = 'CJKPA01'
String ClassNo = '4'
String Membership = '2. SPO'
String AllDiagnosis = '[[1,"S23","Dislocation, sprain and strain of joints and ligaments of thorax","Initial Primary","","","","","","","","","Uncovered","","","","","",0,"1","0"]]'
String AllDoctors = 'tsg'
String Gender = 'F'
String DOB = '1992-04-29'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '52de3004479c45dcae368d80e9a505f567a289fc85f8c020f8df7aac66618c58'
String TreatmentRoomChoosen = 'KELAS I'
String AppropriateRBClassChoosen = 'KELAS I'
String AppropriateRBRateChoosen = '650000'
String TreatmentRoomAmountChoosen = '650000'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'KELAS I'
String AppropriateRBRate = '650000'
String TreatmentEnd = ''
String IsSpecialCondition = '0'
String ClientClassNo = '4'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = ''
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = '[["S23",376,0,null,"0","","Dislocation, sprain and strain of joints and ligaments of thorax","Covered",0,"","","","",1,"1","OBT","03/05/2021 10:04","",0,"4","4","16",0,null,0,"tsg","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi Garda Medika terkait tindakan PROTAP COVID\n\nDokumen yang perlu dilengkapi  : \n - \n","tsg","Obat","0"]]'
String Source = 'heo'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '0'
String OPNO = '105377'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '0'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CCOOutbound/ProcessGL', [('authorization') : GlobalVariable.authorization     	      
, ('MemberNo') : MemberNo	, ('TreatmentDate') : TreatmentDate	, ('DiagnosisCode') : DiagnosisCode	, ('ProductType') : ProductType	, ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo
, ('ProviderID') : ProviderID	, ('PatientPhone') : PatientPhone	, ('CallerName') : CallerName	, ('TreatmentRoom') : TreatmentRoom	, ('TreatmentRoomAmount') : TreatmentRoomAmount	
, ('DoctorName') : DoctorName	, ('RoomOption') : RoomOption	, ('RoomAvailability') : RoomAvailability	, ('UpgradeClass') : UpgradeClass	, ('BenefitAmount') : BenefitAmount	
, ('ProviderEmail') : ProviderEmail	, ('ProviderFax') : ProviderFax	, ('ProviderPhone') : ProviderPhone	, ('ProviderExt') : ProviderExt	, ('IsTiri') : IsTiri	
, ('UserPosition') : UserPosition	, ('Remarks') : Remarks	, ('serviceTypeID') : serviceTypeID	, ('CallStatusID') : CallStatusID	, ('DefaultProviderID') : DefaultProviderID	
, ('Guid') : Guid	, ('TicketNo') : TicketNo	, ('GLType') : GLType	, ('AccountManager') : AccountManager	, ('PreviousGuid') : PreviousGuid	
, ('PreviousTrID') : PreviousTrID	, ('TotalBilled') : TotalBilled	, ('ClientID') : ClientID	, ('ClassNo') : ClassNo	, ('Membership') : Membership	
, ('AllDiagnosis') : AllDiagnosis	, ('AllDoctors') : AllDoctors	, ('Gender') : Gender	, ('DOB') : DOB	, ('NewMemberName') : NewMemberName	
, ('GLStatus') : GLStatus	, ('EmpMemberNo') : EmpMemberNo	, ('NMEmpID') : NMEmpID	, ('FollowUpTaskID') : FollowUpTaskID	, ('TreatmentRoomChoosen') : TreatmentRoomChoosen	
, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen	, ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen	, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen	
, ('IsReferral') : IsReferral	, ('ReferralReasonCode') : ReferralReasonCode	, ('CallInStart') : CallInStart	, ('IsClient') : IsClient	, ('AppropriateRBClass') : AppropriateRBClass	
, ('AppropriateRBRate') : AppropriateRBRate	, ('TreatmentEnd') : TreatmentEnd	, ('IsSpecialCondition') : IsSpecialCondition	, ('ClientClassNo') : ClientClassNo	
, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode	, ('TreatmentCode') : TreatmentCode	, ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem	
, ('MaternityMedicalTreatment') : MaternityMedicalTreatment	, ('Source') : Source	, ('NonMedicalItem') : NonMedicalItem	, ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered	
, ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion	, ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo	, ('IsODS') : IsODS	, ('IsODC') : IsODC	
, ('OPNO') : OPNO, ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation, ('IsProtapCovid') : IsProtapCovid ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}

def AllValidation=API.getResponseData(var).Data.MAMedicalTreatmentValidationF
if (AllValidation!=null){
	if (AllValidation==true){
		KeywordUtil.markPassed('Medical Treatment Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.MAMedicalTreatmentValidationF)
	} else {
		KeywordUtil.markFailed('Medical Treatment Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.MAMedicalTreatmentValidationF)
	}
} else {
	KeywordUtil.markFailed('Confirmation is NULL')
}


