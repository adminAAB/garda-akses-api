import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'A/00132755'
String PreviousTrID = '2892270'
String TreatmentDate = '2021-04-28'
String ProductType = 'IP'
String DiagnosisCode = 'F00'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'TJKRP0146'
String PatientPhone = '081808623854'
String CallerName = '-'
String TreatmentRoom = 'KELAS I'
String TreatmentRoomAmount = '650000'
String DoctorName = 'tsg'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '350000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = '197'
String IsTiri = '0'
String DefaultProviderID = 'TJKRP0146'
String Remarks = ''
String ServiceTypeID = 'PRI'
String Guid = '51b270c989704ab0bfb31098a4f7682afdc0fe2cc76278420790191380b101f1'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'CJKPA0045'
String ClassNo = '1'
String MembershipType = '1. EMP'
String AllDiagnosis = '[[1,"F00","DEMENTIA IN ALZHEIMERS DISEASE","Initial Primary","","","","","","","","","Uncovered","","","","","",0,"1","0"]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String AllDoctors = 'tsg'
String Gender = 'F'
String DOB = '1996-03-16'
String NewMemberName = ''
String PreviousGuid = '11658db2d6f84026bcc0c764828fc0679e8b973cd9189638bf204b3a0bd7586f'
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '11658db2d6f84026bcc0c764828fc0679e8b973cd9189638bf204b3a0bd7586f'
String IsClient = '1'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'KELAS I'
String AppropriateRBRate = '650000'
String TreatmentEnd = ''
String AppropriateRBClassChoosen = 'KELAS I'
String AppropriateRBRateChoosen = '650000'
String TreatmentRoomChoosen = 'KELAS I'
String TreatmentRoomAmountChoosen = '650000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '1'
String UserPosition = ''
String PopUpMsg = ''
String FUStatus = ''
String OPNO = '105330'
String TrIDCallOut = '0'
String BenefitCoverage = ''
String IsDiagnosisNeedNextValidation = '1'
String IsAnnualLimitNeedNextValidation = '1'
String IsNewMemberNeedNextValidation = '1'
String IsExcessNeedNextValidation = '1'
String IsDocumentNeedNextValidation = '1'
String MaternityMedicalTreatment = '[["F00",322,0,"","0","","DEMENTIA IN ALZHEIMERS DISEASE","Covered",0,"","","","",1,"1","OBT","28/04/2021 16:01","",0,"1","1","1",0,null,0,"tsg","Coverage : Dijaminkan\n\nReason : \n -\n\nDokumen yang perlu dilengkapi  : \n - \n","Obat TSG 01","Obat","1"]]'
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String IsEligibilityNeedNextValidation = '1'
String IsMedicalTreatmentNeedNextValidation = '1'
String IsFamilyPlanningNeedNextValidation = '1'
String IsProductNeedNextValidation = '1'
String Source = 'heo'
String IsExcessCalculation = '0'
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String IsGLProvider = 'False'
String IsODS = '0'
String IsODC = '0'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '1'
String MedicalTreatmentResultMsg = ''

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CCOOutbound/GA_ClaimFinalValidation', [('authorization') : GlobalVariable.authorization     	      
,('MemberNo'):MemberNo	,('PreviousTrID'):PreviousTrID	,('TreatmentDate'):TreatmentDate	,('ProductType'):ProductType	,('DiagnosisCode'):DiagnosisCode
,('DiagnosisAdditionalInfo'):DiagnosisAdditionalInfo	,('ProviderID'):ProviderID	,('PatientPhone'):PatientPhone	,('CallerName'):CallerName	,('TreatmentRoom'):TreatmentRoom
,('TreatmentRoomAmount'):TreatmentRoomAmount	,('DoctorName'):DoctorName	,('RoomOption'):RoomOption	,('RoomAvailability'):RoomAvailability	,('UpgradeClass'):UpgradeClass
,('BenefitAmount'):BenefitAmount	,('ProviderEmail'):ProviderEmail	,('ProviderFax'):ProviderFax	,('ProviderPhone'):ProviderPhone	,('ProviderExt'):ProviderExt
,('IsTiri'):IsTiri	,('DefaultProviderID'):DefaultProviderID	,('Remarks'):Remarks	,('ServiceTypeID'):ServiceTypeID	,('Guid'):Guid
,('TicketNo'):TicketNo	,('GLType'):GLType	,('AccountManager'):AccountManager	,('TotalBilled'):TotalBilled	,('ClientID'):ClientID
,('ClassNo'):ClassNo	,('MembershipType'):MembershipType	,('AllDiagnosis'):AllDiagnosis	,('AllDiagnosisAdditionalInfo'):AllDiagnosisAdditionalInfo	,('AllDoctors'):AllDoctors
,('Gender'):Gender	,('DOB'):DOB	,('NewMemberName'):NewMemberName	,('PreviousGuid'):PreviousGuid	,('GLStatus'):GLStatus
,('EmpMemberNo'):EmpMemberNo	,('NMEmpID'):NMEmpID	,('FollowUpTaskID'):FollowUpTaskID	,('IsClient'):IsClient	,('CallStatusID'):CallStatusID
,('AppropriateRBClass'):AppropriateRBClass	,('AppropriateRBRate'):AppropriateRBRate	,('TreatmentEnd'):TreatmentEnd	,('AppropriateRBClassChoosen'):AppropriateRBClassChoosen	,('AppropriateRBRateChoosen'):AppropriateRBRateChoosen
,('TreatmentRoomChoosen'):TreatmentRoomChoosen	,('TreatmentRoomAmountChoosen'):TreatmentRoomAmountChoosen	,('IsReferral'):IsReferral	,('IsSpecialCondition'):IsSpecialCondition	,('ReferralReasonCode'):ReferralReasonCode
,('CallInStart'):CallInStart	,('SecondaryDiagnosisCode'):SecondaryDiagnosisCode	,('ClientClassNo'):ClientClassNo	,('UserPosition'):UserPosition	,('PopUpMsg'):PopUpMsg
,('FUStatus'):FUStatus	,('OPNO'):OPNO	,('TrIDCallOut'):TrIDCallOut	,('BenefitCoverage'):BenefitCoverage	,('IsDiagnosisNeedNextValidation'):IsDiagnosisNeedNextValidation
,('IsAnnualLimitNeedNextValidation'):IsAnnualLimitNeedNextValidation	,('IsNewMemberNeedNextValidation'):IsNewMemberNeedNextValidation	,('IsExcessNeedNextValidation'):IsExcessNeedNextValidation	,('IsDocumentNeedNextValidation'):IsDocumentNeedNextValidation	,('MaternityMedicalTreatment'):MaternityMedicalTreatment
,('MaternityFamilyPlanningItem'):MaternityFamilyPlanningItem	,('TreatmentCode'):TreatmentCode	,('IsEligibilityNeedNextValidation'):IsEligibilityNeedNextValidation	,('IsMedicalTreatmentNeedNextValidation'):IsMedicalTreatmentNeedNextValidation	,('IsFamilyPlanningNeedNextValidation'):IsFamilyPlanningNeedNextValidation
,('IsProductNeedNextValidation'):IsProductNeedNextValidation	,('Source'):Source	,('IsExcessCalculation'):IsExcessCalculation	,('IsFromProcessButton'):IsFromProcessButton	,('NonMedicalItem'):NonMedicalItem
,('IsDiagnosisQuestionNotRegistered'):IsDiagnosisQuestionNotRegistered	,('MedicalTreatmentAdditionalQuestion'):MedicalTreatmentAdditionalQuestion	,('IsGLProvider'):IsGLProvider	,('IsODS'):IsODS	,('IsODC'):IsODC
,('MedicalTreatmentConfirmation'):MedicalTreatmentConfirmation	,('IsProtapCovid'):IsProtapCovid	,('MedicalTreatmentResultMsg'):MedicalTreatmentResultMsg ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailed("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}

def AllValidation=API.getResponseData(var).Data.ClaimValidationResult
def AllDiagnosisFU=API.getResponseData(var).Data.ClaimCallInStatus

if (AllValidation!=null){
	if (AllValidation=='COVER'){
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
		KeywordUtil.markPassed('Covered')
		if (AllDiagnosisFU=='Not Need Follow Up'){
			KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
			KeywordUtil.markPassed('Produce')
		} else {
			KeywordUtil.markFailed(API.getResponseData(var).Data.ClaimCallInStatus)
			KeywordUtil.markFailed('Not Produce Need Follow Up')
		}
	} else {
		KeywordUtil.markFailed(API.getResponseData(var).Data.ClaimValidationResult)
		KeywordUtil.markFailed('Non Covered')
	}
} else {
	KeywordUtil.markFailed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markFailed('NULL')
}

