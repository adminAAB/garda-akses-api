import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String MemberNo = 'S/00137514'
String TreatmentDate = '2021-04-30'
String DiagnosisCode = 'F00'
String ProductType = 'IP'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'TJKRH00005'
String PatientPhone = '081808623854'
String CallerName = ''
String TreatmentRoom = 'KELAS II'
String TreatmentRoomAmount = '400000'
String DoctorName = 'tsg'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '350000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = ''
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'PRI'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKRH00005'
String Guid = '9529e61f87cb47fa90287dc69f33856a227791dcf2b7987b53f48cc4c7ea9672'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String PreviousGuid = 'f2482153ac60423e98d9140d0b2c5429d80becd37992a86b5281d67c0e25baee'
String PreviousTrID = '2892377'
String TotalBilled = '0'
String ClientID = 'CJKPA0045'
String ClassNo = '1'
String Membership = '2. SPO'
String AllDiagnosis = '[[1,"F00","DEMENTIA IN ALZHEIMERS DISEASE","Initial Primary","","","","","","","","","Uncovered","","","","","",0,"1","0"]]'
String AllDoctors = 'tsg'
String Gender = 'F'
String DOB = '1995-08-31'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '9529e61f87cb47fa90287dc69f33856a227791dcf2b7987b53f48cc4c7ea9672'
String TreatmentRoomChoosen = 'KELAS II'
String AppropriateRBClassChoosen = 'KELAS II'
String AppropriateRBRateChoosen = '400000'
String TreatmentRoomAmountChoosen = '400000'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'KELAS II'
String AppropriateRBRate = '400000'
String TreatmentEnd = ''
String IsSpecialCondition = '0'
String ClientClassNo = '1'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = ''
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = '[["F00",322,0,"","0","","DEMENTIA IN ALZHEIMERS DISEASE","Uncovered",0,"","","","",1,"1","OBT","30/04/2021 09:52","",0,"2","2","4",0,null,0,"tsg","Coverage : Dijaminkan\n\nReason : \n -\n\nDokumen yang perlu dilengkapi  : \n - \n","tsg","Obat","1"]]'
String Source = 'heo'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '0'
String OPNO = '105330'
String MedicalTreatmentConfirmation = '[{"ConfirmationType":"AM","PICDesc":"Account Manager","PICName":"AIB","NameDesc":"Arrio Probo Wibowo","ConfirmationDate":"03/05/2021 09:18","Confirmation":"HLD","ConfirmationDesc":"Hold","Remarks":"tsg","FollowUpTime":"60","Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"","Reason":"FUR020","ConfirmationCategory":null,"DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","IsFromTicketBefore":0,"IsEdit":"1","NeedFU":1,"IsCito":0,"IsPrevious":0,"GuidConfirmation":"5e1b2229-a711-414c-9ff4-88768829e171"}]'
String IsProtapCovid = '0'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CCOOutbound/ProcessGL', [('authorization') : GlobalVariable.authorization     	      
, ('MemberNo') : MemberNo	, ('TreatmentDate') : TreatmentDate	, ('DiagnosisCode') : DiagnosisCode	, ('ProductType') : ProductType	, ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo
, ('ProviderID') : ProviderID	, ('PatientPhone') : PatientPhone	, ('CallerName') : CallerName	, ('TreatmentRoom') : TreatmentRoom	, ('TreatmentRoomAmount') : TreatmentRoomAmount	
, ('DoctorName') : DoctorName	, ('RoomOption') : RoomOption	, ('RoomAvailability') : RoomAvailability	, ('UpgradeClass') : UpgradeClass	, ('BenefitAmount') : BenefitAmount	
, ('ProviderEmail') : ProviderEmail	, ('ProviderFax') : ProviderFax	, ('ProviderPhone') : ProviderPhone	, ('ProviderExt') : ProviderExt	, ('IsTiri') : IsTiri	
, ('UserPosition') : UserPosition	, ('Remarks') : Remarks	, ('serviceTypeID') : serviceTypeID	, ('CallStatusID') : CallStatusID	, ('DefaultProviderID') : DefaultProviderID	
, ('Guid') : Guid	, ('TicketNo') : TicketNo	, ('GLType') : GLType	, ('AccountManager') : AccountManager	, ('PreviousGuid') : PreviousGuid	
, ('PreviousTrID') : PreviousTrID	, ('TotalBilled') : TotalBilled	, ('ClientID') : ClientID	, ('ClassNo') : ClassNo	, ('Membership') : Membership	
, ('AllDiagnosis') : AllDiagnosis	, ('AllDoctors') : AllDoctors	, ('Gender') : Gender	, ('DOB') : DOB	, ('NewMemberName') : NewMemberName	
, ('GLStatus') : GLStatus	, ('EmpMemberNo') : EmpMemberNo	, ('NMEmpID') : NMEmpID	, ('FollowUpTaskID') : FollowUpTaskID	, ('TreatmentRoomChoosen') : TreatmentRoomChoosen	
, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen	, ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen	, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen	
, ('IsReferral') : IsReferral	, ('ReferralReasonCode') : ReferralReasonCode	, ('CallInStart') : CallInStart	, ('IsClient') : IsClient	, ('AppropriateRBClass') : AppropriateRBClass	
, ('AppropriateRBRate') : AppropriateRBRate	, ('TreatmentEnd') : TreatmentEnd	, ('IsSpecialCondition') : IsSpecialCondition	, ('ClientClassNo') : ClientClassNo	
, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode	, ('TreatmentCode') : TreatmentCode	, ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem	
, ('MaternityMedicalTreatment') : MaternityMedicalTreatment	, ('Source') : Source	, ('NonMedicalItem') : NonMedicalItem	, ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered	
, ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion	, ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo	, ('IsODS') : IsODS	, ('IsODC') : IsODC	
, ('OPNO') : OPNO, ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation, ('IsProtapCovid') : IsProtapCovid ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}

def AllValidation=API.getResponseData(var).Data.MAMedicalTreatmentValidationF
if (AllValidation!=null){
	if (AllValidation==true){
		KeywordUtil.markPassed('Medical Treatment Confirmation Muncul')
		API.Note(API.getResponseData(var).Data.MAMedicalTreatmentValidationF)
	} else {
		KeywordUtil.markFailed('Medical Treatment Confirmation Tidak Muncul')
		API.Note(API.getResponseData(var).Data.MAMedicalTreatmentValidationF)
	}
} else {
	KeywordUtil.markFailed('Confirmation is NULL')
}


