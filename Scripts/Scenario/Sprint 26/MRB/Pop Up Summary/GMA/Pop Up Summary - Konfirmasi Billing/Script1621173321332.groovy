import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String pIsGLProvider = '1'
String pIsODS = '0'
String pIsODC = '0'
String pIsProtapCovid = '0'
String pTreatmentDate = '2021-05-16'
String pMaternityTreatmentCode = ''
String pClientClassNo = '2'
String pMTAdditionalQuestion = '[[]]'
//String pAllMedicalTreatment = '[["A38",398,0,null,"500000","Terapi - Uncovered","Scarlet fever","Uncovered",0,"","","","",1,"1","TTO","16/05/2021 18:46","",0,null,"0","0",0,null,0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi : \n - \n",null,"Tindakan Medis/Terapi","0"]]'
String pAllMedicalTreatmentConfirmation = ''
String pMemberNo = 'A/00165126'
String pClientID = 'CJKFI02'


String Wording ='Medical Treatment memerlukan konfirmasi billing oleh Garda Medika'

String MaternityMedicalTreatment = '[["A38",375,0,null,"0","","Scarlet fever","Covered",0,"","","","",1,"1","OBT","15/04/2021 12:27","",0,"3","30000","1000001",0,null,0,"Test QC","Coverage : Dijaminkan\n\nReason : \n -\n\nDokumen yang perlu dilengkapi  : \n - \n","Obat sakit","Obat"]]'



String AdminName = ''
String IsGLProvider = 'True'
String MemberNo = 'A/00165126'
String PreviousTrID = '2897392'
String TreatmentDate = '2021-05-16'
String ProductType = 'IP'
String DiagnosisCode = 'A38'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'TJKRI47'
String PatientPhone = '087841632020'
String CallerName = '-'
String TreatmentRoom = 'KELAS I'
String TreatmentRoomAmount = '300000'
String DoctorName = 'Ugha Anugerah'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '450000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = ''
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String Guid = '6185856509b648c7814cadb64cbb1ac93837d210654035d88e10a5852fad5c06'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = ''
String ClientID = 'CJKFI02'
String ClassNo = '2'
String MembershipType = '1. EMP'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"0","0"]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String AllDoctors = 'Ugha Anugerah'
String Gender = 'F'
String DOB = '1985-01-19'
String NewMemberName = ''
String PreviousGuid = '6185856509b648c7814cadb64cbb1ac93837d210654035d88e10a5852fad5c06'
String GLStatus = 'Produce'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '241c581efec34f66955eba6b8a40abff7ca5efa8e9ea788c297364c8cac24549'
String IsClient = '1'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'KELAS I'
String AppropriateRBRate = '300000'
String TreatmentEnd = ''
String AppropriateRBClassChoosen = 'KELAS I'
String AppropriateRBRateChoosen = '300000'
String TreatmentRoomChoosen = 'KELAS I'
String TreatmentRoomAmountChoosen = '300000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '2'
String UserPosition = ''
String OPNO = '105966'
//String MaternityMedicalTreatment = '[["A38",398,0,null,"500000","Terapi - Uncovered","Scarlet fever","Uncovered",0,"","","","",1,"1","TTO","16/05/2021 18:46","",0,null,"0","0",0,null,0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi : \n - \n",null,"Tindakan Medis/Terapi","0"]]'
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String IsODS = '0'
String IsODC = '0'
String IsProducttypeChange = '0'
String IsTreatmentPeriodChange = '0'
String IsMaternityTreatmentChange = '0'
String IsRoomOptionChange = '0'
String IsTreatmentRBClassChange = '0'
String IsODSODCChange = '0'
String IsDocValidityChange = '0'
String IsDocTypeChange = '0'
String suspectDouble = '0'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '0'


String MaternityTreatmentCode = TreatmentCode
String MTAdditionalQuestion = MedicalTreatmentAdditionalQuestion
String pAllMedicalTreatment = MaternityMedicalTreatment
String AllMedicalTreatmentConfirmation = MedicalTreatmentConfirmation

WebUI.callTestCase(findTestCase('Pages/GMA/Login/Login'), [:])



def getResult = WS.sendRequest(findTestObject('GMA/CreateTreatmentGL/GetMedicalTreatmentValidationResult', [('IsGLProvider') : pIsGLProvider
			, ('IsODS') : pIsODS, ('IsODC') : pIsODC, ('IsProtapCovid') : pIsProtapCovid, ('TreatmentDate') : pTreatmentDate
			, ('MaternityTreatmentCode') : pMaternityTreatmentCode, ('ClientClassNo') : pClientClassNo, ('MTAdditionalQuestion') : pMTAdditionalQuestion
			, ('AllMedicalTreatment') : pAllMedicalTreatment, ('AllMedicalTreatmentConfirmation') : pAllMedicalTreatmentConfirmation
			, ('MemberNo') : pMemberNo, ('ClientID') : pClientID]))

if (API.getResponseData(getResult).status == true) {
	KeywordUtil.markPassed("Respon Didapatkan")
}else {
	KeywordUtil.markFailed("API ERROR")
	KeywordUtil.markFailed(API.getResponseData(getResult).data)
	KeywordUtil.markFailed(API.getResponseData(getResult).status)
}
String MedicalTreatmentResultMsg = API.getResponseData(getResult).data

def var = WS.sendRequest(findTestObject('Object Repository/GMA/CreateTreatmentGL/GA_FinalValidationAPTTOInTreatment', [('authorization') : GlobalVariable.authorization
            , ('AdminName') : AdminName, ('IsGLProvider') : IsGLProvider, ('MemberNo') : MemberNo, ('PreviousTrID') : PreviousTrID
            , ('TreatmentDate') : TreatmentDate, ('ProductType') : ProductType, ('DiagnosisCode') : DiagnosisCode, ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
            , ('ProviderID') : ProviderID, ('PatientPhone') : PatientPhone, ('CallerName') : CallerName, ('TreatmentRoom') : TreatmentRoom
            , ('TreatmentRoomAmount') : TreatmentRoomAmount, ('DoctorName') : DoctorName, ('RoomOption') : RoomOption, ('RoomAvailability') : RoomAvailability
            , ('UpgradeClass') : UpgradeClass, ('BenefitAmount') : BenefitAmount, ('ProviderEmail') : ProviderEmail, ('ProviderFax') : ProviderFax
            , ('ProviderPhone') : ProviderPhone, ('ProviderExt') : ProviderExt, ('IsTiri') : IsTiri, ('DefaultProviderID') : DefaultProviderID
            , ('Remarks') : Remarks, ('Guid') : Guid, ('TicketNo') : TicketNo, ('GLType') : GLType, ('AccountManager') : AccountManager
            , ('TotalBilled') : TotalBilled, ('ClientID') : ClientID, ('ClassNo') : ClassNo, ('MembershipType') : MembershipType
            , ('AllDiagnosis') : AllDiagnosis, ('AllDoctors') : AllDoctors, ('Gender') : Gender, ('DOB') : DOB, ('NewMemberName') : NewMemberName
            , ('PreviousGuid') : PreviousGuid, ('GLStatus') : GLStatus, ('EmpMemberNo') : EmpMemberNo, ('NMEmpID') : NMEmpID
            , ('FollowUpTaskID') : FollowUpTaskID, ('IsClient') : IsClient, ('CallStatusID') : CallStatusID, ('AppropriateRBClass') : AppropriateRBClass
            , ('AppropriateRBRate') : AppropriateRBRate, ('TreatmentEnd') : TreatmentEnd, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen
            , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen, ('TreatmentRoomChoosen') : TreatmentRoomChoosen, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
            , ('IsReferral') : IsReferral, ('IsSpecialCondition') : IsSpecialCondition, ('ReferralReasonCode') : ReferralReasonCode
            , ('CallInStart') : CallInStart, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode, ('ClientClassNo') : ClientClassNo
            , ('UserPosition') : UserPosition, ('OPNO') : OPNO, ('MaternityMedicalTreatment') : MaternityMedicalTreatment
            , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem, ('TreatmentCode') : TreatmentCode, ('IsFromProcessButton') : IsFromProcessButton
            , ('NonMedicalItem') : NonMedicalItem, ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered
            , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion, ('IsODS') : IsODS, ('IsODC') : IsODC
            , ('IsProducttypeChange') : IsProducttypeChange, ('IsTreatmentPeriodChange') : IsTreatmentPeriodChange, ('IsMaternityTreatmentChange') : IsMaternityTreatmentChange
            , ('IsRoomOptionChange') : IsRoomOptionChange, ('IsTreatmentRBClassChange') : IsTreatmentRBClassChange, ('IsODSODCChange') : IsODSODCChange
            , ('IsDocValidityChange') : IsDocValidityChange, ('IsDocTypeChange') : IsDocTypeChange, ('suspectDouble') : suspectDouble
            , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo, ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation
            , ('IsProtapCovid') : IsProtapCovid,('MedicalTreatmentResultMsg') : MedicalTreatmentResultMsg]))

API.Note(API.getResponseData(var).Status)

API.Note(API.getResponseData(var).Data)

def popUpInfo = API.getResponseData(var).Data.ClaimValidationFinalResult


if (popUpInfo.contains(Wording)) {
    KeywordUtil.markPassed('Informasi Validasi MT yang muncul:')

    KeywordUtil.markPassed(Wording)

    KeywordUtil.markPassed(popUpInfo)
	
} else {
    KeywordUtil.markFailed('Informasi Validasi MT yang diharapkan tidak muncul')

    KeywordUtil.markFailed(popUpInfo)
}

//Cek Status API
if (API.getResponseData(var).Status) {
    KeywordUtil.markPassed('API OK')
} else {
    KeywordUtil.markFailedAndStop('Terjadi kesalahan pada API! - ' + API.getResponseData(var).ErrorMessage)
}

