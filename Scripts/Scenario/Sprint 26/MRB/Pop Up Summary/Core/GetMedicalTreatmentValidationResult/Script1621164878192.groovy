import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'), [:])

String IsGLProvider = '0'
String IsODS = '0'
String IsODC = '0'
String IsProtapCovid = '0'
String TreatmentDate = '2021-05-16'
String MaternityTreatmentCode = ''
String ClientClassNo = '2'
String MTAdditionalQuestion = '[[]]'
String AllMedicalTreatment = '[["A38",398,0,null,"500000","Terapi - Uncovered","Scarlet fever","Uncovered",0,"","","","",1,"1","TTO","16/05/2021 18:46","",0,null,"0","0",0,null,0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi : \n - \n",null,"Tindakan Medis/Terapi","0"]]'
String AllMedicalTreatmentConfirmation = ''
String MemberNo = 'A/00165126'
String ClientID = 'CJKFI02'



def var = WS.sendRequest(findTestObject('GA/CreateTreatmentGL/GetMedicalTreatmentValidationResult', [('IsGLProvider') : IsGLProvider
			, ('IsODS') : IsODS, ('IsODC') : IsODC, ('IsProtapCovid') : IsProtapCovid, ('TreatmentDate') : TreatmentDate
			, ('MaternityTreatmentCode') : MaternityTreatmentCode, ('ClientClassNo') : ClientClassNo, ('MTAdditionalQuestion') : MTAdditionalQuestion
			, ('AllMedicalTreatment') : AllMedicalTreatment, ('AllMedicalTreatmentConfirmation') : AllMedicalTreatmentConfirmation
			, ('MemberNo') : MemberNo, ('ClientID') : ClientID]))

if (API.getResponseData(var).status == true) {
	KeywordUtil.markPassed("Respon Didapatkan")
	GlobalVariable.MedicalTreatmentResultMsg = API.getResponseData(var).data
}else {
	KeywordUtil.markFailed("API ERROR")
	KeywordUtil.markFailed(API.getResponseData(var).data)
	KeywordUtil.markFailed(API.getResponseData(var).status)
}

