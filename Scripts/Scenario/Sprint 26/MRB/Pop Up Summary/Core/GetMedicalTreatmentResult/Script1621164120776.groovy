import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API


String IsGLProvider = '1'

String IsODS = '0'

String IsODC = '0'

String IsProtapCovid = '1'

String TreatmentDate = '2021-05-16'

String MaternityTreatmentCode = ''

String ClientClassNo = '15'

String MTAdditionalQuestion = ''

String IsUnregistered = '1'

String DiagnosisID = 'A38'

String MedicalTreatmentID = ''

String Billed = '400000'

String Category = 'TTO'

String UnknownBilled = '0'

String UnknownPatientCondition = '0'

String UnregisteredMedicalTreatment = 'Unregister Medical Treatm'

String MedicalTreatmentDescription = ''

String MemberNo = 'A/00099220'

String ClientID = 'CJKPP0009'

String UnknownTotalBilled = '0'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def sendReq = WS.sendRequest(findTestObject('GMA/CreateTreatmentGL/GetMedicalTreatmentResult', [('IsGLProvider') : IsGLProvider , ('IsODS') : IsODS , ('IsODC') : IsODC , ('IsProtapCovid') : IsProtapCovid , ('TreatmentDate') : TreatmentDate , ('MaternityTreatmentCode') : MaternityTreatmentCode , ('ClientClassNo') : ClientClassNo , ('MTAdditionalQuestion') : MTAdditionalQuestion , ('IsUnregistered') : IsUnregistered , ('DiagnosisID') : DiagnosisID , ('MedicalTreatmentID') : MedicalTreatmentID , ('Billed') : Billed , ('Category') : Category , ('UnknownBilled') : UnknownBilled , ('UnknownPatientCondition') : UnknownPatientCondition , ('UnregisteredMedicalTreatment') : UnregisteredMedicalTreatment , ('MedicalTreatmentDescription') : MedicalTreatmentDescription , ('MemberNo') : MemberNo , ('ClientID') : ClientID , ('UnknownTotalBilled') : UnknownTotalBilled])) 

GlobalVariable.ParsedData = API.getResponseData(sendReq).data.Data

println(GlobalVariable.ParsedData)

