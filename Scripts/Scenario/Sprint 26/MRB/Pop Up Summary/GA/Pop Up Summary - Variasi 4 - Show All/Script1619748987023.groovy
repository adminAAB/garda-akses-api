import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String Wording1 = 'Tindakan hanya dijaminkan pada transaksi ODS'

String Wording2 = 'Medical Treatment memerlukan konfirmasi Garda Medika terkait Dokumen Tambahan'

String Wording3 = 'Medical Treatment memerlukan konfirmasi Garda Medika terkait tindakan PROTAP COVID'

String Wording4 = 'Medical Treatment memerlukan konfirmasi billing oleh Garda Medika'

String Wording5 = 'Medical Treatment memerlukan kelengkapan informasi Tindakan Medis/Terapi/Obat dari Rumah Sakit'

String Wording6 = 'Medical Treatment memerlukan konfirmasi Garda Medika'

String Wording7 = 'Medical Treatment memerlukan konfirmasi Dokter Garda Medika'


String AdminName = ''
String IsGLProvider = 'False'
String MemberNo = 'A/00099043'
String PreviousTrID = ''
String TreatmentDate = '2021-05-03'
String ProductType = 'IP'
String DiagnosisCode = 'A38'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'TJKLP0004'
String PatientPhone = '087841632020'
String CallerName = '-'
String TreatmentRoom = 'Tes'
String TreatmentRoomAmount = '30000'
String DoctorName = 'Ugha Anugerah'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '800000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '(0411) 457000'
String ProviderPhone = '081806221986'
String ProviderExt = ''
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String Guid = '4524a892606545cabc3953ccf09640366bc7a854b0f679163d74e7f23eb461d8'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'CJKPP0009'
String ClassNo = '14'
String MembershipType = '3. CHI'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"1","0"]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String AllDoctors = 'Ugha Anugerah'
String Gender = 'F'
String DOB = '2007-08-01'
String NewMemberName = ''
String PreviousGuid = ''
String GLStatus = ''
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '4524a892606545cabc3953ccf09640366bc7a854b0f679163d74e7f23eb461d8'
String IsClient = '1'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'Tes'
String AppropriateRBRate = '30000'
String TreatmentEnd = ''
String AppropriateRBClassChoosen = ''
String AppropriateRBRateChoosen = ''
String TreatmentRoomChoosen = ''
String TreatmentRoomAmountChoosen = ''
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '10'
String UserPosition = ''
String OPNO = '105323'
String MaternityMedicalTreatment = '[["A38",392,0,null,"0","","Scarlet fever","Covered",1,"Ada Info","Ini Juga Info","BILPA|BNDNG"," Billing Perawatan Asli, Billing Perbandingan",1,"1","OBT","03/05/2021 09:13","",0,"3","3000000","9000000",0,null,1,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Tindakan hanya dijaminkan pada transaksi ODS\n2. Memerlukan Konfirmasi Garda Medika terkait Total Billed\n3. Memerlukan konfirmasi Garda Medika terkait tindakan PROTAP COVID\n4. Memerlukan kelengkapan informasi Tindakan Medis/Terapi/Obat dari Rumah Sakit\n5. Memerlukan konfirmasi Garda Medika terkait Dokumen Tambahan\n\nDokumen yang perlu dilengkapi : \n1. Billing Perawatan Asli, Billing Perbandingan\n","Obat Sakit","Obat","0"],["A38",3,0,null,"50000","Caldwell Luc","Scarlet fever","Uncovered",0,"","","","",0,"1","TTO","03/05/2021 09:13","Minor",0,null,"0","0",0,null,0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi : \n - \n",null,"Tindakan Medis/Terapi","0"],["A38","",1,"Medical Treatment","50000","","Scarlet fever","",0,"","","","",0,"1","TTO","03/05/2021 09:14",null,0,null,"0","0",0,null,0,"Test QC Lagi","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi Garda Medika\n2. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi : \n - \n",null,"Tindakan Medis/Terapi","0"]]'
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[],[{"QuestionID":"328","QuestionDescription":"TSG Question True Uncovered","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocDesc":""}],[]]'
String IsODS = '0'
String IsODC = '0'
String IsProducttypeChange = '1'
String IsTreatmentPeriodChange = '1'
String IsMaternityTreatmentChange = '1'
String IsRoomOptionChange = '1'
String IsTreatmentRBClassChange = '1'
String IsODSODCChange = '1'
String IsDocValidityChange = '1'
String IsDocTypeChange = '1'
String suspectDouble = '0'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '0'
String MedicalTreatmentResultMsg = '- Tindakan hanya dijaminkan pada transaksi ODS<br> &ensp;Medical Treatment : Obat Sakit <br> &ensp;Coverage : Covered <br> &ensp;Billed : 9.000.000 <br> &ensp;Additional Info : Ada Info <br> &ensp;Additional Documents :  Billing Perawatan Asli, Billing Perbandingan <br> <br>- Medical Treatment memerlukan konfirmasi billing oleh Garda Medika<br> &ensp;Medical Treatment : Obat Sakit <br> &ensp;Coverage : Covered <br> &ensp;Billed : 9.000.000 <br> &ensp;Additional Info : Ada Info <br> &ensp;Additional Documents :  Billing Perawatan Asli, Billing Perbandingan <br> <br>- Medical Treatment memerlukan konfirmasi Garda Medika terkait tindakan PROTAP COVID<br> &ensp;Medical Treatment : Obat Sakit <br> &ensp;Coverage : Covered <br> &ensp;Billed : 9.000.000 <br> &ensp;Additional Info : Ada Info <br> &ensp;Additional Documents :  Billing Perawatan Asli, Billing Perbandingan <br> <br>- Medical Treatment memerlukan kelengkapan informasi Tindakan Medis/Terapi/Obat dari Rumah Sakit<br> &ensp;Medical Treatment : Obat Sakit <br> &ensp;Coverage : Covered <br> &ensp;Billed : 9.000.000 <br> &ensp;Additional Info : Ada Info <br> &ensp;Additional Documents :  Billing Perawatan Asli, Billing Perbandingan <br> <br>- Medical Treatment memerlukan konfirmasi Garda Medika terkait Dokumen Tambahan<br> &ensp;Medical Treatment : Obat Sakit <br> &ensp;Coverage : Covered <br> &ensp;Billed : 9.000.000 <br> &ensp;Additional Info : Ada Info <br> &ensp;Additional Documents :  Billing Perawatan Asli, Billing Perbandingan <br> <br>- Medical Treatment memerlukan konfirmasi Dokter Garda Medika<br> &ensp;Medical Treatment : Caldwell Luc <br> &ensp;Coverage : Confirm <br> &ensp;Billed : 50.000 <br> &ensp;Additional Info : - <br> &ensp;Additional Documents : - <br> <br>- Medical Treatment memerlukan konfirmasi Garda Medika<br> &ensp;Medical Treatment : Medical Treatment <br> &ensp;Coverage : Confirm <br> &ensp;Billed : 50.000 <br> &ensp;Additional Info : - <br> &ensp;Additional Documents : - <br> <br>' 

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'), [:])

def var = WS.sendRequest(findTestObject('GA/CreateTreatmentGL/GA_FinalValidationAPTTOInTreatment', [('authorization') : GlobalVariable.authorization
            , ('AdminName') : AdminName, ('IsGLProvider') : IsGLProvider, ('MemberNo') : MemberNo, ('PreviousTrID') : PreviousTrID
            , ('TreatmentDate') : TreatmentDate, ('ProductType') : ProductType, ('DiagnosisCode') : DiagnosisCode, ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
            , ('ProviderID') : ProviderID, ('PatientPhone') : PatientPhone, ('CallerName') : CallerName, ('TreatmentRoom') : TreatmentRoom
            , ('TreatmentRoomAmount') : TreatmentRoomAmount, ('DoctorName') : DoctorName, ('RoomOption') : RoomOption, ('RoomAvailability') : RoomAvailability
            , ('UpgradeClass') : UpgradeClass, ('BenefitAmount') : BenefitAmount, ('ProviderEmail') : ProviderEmail, ('ProviderFax') : ProviderFax
            , ('ProviderPhone') : ProviderPhone, ('ProviderExt') : ProviderExt, ('IsTiri') : IsTiri, ('DefaultProviderID') : DefaultProviderID
            , ('Remarks') : Remarks, ('Guid') : Guid, ('TicketNo') : TicketNo, ('GLType') : GLType, ('AccountManager') : AccountManager
            , ('TotalBilled') : TotalBilled, ('ClientID') : ClientID, ('ClassNo') : ClassNo, ('MembershipType') : MembershipType
            , ('AllDiagnosis') : AllDiagnosis, ('AllDoctors') : AllDoctors, ('Gender') : Gender, ('DOB') : DOB, ('NewMemberName') : NewMemberName
            , ('PreviousGuid') : PreviousGuid, ('GLStatus') : GLStatus, ('EmpMemberNo') : EmpMemberNo, ('NMEmpID') : NMEmpID
            , ('FollowUpTaskID') : FollowUpTaskID, ('IsClient') : IsClient, ('CallStatusID') : CallStatusID, ('AppropriateRBClass') : AppropriateRBClass
            , ('AppropriateRBRate') : AppropriateRBRate, ('TreatmentEnd') : TreatmentEnd, ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen
            , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen, ('TreatmentRoomChoosen') : TreatmentRoomChoosen, ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
            , ('IsReferral') : IsReferral, ('IsSpecialCondition') : IsSpecialCondition, ('ReferralReasonCode') : ReferralReasonCode
            , ('CallInStart') : CallInStart, ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode, ('ClientClassNo') : ClientClassNo
            , ('UserPosition') : UserPosition, ('OPNO') : OPNO, ('MaternityMedicalTreatment') : MaternityMedicalTreatment
            , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem, ('TreatmentCode') : TreatmentCode, ('IsFromProcessButton') : IsFromProcessButton
            , ('NonMedicalItem') : NonMedicalItem, ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered
            , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion, ('IsODS') : IsODS, ('IsODC') : IsODC
            , ('IsProducttypeChange') : IsProducttypeChange, ('IsTreatmentPeriodChange') : IsTreatmentPeriodChange, ('IsMaternityTreatmentChange') : IsMaternityTreatmentChange
            , ('IsRoomOptionChange') : IsRoomOptionChange, ('IsTreatmentRBClassChange') : IsTreatmentRBClassChange, ('IsODSODCChange') : IsODSODCChange
            , ('IsDocValidityChange') : IsDocValidityChange, ('IsDocTypeChange') : IsDocTypeChange, ('suspectDouble') : suspectDouble
            , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo, ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation
            , ('IsProtapCovid') : IsProtapCovid, ('MedicalTreatmentResultMsg') : MedicalTreatmentResultMsg]))

API.Note(API.getResponseData(var).Status)

API.Note(API.getResponseData(var).Data)

def popUpInfo = API.getResponseData(var).Data.ClaimValidationAdditionalRemarks

if ((popUpInfo.contains(Wording1) && popUpInfo.contains(Wording2)) && popUpInfo.contains(Wording3) && popUpInfo.contains(Wording4) && popUpInfo.contains(Wording5) && popUpInfo.contains(Wording6) && popUpInfo.contains(Wording7)) {
    KeywordUtil.markPassed('Informasi Validasi MT yang muncul:')

    KeywordUtil.markPassed(Wording1)

    KeywordUtil.markPassed(Wording2)

    KeywordUtil.markPassed(Wording3)

	KeywordUtil.markPassed(Wording4)
	
	KeywordUtil.markPassed(Wording5)

	KeywordUtil.markPassed(Wording6)
	
	KeywordUtil.markPassed(Wording7)
	
    KeywordUtil.markPassed(popUpInfo)
} else {
    KeywordUtil.markFailed('Informasi Validasi MT yang diharapkan tidak muncul')

    KeywordUtil.markFailed(popUpInfo)
}

//Cek Status API
if (API.getResponseData(var).Status) {
    KeywordUtil.markPassed('API OK')
} else {
    KeywordUtil.markFailedAndStop('Terjadi kesalahan pada API! - ' + API.getResponseData(var).ErrorMessage)
}

