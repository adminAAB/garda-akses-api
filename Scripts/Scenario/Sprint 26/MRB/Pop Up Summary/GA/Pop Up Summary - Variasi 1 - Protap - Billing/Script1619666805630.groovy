import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil


String Wording1 ='Medical Treatment memerlukan konfirmasi Garda Medika terkait tindakan PROTAP COVID'
String Wording2 ='Medical Treatment memerlukan konfirmasi billing oleh Garda Medika'

String MaternityMedicalTreatment = '[["A38",390,0,"","0","","Scarlet fever","Covered",1,"","","","",1,"1","OBT","16/04/2021 15:03","",0,"3","30000","1200000",0,null,0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Tindakan hanya dijaminkan pada transaksi ODS\n\nDokumen yang perlu dilengkapi  : \n - \n","Obat Sakit","Obat","1"]]'

String AdminName = ''
String IsGLProvider = 'False'
String MemberNo = 'A/00099096'
String PreviousTrID = '2892280'
String TreatmentDate = '2021-04-28'
String ProductType = 'IP'
String DiagnosisCode = 'A32'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'TJKRI47'
String PatientPhone = '087841632020'
String CallerName = '-'
String TreatmentRoom = 'VVIP'
String TreatmentRoomAmount = '975000'
String DoctorName = 'Ugha Anugerah'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '800000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = ''
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String Guid = 'b7400666680044a88332b993e65e116ba563415699ef6ce8948614e794c7e302'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'CJKPP0009'
String ClassNo = '14'
String MembershipType = '3. CHI'
String AllDiagnosis = '[[1,"A32","Listeriosis","Initial Primary","","","","","","","","","Covered","","","","","",0,"0","0"]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String AllDoctors = 'Ugha Anugerah'
String Gender = 'F'
String DOB = '2005-10-25'
String NewMemberName = ''
String PreviousGuid = 'b7400666680044a88332b993e65e116ba563415699ef6ce8948614e794c7e302'
String GLStatus = 'Produce'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '7694a85448a2478084c1e59e6f13bc7fd1a3d489dad8d32c4e4d743685f4a05f'
String IsClient = '1'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'VVIP'
String AppropriateRBRate = '975000'
String TreatmentEnd = ''
String AppropriateRBClassChoosen = 'VVIP'
String AppropriateRBRateChoosen = '975000'
String TreatmentRoomChoosen = 'VVIP'
String TreatmentRoomAmountChoosen = '975000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '10'
String UserPosition = 'Customer Service'
String OPNO = '105323'
//String MaternityMedicalTreatment = '[["A38",322,0,null,"0","","Scarlet fever","Covered",0,"","","","",1,"1","OBT","15/04/2021 12:27","",0,"3","30000","900000",0,null,0,"Test QC","Coverage : Dijaminkan\n\nReason : \n -\n\nDokumen yang perlu dilengkapi  : \n - \n","Obat sakit","Obat"]]'
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[],[]]'
String IsODS = '0'
String IsODC = '0'
String IsProducttypeChange = '0'
String IsTreatmentPeriodChange = '0'
String IsMaternityTreatmentChange = '0'
String IsRoomOptionChange = '0'
String IsTreatmentRBClassChange = '0'
String IsODSODCChange = '0'
String IsDocValidityChange = '0'
String IsDocTypeChange = '0'
String suspectDouble = '0'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '0'
String MedicalTreatmentResultMsg = '- Medical Treatment memerlukan konfirmasi billing oleh Garda Medika<br>\r\n&ensp;Medical Treatment : Obat Sakit <br>\r\n&ensp;Billed : 900.000.000 <br>\r\n&ensp;Additional Info : - <br>\r\n&ensp;Additional Documents : - <br>\r\n<br>- Medical Treatment memerlukan konfirmasi Garda Medika terkait tindakan PROTAP COVID<br>\r\n&ensp;Medical Treatment : Obat Sakit <br>\r\n&ensp;Billed : 900.000.000 <br>\r\n&ensp;Additional Info : - <br>\r\n&ensp;Additional Documents : - <br>\r\n<br> '


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/GA_FinalValidationAPTTOInTreatment', [('authorization') : GlobalVariable.authorization
 , ('AdminName') : AdminName , ('IsGLProvider') : IsGLProvider , ('MemberNo') : MemberNo , ('PreviousTrID') : PreviousTrID
 , ('TreatmentDate') : TreatmentDate , ('ProductType') : ProductType , ('DiagnosisCode') : DiagnosisCode , ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
 , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom
 , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability
 , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax
 , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('DefaultProviderID') : DefaultProviderID
 , ('Remarks') : Remarks , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType
 , ('AccountManager') : AccountManager , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo
 , ('MembershipType') : MembershipType , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender
 , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('PreviousGuid') : PreviousGuid , ('GLStatus') : GLStatus
 , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('IsClient') : IsClient
 , ('CallStatusID') : CallStatusID , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd
 , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
 , ('IsReferral') : IsReferral , ('IsSpecialCondition') : IsSpecialCondition , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart
 , ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('ClientClassNo') : ClientClassNo , ('UserPosition') : UserPosition , ('OPNO') : OPNO
 , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('TreatmentCode') : TreatmentCode , ('IsFromProcessButton') : IsFromProcessButton
 , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('IsODS') : IsODS
 , ('IsODC') : IsODC , ('IsProducttypeChange') : IsProducttypeChange , ('IsTreatmentPeriodChange') : IsTreatmentPeriodChange , ('IsMaternityTreatmentChange') : IsMaternityTreatmentChange
 , ('IsRoomOptionChange') : IsRoomOptionChange , ('IsTreatmentRBClassChange') : IsTreatmentRBClassChange , ('IsODSODCChange') : IsODSODCChange , ('IsDocValidityChange') : IsDocValidityChange
 , ('IsDocTypeChange') : IsDocTypeChange , ('suspectDouble') : suspectDouble , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation
 , ('IsProtapCovid') : IsProtapCovid, ('MedicalTreatmentResultMsg') : MedicalTreatmentResultMsg ]))


API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)

def popUpInfo = API.getResponseData(var).Data.ClaimValidationFinalResult
	if (popUpInfo.contains(Wording1) && popUpInfo.contains(Wording2)  ) {
		KeywordUtil.markPassed("Informasi Validasi MT yang muncul:")
		KeywordUtil.markPassed(Wording1)
		KeywordUtil.markPassed(Wording2)
		KeywordUtil.markPassed(popUpInfo)

	}else {
		KeywordUtil.markFailed("Informasi Validasi MT yang diharapkan tidak muncul")
		KeywordUtil.markFailed(popUpInfo)
	}


//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}