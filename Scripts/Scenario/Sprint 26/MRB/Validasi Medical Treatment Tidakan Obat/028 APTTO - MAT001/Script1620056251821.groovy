import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String expected = 'Need FU'

String MaternityMedicalTreatment = '[["O80",412,0,null,"500000","Terapi - Covered","SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY","Covered",0,"","","","",1,"1","TTO","04/05/2021 11:22","",0,null,"0","0",0,null,0,"Twst","Coverage : Dijaminkan\n\nReason : \n -\n\nDokumen yang perlu dilengkapi  : \n - \n",null,"Tindakan Medis/Terapi","0"]]'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String MedicalTreatmentConfirmation = ''


String MemberNo = 'A/94821'
String TreatmentDate = '2021-05-03'
String DiagnosisCode = 'O80'
String ProductType = 'MA'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'TJKSH0010'
String PatientPhone = '087841632020'
String CallerName = ''
String TreatmentRoom = 'BASIC (KELAS III)'
String TreatmentRoomAmount = '225000'
String DoctorName = 'Ugha'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '450000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '02129531922'
String ProviderPhone = '081806221986'
String ProviderExt = ''
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'PRI'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKSH0010'
String Guid = '0d96d0dce942423291c5e17dcd74917b00ca837a8e2a87e9f2fcec39896f208a'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String PreviousGuid = '0427a2465e5546b8ab184739b4ecf17338bb69b465ccded6258a577217f9a95e'
String PreviousTrID = '2892452'
String TotalBilled = '0'
String ClientID = 'CJKFI02'
String ClassNo = '8'
String Membership = '2. SPO'
String AllDiagnosis = '[[1,"O80","SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY","Initial Primary","","","","0","0","0","2","","Covered","","","","","Covered",0,"1","1"]]'
String AllDoctors = 'Ugha'
String Gender = 'F'
String DOB = '1990-11-08'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '0d96d0dce942423291c5e17dcd74917b00ca837a8e2a87e9f2fcec39896f208a'
String TreatmentRoomChoosen = 'BASIC (KELAS III)'
String AppropriateRBClassChoosen = 'BASIC (KELAS III)'
String AppropriateRBRateChoosen = '225000'
String TreatmentRoomAmountChoosen = '225000'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'BASIC (KELAS III)'
String AppropriateRBRate = '225000'
String TreatmentEnd = ''
String IsSpecialCondition = '0'
String ClientClassNo = '8'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = 'MAT001'
String MaternityFamilyPlanningItem = ''
//String MaternityMedicalTreatment = '[["O80",402,0,null,"50000","Terapi - Question - Tr Cv - Fl Cf- Uk - Uc - AddDoc","SINGLE SPONTANEOUS DELIVERY/NORMAL DELIVERY","Covered",0,"","","BILPA, BNDNG"," Billing Perawatan Asli, Billing Perbandingan",1,"1","TTO","04/05/2021 11:11","",0,null,"0","0",0,null,0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi Garda Medika terkait Dokumen Tambahan\n\nDokumen yang perlu dilengkapi : \n1. Billing Perawatan Asli, Billing Perbandingan\n",null,"Tindakan Medis/Terapi","0"]]'
String Source = 'DOO'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
//String MedicalTreatmentAdditionalQuestion = '[[{"QuestionID":"384","QuestionDescription":"Tr Cv - Fl Cf- Uk - Uc","AnswerValue":"1","Remarks":"","AdditionalInfo":"","AdditionalInfoProvider":"","AdditionalDocDesc":" Billing Perawatan Asli, Billing Perbandingan"}]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '0'
String OPNO = '105966'
String IsMedicalTreatmentNeedNextValidation = '1'
String IsDocumentNeedNextValidation = '1'
String IsGLProvider = 'False'
//String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '1'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('GA/CCOOutbound/ProcessGLAPPTOInTreatment', [('MemberNo') : MemberNo , ('TreatmentDate') : TreatmentDate , ('DiagnosisCode') : DiagnosisCode , ('ProductType') : ProductType , ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo , ('ProviderID') : ProviderID ,
('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability ,('UpgradeClass') : UpgradeClass ,
('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('UserPosition') : UserPosition , ('Remarks') : Remarks , ('serviceTypeID') : serviceTypeID ,
('CallStatusID') : CallStatusID , ('DefaultProviderID') : DefaultProviderID , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType , ('AccountManager') : AccountManager , ('PreviousGuid') : PreviousGuid , ('PreviousTrID') : PreviousTrID , ('TotalBilled') : TotalBilled ,
('ClientID') : ClientID , ('ClassNo') : ClassNo , ('Membership') : Membership , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('GLStatus') : GLStatus , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID ,
('FollowUpTaskID') : FollowUpTaskID , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen ,  ('IsReferral') : IsReferral ,
('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart , ('IsClient') : IsClient , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd , ('IsSpecialCondition') : IsSpecialCondition , ('ClientClassNo') : ClientClassNo ,
('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('TreatmentCode') : TreatmentCode , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('Source') : Source , ('NonMedicalItem') : NonMedicalItem ,
('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('IsODS') : IsODS , ('IsODC') : IsODC , ('OPNO') : OPNO , ('IsMedicalTreatmentNeedNextValidation') : IsMedicalTreatmentNeedNextValidation ,
('IsDocumentNeedNextValidation') : IsDocumentNeedNextValidation , ('IsGLProvider') : IsGLProvider , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation , ('IsProtapCovid') : IsProtapCovid ]))

def result =API.getResponseData(var).Data.MAMedicalTreatmentValidationF

if(result == true && expected == 'Need FU') {
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Result Need FU = '+ result)
	KeywordUtil.markPassed('Terkena Validasi MT')
}else if(result == false && expected == 'Covered') {
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Result Need FU = '+ result)
	KeywordUtil.markPassed('Tidak Terkena Validasi MT')
}else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai :(')
	KeywordUtil.markFailed('Result Need FU = '+ result)
}


