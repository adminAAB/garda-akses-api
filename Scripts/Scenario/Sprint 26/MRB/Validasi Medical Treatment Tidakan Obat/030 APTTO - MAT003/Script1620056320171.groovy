import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String expected = 'Need FU'
String MemberNo = 'T/20961'
String TreatmentDate = '2021-05-03'
String DiagnosisCode = 'O00'
String ProductType = 'MA'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'TJKSH0010'
String PatientPhone = '087841632020'
String CallerName = ''
String TreatmentRoom = 'BASIC (KELAS III)'
String TreatmentRoomAmount = '225000'
String DoctorName = 'Ugha Anugerah'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '300000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '02129531922'
String ProviderPhone = '081806221986'
String ProviderExt = ''
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'PRI'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKSH0010'
String Guid = 'b930133b69b34b889c557e86c69d591eed4773c36341ea7eb393beba4083e54b'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String PreviousGuid = '6f20f77239b74e2fa0cf8443836ec3e684144a53ebe4aab15fe5fd7a72b5d74d'
String PreviousTrID = '2892454'
String TotalBilled = '0'
String ClientID = 'CJKFI02'
String ClassNo = '7'
String Membership = '2. SPO'
String AllDiagnosis = '[[1,"O00","ECTOPIC PREGNANCY","Initial Primary","","","","0","0","0","0","","Covered","Abortion","","","","Covered",0,"1","1"]]'
String AllDoctors = 'Ugha Anugerah'
String Gender = 'F'
String DOB = '1991-10-10'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = 'b930133b69b34b889c557e86c69d591eed4773c36341ea7eb393beba4083e54b'
String TreatmentRoomChoosen = 'BASIC (KELAS III)'
String AppropriateRBClassChoosen = 'BASIC (KELAS III)'
String AppropriateRBRateChoosen = '225000'
String TreatmentRoomAmountChoosen = '225000'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'BASIC (KELAS III)'
String AppropriateRBRate = '225000'
String TreatmentEnd = ''
String IsSpecialCondition = '0'
String ClientClassNo = '7'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = 'MAT003'
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = '[["O00",409,0,null,"50000","Terapi - Client ID - Covered","ECTOPIC PREGNANCY","Covered",0,"","","","",1,"1","TTO","03/05/2021 22:14","",0,null,"0","0",0,null,0,"Trdy WC","Coverage : Dijaminkan\n\nReason : \n -\n\nDokumen yang perlu dilengkapi : \n - \n",null,"Tindakan Medis/Terapi","1"]]'
String Source = 'DOO'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '0'
String OPNO = '105966'
String IsMedicalTreatmentNeedNextValidation = '1'
String IsDocumentNeedNextValidation = '1'
String IsGLProvider = 'False'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '1'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('GA/CCOOutbound/ProcessGLAPPTOInTreatment', [('MemberNo') : MemberNo , ('TreatmentDate') : TreatmentDate , ('DiagnosisCode') : DiagnosisCode , ('ProductType') : ProductType , ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo , ('ProviderID') : ProviderID ,
('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability ,('UpgradeClass') : UpgradeClass ,
('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('UserPosition') : UserPosition , ('Remarks') : Remarks , ('serviceTypeID') : serviceTypeID ,
('CallStatusID') : CallStatusID , ('DefaultProviderID') : DefaultProviderID , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType , ('AccountManager') : AccountManager , ('PreviousGuid') : PreviousGuid , ('PreviousTrID') : PreviousTrID , ('TotalBilled') : TotalBilled ,
('ClientID') : ClientID , ('ClassNo') : ClassNo , ('Membership') : Membership , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('GLStatus') : GLStatus , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID ,
('FollowUpTaskID') : FollowUpTaskID , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen ,  ('IsReferral') : IsReferral ,
('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart , ('IsClient') : IsClient , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd , ('IsSpecialCondition') : IsSpecialCondition , ('ClientClassNo') : ClientClassNo ,
('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('TreatmentCode') : TreatmentCode , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('Source') : Source , ('NonMedicalItem') : NonMedicalItem ,
('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('IsODS') : IsODS , ('IsODC') : IsODC , ('OPNO') : OPNO , ('IsMedicalTreatmentNeedNextValidation') : IsMedicalTreatmentNeedNextValidation ,
('IsDocumentNeedNextValidation') : IsDocumentNeedNextValidation , ('IsGLProvider') : IsGLProvider , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation , ('IsProtapCovid') : IsProtapCovid ]))

def result =API.getResponseData(var).Data.MAMedicalTreatmentValidationF

if(result == true && expected == 'Need FU') {
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Result Need FU = '+ result)
	KeywordUtil.markPassed('Terkena Validasi MT')
}else if(result == false && expected == 'Covered') {
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Result Need FU = '+ result)
	KeywordUtil.markPassed('Tidak Terkena Validasi MT')
}else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai :(')
	KeywordUtil.markFailed('Result Need FU = '+ result)
}


