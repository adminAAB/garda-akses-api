import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String expected = 'Covered'

String MaternityMedicalTreatment = '[["A38",397,0,"","500000","Terapi - Covered - AddDoc","Scarlet fever","Covered",0,"","","BILPA|BNDNG"," Billing Perawatan Asli, Billing Perbandingan",1,"1","TTO","03/05/2021 18:39","",0,null,"0","0",0,null,0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi Garda Medika terkait Dokumen Tambahan\n\nDokumen yang perlu dilengkapi  : \n1.  Billing Perawatan Asli, Billing Perbandingan\n",null,"Tindakan Medis/Terapi","1"]]'
String MedicalTreatmentConfirmation = '[{"ConfirmationType":"AM","PICDesc":"Account Manager","PICName":"AIB","NameDesc":"Arrio Probo Wibowo","ConfirmationDate":"04/05/2021 22:25","Confirmation":"NOD","ConfirmationDesc":"Need Others Documents","Remarks":"","FollowUpTime":"1","Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"BC001","Reason":null,"ConfirmationCategory":null,"DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":1,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","IsFromTicketBefore":0,"IsEdit":1,"NeedFU":1,"IsCito":0,"GuidConfirmation":"b6222428-b7af-4da1-a9c2-55303021ac6d"},{"ConfirmationType":"AM","PICDesc":"Account Manager","PICName":"AIB","NameDesc":"Arrio Probo Wibowo","ConfirmationDate":"04/05/2021 22:45","Confirmation":"HLD","ConfirmationDesc":"Hold","Remarks":"","FollowUpTime":"1","Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"BC001","Reason":"FUR020","ConfirmationCategory":null,"DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","IsFromTicketBefore":0,"IsEdit":"1","NeedFU":1,"IsCito":0,"IsPrevious":0,"GuidConfirmation":"580b9c73-60f3-48f4-b60a-c3953d5ca270"},{"ConfirmationType":"AM","PICDesc":"Account Manager","PICName":"AIB","NameDesc":"Arrio Probo Wibowo","ConfirmationDate":"04/05/2021 22:45","Confirmation":"DOCR","ConfirmationDesc":"Document Received","Remarks":"","FollowUpTime":0,"Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"","Reason":"","ConfirmationCategory":null,"DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[{"DocSource":"CALL","DocSourceDescription":"Call","DocStatus":"Informasi Mandatory Dokumen dari hasil Validasi Medical Treatment","DocType":"CLT01","DocTypeDescription":"CL Benjolan","FileDoc":"JVBERi0xLjYKJcOkw7zDtsOfCjIgMCBvYmoKPDwvTGVuZ3RoIDMgMCBSL0ZpbHRlci9GbGF0ZURlY29kZT4+CnN0cmVhbQp4nCWLOwqAMBAF+z3Fqy3ibtQkQkghaGEnLFiInZ9OMI3XV5AppplhI3joBoMN2wAnYlov8O3vvNNc4PqLj3xSp9Q4E+Bt/c26oRwEYqHHElmSC5FtkshVWnWkXmmiCS91fRYpCmVuZHN0cmVhbQplbmRvYmoKCjMgMCBvYmoKMTAwCmVuZG9iagoKNSAwIG9iago8PC9MZW5ndGggNiAwIFIvRmlsdGVyL0ZsYXRlRGVjb2RlL0xlbmd0aDEgODA0MD4+CnN0cmVhbQp4nOU4a3BbVXrfuVe25Ucs2dhKghLpiBuH+CU5dhLycqzYlmzHTiy/guSEWNfStaXEelSSHRKWQW0HNqOQJpttYQOZgR+7DOzQyTVmt2aHbgwt29LtLku32yldsmRmd6c/mkxcFvgBJe53zr1ynBBg2um/Hvuc+72f5xzf60xqWoEyyIII7lBMTlYRgwAA/whAKkMzGdraX70L4asAwj9NJCdjz/zV4Y8ADK8CFL06OXVi4gcvPR4GKIsAiO6IIoc/WftAI4DpRbSxLYKEfTdPFCGO+rAhEss8/Db5iRnAbEDcPJUIyduFcYJgFeIlMfnhpMOwA/2bKeI0LscU2mboRtwNUDqTTKQzYdiwBGCdZ/xkSkn2PTP+FuL/iv7PIY3gDxtlCBYyXBDh//UoOAPV0F3QCiZI8vW2Ib4Ma+ECwNI1ht1ab/Ytffp/GYVRe3wHXoBX4Qy8Bw/pDC/4IArTSFk53oB3kcqGD0bh+5D7ErMvwzzyNbkgnGWZ3HX44GmYg7+7zYsPYvAIxvIDeI9shrdxqyTgQ2KEP4a30OqHSNt/N1NCOS4THJxYQf01PCuchn3C7xC5wDiCSzDD38JFcgQtZzDPM8sZ7/6C0W/Co7gOQQRmEOajoPW//g2Kl/6AWT0K++BPYC9MrdB4nTwnlmD/huE5rOkbnObKM4u6xaPCDwXh828j8i2YxCkTzF04I+79kgr9j4c4AqtIrVgDxXfjClvAdPNToXnpI3EDlMDI0mKettS79AdRvhk3jBnWFbQafvpVPgq/ZYihNiz9/uYjN8MFBwpewG7hzeLuOjQa8I8MDw0O+PoP7O/r3dfT3eX1dHa073W37WndvWvnju0PbNu6ucnlbGzYdP/Gmg3SfQ77mqoKs6l8VWlJsbGosMAgCgQaqEqCHlWsoRVeWfJIcndjA/WsiXQ2Nngkb1ClMlXxYdgodXdzkiSrNEjVjfiQV5CDqhslJ+6QdGuS7mVJYqa7YTdzIVH1Z50SnSejA36Ez3RKAape5/B+Dhs2cmQVIg4HavCoWLTUo3pnIjlPEGMks6UlHVKHUtLYALMlpQiWIqRukpKzZNMewgFhk2fnrADGVcwtZuqRw6pvwO/ptDocgcaGHrVc6uQs6OAm1cIOtYibpFEWOpymsw0LuSfnzTAerC8LS2H5sF8VZdTNiZ5c7ptqRb1aK3WqtSd/twYzV9QGqdOj1jOrvYPLfnpvuSRqQY1ZormPAdORrl+7nSLrlMIa88fAQFXoUMmg38GG1Yu1zuW8EvXmgjl5fik7LlGzlJstK8slPVhu8PnRxPzSj05bVe+TAdUcjJCdAT1172Cves/AIb8q1HhpREYK/rZJju1WR8WyjO/L2IBlweJghR0OVobT824YR0TNDvg1nMK49RVwu+oDqhBknIU8p3qEcbJ5zrJ6UMLe9g75c6qhpicsebDip2U1O4676yhrjGRWyz+xOqRcZQXd4QpwWYpR9YSjVC3YiEVCrZUKuG+YSs7MkfJPtMd1KzrYWFFJd0hohtnxSJ6g/jsTWYMGKBa6u17bCMN+1d2JgFvWO+aZbXKhhhzEhkU7eTNVl5RUq6T25e6ysDzRIT9X0dXUqg4VgiFdS3V5+LminlywUwuB2ZIG/K9By9LV2S3UOtcCWyDQyYQtHbjLNnpy/vCEag9aw3juJqjf6lDdAexwQPIrAbbtsEK1V618cwT4Xhn29w5JvQOj/u16IBqDmTPUeO4wI/mtmhncgKqxxkj9glUMoKAZCdSLgNS+G1e1qMaI04wF51S2cdt3Uz+xQl4aw1BrqUfp1OUYfpvRAradOrrz1goZinY6uq2OgEMbjQ0CsqnuGDWMrKjdeRZeU8gw4v7s6OYkVss1bNNTv6RIASlCVbfPz3Jj5eFV1ovBa673avg2bEWxsEzgQHYeYcVUvfXWlcVVuzi+jHbfwe7Js2nOKPUO5ZhxSTcIGHmPCmwLu7dXWPldwA60hHcvNeOR5gc6N+t2s8Mc2cmMSD3hnDTk382l8T551HqS+aqEXtI73N7YgFdb+6xETg3MusmpoVH/a2Z8Lzw17H9FIEJHsD0wuwF5/tco/tHgVIFRGZEhlCHM0iAiRi5vfc0NkOVcAydwPDRPgNOMeRqB0Lyg0cyao43ckRsE5Bg0jjsvbUCaUaNlOY2PWWAlc5cUuI3uYneZsEqwzhJGegUpP8L32GICc2VkFbHOotYgJ8+T7Gyx26pJZFHCrUV4auSW65FR/1wZ/nW28hUdtbOB22VNBJuNf1Y8NMw2yjcCkVwwwA4bWLA1+EtUIu3BNkl7MJDCMrVEUtrVUqmd0dsYvU2jFzJ6EW5RYiGonsXe+1TCdsAhvwOPJL33bWvOfJ11KoCXSs78+0asWA1+N7yB76BVZLf7SqVQKhjFaksZGEmxaDQWV4jFYjBQLFYKIIwFoLLNQkwWctVCLlvIWQt5zELGLASJlNOPLVrIOxbyPOclLaTfQuycodFVC3mOsxJczW0hTVwALOQDzs1yehOn7FrifjS1s5zRz3mLnK7mfWgKlOssckML3E2WczE0V97HQ8vjj/IjpY8jd9C/wGE8aKuvgJY1fK1oWeMaO/JQS0UlWb2jomVzk2PrAxXSfSYiVTgqpPudpJ5UrK4mu37V8vlD1g7DxU6r7R8e3vyrrVbD01Xvkl0333q3qPSzY9at/LUMfEvXRK/4Fn4TrIMz7tG1hJjuNVabqtfb1oIvYFprXyuUiWvXllVWWnyBSnNZwUCgzLJgI6qNPG8j52wkayNJGwnaiM9GwEb24MNtI002Qm3EbCOLXA6F8oktZ/UQJgUspUrYwTNCiOzAjDBDlhaprrKRluZtD1SXE+m+jRVbtrXQimpyX2G1Y8tGYmh9bHLbnzc1fe/gr3/688skevPpSIKcP0zeq8xd8FWWbrc7r5GCTz68OTFILr743bkL7EtweOma8EvMdRME3FscRVX3roIqqK1b5RBXr7b5AtbVZrHUFygSLdk6kqwjwTriqyO0jlyqI2N1pL+O5PsEbS0s9BYe+45bYbOoqwox2Pu3tqy2tDRv3eIiTmErRt68ulq6f6OEwVdZVttE4Zezf+l9qalxc+/Db14IKIebXzo3+ayrbmtqYGT/gW+PtknE+OS59ZX//qedL5zcst7RGfJ+46z9ZzGXr3PHgXubnR0HAfh3LX4ZO6T3nhkz7f5YsGvfVH/f+Yuf33pjXrrGT9jyB5emV+S46YEHV1JuG4bCHbj8FmrEM+AT18Mwp75JtpPndWkD1DLfPAIzfmccRuBvxJ+AyLk2El+2eXDZPkHJgzosQBF+E2mwCFb88tJgA8qc0uECKMfvQw0uxO/UF3S4CE7iW78GG/HucOlwMZSTDh0uIXEyoMOlsE64vPxfACd+mWnwKtgqFutwOdwr7mHRG9jXy8uiX4cJUINBhwUoN2zQYRG2GZp12IAyER0ugHWGUzpcCDbDd3W4CD4yvKnDRthU8EMdLoZ1Bb/R4RLh/YJPdbgUthv/RYfL4HBxuQ6vgqPFR3W4HLYU/3NndDKaiZ5UwjQsZ2QaSiRPpKKTkQzdFKqlzU2bm2hXIjE5pdCORCqZSMmZaCLuLOm4U6yZDqKJbjnTQHviIWdfdFzRZOmQkopODCqT01Nyam86pMTDSoo20jsl7sQPKqk0Q5qdTU3OllvcO4WjafykyqTksBKTU8doYuL2QGhKmYymM0oKidE4HXEOOalPzijxDJXjYTq8rNg/MRENKZwYUlIZGYUTmQiGenQ6FU2HoyHmLe1czmBFOYYyyoxC98uZjJJOxNvlNPrCyIaj8US6gR6PREMRelxO07CSjk7GkTl+gt6uQ5ErYy7xeGIGTc4oDRj3REpJR6LxSZpmKevaNBORMyzpmJJJRUPy1NQJ7FksiVrj2KTj0UwEHceUND2gHKeDiZgc/75TCwVrM4FFpdFYMpWY4TE2pkMpRYmjMzksj0enohm0FpFTcggrhmWLhtK8IlgImpTjjZ7pVCKpYKQPdvXdEsQAtWqmE1Mz6JlJxxUlzDxi2DPKFCqh46lE4hjLZyKRwkDDmUjjisgnEvEMqiaoHA5j4litRGg6xvqEZc7kg5NDqQTyklNyBq3E0s5IJpPc6XIdP37cKeutCWFnnGjZ9VW8zImkovcjxazEpvqw/XHWumneX5bEUE8f7U9ifbwYHNUFGmh+a252btZdYBmjyUzamY5OOROpSVe/tw86IQqTODM4T4ICYaA4ZcRlhEKQgCScgBSXiiCV4l+VEN6KFJqhCTbjpNCFUgnkT6E+hQ6EU6jFVpnbTUAcnFDCOV9trRmhQT2Kbq7dgFAP6ofQQh/qjSN3pV0KQ5wSxXuWaU7CNMYhI2UvpFFLQZkwl6DQiPPrbHwd/yCH0sucZoyrCX+c0HJX3a+zHEVblNc6wzks1hiP/xjSEqj3VRWhKKfw/qWRo3AszK0y2yMoMcSlfFyT1SLDvcW51PBdPPajxwnUD/Fe5iVD3DbbE5rlBMIRvapHseIpHkGY6+VzS6PnL/bg7rtjiEc3w33u53SGpzmvHfG0npdWs2EeRQKprBbHMRLmN8JhmdczzLXZLovrmuO47+hX+qG6rqz3Jc59zOhRMp0Gvd4TfE1zv3H0QXl8Wpdv9015nWReda3TMeRmuGwI6VP4c0I/ZzGsiuZrXD9Jx/m5jOgZx7hdCgfweZzvigTvW9xxH+/xrapo+2ZC36mU6yYRTvAs8nVs5L1hmSg8UgbJ/OyPo8YU963FFuG7Q+a9VfReZ3gG+XqF9UxZ1ElOaQQP3xfsxCt6TR/Em6Lvrha1Cq7cm6wnUzze9ArbcR5teDlHrdpMakr3pGU8xW+kY8v9meD7TatomFtr/JKaT/DaZHSvCR5RGH+0jmt7K4G607wf2nnSdnPmC5WTeX0Tul6S30sZPZYYPx8RvgOTsBPfLV0YHftx8n248tSE9DPj1GN2/a/1WFxJXsGV5yO1HEsMY+zTT398+dRNrzi/+U4M4R3Ux++LpL5/vHrl6B0W2Km589bcjP4235GFthujiGd4PGleSyfPYRL5/eihj79Ha2/8DozpLmO22Ld3nChASIRMwj1gJ0E4QMZghOyFVuLGpxt57fjsQJw9naQVsijXivQ9iO9G+i68PO24tuHsx3kWpwGnJtGEEi58unS8EfEG1HgHV8Ino7YhlT33Id6Nzy796UW6B58eHe9BHJ8QJEX4It7G18vE4J4jVz8n73xO6Ofksc+I7zOS/fDch8J/LtbaLy1eXhT6b4zduHRDbLpBTDeIEa6br/uuB68nrz9/vbDEdI2UwX+Qit9e3W7/oPXKyG9a3x+BK5jZlaYrvivZK+qVgitEHHlftNjNC3ShaSG5kF34xcLVhcUFY/bH534s/PXrLrvpdfvrgn2uf+6xOTH4IjG9aH9R8D0bfFY4d5GYLtovui6Kz1xw2i902exPP3W//epTi08J80sLc0+tqvC+TvpJH7RiDQ/MiUv2S3uryX5My4SrHacLZz/OBM6zOPG7B8XtOF2kz71dHPsLUnreer7+/CPnT58vSD6RfeLcE2L28XOPC5dmLs8IaV+tPRGvt8e76uxrW9aMFLWII4XoBr27e8ZrNnmDY277GAodGm2yj3bV2u9pqRwpwIQNKGgS7WKb2C8mxLPiZbHIOOiz2QdwXvUt+gS3r7jMa+q397v6xfmlq26l14HW9iX3ZfeJPd5ae3fXdrupy97l6nqn64OuG12FY13kOfz1XvJe9opub63L6/baHN513dYRS0v1SAUxjZhbTCMCwUa3wIjLtGQSTKYx02Mm0QRtIGQtpIDMk3Ozw0P19b3zRUuDvarRd0glp9SaIba6B0bVwlMqjIwe8s8S8meBx8+cgfb1vWrzkF8Nrg/0qmEE3AzIImBeP2uB9kA6nanng9TXIzyNK9RP1yPxSFqjwjIf6tMkjXdUmiuReiag4QTXesZDAtMjqH0kDWxhzHpNiWmndXNcWVs4sObIfwPVGv5GCmVuZHN0cmVhbQplbmRvYmoKCjYgMCBvYmoKNDUwNwplbmRvYmoKCjcgMCBvYmoKPDwvVHlwZS9Gb250RGVzY3JpcHRvci9Gb250TmFtZS9CQUFBQUErTGliZXJhdGlvblNlcmlmCi9GbGFncyA2Ci9Gb250QkJveFstNTQzIC0zMDMgMTI3OCA5ODJdL0l0YWxpY0FuZ2xlIDAKL0FzY2VudCA4OTEKL0Rlc2NlbnQgLTIxNgovQ2FwSGVpZ2h0IDk4MQovU3RlbVYgODAKL0ZvbnRGaWxlMiA1IDAgUgo+PgplbmRvYmoKCjggMCBvYmoKPDwvTGVuZ3RoIDIzNi9GaWx0ZXIvRmxhdGVEZWNvZGU+PgpzdHJlYW0KeJxdUEFOwzAQvPsVe2wPlZ2EwiWyhIoq5VBABB7g2JtgidiW4xzye9ZuAYmDrRntzGh2+al76pxN/DV63WOC0ToTcfFr1AgDTtaxqgZjdbqx8utZBcbJ229Lwrlzo29bxt9otqS4we7R+AH3jL9Eg9G6CXYfp554v4bwhTO6BIJJCQZHyrmo8Kxm5MV16AyNbdoOZPkTvG8BoS68ulbR3uASlMao3ISsFUJCez5Lhs78mzVXxzDqTxVJWZFSiOOdJFwXfH/MuCn4oSkZN3VOy+v+tAS9xkgNy01KtVzKOvw9W/Ahu8r7Bt4mchQKZW5kc3RyZWFtCmVuZG9iagoKOSAwIG9iago8PC9UeXBlL0ZvbnQvU3VidHlwZS9UcnVlVHlwZS9CYXNlRm9udC9CQUFBQUErTGliZXJhdGlvblNlcmlmCi9GaXJzdENoYXIgMAovTGFzdENoYXIgMwovV2lkdGhzWzc3NyA2MTAgNDQzIDM4OSBdCi9Gb250RGVzY3JpcHRvciA3IDAgUgovVG9Vbmljb2RlIDggMCBSCj4+CmVuZG9iagoKMTAgMCBvYmoKPDwvRjEgOSAwIFIKPj4KZW5kb2JqCgoxMSAwIG9iago8PC9Gb250IDEwIDAgUgovUHJvY1NldFsvUERGL1RleHRdCj4+CmVuZG9iagoKMSAwIG9iago8PC9UeXBlL1BhZ2UvUGFyZW50IDQgMCBSL1Jlc291cmNlcyAxMSAwIFIvTWVkaWFCb3hbMCAwIDYxMiA3OTJdL0dyb3VwPDwvUy9UcmFuc3BhcmVuY3kvQ1MvRGV2aWNlUkdCL0kgdHJ1ZT4+L0NvbnRlbnRzIDIgMCBSPj4KZW5kb2JqCgo0IDAgb2JqCjw8L1R5cGUvUGFnZXMKL1Jlc291cmNlcyAxMSAwIFIKL01lZGlhQm94WyAwIDAgNjEyIDc5MiBdCi9LaWRzWyAxIDAgUiBdCi9Db3VudCAxPj4KZW5kb2JqCgoxMiAwIG9iago8PC9UeXBlL0NhdGFsb2cvUGFnZXMgNCAwIFIKL09wZW5BY3Rpb25bMSAwIFIgL1hZWiBudWxsIG51bGwgMF0KL0xhbmcoZW4tVVMpCj4+CmVuZG9iagoKMTMgMCBvYmoKPDwvQ3JlYXRvcjxGRUZGMDA1NzAwNzIwMDY5MDA3NDAwNjUwMDcyPgovUHJvZHVjZXI8RkVGRjAwNEMwMDY5MDA2MjAwNzIwMDY1MDA0RjAwNjYwMDY2MDA2OTAwNjMwMDY1MDAyMDAwMzcwMDJFMDAzMD4KL0NyZWF0aW9uRGF0ZShEOjIwMjEwNTA0MjIzOTAwKzA3JzAwJyk+PgplbmRvYmoKCnhyZWYKMCAxNAowMDAwMDAwMDAwIDY1NTM1IGYgCjAwMDAwMDU1ODAgMDAwMDAgbiAKMDAwMDAwMDAxOSAwMDAwMCBuIAowMDAwMDAwMTkwIDAwMDAwIG4gCjAwMDAwMDU3MjMgMDAwMDAgbiAKMDAwMDAwMDIxMCAwMDAwMCBuIAowMDAwMDA0ODAxIDAwMDAwIG4gCjAwMDAwMDQ4MjIgMDAwMDAgbiAKMDAwMDAwNTAxNyAwMDAwMCBuIAowMDAwMDA1MzIyIDAwMDAwIG4gCjAwMDAwMDU0OTMgMDAwMDAgbiAKMDAwMDAwNTUyNSAwMDAwMCBuIAowMDAwMDA1ODIyIDAwMDAwIG4gCjAwMDAwMDU5MTkgMDAwMDAgbiAKdHJhaWxlcgo8PC9TaXplIDE0L1Jvb3QgMTIgMCBSCi9JbmZvIDEzIDAgUgovSUQgWyA8MDlFNkMxOEQ5Q0NDMDRENDM2NTE3RjNFMTIwNDgzMTQ+CjwwOUU2QzE4RDlDQ0MwNEQ0MzY1MTdGM0UxMjA0ODMxND4gXQovRG9jQ2hlY2tzdW0gL0YwOUQzN0U5RUQ2MzZGNDJFNzZFMjEyOEUzNUQzQ0U1Cj4+CnN0YXJ0eHJlZgo2MDk0CiUlRU9GCg==","FileNames":"Sample.pdf","FileTypes":"application/pdf","GLType":null,"ReceivedDate":"04/05/2021","ValidityCode":"DOVLD","ValidityDesc":"Dokumen Valid","IsMTDoc":1,"GuidConfirmation":"bdcc1e5c-70ad-449e-8c51-df820483b064"}],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","IsFromTicketBefore":0,"IsEdit":"1","NeedFU":0,"IsCito":0,"IsPrevious":0,"GuidConfirmation":"bdcc1e5c-70ad-449e-8c51-df820483b064"}]'

String MemberNo = 'A/00098696'
String TreatmentDate = '2021-05-03'
String DiagnosisCode = 'A38'
String ProductType = 'IP'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'TJKRI47'
String PatientPhone = '087841632020'
String CallerName = ''
String TreatmentRoom = 'ODC (One Day Care)'
String TreatmentRoomAmount = '300000'
String DoctorName = 'Ugha Anugerah'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '300000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = ''
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'PRI'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKRI47'
String Guid = '8624323fd7bf4f6dad83186900641d8c66e64d0afc7d29c8d3e5efb8f5cf52f9'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String PreviousGuid = '31f483244f724f5fb423039c43d072468f4c972419d5942a950af2c1ca75f127'
String PreviousTrID = '2892449'
String TotalBilled = '0'
String ClientID = 'CJKFI02'
String ClassNo = '7'
String Membership = '3. CHI'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"1","0"]]'
String AllDoctors = 'Ugha Anugerah'
String Gender = 'F'
String DOB = '2013-06-22'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '8624323fd7bf4f6dad83186900641d8c66e64d0afc7d29c8d3e5efb8f5cf52f9'
String TreatmentRoomChoosen = ''
String AppropriateRBClassChoosen = ''
String AppropriateRBRateChoosen = ''
String TreatmentRoomAmountChoosen = ''
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'ODC (One Day Care)'
String AppropriateRBRate = '300000'
String TreatmentEnd = '2021-05-03'
String IsSpecialCondition = '0'
String ClientClassNo = '7'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = ''
String MaternityFamilyPlanningItem = ''
String Source = 'DOO'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '1'
String OPNO = '105966'
String IsMedicalTreatmentNeedNextValidation = '1'
String IsDocumentNeedNextValidation = '1'
String IsGLProvider = 'False'
String IsProtapCovid = '0'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('GA/CCOOutbound/ProcessGLAPPTOInTreatment', [('MemberNo') : MemberNo , ('TreatmentDate') : TreatmentDate , ('DiagnosisCode') : DiagnosisCode , ('ProductType') : ProductType , ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo , ('ProviderID') : ProviderID ,
('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability ,('UpgradeClass') : UpgradeClass , 
('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('UserPosition') : UserPosition , ('Remarks') : Remarks , ('serviceTypeID') : serviceTypeID ,
('CallStatusID') : CallStatusID , ('DefaultProviderID') : DefaultProviderID , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType , ('AccountManager') : AccountManager , ('PreviousGuid') : PreviousGuid , ('PreviousTrID') : PreviousTrID , ('TotalBilled') : TotalBilled , 
('ClientID') : ClientID , ('ClassNo') : ClassNo , ('Membership') : Membership , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('GLStatus') : GLStatus , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID ,
('FollowUpTaskID') : FollowUpTaskID , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen ,  ('IsReferral') : IsReferral ,
('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart , ('IsClient') : IsClient , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd , ('IsSpecialCondition') : IsSpecialCondition , ('ClientClassNo') : ClientClassNo ,
('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('TreatmentCode') : TreatmentCode , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('Source') : Source , ('NonMedicalItem') : NonMedicalItem ,
('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('IsODS') : IsODS , ('IsODC') : IsODC , ('OPNO') : OPNO , ('IsMedicalTreatmentNeedNextValidation') : IsMedicalTreatmentNeedNextValidation ,
('IsDocumentNeedNextValidation') : IsDocumentNeedNextValidation , ('IsGLProvider') : IsGLProvider , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation , ('IsProtapCovid') : IsProtapCovid ]))

def result =API.getResponseData(var).Data.MAMedicalTreatmentValidationF

if(result == true && expected == 'Need FU') {
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Result Need FU = '+ result)
	KeywordUtil.markPassed('Terkena Validasi MT')
}else if(result == false && expected == 'Covered') {
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Result Need FU = '+ result)
	KeywordUtil.markPassed('Tidak Terkena Validasi MT')
}else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai :(')
	KeywordUtil.markFailed('Result Need FU = '+ result)
}

