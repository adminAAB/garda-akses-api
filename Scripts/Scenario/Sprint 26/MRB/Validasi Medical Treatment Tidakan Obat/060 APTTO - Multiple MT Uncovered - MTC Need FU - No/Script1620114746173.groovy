import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String expected = 'Covered'

String MaternityMedicalTreatment = '[["A38",418,0,"","500000","Terapi - MTC - Covered","Scarlet fever","Covered",0,"","","","",1,"1","TTO","04/05/2021 12:47","",0,null,"0","0",0,null,0,"Test QC","Coverage : Dijaminkan\n\nReason : \n -\n\nDokumen yang perlu dilengkapi : \n - \n",null,"Tindakan Medis/Terapi","1"]]'
String MedicalTreatmentConfirmation = '[{"ConfirmationType":"AM","PICDesc":"Account Manager","PICName":"AIB","NameDesc":"Arrio Probo Wibowo","ConfirmationDate":"04/05/2021 14:02","Confirmation":"HLD","ConfirmationDesc":"Hold","Remarks":"","FollowUpTime":"0","Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"","Reason":null,"ConfirmationCategory":null,"DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":1,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","IsFromTicketBefore":0,"IsEdit":1,"NeedFU":0,"IsCito":0,"GuidConfirmation":"ce5b9824-4308-4e40-b7fc-75d8297f33a7"},{"ConfirmationType":"Doctor","PICDesc":"Doctor","PICName":"IEA","NameDesc":"Irna Putri Perdana","ConfirmationDate":"04/05/2021 14:20","Confirmation":"HLD","ConfirmationDesc":"Hold","Remarks":"","FollowUpTime":"1","Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"","Reason":"FUR020","ConfirmationCategory":null,"DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","IsFromTicketBefore":0,"IsEdit":"1","NeedFU":0,"IsCito":0,"IsPrevious":0,"GuidConfirmation":"0c5d10f3-4aa9-4c6a-867a-8d9f04cc452a"}]'

String MemberNo = 'A/00098696'
String TreatmentDate = '2021-05-03'
String DiagnosisCode = 'A38'
String ProductType = 'IP'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'TJKRI47'
String PatientPhone = '087841632020'
String CallerName = ''
String TreatmentRoom = 'ODC (One Day Care)'
String TreatmentRoomAmount = '300000'
String DoctorName = 'Ugha Anugerah'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '300000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = ''
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'PRI'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKRI47'
String Guid = '8624323fd7bf4f6dad83186900641d8c66e64d0afc7d29c8d3e5efb8f5cf52f9'
String TicketNo = ''
String GLType = 'GL Awal'
String AccountManager = ''
String PreviousGuid = '31f483244f724f5fb423039c43d072468f4c972419d5942a950af2c1ca75f127'
String PreviousTrID = '2892449'
String TotalBilled = '0'
String ClientID = 'CJKFI02'
String ClassNo = '7'
String Membership = '3. CHI'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"1","0"]]'
String AllDoctors = 'Ugha Anugerah'
String Gender = 'F'
String DOB = '2013-06-22'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '8624323fd7bf4f6dad83186900641d8c66e64d0afc7d29c8d3e5efb8f5cf52f9'
String TreatmentRoomChoosen = ''
String AppropriateRBClassChoosen = ''
String AppropriateRBRateChoosen = ''
String TreatmentRoomAmountChoosen = ''
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'ODC (One Day Care)'
String AppropriateRBRate = '300000'
String TreatmentEnd = '2021-05-03'
String IsSpecialCondition = '0'
String ClientClassNo = '7'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = ''
String MaternityFamilyPlanningItem = ''
String Source = 'DOO'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '1'
String OPNO = '105966'
String IsMedicalTreatmentNeedNextValidation = '1'
String IsDocumentNeedNextValidation = '1'
String IsGLProvider = 'False'
String IsProtapCovid = '0'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('GA/CCOOutbound/ProcessGLAPPTOInTreatment', [('MemberNo') : MemberNo , ('TreatmentDate') : TreatmentDate , ('DiagnosisCode') : DiagnosisCode , ('ProductType') : ProductType , ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo , ('ProviderID') : ProviderID ,
('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability ,('UpgradeClass') : UpgradeClass , 
('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('UserPosition') : UserPosition , ('Remarks') : Remarks , ('serviceTypeID') : serviceTypeID ,
('CallStatusID') : CallStatusID , ('DefaultProviderID') : DefaultProviderID , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType , ('AccountManager') : AccountManager , ('PreviousGuid') : PreviousGuid , ('PreviousTrID') : PreviousTrID , ('TotalBilled') : TotalBilled , 
('ClientID') : ClientID , ('ClassNo') : ClassNo , ('Membership') : Membership , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('GLStatus') : GLStatus , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID ,
('FollowUpTaskID') : FollowUpTaskID , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen ,  ('IsReferral') : IsReferral ,
('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart , ('IsClient') : IsClient , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd , ('IsSpecialCondition') : IsSpecialCondition , ('ClientClassNo') : ClientClassNo ,
('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('TreatmentCode') : TreatmentCode , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('Source') : Source , ('NonMedicalItem') : NonMedicalItem ,
('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('IsODS') : IsODS , ('IsODC') : IsODC , ('OPNO') : OPNO , ('IsMedicalTreatmentNeedNextValidation') : IsMedicalTreatmentNeedNextValidation ,
('IsDocumentNeedNextValidation') : IsDocumentNeedNextValidation , ('IsGLProvider') : IsGLProvider , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation , ('IsProtapCovid') : IsProtapCovid ]))

def result =API.getResponseData(var).Data.MAMedicalTreatmentValidationF

if(result == true && expected == 'Need FU') {
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Result Need FU = '+ result)
	KeywordUtil.markPassed('Terkena Validasi MT')
}else if(result == false && expected == 'Covered') {
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Result Need FU = '+ result)
	KeywordUtil.markPassed('Tidak Terkena Validasi MT')
}else {
	KeywordUtil.markFailed('Validasi Diagnosis Cover Tidak Sesuai :(')
	KeywordUtil.markFailed('Result Need FU = '+ result)
}

