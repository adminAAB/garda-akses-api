import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String expected = 'Follow Up'
String confirmation = 'Head CCO - Hold'

String MemberNo = 'A/00099027'
String TreatmentDate = '2021-05-05'
String DiagnosisCode = 'A38'
String ProductType = 'IP'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'TJKSH07'
String PatientPhone = '087841632020'
String CallerName = ''
String TreatmentRoom = 'GOLD'
String TreatmentRoomAmount = '30000'
String DoctorName = 'Ugha Anugerah'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '1500000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = ''
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'PLG'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'TJKSH07'
String Guid = 'cccb914eca674b63ae5b0f6bde593557ae64612506f06e6d52933f271a74e2e7'
String TicketNo = ''
String GLType = 'GL Akhir'
String AccountManager = ''
String PreviousGuid = 'b1f0db946ed04f7d9613ffcf55a5624eeff48382fb7f25a3af330e72c48fed5f'
String PreviousTrID = '2892564'
String TotalBilled = '500000'
String ClientID = 'CJKPP0009'
String ClassNo = '13'
String Membership = '3. CHI'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"1","0"]]'
String AllDoctors = 'Ugha Anugerah'
String Gender = 'F'
String DOB = '2008-12-20'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = 'cccb914eca674b63ae5b0f6bde593557ae64612506f06e6d52933f271a74e2e7'
String TreatmentRoomChoosen = ''
String AppropriateRBClassChoosen = ''
String AppropriateRBRateChoosen = '0'
String TreatmentRoomAmountChoosen = '0'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'GOLD'
String AppropriateRBRate = '30000'
String TreatmentEnd = '2021-05-07'
String IsSpecialCondition = '0'
String ClientClassNo = '16'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = ''
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = '[["A38",358,0,"","0","Obat - Uncovered","Scarlet fever","Uncovered",0,"","","","",1,"1","OBT","05/05/2021 13:45","",0,"5","50000","250000",0,"",0,"Tes tQC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan Konfirmasi Garda Medika terkait Total Billed\n2. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi : \n - \n","Obat Sakit","Obat","1"]]'
String Source = 'DOO'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '0'
String OPNO = '105323'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '1'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def request = WS.sendRequest(findTestObject('GA/CCOOutbound/ProcessGL', [('MemberNo') : MemberNo , ('TreatmentDate') : TreatmentDate , ('DiagnosisCode') : DiagnosisCode , ('ProductType') : ProductType , ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName ,('RoomOption') : RoomOption ,
('RoomAvailability') : RoomAvailability , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('UserPosition') : UserPosition , ('Remarks') : Remarks , ('serviceTypeID') : serviceTypeID , ('CallStatusID') : CallStatusID , ('DefaultProviderID') : DefaultProviderID , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType ,
('AccountManager') : AccountManager , ('PreviousGuid') : PreviousGuid , ('PreviousTrID') : PreviousTrID , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo , ('Membership') : Membership , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('GLStatus') : GLStatus , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('TreatmentRoomChoosen') : TreatmentRoomChoosen ,
('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen , ('IsReferral') : IsReferral , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart , ('IsClient') : IsClient , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd , ('IsSpecialCondition') : IsSpecialCondition ,  ('ClientClassNo') : ClientClassNo ,
('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('TreatmentCode') : TreatmentCode , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('Source') : Source , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo ,('IsODS') : IsODS ,  ('IsODC') : IsODC ,
('OPNO') : OPNO , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation , ('IsProtapCovid') : IsProtapCovid]))

def GridMT = API.getResponseData(request).Data.MAMedicalTreatmentValidationF
if(GridMT == true) {
	KeywordUtil.markPassed('Grid Validasi Medical Treatment Muncul')
}else {
	KeywordUtil.markFailed('Grid Validasi Medical Treatment Tidak Muncul')
}

String ServiceType = 'PLG'
//String MemberNo = 'A/00099027'
String MemberName = 'ANNISA FAIHA DIANNYRA'
//String ClientID = 'CJKPP0009'
//String ProviderID = 'TJKSH07'
//String PreviousTrID = '2892564'
//String GLType = 'GL Akhir'
String StartFollowUp = '2021-05-05 14:48:59'
//String Guid = 'cccb914eca674b63ae5b0f6bde593557ae64612506f06e6d52933f271a74e2e7'
String ConfirmationItem = '[{"ConfirmationType":"CCO","PICDesc":"Contact Center","PICName":"JCR","NameDesc":"JCR - JUNITA CHARA","ConfirmationDate":"05/05/2021 14:53","Confirmation":"COV","ConfirmationDesc":"Covered","Remarks":"","FollowUpTime":"0","Channel":"","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"BC001","Reason":"","ConfirmationCategory":"Excess","DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":null,"ClientHR":null,"IsFromTicketBefore":0,"IsEdit":"0"},{"ConfirmationType":"AM","PICDesc":"AM","PICName":"AIB","NameDesc":"Arrio Probo Wibowo","ConfirmationDate":"05/05/2021 14:53","Confirmation":"COV","ConfirmationDesc":"Covered","Remarks":"","FollowUpTime":"","Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"BC001","Reason":"","ConfirmationCategory":"Document","DateTimeConfirmation":null,"DocumentGLNeeded":"BILPA","fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":null,"ClientHR":null,"IsFromTicketBefore":0,"IsEdit":"0"},{"ConfirmationType":"HeadCCO","PICDesc":"Head Contact Center","PICName":"RFZ","NameDesc":"Raissa Fauzia","ConfirmationDate":"05/05/2021 14:51","Confirmation":"HLD","ConfirmationDesc":"Hold","Remarks":"Tunggu","FollowUpTime":"1","Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"","Reason":"FUR020","ConfirmationCategory":"MedicalTreatment","DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","IsFromTicketBefore":0,"IsEdit":"0"}]'
String diagnosisConfirmationItem = '[]'
String FamilyPlanningItemConfirmation = '[]'
String email = 'ehu@beyond.asuransi.astra.co.id'
String hp = '087841632020'
String providerconfirmationitem = '[]'
String IsFUWaved = '0'
String WavedReasonCode = ''
//String ProductType = 'IP'
//String NonMedicalItem = '[]'
String claimdetails = '[]'
String ReferenceClaimLimitRecovery = ''
String MandatoryDoc = '["BILPA"]'
String medicalTreatmentConfirmationItem = '[{"DiagnosisID":"A38","MedicalTreatmentID":358,"MedicalTreatmentDesc":"Obat Sakit","Coverage":"","SurgeryType":"","Category":"OBT"}]'
String IsAPTTOInTreatment = '0'


def process = WS.sendRequest(findTestObject('GA/CCOOutbound/FollowUpTicketClaim', [('ServiceType') : ServiceType , ('MemberNo') : MemberNo , ('MemberName') : MemberName , ('ClientID') : ClientID ,
	('ProviderID') : ProviderID , ('PreviousTrID') : PreviousTrID , ('GLType') : GLType , ('StartFollowUp') : StartFollowUp , ('Guid') : Guid , ('ConfirmationItem') : ConfirmationItem ,
	('diagnosisConfirmationItem') : diagnosisConfirmationItem , ('FamilyPlanningItemConfirmation') : FamilyPlanningItemConfirmation , ('email') : email , ('hp') : hp , ('providerconfirmationitem') : providerconfirmationitem ,
	('IsFUWaved') : IsFUWaved , ('WavedReasonCode') : WavedReasonCode , ('ProductType') : ProductType , ('NonMedicalItem') : NonMedicalItem , ('claimdetails') : claimdetails , ('ReferenceClaimLimitRecovery') : ReferenceClaimLimitRecovery ,
	('MandatoryDoc') : MandatoryDoc , ('medicalTreatmentConfirmationItem') : medicalTreatmentConfirmationItem , ('IsAPTTOInTreatment') : IsAPTTOInTreatment , ]))

def FollowUpStatus = API.getResponseData(process).data.FollowUpStatus

if(FollowUpStatus == expected) {
	KeywordUtil.markPassed('Konfirmasi = '+ confirmation)
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Hasil Follow Up Status = '+ FollowUpStatus)
	KeywordUtil.markPassed('Hasil validasi Telah Sesuai')
}else{
	KeywordUtil.markFailed('Expected Result = '+ expected)
	KeywordUtil.markFailed('Hasil Follow Up Status = '+ FollowUpStatus)
	KeywordUtil.markFailed('Hasil validasi Tidak Sesuai')
}
