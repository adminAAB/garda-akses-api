import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String expected = 'Follow Up'
String confirmation = 'Account Manager - AM Not Available'

String ServiceType = 'ACP'
String MemberNo = 'A/00100192'
String MemberName = 'ADILLA WAHID'
String ClientID = 'CJKPP0009'
String ProviderID = 'TJKSH0010'
String PreviousTrID = '2892563'
String GLType = 'GL Lanjutan'
String StartFollowUp = '2021-05-05 15:58:22'
String Guid = '421d5b53a04d43a7b89ff89f7c97ee422dbd0535b7e675316ea5eab18a2fe345'
String ConfirmationItem = '[{"ConfirmationType":"AM","PICDesc":"Account Manager","PICName":"BHN","NameDesc":"Bv Taufan Hanantoro","ConfirmationDate":"06/05/2021 09:36","Confirmation":"AMNA","ConfirmationDesc":"AM not available","Remarks":"","FollowUpTime":"1","Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"","Reason":"","ConfirmationCategory":"MedicalTreatment","DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","IsFromTicketBefore":0,"IsEdit":"0"}]'
String diagnosisConfirmationItem = '[]'
String FamilyPlanningItemConfirmation = '[]'
String email = 'ehu@beyond.asuransi.astra.co.id'
String hp = '087841632020'
String providerconfirmationitem = '[]'
String IsFUWaved = '0'
String WavedReasonCode = ''
String ProductType = 'IP'
String NonMedicalItem = '[]'
String claimdetails = '[]'
String ReferenceClaimLimitRecovery = ''
String MandatoryDoc = ''
String medicalTreatmentConfirmationItem = '[{"DiagnosisID":"A38","MedicalTreatmentID":358,"MedicalTreatmentDesc":"Obat Sakit","Coverage":"Uncovered","SurgeryType":"","Category":"OBT"}]'
String IsAPTTOInTreatment = '1'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def process = WS.sendRequest(findTestObject('GA/CCOOutbound/FollowUpTicketClaim', [('ServiceType') : ServiceType , ('MemberNo') : MemberNo , ('MemberName') : MemberName , ('ClientID') : ClientID ,
	('ProviderID') : ProviderID , ('PreviousTrID') : PreviousTrID , ('GLType') : GLType , ('StartFollowUp') : StartFollowUp , ('Guid') : Guid , ('ConfirmationItem') : ConfirmationItem ,
	('diagnosisConfirmationItem') : diagnosisConfirmationItem , ('FamilyPlanningItemConfirmation') : FamilyPlanningItemConfirmation , ('email') : email , ('hp') : hp , ('providerconfirmationitem') : providerconfirmationitem ,
	('IsFUWaved') : IsFUWaved , ('WavedReasonCode') : WavedReasonCode , ('ProductType') : ProductType , ('NonMedicalItem') : NonMedicalItem , ('claimdetails') : claimdetails , ('ReferenceClaimLimitRecovery') : ReferenceClaimLimitRecovery ,
	('MandatoryDoc') : MandatoryDoc , ('medicalTreatmentConfirmationItem') : medicalTreatmentConfirmationItem , ('IsAPTTOInTreatment') : IsAPTTOInTreatment , ]))

def FollowUpStatus = API.getResponseData(process).data.FollowUpStatus

if(FollowUpStatus == expected) {
	KeywordUtil.markPassed('Konfirmasi = '+ confirmation)
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Hasil Follow Up Status = '+ FollowUpStatus)
	KeywordUtil.markPassed('Hasil validasi Telah Sesuai')
}else{
	KeywordUtil.markFailed('Expected Result = '+ expected)
	KeywordUtil.markFailed('Hasil Follow Up Status = '+ FollowUpStatus)
	KeywordUtil.markFailed('Hasil validasi Tidak Sesuai')
}