import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String expected = 'Not Need Follow Up'
String confirmation = 'Account Manager - Uncovered'

String MemberNo = 'A/00099041'
String TreatmentDate = '2021-05-06'
String DiagnosisCode = 'A38'
String ProductType = 'IP'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'OJKRI00001'
String PatientPhone = '087841632020'
String CallerName = ''
String TreatmentRoom = 'ISOLASI'
String TreatmentRoomAmount = '1680000'
String DoctorName = 'Ugha'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '1500000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '021 7455800'
String ProviderPhone = '081806221986'
String ProviderExt = '32217040'
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'GLX'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'OJKRI00001'
String Guid = '5a22e073056b40af8d864896eae971ed2a081fb25feb96aa8709fe8f93e53f6f'
String TicketNo = ''
String GLType = 'GL Lanjutan'
String AccountManager = ''
String PreviousGuid = 'b07316383f264ec5b010cc681322d1d69b95d474336f98b521072af4b148a549'
String PreviousTrID = '2892703'
String TotalBilled = '0'
String ClientID = 'CJKPP0009'
String ClassNo = '13'
String Membership = '3. CHI'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"0","0"]]'
String AllDoctors = 'Ugha'
String Gender = 'F'
String DOB = '2001-05-16'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '5a22e073056b40af8d864896eae971ed2a081fb25feb96aa8709fe8f93e53f6f'
String TreatmentRoomChoosen = 'ISOLASI'
String AppropriateRBClassChoosen = 'ISOLASI'
String AppropriateRBRateChoosen = '1680000'
String TreatmentRoomAmountChoosen = '1680000'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'ISOLASI'
String AppropriateRBRate = '1680000'
String TreatmentEnd = ''
String IsSpecialCondition = '0'
String ClientClassNo = '16'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = ''
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = '[["A38",398,0,"","500000","Terapi - Uncovered","Scarlet fever","Uncovered",0,"","","","",1,"1","TTO","06/05/2021 11:59","",0,null,"0","0",0,"",0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi : \n - \n","","Tindakan Medis/Terapi","1"]]'
String Source = 'OJKRI00001'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '0'
String OPNO = '105323'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '1'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def request = WS.sendRequest(findTestObject('GA/CCOOutbound/ProcessGL', [('MemberNo') : MemberNo , ('TreatmentDate') : TreatmentDate , ('DiagnosisCode') : DiagnosisCode , ('ProductType') : ProductType , ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName ,('RoomOption') : RoomOption ,
('RoomAvailability') : RoomAvailability , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('UserPosition') : UserPosition , ('Remarks') : Remarks , ('serviceTypeID') : serviceTypeID , ('CallStatusID') : CallStatusID , ('DefaultProviderID') : DefaultProviderID , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType ,
('AccountManager') : AccountManager , ('PreviousGuid') : PreviousGuid , ('PreviousTrID') : PreviousTrID , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo , ('Membership') : Membership , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('GLStatus') : GLStatus , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('TreatmentRoomChoosen') : TreatmentRoomChoosen ,
('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen , ('IsReferral') : IsReferral , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart , ('IsClient') : IsClient , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd , ('IsSpecialCondition') : IsSpecialCondition ,  ('ClientClassNo') : ClientClassNo ,
('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('TreatmentCode') : TreatmentCode , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('Source') : Source , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo ,('IsODS') : IsODS ,  ('IsODC') : IsODC ,
('OPNO') : OPNO , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation , ('IsProtapCovid') : IsProtapCovid]))

def GridMT = API.getResponseData(request).Data.MAMedicalTreatmentValidationF
if(GridMT == true) {
	KeywordUtil.markPassed('Grid Validasi Medical Treatment Muncul')
}else {
	KeywordUtil.markFailed('Grid Validasi Medical Treatment Tidak Muncul')
}

String ServiceType = 'GLX'
String MemberName = 'ANGELINA SACHIKA S.'
String StartFollowUp = '2021-05-06 13:27:33'
String ConfirmationItem = '[{"ConfirmationType":"Provider","PICDesc":"Provider","PICName":"RS Premier Bintaro","NameDesc":"RS Premier Bintaro","ConfirmationDate":"06/05/2021 13:38","Confirmation":"DOCR","ConfirmationDesc":"Document Received","Remarks":"","FollowUpTime":"0","Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"","Reason":"","ConfirmationCategory":"MedicalTreatment","DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","IsFromTicketBefore":0,"IsEdit":"0"}]'
String diagnosisConfirmationItem = '[]'
String FamilyPlanningItemConfirmation = '[]'
String email = 'ehu@beyond.asuransi.astra.co.id'
String hp = '087841632020'
String providerconfirmationitem = '[]'
String IsFUWaved = '0'
String WavedReasonCode = ''
String claimdetails = '[]'
String ReferenceClaimLimitRecovery = ''
String MandatoryDoc = ''
String medicalTreatmentConfirmationItem = '[{"DiagnosisID":"A38","MedicalTreatmentID":398,"MedicalTreatmentDesc":"Terapi - Uncovered","Coverage":"Guaranteed but Uncovered","SurgeryType":"","Category":"TTO"}]'
String IsAPTTOInTreatment = '0'


def process = WS.sendRequest(findTestObject('GA/CCOOutbound/FollowUpTicketClaim', [('ServiceType') : ServiceType , ('MemberNo') : MemberNo , ('MemberName') : MemberName , ('ClientID') : ClientID ,
	('ProviderID') : ProviderID , ('PreviousTrID') : PreviousTrID , ('GLType') : GLType , ('StartFollowUp') : StartFollowUp , ('Guid') : Guid , ('ConfirmationItem') : ConfirmationItem ,
	('diagnosisConfirmationItem') : diagnosisConfirmationItem , ('FamilyPlanningItemConfirmation') : FamilyPlanningItemConfirmation , ('email') : email , ('hp') : hp , ('providerconfirmationitem') : providerconfirmationitem ,
	('IsFUWaved') : IsFUWaved , ('WavedReasonCode') : WavedReasonCode , ('ProductType') : ProductType , ('NonMedicalItem') : NonMedicalItem , ('claimdetails') : claimdetails , ('ReferenceClaimLimitRecovery') : ReferenceClaimLimitRecovery ,
	('MandatoryDoc') : MandatoryDoc , ('medicalTreatmentConfirmationItem') : medicalTreatmentConfirmationItem , ('IsAPTTOInTreatment') : IsAPTTOInTreatment , ]))

def FollowUpStatus = API.getResponseData(process).data.FollowUpStatus

if(FollowUpStatus == expected) {
	KeywordUtil.markPassed('Konfirmasi = '+ confirmation)
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Hasil Follow Up Status = '+ FollowUpStatus)
	KeywordUtil.markPassed('Hasil validasi Telah Sesuai')
}else{
	KeywordUtil.markFailed('Expected Result = '+ expected)
	KeywordUtil.markFailed('Hasil Follow Up Status = '+ FollowUpStatus)
	KeywordUtil.markFailed('Hasil validasi Tidak Sesuai')
}
