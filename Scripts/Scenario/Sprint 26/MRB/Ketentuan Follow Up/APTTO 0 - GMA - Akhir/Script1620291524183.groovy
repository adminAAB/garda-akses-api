import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

String expected = 'Follow Up'
String confirmation = 'Provider - Hold'

String MemberNo = 'A/00098982'
String TreatmentDate = '2021-05-06'
String DiagnosisCode = 'A38'
String ProductType = 'IP'
String AdditionalDiagnosisInfo = '[[]]'
String ProviderID = 'OJKRI00001'
String PatientPhone = '087841632020'
String CallerName = ''
String TreatmentRoom = 'ISOLASI'
String TreatmentRoomAmount = '1680000'
String DoctorName = 'Ugha Anugerah'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '1500000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = '021 7455800'
String ProviderPhone = '081806221986'
String ProviderExt = '211'
String IsTiri = '0'
String UserPosition = ''
String Remarks = ''
String serviceTypeID = 'PLG'
String CallStatusID = 'Not Need Follow Up'
String DefaultProviderID = 'OJKRI00001'
String Guid = '82f26216ed5a4a17a832e15b476ff57ee7e1cef98be78e47d90ace9c6610543d'
String TicketNo = ''
String GLType = 'GL Akhir'
String AccountManager = ''
String PreviousGuid = 'c4748e17b166431096223fcd756aba9df5284aa42e94fea58ee1250ebf95da86'
String PreviousTrID = '2892704'
String TotalBilled = '50000'
String ClientID = 'CJKPP0009'
String ClassNo = '5'
String Membership = '1. EMP'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"1","0"]]'
String AllDoctors = 'Ugha Anugerah'
String Gender = 'F'
String DOB = '1977-03-12'
String NewMemberName = ''
String GLStatus = 'Not Produce - Need Follow Up'
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = '82f26216ed5a4a17a832e15b476ff57ee7e1cef98be78e47d90ace9c6610543d'
String TreatmentRoomChoosen = 'ISOLASI'
String AppropriateRBClassChoosen = 'ISOLASI'
String AppropriateRBRateChoosen = '1680000'
String TreatmentRoomAmountChoosen = '1680000'
String IsReferral = '0'
String ReferralReasonCode = ''
String CallInStart = ''
String IsClient = '1'
String AppropriateRBClass = 'ISOLASI'
String AppropriateRBRate = '1680000'
String TreatmentEnd = '2021-05-08'
String IsSpecialCondition = '0'
String ClientClassNo = '15'
String SecondaryDiagnosisCode = '[]'
String TreatmentCode = ''
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = '[["A38",358,0,"","0","Obat - Uncovered","Scarlet fever","Uncovered",0,"","","","",1,"1","OBT","06/05/2021 12:02","",0,"5","55555","277775",0,"",0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan Konfirmasi Garda Medika terkait Total Billed\n2. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi : \n - \n","Obat","Obat","1"]]'
String Source = 'OJKRI00001'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String IsODS = '0'
String IsODC = '0'
String OPNO = '105323'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '1'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def request = WS.sendRequest(findTestObject('GA/CCOOutbound/ProcessGL', [('MemberNo') : MemberNo , ('TreatmentDate') : TreatmentDate , ('DiagnosisCode') : DiagnosisCode , ('ProductType') : ProductType , ('AdditionalDiagnosisInfo') : AdditionalDiagnosisInfo , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName ,('RoomOption') : RoomOption ,
('RoomAvailability') : RoomAvailability , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('UserPosition') : UserPosition , ('Remarks') : Remarks , ('serviceTypeID') : serviceTypeID , ('CallStatusID') : CallStatusID , ('DefaultProviderID') : DefaultProviderID , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType ,
('AccountManager') : AccountManager , ('PreviousGuid') : PreviousGuid , ('PreviousTrID') : PreviousTrID , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo , ('Membership') : Membership , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('GLStatus') : GLStatus , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('FollowUpTaskID') : FollowUpTaskID , ('TreatmentRoomChoosen') : TreatmentRoomChoosen ,
('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen , ('IsReferral') : IsReferral , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart , ('IsClient') : IsClient , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd , ('IsSpecialCondition') : IsSpecialCondition ,  ('ClientClassNo') : ClientClassNo ,
('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('TreatmentCode') : TreatmentCode , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('Source') : Source , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo ,('IsODS') : IsODS ,  ('IsODC') : IsODC ,
('OPNO') : OPNO , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation , ('IsProtapCovid') : IsProtapCovid]))

def GridMT = API.getResponseData(request).Data.MAMedicalTreatmentValidationF
if(GridMT == true) {
	KeywordUtil.markPassed('Grid Validasi Medical Treatment Muncul')
}else {
	KeywordUtil.markFailed('Grid Validasi Medical Treatment Tidak Muncul')
}

String ServiceType = 'PLG'
String MemberName = 'ADI SUBAGIONO'
String StartFollowUp = '2021-05-06 15:52:17'
String ConfirmationItem = '[{"ConfirmationType":"CCO","PICDesc":"Contact Center","PICName":"JCR","NameDesc":"JCR - JUNITA CHARA","ConfirmationDate":"06/05/2021 16:01","Confirmation":"COV","ConfirmationDesc":"Covered","Remarks":"","FollowUpTime":"0","Channel":"","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"BC001","Reason":"","ConfirmationCategory":"Excess","DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":null,"ClientHR":null,"IsFromTicketBefore":0,"IsEdit":"0"},{"ConfirmationType":"AM","PICDesc":"AM","PICName":"AIB","NameDesc":"Arrio Probo Wibowo","ConfirmationDate":"06/05/2021 16:00","Confirmation":"COV","ConfirmationDesc":"Covered","Remarks":"","FollowUpTime":"","Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"BC001","Reason":"","ConfirmationCategory":"Document","DateTimeConfirmation":null,"DocumentGLNeeded":"BILPA","fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":null,"ClientHR":null,"IsFromTicketBefore":0,"IsEdit":"0"},{"ConfirmationType":"Provider","PICDesc":"Provider","PICName":"RS Premier Bintaro","NameDesc":"RS Premier Bintaro","ConfirmationDate":"06/05/2021 15:59","Confirmation":"HLD","ConfirmationDesc":"Hold","Remarks":"","FollowUpTime":"0","Channel":"CALL","PreviousLimit":null,"CurrentLimit":null,"BenefitCoverage":"","Reason":"FUR006","ConfirmationCategory":"MedicalTreatment","DateTimeConfirmation":null,"DocumentGLNeeded":null,"fileCLByte":null,"fileNames":null,"isPrevious":0,"FollowUpTicketDocuments":[],"FollowUpTicketDocumentsNeeded":[],"Payer":"","ClientHR":"","IsFromTicketBefore":0,"IsEdit":"0"}]'
String diagnosisConfirmationItem = '[]'
String FamilyPlanningItemConfirmation = '[]'
String email = 'ehu@beyond.asuransi.astra.co.id'
String hp = '087841632020'
String providerconfirmationitem = '[]'
String IsFUWaved = '0'
String WavedReasonCode = ''
String claimdetails = '[]'
String ReferenceClaimLimitRecovery = ''
String MandatoryDoc = '["BILPA"]'
String medicalTreatmentConfirmationItem = '[{"DiagnosisID":"A38","MedicalTreatmentID":358,"MedicalTreatmentDesc":"Obat","Coverage":"","SurgeryType":"","Category":"OBT"}]'
String IsAPTTOInTreatment = '0'


def process = WS.sendRequest(findTestObject('GA/CCOOutbound/FollowUpTicketClaim', [('ServiceType') : ServiceType , ('MemberNo') : MemberNo , ('MemberName') : MemberName , ('ClientID') : ClientID ,
	('ProviderID') : ProviderID , ('PreviousTrID') : PreviousTrID , ('GLType') : GLType , ('StartFollowUp') : StartFollowUp , ('Guid') : Guid , ('ConfirmationItem') : ConfirmationItem ,
	('diagnosisConfirmationItem') : diagnosisConfirmationItem , ('FamilyPlanningItemConfirmation') : FamilyPlanningItemConfirmation , ('email') : email , ('hp') : hp , ('providerconfirmationitem') : providerconfirmationitem ,
	('IsFUWaved') : IsFUWaved , ('WavedReasonCode') : WavedReasonCode , ('ProductType') : ProductType , ('NonMedicalItem') : NonMedicalItem , ('claimdetails') : claimdetails , ('ReferenceClaimLimitRecovery') : ReferenceClaimLimitRecovery ,
	('MandatoryDoc') : MandatoryDoc , ('medicalTreatmentConfirmationItem') : medicalTreatmentConfirmationItem , ('IsAPTTOInTreatment') : IsAPTTOInTreatment , ]))

def FollowUpStatus = API.getResponseData(process).data.FollowUpStatus

if(FollowUpStatus == expected) {
	KeywordUtil.markPassed('Konfirmasi = '+ confirmation)
	KeywordUtil.markPassed('Expected Result = '+ expected)
	KeywordUtil.markPassed('Hasil Follow Up Status = '+ FollowUpStatus)
	KeywordUtil.markPassed('Hasil validasi Telah Sesuai')
}else{
	KeywordUtil.markFailed('Expected Result = '+ expected)
	KeywordUtil.markFailed('Hasil Follow Up Status = '+ FollowUpStatus)
	KeywordUtil.markFailed('Hasil validasi Tidak Sesuai')
}
