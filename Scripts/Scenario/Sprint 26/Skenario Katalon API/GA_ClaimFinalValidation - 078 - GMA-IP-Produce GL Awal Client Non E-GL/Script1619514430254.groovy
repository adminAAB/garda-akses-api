import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String AdminName = ''
String IsGLProvider = 'False'
String MemberNo = 'N/28806'
String PreviousTrID = ''
String TreatmentDate = '2021-04-27'
String ProductType = 'IP'
String DiagnosisCode = 'A38'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'TJKRP0146'
String PatientPhone = '081808623854'
String CallerName = 'Maste'
String TreatmentRoom = 'KELAS III'
String TreatmentRoomAmount = '190000'
String DoctorName = 'tsg'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String BenefitAmount = '400000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = '197'
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String Guid = 'a74f8629f43e4397b923cc285d0d6e7ab0eefb8a7f393340c80f37effdccf021'
String TicketNo = 'CL212478'
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'CJKPU01'
String ClassNo = '2'
String MembershipType = '1. EMP'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"1","0"]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String AllDoctors = 'tsg'
String Gender = 'F'
String DOB = '1987-12-01'
String NewMemberName = ''
String PreviousGuid = ''
String GLStatus = ''
String EmpMemberNo = ''
String NMEmpID = ''
String FollowUpTaskID = 'a74f8629f43e4397b923cc285d0d6e7ab0eefb8a7f393340c80f37effdccf021'
String IsClient = '0'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'KELAS III'
String AppropriateRBRate = '190000'
String TreatmentEnd = ''
String AppropriateRBClassChoosen = 'KELAS III'
String AppropriateRBRateChoosen = '190000'
String TreatmentRoomChoosen = 'KELAS III'
String TreatmentRoomAmountChoosen = '190000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = '2021-04-27 08:09:00'
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '1'
String UserPosition = 'Customer Service'
String OPNO = '88849'
String MaternityMedicalTreatment = ''
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String IsFromProcessButton = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = ''
String IsODS = '0'
String IsODC = '0'
String IsProducttypeChange = '1'
String IsTreatmentPeriodChange = '1'
String IsMaternityTreatmentChange = '1'
String IsRoomOptionChange = '1'
String IsTreatmentRBClassChange = '1'
String IsODSODCChange = '1'
String IsDocValidityChange = '1'
String IsDocTypeChange = '1'
String suspectDouble = '0'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '0'

WebUI.callTestCase(findTestCase('Pages/GMA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GMA/CreateTreatmentGL/GA_ClaimFinalValidation', [('authorization') : GlobalVariable.authorization     	      
 ,('AdminName'):AdminName	,('IsGLProvider'):IsGLProvider	,('MemberNo'):MemberNo	,('PreviousTrID'):PreviousTrID	,('TreatmentDate'):TreatmentDate
,('ProductType'):ProductType	,('DiagnosisCode'):DiagnosisCode	,('DiagnosisAdditionalInfo'):DiagnosisAdditionalInfo	,('ProviderID'):ProviderID	,('PatientPhone'):PatientPhone
,('CallerName'):CallerName	,('TreatmentRoom'):TreatmentRoom	,('TreatmentRoomAmount'):TreatmentRoomAmount	,('DoctorName'):DoctorName	,('RoomOption'):RoomOption
,('RoomAvailability'):RoomAvailability	,('UpgradeClass'):UpgradeClass	,('BenefitAmount'):BenefitAmount	,('ProviderEmail'):ProviderEmail	,('ProviderFax'):ProviderFax
,('ProviderPhone'):ProviderPhone	,('ProviderExt'):ProviderExt	,('IsTiri'):IsTiri	,('DefaultProviderID'):DefaultProviderID	,('Remarks'):Remarks
,('Guid'):Guid	,('TicketNo'):TicketNo	,('GLType'):GLType	,('AccountManager'):AccountManager	,('TotalBilled'):TotalBilled
,('ClientID'):ClientID	,('ClassNo'):ClassNo	,('MembershipType'):MembershipType	,('AllDiagnosis'):AllDiagnosis	,('AllDiagnosisAdditionalInfo'):AllDiagnosisAdditionalInfo
,('AllDoctors'):AllDoctors	,('Gender'):Gender	,('DOB'):DOB	,('NewMemberName'):NewMemberName	,('PreviousGuid'):PreviousGuid
,('GLStatus'):GLStatus	,('EmpMemberNo'):EmpMemberNo	,('NMEmpID'):NMEmpID	,('FollowUpTaskID'):FollowUpTaskID	,('IsClient'):IsClient
,('CallStatusID'):CallStatusID	,('AppropriateRBClass'):AppropriateRBClass	,('AppropriateRBRate'):AppropriateRBRate	,('TreatmentEnd'):TreatmentEnd	,('AppropriateRBClassChoosen'):AppropriateRBClassChoosen
,('AppropriateRBRateChoosen'):AppropriateRBRateChoosen	,('TreatmentRoomChoosen'):TreatmentRoomChoosen	,('TreatmentRoomAmountChoosen'):TreatmentRoomAmountChoosen	,('IsReferral'):IsReferral	,('IsSpecialCondition'):IsSpecialCondition
,('ReferralReasonCode'):ReferralReasonCode	,('CallInStart'):CallInStart	,('SecondaryDiagnosisCode'):SecondaryDiagnosisCode	,('ClientClassNo'):ClientClassNo	,('UserPosition'):UserPosition
,('OPNO'):OPNO	,('MaternityMedicalTreatment'):MaternityMedicalTreatment	,('MaternityFamilyPlanningItem'):MaternityFamilyPlanningItem	,('TreatmentCode'):TreatmentCode	,('IsFromProcessButton'):IsFromProcessButton
,('NonMedicalItem'):NonMedicalItem	,('IsDiagnosisQuestionNotRegistered'):IsDiagnosisQuestionNotRegistered	,('MedicalTreatmentAdditionalQuestion'):MedicalTreatmentAdditionalQuestion	,('IsODS'):IsODS	,('IsODC'):IsODC
,('IsProducttypeChange'):IsProducttypeChange	,('IsTreatmentPeriodChange'):IsTreatmentPeriodChange	,('IsMaternityTreatmentChange'):IsMaternityTreatmentChange	,('IsRoomOptionChange'):IsRoomOptionChange	,('IsTreatmentRBClassChange'):IsTreatmentRBClassChange
,('IsODSODCChange'):IsODSODCChange	,('IsDocValidityChange'):IsDocValidityChange	,('IsDocTypeChange'):IsDocTypeChange	,('suspectDouble'):suspectDouble	,('MedicalTreatmentConfirmation'):MedicalTreatmentConfirmation
,('IsProtapCovid'):IsProtapCovid ]))


API.Note(API.getResponseData(var).Status)
//API.Note(API.getResponseData(var).Data)
//API.Note(API.getResponseData(var).Data.ClaimValidationMessage)
//API.Note(API.getResponseData(var).Data.ClaimValidationNonCoverReason)
//API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
//API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailed("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}

def AllValidation=API.getResponseData(var).Data.ClaimValidationResult
def AllDiagnosisFU=API.getResponseData(var).Data.ClaimCallInStatus

if (AllValidation!=null){
	if (AllValidation=='COVER'){
		KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimValidationResult)
		KeywordUtil.markPassed('Covered')
		if (AllDiagnosisFU=='Not Need Follow Up'){
			KeywordUtil.markPassed(API.getResponseData(var).Data.ClaimCallInStatus)
			KeywordUtil.markPassed('Produce')
		} else {
			KeywordUtil.markFailed(API.getResponseData(var).Data.ClaimCallInStatus)
			KeywordUtil.markFailed('Not Produce Need Follow Up')
		}
	} else {
		KeywordUtil.markFailed(API.getResponseData(var).Data.ClaimValidationResult)
		KeywordUtil.markFailed('Non Covered')
	}
} else {
	KeywordUtil.markFailed(API.getResponseData(var).Data.ClaimValidationResult)
	KeywordUtil.markFailed('NULL')
}

