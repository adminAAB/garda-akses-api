import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

String AdminName = ''
String IsGLProvider = 'False'
String MemberNo = 'V/03363'
String PreviousTrID = ''
String TreatmentDate = '2021-04-27'
String ProductType = 'IP'
String DiagnosisCode = 'A38'
String DiagnosisAdditionalInfo = ''
String ProviderID = 'TJKRP0146'
String PatientPhone = '081808623854'
String CallerName = 'Maste'
String TreatmentRoom = 'HCU'
String DoctorName = 'Arief Rachman'
String RoomOption = 'On Plan'
String RoomAvailability = 'NONE'
String UpgradeClass = 'NONE'
String TreatmentRoomAmount = '850000'
String BenefitAmount = '900000'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhone = '081806221986'
String ProviderExt = '197'
String IsTiri = '0'
String DefaultProviderID = ''
String Remarks = ''
String Guid = 'bca39d4fddc74796b583a06834a130dc439e87a1c7774f510408d6d5537cc3f2'
String TicketNo = 'CL212473'
String GLType = 'GL Awal'
String AccountManager = ''
String TotalBilled = '0'
String ClientID = 'C01AA00001'
String ClassNo = '4'
String MembershipType = '1. EMP'
String AllDiagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"1","0"]]'
String AllDiagnosisAdditionalInfo = '[[]]'
String AllDoctors = 'Arief Rachman'
String Gender = 'F'
String DOB = '1986-09-10'
String NewMemberName = ''
String PreviousGuid = ''
String GLStatus = ''
String IsClient = '1'
String CallStatusID = 'Not Need Follow Up'
String AppropriateRBClass = 'HCU'
String AppropriateRBRate = '850000'
String AppropriateRBClassChoosen = 'HCU'
String AppropriateRBRateChoosen = '850000'
String TreatmentRoomChoosen = 'HCU'
String TreatmentRoomAmountChoosen = '850000'
String TreatmentEnd = ''
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = '2021-04-27 08:09:00'
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '3'
String UserPosition = 'Customer Service'
String OPNO = '106186'
String MaternityMedicalTreatment = ''
String MaternityFamilyPlanningItem = ''
String EmpMemberNo = ''
String NMEmpID = ''
String TreatmentCode = ''
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = ''
String IsODS = '0'
String IsODC = '0'
String IsProducttypeChange = '1'
String IsTreatmentPeriodChange = '1'
String IsMaternityTreatmentChange = '1'
String IsRoomOptionChange = '1'
String IsTreatmentRBClassChange = '1'
String IsODSODCChange = '1'
String IsDocValidityChange = '1'
String IsDocTypeChange = '1'
String suspectDouble = '0'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '0'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/GA_CreateClientHealthClaimGardaAkses', [('authorization') : GlobalVariable.authorization     	      
 ,('AdminName'):AdminName	,('IsGLProvider'):IsGLProvider	,('MemberNo'):MemberNo	,('PreviousTrID'):PreviousTrID	,('TreatmentDate'):TreatmentDate
,('ProductType'):ProductType	,('DiagnosisCode'):DiagnosisCode	,('DiagnosisAdditionalInfo'):DiagnosisAdditionalInfo	,('ProviderID'):ProviderID	,('PatientPhone'):PatientPhone
,('CallerName'):CallerName	,('TreatmentRoom'):TreatmentRoom	,('DoctorName'):DoctorName	,('RoomOption'):RoomOption	,('RoomAvailability'):RoomAvailability
,('UpgradeClass'):UpgradeClass	,('TreatmentRoomAmount'):TreatmentRoomAmount	,('BenefitAmount'):BenefitAmount	,('ProviderEmail'):ProviderEmail	,('ProviderFax'):ProviderFax
,('ProviderPhone'):ProviderPhone	,('ProviderExt'):ProviderExt	,('IsTiri'):IsTiri	,('DefaultProviderID'):DefaultProviderID	,('Remarks'):Remarks
,('Guid'):Guid	,('TicketNo'):TicketNo	,('GLType'):GLType	,('AccountManager'):AccountManager	,('TotalBilled'):TotalBilled
,('ClientID'):ClientID	,('ClassNo'):ClassNo	,('MembershipType'):MembershipType	,('AllDiagnosis'):AllDiagnosis	,('AllDiagnosisAdditionalInfo'):AllDiagnosisAdditionalInfo
,('AllDoctors'):AllDoctors	,('Gender'):Gender	,('DOB'):DOB	,('NewMemberName'):NewMemberName	,('PreviousGuid'):PreviousGuid
,('GLStatus'):GLStatus	,('IsClient'):IsClient	,('CallStatusID'):CallStatusID	,('AppropriateRBClass'):AppropriateRBClass	,('AppropriateRBRate'):AppropriateRBRate
,('AppropriateRBClassChoosen'):AppropriateRBClassChoosen	,('AppropriateRBRateChoosen'):AppropriateRBRateChoosen	,('TreatmentRoomChoosen'):TreatmentRoomChoosen	,('TreatmentRoomAmountChoosen'):TreatmentRoomAmountChoosen	,('TreatmentEnd'):TreatmentEnd
,('IsReferral'):IsReferral	,('IsSpecialCondition'):IsSpecialCondition	,('ReferralReasonCode'):ReferralReasonCode	,('CallInStart'):CallInStart	,('SecondaryDiagnosisCode'):SecondaryDiagnosisCode
,('ClientClassNo'):ClientClassNo	,('UserPosition'):UserPosition	,('OPNO'):OPNO	,('MaternityMedicalTreatment'):MaternityMedicalTreatment	,('MaternityFamilyPlanningItem'):MaternityFamilyPlanningItem
,('EmpMemberNo'):EmpMemberNo	,('NMEmpID'):NMEmpID	,('TreatmentCode'):TreatmentCode	,('NonMedicalItem'):NonMedicalItem	,('IsDiagnosisQuestionNotRegistered'):IsDiagnosisQuestionNotRegistered
,('MedicalTreatmentAdditionalQuestion'):MedicalTreatmentAdditionalQuestion	,('IsODS'):IsODS	,('IsODC'):IsODC	,('IsProducttypeChange'):IsProducttypeChange	,('IsTreatmentPeriodChange'):IsTreatmentPeriodChange
,('IsMaternityTreatmentChange'):IsMaternityTreatmentChange	,('IsRoomOptionChange'):IsRoomOptionChange	,('IsTreatmentRBClassChange'):IsTreatmentRBClassChange	,('IsODSODCChange'):IsODSODCChange	,('IsDocValidityChange'):IsDocValidityChange
,('IsDocTypeChange'):IsDocTypeChange	,('suspectDouble'):suspectDouble	,('MedicalTreatmentConfirmation'):MedicalTreatmentConfirmation	,('IsProtapCovid'):IsProtapCovid	
 ]))

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailed("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}

/*
API.Note(API.getResponseData(var).Status)
API.Note(API.getResponseData(var).Data)
API.Note(API.getResponseData(var).Data2)

//Cek Hasil Validasi Diagnosa
 * 
def AllValidation=API.getResponseData(var).Data2.ClaimValidationResult

if (AllValidation=="COVER"){
	KeywordUtil.markPassed('GL Produce Terbentuk')
	KeywordUtil.markPassed(API.getResponseData(var).Data.CNO)
	KeywordUtil.markPassed(API.getResponseData(var).Data2.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data2.ClaimValidationFinalResult)
} else {
	KeywordUtil.markFailed('GL Produce Tidak Terbentuk')
	KeywordUtil.markPassed(API.getResponseData(var).Data.CNO)
	KeywordUtil.markPassed(API.getResponseData(var).Data2.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var).Data2.ClaimValidationFinalResult)
}
*/
