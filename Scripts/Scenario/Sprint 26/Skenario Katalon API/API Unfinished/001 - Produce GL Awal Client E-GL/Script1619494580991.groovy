import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.kms.katalon.core.util.KeywordUtil

def var00 = WS.sendRequest(findTestObject('Object Repository/GA/CreateTicket/GenerateGUID', [('authorization') : GlobalVariable.authorization]))

String pageSalt = 'MLQCN'

def var01 = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/GetApplicationPageGuid', [('authorization') : GlobalVariable.authorization
,('pageSalt'):pageSalt]))

String pIsGLProvider = '0'
String Guid = API.getResponseData(var01).data
String TicketNo = ''
String MemberNo = 'V/03363'
String MemberName = 'VERA PUSPITA PERDANA PUTRI'
String Membership = '1. EMP'
String ClassNo = '4'
String ClientID = 'C01AA00001'
String ClientName = 'Asuransi Astra Buana'
String BenefitInfo = 'Benefit boleh diketahui oleh Peserta'
String EmpID = '1739'
String GLType = 'GL Awal'
String DOB = '1986-09-10'
String ProductType = 'IP'
String TreatmentStart = '2021-04-27'
String TreatmentEnd = ''
String ProviderID = 'TJKRP0146'
String ProviderName = 'RS Permata Depok'
String ProviderEmail = 'ehu@beyond.asuransi.astra.co.id'
String ProviderFax = ''
String ProviderPhoneNo = '081806221986'
String ProviderExt = '197'
String IsPassedAway = '0'
String Diagnosis = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"1","0"]]'
String AccountManager = ''
String Doctor = 'Arief Rachman'
String Remarks = ''
String AppropriateRBClass = 'HCU'
String TreatmentRBClass = 'HCU'
String AppropriateRBRate = '850000'
String TreatmentRBRate = '850000'
String RoomOption = 'On Plan'
String RemainingLimit = '45000000'
String RemainingLimitType = ''
String RBScheme = 'R&B Scheme : Benefit Limit R&B Scheme Info : Benefit Limit Rp. 900,000.00 Benefit Kamar Khusus :  3. HS10 - Biaya Perawatan Intensif ICU/ICCU 2. HS10 - Biaya Perawatan Intensif ICU/ICCU 1. HS10 - Biaya Perawatan Intensif ICU/ICCU'
String ICULimit = ''
String IsolationLimit = ''
String TotalBilled = '0'
String AdditionalDiagnosisInfo = '[[]]'
String FamilyPhone = '081808623854'
String PreviousGuid = ''
String PreviousTrID = ''
String GLStatus = ''
String CallerName = 'Maste'
String IsPreAdmission = '0'
String IsInteruptedCalls = '0'
String IsReject = '0'
String NMMemberType = '1. EMP'
String NMMemberName = ''
String NMEmpID = ''
String NMEmployee = ''
String NMClassification = '4'
String NMClientId = 'C01AA00001'
String NMDOB = '2021-04-27'
String NMGender = 'F'
String NMIsNewBorn = '0'
String NMIsTwin = '0'
String IsNeedFollowUpF = '0'
String AppropriateRBClassActual = 'HCU'
String AppropriateRBRateActual = '850000'
String TreatmentRBClassActual = 'HCU'
String TreatmentRBRateActual = '850000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String SpecialConditionReason = ''
String Actor = 'heo'
String isFromCreateTicket = 'true'
String inboundCallGuid = API.getResponseData(var00).guid
String TreatmentTT = 'Inpatient'
String AppropriateTT = 'Inpatient'
String ClientClassNo = '3'
String MaternityScheme = 'Birth Delivery Type:  CoveredQty:  Always Guaranted:  Maternity Type:  Coverage Type: Full Coverage'
String MaternityPackagePrice = '0'
String MaternityFamilyPlanningItem = ''
String MaternityMedicalTreatment = ''
String MaternityTreatmentCode = ''
String ProviderAdminName = ''
String TreatmentDuration = ''
String PassedAwayDate = ''
String DoctorOnCall = ''
String LastCondition = ''
String LastTreatment = ''
String ResusitasiF = '0'
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String AdditionalMedicalTreatment = ''
String IsODS = '0'
String IsODC = '0'
String DoctorID = '1388'
String SchemeType = 'Benefit Limit'
String GMRBClass = ''
String ProviderRBClass = ''
String AmountLimit = '900000'
String RBAmount = '0'
String IsCovid = '0'
String IsAPTTO = '0'

WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/SaveTempGL', [('authorization') : GlobalVariable.authorization
,('pIsGLProvider'):pIsGLProvider	,('Guid'):Guid	,('TicketNo'):TicketNo	,('MemberNo'):MemberNo	,('MemberName'):MemberName
,('Membership'):Membership	,('ClassNo'):ClassNo	,('ClientID'):ClientID	,('ClientName'):ClientName	,('BenefitInfo'):BenefitInfo
,('EmpID'):EmpID	,('GLType'):GLType	,('DOB'):DOB	,('ProductType'):ProductType	,('TreatmentStart'):TreatmentStart
,('TreatmentEnd'):TreatmentEnd	,('ProviderID'):ProviderID	,('ProviderName'):ProviderName	,('ProviderEmail'):ProviderEmail	,('ProviderFax'):ProviderFax
,('ProviderPhoneNo'):ProviderPhoneNo	,('ProviderExt'):ProviderExt	,('IsPassedAway'):IsPassedAway	,('Diagnosis'):Diagnosis	,('AccountManager'):AccountManager
,('Doctor'):Doctor	,('Remarks'):Remarks	,('AppropriateRBClass'):AppropriateRBClass	,('TreatmentRBClass'):TreatmentRBClass	,('AppropriateRBRate'):AppropriateRBRate
,('TreatmentRBRate'):TreatmentRBRate	,('RoomOption'):RoomOption	,('RemainingLimit'):RemainingLimit	,('RemainingLimitType'):RemainingLimitType	,('RBScheme'):RBScheme
,('ICULimit'):ICULimit	,('IsolationLimit'):IsolationLimit	,('TotalBilled'):TotalBilled	,('AdditionalDiagnosisInfo'):AdditionalDiagnosisInfo	,('FamilyPhone'):FamilyPhone
,('PreviousGuid'):PreviousGuid	,('PreviousTrID'):PreviousTrID	,('GLStatus'):GLStatus	,('CallerName'):CallerName	,('IsPreAdmission'):IsPreAdmission
,('IsInteruptedCalls'):IsInteruptedCalls	,('IsReject'):IsReject	,('NMMemberType'):NMMemberType	,('NMMemberName'):NMMemberName	,('NMEmpID'):NMEmpID
,('NMEmployee'):NMEmployee	,('NMClassification'):NMClassification	,('NMClientId'):NMClientId	,('NMDOB'):NMDOB	,('NMGender'):NMGender
,('NMIsNewBorn'):NMIsNewBorn	,('NMIsTwin'):NMIsTwin	,('IsNeedFollowUpF'):IsNeedFollowUpF	,('AppropriateRBClassActual'):AppropriateRBClassActual	,('AppropriateRBRateActual'):AppropriateRBRateActual
,('TreatmentRBClassActual'):TreatmentRBClassActual	,('TreatmentRBRateActual'):TreatmentRBRateActual	,('IsReferral'):IsReferral	,('IsSpecialCondition'):IsSpecialCondition	,('ReferralReasonCode'):ReferralReasonCode
,('SpecialConditionReason'):SpecialConditionReason	,('Actor'):Actor	,('isFromCreateTicket'):isFromCreateTicket	,('inboundCallGuid'):inboundCallGuid	,('TreatmentTT'):TreatmentTT
,('AppropriateTT'):AppropriateTT	,('ClientClassNo'):ClientClassNo	,('MaternityScheme'):MaternityScheme	,('MaternityPackagePrice'):MaternityPackagePrice	,('MaternityFamilyPlanningItem'):MaternityFamilyPlanningItem
,('MaternityMedicalTreatment'):MaternityMedicalTreatment	,('MaternityTreatmentCode'):MaternityTreatmentCode	,('ProviderAdminName'):ProviderAdminName	,('TreatmentDuration'):TreatmentDuration	,('PassedAwayDate'):PassedAwayDate
,('DoctorOnCall'):DoctorOnCall	,('LastCondition'):LastCondition	,('LastTreatment'):LastTreatment	,('ResusitasiF'):ResusitasiF	,('NonMedicalItem'):NonMedicalItem
,('IsDiagnosisQuestionNotRegistered'):IsDiagnosisQuestionNotRegistered	,('AdditionalMedicalTreatment'):AdditionalMedicalTreatment	,('IsODS'):IsODS	,('IsODC'):IsODC	,('DoctorID'):DoctorID
,('SchemeType'):SchemeType	,('GMRBClass'):GMRBClass	,('ProviderRBClass'):ProviderRBClass	,('AmountLimit'):AmountLimit	,('RBAmount'):RBAmount
,('IsCovid'):IsCovid	,('IsAPTTO'):IsAPTTO]))

String AdminName = ''

String DiagnosisCode = 'A38'
String DiagnosisAdditionalInfo = ''

String DoctorName = Doctor
String BenefitAmount = '0'

String DefaultProviderID = ''


String Guid = API.getResponseData(var).Guid
String TicketNo = API.getResponseData(var).TicketNo

String AllDiagnosis = Diagnosis
String AllDiagnosisAdditionalInfo = '[[]]'
String AllDoctors = Doctor
String Gender = 'F'
String NewMemberName = ''
String PreviousGuid = API.getResponseData(var01).data

String EmpMemberNo = ''

String CallStatusID = 'Not Need Follow Up'


String AppropriateRBClassChoosen = AppropriateRBClassActual
String AppropriateRBRateChoosen = AppropriateRBRateActual
String TreatmentRoomChoosen = 'KELAS III'
String TreatmentRoomAmountChoosen = '190000'
String IsReferral = '0'
String IsSpecialCondition = '0'
String ReferralReasonCode = ''
String CallInStart = '2021-04-12 08:29:00'
String SecondaryDiagnosisCode = '[]'
String ClientClassNo = '2'
String UserPosition = 'Customer Service'
String OPNO = '106186'
String MaternityMedicalTreatment = '[["A38",16,0,null,"999999","Fistulectomy","Scarlet fever","Covered",0,"","","","",1,"1","TTO","12/04/2021 10:21","",0,null,"0","0",0,null,0,"tsg","- \n",null,"Tindakan Medis/Terapi"]]'
String MaternityFamilyPlanningItem = ''
String TreatmentCode = ''
String NonMedicalItem = ''
String IsDiagnosisQuestionNotRegistered = '0'
String MedicalTreatmentAdditionalQuestion = '[[],[]]'
String IsODS = '0'
String IsODC = '0'
String IsProducttypeChange = '0'
String IsTreatmentPeriodChange = '0'
String IsMaternityTreatmentChange = '0'
String IsRoomOptionChange = '0'
String IsTreatmentRBClassChange = '0'
String IsODSODCChange = '0'
String IsDocValidityChange = '0'
String IsDocTypeChange = '0'
String suspectDouble = '0'
String MedicalTreatmentConfirmation = ''
String IsProtapCovid = '0'

//WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),[:])

def var2 = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/GA_FinalValidationAPTTOInTreatment', [('authorization') : GlobalVariable.authorization     	      
 , ('AdminName') : AdminName , ('IsGLProvider') : IsGLProvider , ('MemberNo') : MemberNo , ('PreviousTrID') : PreviousTrID
 , ('TreatmentDate') : TreatmentDate , ('ProductType') : ProductType , ('DiagnosisCode') : DiagnosisCode , ('DiagnosisAdditionalInfo') : DiagnosisAdditionalInfo
 , ('ProviderID') : ProviderID , ('PatientPhone') : PatientPhone , ('CallerName') : CallerName , ('TreatmentRoom') : TreatmentRoom
 , ('TreatmentRoomAmount') : TreatmentRoomAmount , ('DoctorName') : DoctorName , ('RoomOption') : RoomOption , ('RoomAvailability') : RoomAvailability
 , ('UpgradeClass') : UpgradeClass , ('BenefitAmount') : BenefitAmount , ('ProviderEmail') : ProviderEmail , ('ProviderFax') : ProviderFax
 , ('ProviderPhone') : ProviderPhone , ('ProviderExt') : ProviderExt , ('IsTiri') : IsTiri , ('DefaultProviderID') : DefaultProviderID
 , ('Remarks') : Remarks , ('Guid') : Guid , ('TicketNo') : TicketNo , ('GLType') : GLType
 , ('AccountManager') : AccountManager , ('TotalBilled') : TotalBilled , ('ClientID') : ClientID , ('ClassNo') : ClassNo
 , ('MembershipType') : MembershipType , ('AllDiagnosis') : AllDiagnosis , ('AllDoctors') : AllDoctors , ('Gender') : Gender
 , ('DOB') : DOB , ('NewMemberName') : NewMemberName , ('PreviousGuid') : PreviousGuid , ('GLStatus') : GLStatus
 , ('EmpMemberNo') : EmpMemberNo , ('NMEmpID') : NMEmpID , ('IsClient') : IsClient
 , ('CallStatusID') : CallStatusID , ('AppropriateRBClass') : AppropriateRBClass , ('AppropriateRBRate') : AppropriateRBRate , ('TreatmentEnd') : TreatmentEnd
 , ('AppropriateRBClassChoosen') : AppropriateRBClassChoosen , ('AppropriateRBRateChoosen') : AppropriateRBRateChoosen , ('TreatmentRoomChoosen') : TreatmentRoomChoosen , ('TreatmentRoomAmountChoosen') : TreatmentRoomAmountChoosen
 , ('IsReferral') : IsReferral , ('IsSpecialCondition') : IsSpecialCondition , ('ReferralReasonCode') : ReferralReasonCode , ('CallInStart') : CallInStart
 , ('SecondaryDiagnosisCode') : SecondaryDiagnosisCode , ('ClientClassNo') : ClientClassNo , ('UserPosition') : UserPosition , ('OPNO') : OPNO
 , ('MaternityMedicalTreatment') : MaternityMedicalTreatment , ('MaternityFamilyPlanningItem') : MaternityFamilyPlanningItem , ('TreatmentCode') : TreatmentCode
 , ('NonMedicalItem') : NonMedicalItem , ('IsDiagnosisQuestionNotRegistered') : IsDiagnosisQuestionNotRegistered , ('MedicalTreatmentAdditionalQuestion') : MedicalTreatmentAdditionalQuestion , ('IsODS') : IsODS
 , ('IsODC') : IsODC , ('IsProducttypeChange') : IsProducttypeChange , ('IsTreatmentPeriodChange') : IsTreatmentPeriodChange , ('IsMaternityTreatmentChange') : IsMaternityTreatmentChange
 , ('IsRoomOptionChange') : IsRoomOptionChange , ('IsTreatmentRBClassChange') : IsTreatmentRBClassChange , ('IsODSODCChange') : IsODSODCChange , ('IsDocValidityChange') : IsDocValidityChange
 , ('IsDocTypeChange') : IsDocTypeChange , ('suspectDouble') : suspectDouble , ('AllDiagnosisAdditionalInfo') : AllDiagnosisAdditionalInfo , ('MedicalTreatmentConfirmation') : MedicalTreatmentConfirmation
 , ('IsProtapCovid') : IsProtapCovid ]))


API.Note(API.getResponseData(var2).Status)
API.Note(API.getResponseData(var2).Data)
API.Note(API.getResponseData(var2).Data2)
//API.Note(API.getResponseData(var).Data.ClaimValidationMessage)
//API.Note(API.getResponseData(var).Data.ClaimValidationNonCoverReason)
//API.Note(API.getResponseData(var).Data.ClaimValidationAdditionalRemarks)
//API.Note(API.getResponseData(var).Data.AllClaimValidationNonCoverReason)


//Cek Hasil Validasi Diagnosa

def AllValidation=API.getResponseData(var2).Data2.ClaimValidationResult

if (AllValidation=="COVER"){
	KeywordUtil.markPassed('GL Produce Terbentuk')
	KeywordUtil.markPassed(API.getResponseData(var2).Data.CNO)
	KeywordUtil.markPassed(API.getResponseData(var2).Data2.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var2).Data2.ClaimValidationFinalResult)
} else {
	KeywordUtil.markFailed('GL Produce Tidak Terbentuk')
	KeywordUtil.markPassed(API.getResponseData(var2).Data.CNO)
	KeywordUtil.markPassed(API.getResponseData(var2).Data2.ClaimValidationResult)
	KeywordUtil.markPassed(API.getResponseData(var2).Data2.ClaimValidationFinalResult)
}


/*
	} else {
		KeywordUtil.markFailed('NON COVER FAILED')
	}
} else {
	KeywordUtil.markFailed('NON COVER IS NULL')
}
*/

//Cek Status API
if (API.getResponseData(var).Status) {
	KeywordUtil.markPassed('API OK')
} else {
	KeywordUtil.markFailedAndStop("Terjadi kesalahan pada API! - " + API.getResponseData(var).ErrorMessage)
}