import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import java.util.Map as Map
import groovy.json.JsonSlurper
import com.kms.katalon.core.testobject.ResponseObject


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'), [:], FailureHandling.STOP_ON_FAILURE)

String pTrID = '2500364'

String pMemberNo = 'Z/00014423'

String pGuid = '3cc8efba812a45069caf06fb97959becd409eb5156ebade9a706a2ef11fb5397'

String pUserID = 'shp.rumahsakitklinik@gmail.com'

String pIsGLProvider = 'true'

String pProviderName = ''

ResponseObject request = WS.sendRequest(findTestObject('GA/CreateTreatmentGL/GetGLData', [('pTrID') : pTrID, ('pMemberNo') : pMemberNo
            , ('pGuid') : pGuid, ('pUserID') : pUserID, ('pIsGLProvider') : pIsGLProvider, ('pProviderName') : pProviderName]))

GlobalVariable.GLData = request.getResponseBodyContent()

