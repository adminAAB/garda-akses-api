import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Map MemberData =  GlobalVariable.MemberData
String MemberNo = GlobalVariable.MemberData["MemberNo"]
String Nama = GlobalVariable.MemberData["Nama"]

println(MemberData["MemberNo"])
println(MemberData["Nama"])

println(MemberNo)
println(Nama)


GlobalVariable.MemberData["Nama"] = 'Nama Baru'
GlobalVariable.MemberData["MemberNo"] = 'Nomor Baru'
GlobalVariable.MemberData["MemberNo2"] = 'INI SEHARUSNYA TIDAK ADA'

GlobalVariable.MemberData["AdminName"] = ''
GlobalVariable.MemberData["IsGLProvider"] = 'False'
GlobalVariable.MemberData["MemberNo"] = 'A/00165126'
GlobalVariable.MemberData["PreviousTrID"] = '2897392'
GlobalVariable.MemberData["TreatmentDate"] = '2021-05-16'
GlobalVariable.MemberData["ProductType"] = 'IP'
GlobalVariable.MemberData["DiagnosisCode"] = 'A38'
GlobalVariable.MemberData["DiagnosisAdditionalInfo"] = ''
GlobalVariable.MemberData["ProviderID"] = 'TJKRI47'
GlobalVariable.MemberData["PatientPhone"] = '087841632020'
GlobalVariable.MemberData["CallerName"] = '-'
GlobalVariable.MemberData["TreatmentRoom"] = 'KELAS I'
GlobalVariable.MemberData["TreatmentRoomAmount"] = '300000'
GlobalVariable.MemberData["DoctorName"] = 'Ugha Anugerah'
GlobalVariable.MemberData["RoomOption"] = 'On Plan'
GlobalVariable.MemberData["RoomAvailability"] = 'NONE'
GlobalVariable.MemberData["UpgradeClass"] = 'NONE'
GlobalVariable.MemberData["BenefitAmount"] = '450000'
GlobalVariable.MemberData["ProviderEmail"] = 'ehu@beyond.asuransi.astra.co.id'
GlobalVariable.MemberData["ProviderFax"] = ''
GlobalVariable.MemberData["ProviderPhone"] = '081806221986'
GlobalVariable.MemberData["ProviderExt"] = ''
GlobalVariable.MemberData["IsTiri"] = '0'
GlobalVariable.MemberData["DefaultProviderID"] = ''
GlobalVariable.MemberData["Remarks"] = ''
GlobalVariable.MemberData["Guid"] = '6185856509b648c7814cadb64cbb1ac93837d210654035d88e10a5852fad5c06'
GlobalVariable.MemberData["TicketNo"] = ''
GlobalVariable.MemberData["GLType"] = 'GL Awal'
GlobalVariable.MemberData["AccountManager"] = ''
GlobalVariable.MemberData["TotalBilled"] = ''
GlobalVariable.MemberData["ClientID"] = 'CJKFI02'
GlobalVariable.MemberData["ClassNo"] = '2'
GlobalVariable.MemberData["MembershipType"] = '1. EMP'
GlobalVariable.MemberData["AllDiagnosis"] = '[[1,"A38","Scarlet fever","Initial Primary","","","","","","","","","Covered","","","","","",0,"0","0"]]'
GlobalVariable.MemberData["AllDiagnosisAdditionalInfo"] = '[[]]'
GlobalVariable.MemberData["AllDoctors"] = 'Ugha Anugerah'
GlobalVariable.MemberData["Gender"] = 'F'
GlobalVariable.MemberData["DOB"] = '1985-01-19'
GlobalVariable.MemberData["NewMemberName"] = ''
GlobalVariable.MemberData["PreviousGuid"] = '6185856509b648c7814cadb64cbb1ac93837d210654035d88e10a5852fad5c06'
GlobalVariable.MemberData["GLStatus"] = 'Produce'
GlobalVariable.MemberData["EmpMemberNo"] = ''
GlobalVariable.MemberData["NMEmpID"] = ''
GlobalVariable.MemberData["FollowUpTaskID"] = '241c581efec34f66955eba6b8a40abff7ca5efa8e9ea788c297364c8cac24549'
GlobalVariable.MemberData["IsClient"] = '1'
GlobalVariable.MemberData["CallStatusID"] = 'Not Need Follow Up'
GlobalVariable.MemberData["AppropriateRBClass"] = 'KELAS I'
GlobalVariable.MemberData["AppropriateRBRate"] = '300000'
GlobalVariable.MemberData["TreatmentEnd"] = ''
GlobalVariable.MemberData["AppropriateRBClassChoosen"] = 'KELAS I'
GlobalVariable.MemberData["AppropriateRBRateChoosen"] = '300000'
GlobalVariable.MemberData["TreatmentRoomChoosen"] = 'KELAS I'
GlobalVariable.MemberData["TreatmentRoomAmountChoosen"] = '300000'
GlobalVariable.MemberData["IsReferral"] = '0'
GlobalVariable.MemberData["IsSpecialCondition"] = '0'
GlobalVariable.MemberData["ReferralReasonCode"] = ''
GlobalVariable.MemberData["CallInStart"] = ''
GlobalVariable.MemberData["SecondaryDiagnosisCode"] = '[]'
GlobalVariable.MemberData["ClientClassNo"] = '2'
GlobalVariable.MemberData["UserPosition"] = ''
GlobalVariable.MemberData["OPNO"] = '105966'
GlobalVariable.MemberData["MaternityMedicalTreatment"] = '[["A38",398,0,null,"500000","Terapi - Uncovered","Scarlet fever","Uncovered",0,"","","","",1,"1","TTO","16/05/2021 18:46","",0,null,"0","0",0,null,0,"Test QC","Coverage : Membutuhkan Konfirmasi\n\nReason : \n1. Memerlukan konfirmasi dokter Garda Medika\n\nDokumen yang perlu dilengkapi : \n - \n",null,"Tindakan Medis/Terapi","0"]]'
GlobalVariable.MemberData["MaternityFamilyPlanningItem"] = ''
GlobalVariable.MemberData["TreatmentCode"] = ''
GlobalVariable.MemberData["IsFromProcessButton"] = '0'
GlobalVariable.MemberData["NonMedicalItem"] = ''
GlobalVariable.MemberData["IsDiagnosisQuestionNotRegistered"] = '0'
GlobalVariable.MemberData["MedicalTreatmentAdditionalQuestion"] = '[[]]'
GlobalVariable.MemberData["IsODS"] = '0'
GlobalVariable.MemberData["IsODC"] = '0'
GlobalVariable.MemberData["IsProducttypeChange"] = '0'
GlobalVariable.MemberData["IsTreatmentPeriodChange"] = '0'
GlobalVariable.MemberData["IsMaternityTreatmentChange"] = '0'
GlobalVariable.MemberData["IsRoomOptionChange"] = '0'
GlobalVariable.MemberData["IsTreatmentRBClassChange"] = '0'
GlobalVariable.MemberData["IsODSODCChange"] = '0'
GlobalVariable.MemberData["IsDocValidityChange"] = '0'
GlobalVariable.MemberData["IsDocTypeChange"] = '0'
GlobalVariable.MemberData["suspectDouble"] = '0'
GlobalVariable.MemberData["MedicalTreatmentConfirmation"] = ''
GlobalVariable.MemberData["IsProtapCovid"] = '0'
GlobalVariable.MemberData["MedicalTreatmentResultMsg"] = '- Medical Treatment memerlukan konfirmasi Dokter Garda Medika<br> &ensp;Medical Treatment : Terapi - Uncovered <br> &ensp;Coverage : Uncovered <br> &ensp;Billed : 500.000 <br> &ensp;Additional Info : - <br> &ensp;Additional Documents : - <br> <br>'



