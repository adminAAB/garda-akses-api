import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


Map MemberData =  GlobalVariable.MemberData
WebUI.callTestCase(findTestCase('Scenario/Sprint 26/New Folder/Test Global'), [:], FailureHandling.STOP_ON_FAILURE)

String AdminName = MemberData["AdminName"]
String IsGLProvider = MemberData["IsGLProvider"]
String MemberNo = MemberData["MemberNo"]
String PreviousTrID = MemberData["PreviousTrID"]
String TreatmentDate = MemberData["TreatmentDate"]
String ProductType = MemberData["ProductType"]
String DiagnosisCode = MemberData["DiagnosisCode"]
String DiagnosisAdditionalInfo = MemberData["DiagnosisAdditionalInfo"]
String ProviderID = MemberData["ProviderID"]
String PatientPhone = MemberData["PatientPhone"]
String CallerName = MemberData["CallerName"]
String TreatmentRoom = MemberData["TreatmentRoom"]
String TreatmentRoomAmount = MemberData["TreatmentRoomAmount"]
String DoctorName = MemberData["DoctorName"]
String RoomOption = MemberData["RoomOption"]
String RoomAvailability = MemberData["RoomAvailability"]
String UpgradeClass = MemberData["UpgradeClass"]
String BenefitAmount = MemberData["BenefitAmount"]
String ProviderEmail = MemberData["ProviderEmail"]
String ProviderFax = MemberData["ProviderFax"]
String ProviderPhone = MemberData["ProviderPhone"]
String ProviderExt = MemberData["ProviderExt"]
String IsTiri = MemberData["IsTiri"]
String DefaultProviderID = MemberData["DefaultProviderID"]
String Remarks = MemberData["Remarks"]
String Guid = MemberData["Guid"]
String TicketNo = MemberData["TicketNo"]
String GLType = MemberData["GLType"]
String AccountManager = MemberData["AccountManager"]
String TotalBilled = MemberData["TotalBilled"]
String ClientID = MemberData["ClientID"]
String ClassNo = MemberData["ClassNo"]
String MembershipType = MemberData["MembershipType"]
String AllDiagnosis = MemberData["AllDiagnosis"]
String AllDiagnosisAdditionalInfo = MemberData["AllDiagnosisAdditionalInfo"]
String AllDoctors = MemberData["AllDoctors"]
String Gender = MemberData["Gender"]
String DOB = MemberData["DOB"]
String NewMemberName = MemberData["NewMemberName"]
String PreviousGuid = MemberData["PreviousGuid"]
String GLStatus = MemberData["GLStatus"]
String EmpMemberNo = MemberData["EmpMemberNo"]
String NMEmpID = MemberData["NMEmpID"]
String FollowUpTaskID = MemberData["FollowUpTaskID"]
String IsClient = MemberData["IsClient"]
String CallStatusID = MemberData["CallStatusID"]
String AppropriateRBClass = MemberData["AppropriateRBClass"]
String AppropriateRBRate = MemberData["AppropriateRBRate"]
String TreatmentEnd = MemberData["TreatmentEnd"]
String AppropriateRBClassChoosen = MemberData["AppropriateRBClassChoosen"]
String AppropriateRBRateChoosen = MemberData["AppropriateRBRateChoosen"]
String TreatmentRoomChoosen = MemberData["TreatmentRoomChoosen"]
String TreatmentRoomAmountChoosen = MemberData["TreatmentRoomAmountChoosen"]
String IsReferral = MemberData["IsReferral"]
String IsSpecialCondition = MemberData["IsSpecialCondition"]
String ReferralReasonCode = MemberData["ReferralReasonCode"]
String CallInStart = MemberData["CallInStart"]
String SecondaryDiagnosisCode = MemberData["SecondaryDiagnosisCode"]
String ClientClassNo = MemberData["ClientClassNo"]
String UserPosition = MemberData["UserPosition"]
String OPNO = MemberData["OPNO"]
String MaternityMedicalTreatment = MemberData["MaternityMedicalTreatment"]
String MaternityFamilyPlanningItem = MemberData["MaternityFamilyPlanningItem"]
String TreatmentCode = MemberData["TreatmentCode"]
String IsFromProcessButton = MemberData["IsFromProcessButton"]
String NonMedicalItem = MemberData["NonMedicalItem"]
String IsDiagnosisQuestionNotRegistered = MemberData["IsDiagnosisQuestionNotRegistered"]
String MedicalTreatmentAdditionalQuestion = MemberData["MedicalTreatmentAdditionalQuestion"]
String IsODS = MemberData["IsODS"]
String IsODC = MemberData["IsODC"]
String IsProducttypeChange = MemberData["IsProducttypeChange"]
String IsTreatmentPeriodChange = MemberData["IsTreatmentPeriodChange"]
String IsMaternityTreatmentChange = MemberData["IsMaternityTreatmentChange"]
String IsRoomOptionChange = MemberData["IsRoomOptionChange"]
String IsTreatmentRBClassChange = MemberData["IsTreatmentRBClassChange"]
String IsODSODCChange = MemberData["IsODSODCChange"]
String IsDocValidityChange = MemberData["IsDocValidityChange"]
String IsDocTypeChange = MemberData["IsDocTypeChange"]
String suspectDouble = MemberData["suspectDouble"]
String MedicalTreatmentConfirmation = MemberData["MedicalTreatmentConfirmation"]
String IsProtapCovid = MemberData["IsProtapCovid"]
String MedicalTreatmentResultMsg = MemberData["MedicalTreatmentResultMsg"]

println(AdminName)
println(IsGLProvider)
println(MemberNo)
println(PreviousTrID)
println(TreatmentDate)
println(ProductType)
println(DiagnosisCode)
println(DiagnosisAdditionalInfo)
println(ProviderID)
println(PatientPhone)
println(CallerName)
println(TreatmentRoom)
println(TreatmentRoomAmount)
println(DoctorName)
println(RoomOption)
println(RoomAvailability)
println(UpgradeClass)
println(BenefitAmount)
println(ProviderEmail)
println(ProviderFax)
println(ProviderPhone)
println(ProviderExt)
println(IsTiri)
println(DefaultProviderID)
println(Remarks)
println(Guid)
println(TicketNo)
println(GLType)
println(AccountManager)
println(TotalBilled)
println(ClientID)
println(ClassNo)
println(MembershipType)
println(AllDiagnosis)
println(AllDiagnosisAdditionalInfo)
println(AllDoctors)
println(Gender)
println(DOB)
println(NewMemberName)
println(PreviousGuid)
println(GLStatus)
println(EmpMemberNo)
println(NMEmpID)
println(FollowUpTaskID)
println(IsClient)
println(CallStatusID)
println(AppropriateRBClass)
println(AppropriateRBRate)
println(TreatmentEnd)
println(AppropriateRBClassChoosen)
println(AppropriateRBRateChoosen)
println(TreatmentRoomChoosen)
println(TreatmentRoomAmountChoosen)
println(IsReferral)
println(IsSpecialCondition)
println(ReferralReasonCode)
println(CallInStart)
println(SecondaryDiagnosisCode)
println(ClientClassNo)
println(UserPosition)
println(OPNO)
println(MaternityMedicalTreatment)
println(MaternityFamilyPlanningItem)
println(TreatmentCode)
println(IsFromProcessButton)
println(NonMedicalItem)
println(IsDiagnosisQuestionNotRegistered)
println(MedicalTreatmentAdditionalQuestion)
println(IsODS)
println(IsODC)
println(IsProducttypeChange)
println(IsTreatmentPeriodChange)
println(IsMaternityTreatmentChange)
println(IsRoomOptionChange)
println(IsTreatmentRBClassChange)
println(IsODSODCChange)
println(IsDocValidityChange)
println(IsDocTypeChange)
println(suspectDouble)
println(MedicalTreatmentConfirmation)
println(IsProtapCovid)
println(MedicalTreatmentResultMsg)

