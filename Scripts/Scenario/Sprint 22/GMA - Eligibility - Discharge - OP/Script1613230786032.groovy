import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

//Variable Page Login
String username = GlobalVariable.usernameGMAOP

String password = GlobalVariable.passwordGMAOP

String biztype = '12'

String application = '12'

String applicationID = '12'

//Variable Page Eligi
String MemberNo = 'KI/1312' 
//String MemberNo = findTestData('MemberGMAOP').getValue(1, 1)

String TreatmentType = 'OP'

String ClaimNo = ''

String TreatmentTypeIcon = 'OP'

String dataGetClaimDetails = '''{"CNO":0,"MemberNo":"'''+ MemberNo +'''","ProductType":"'''+ TreatmentType +'''","RoomOption":"","ClaimNo":"","DocumentNo":0,"ReferenceClaimNo":"","ReferenceDocumentNo":0,"RefLimitRecoveryClaimNo":"","RefLimitRecoveryDocumentNo":0}'''

String dataGetPolicyData = '''{"MemberNo":"'''+ MemberNo +'''"}'''

String dataGetAnnualAndRemainingLimit = '''{"MemberNo":"'''+ MemberNo +'''"}'''

String pDiagnosisID = 'A36'

String PrimaryDiagnosis = 'A36'

String SecondDiagnosis = ''

String Billed = '300000'

String Verified = '300000'

String Accepted = '300000' //Jika Diagnosis Covered

String Unpaid = '' //Jika Diagnosis Uncovered

String ExcessTotal = ''

String ExcessCompany = ''

String ExcessEmployee = ''

API.Note(MemberNo)

//API Page Login
def signin = WS.sendRequest(findTestObject('Object Repository/GMA/root/signin',
	[('username') : username
		, ('password') : password
		, ('biztype') : biztype]))

API.Note(signin.getResponseBodyContent())

if (signin.getStatusCode() == 200) {
	GlobalVariable.authorization = API.getResponseData(signin).token
	API.Note(GlobalVariable.authorization)
	KeywordUtil.markPassed("API signin Passed")
} else {
	API.Note((API.getResponseData(msgCheck).Status))
	API.Note((API.getResponseData(msgCheck).Message))
	API.Note((API.getResponseData(msgCheck).Data))
	KeywordUtil.markFailedAndStop("API signin Failed")
}

def msgCheck = WS.sendRequest(findTestObject('Object Repository/GMA/root/msgCheck',
	[('application') : application]))

API.Note(msgCheck.getResponseBodyContent())

if (API.getResponseData(msgCheck).status) {
	KeywordUtil.markPassed("API msgCheck Passed")
}  else {
	API.Note((API.getResponseData(msgCheck).status))
	API.Note((API.getResponseData(msgCheck).sessage))
	KeywordUtil.markFailedAndStop("Status False")
}

def checkMessage = WS.sendRequest(findTestObject('Object Repository/GMA/desktopNotification/checkMessage',
	[('application') : application]))

API.Note(checkMessage.getResponseBodyContent())

if (API.getResponseData(checkMessage).status) {
	KeywordUtil.markPassed("API checkMessage Passed")
}  else {
	API.Note((API.getResponseData(checkMessage).Status))
	API.Note((API.getResponseData(checkMessage).Message))
	API.Note((API.getResponseData(checkMessage).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def getConnectionDescription = WS.sendRequest(findTestObject('Object Repository/GMA/root/getConnectionDescription',
	[('applicationID') : applicationID]))

API.Note(getConnectionDescription.getResponseBodyContent())

if (!(API.getResponseData(getConnectionDescription).status)) {
	KeywordUtil.markPassed("API getConnectionDescription Passed")
}  else {
	API.Note((API.getResponseData(getConnectionDescription).status))
	API.Note((API.getResponseData(getConnectionDescription).message))
	KeywordUtil.markFailedAndStop("Status False")
}

//API Page Eligibility
def GetMasterDiagnose = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetMasterDiagnose'))

API.Note(GetMasterDiagnose.getResponseBodyContent())

if (API.getResponseData(GetMasterDiagnose).Status) {
	KeywordUtil.markPassed("API GetMasterDiagnose Passed")
}  else {
	API.Note((API.getResponseData(GetMasterDiagnose).Status))
	API.Note((API.getResponseData(GetMasterDiagnose).Message))
	API.Note((API.getResponseData(GetMasterDiagnose).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def CheckBenefitMember = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/CheckBenefitMember',
	[('MemberNo') : MemberNo]))

API.Note(CheckBenefitMember.getResponseBodyContent())

if (API.getResponseData(CheckBenefitMember).Status) {
	KeywordUtil.markPassed("API CheckBenefitMember Passed")
}  else {
	API.Note((API.getResponseData(CheckBenefitMember).Status))
	API.Note((API.getResponseData(CheckBenefitMember).Message))
	API.Note((API.getResponseData(CheckBenefitMember).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def CheckDoubleProductType = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/CheckBenefitMember',
	[('MemberNo') : MemberNo,
		('TreatmentType') : TreatmentType,
		('ClaimNo') : ClaimNo]))

API.Note(CheckDoubleProductType.getResponseBodyContent())

if (API.getResponseData(CheckDoubleProductType).Status) {
	KeywordUtil.markPassed("API CheckDoubleProductType Passed")
}  else {
	API.Note((API.getResponseData(CheckDoubleProductType).Status))
	API.Note((API.getResponseData(CheckDoubleProductType).Message))
	API.Note((API.getResponseData(CheckDoubleProductType).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def GetProductTypeActualByIcon = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetProductTypeActualByIcon',
	[('MemberNo') : MemberNo,
		('TreatmentType') : TreatmentType,
		('ClaimNo') : ClaimNo,
		('TreatmentTypeIcon') : TreatmentTypeIcon]))

API.Note(GetProductTypeActualByIcon.getResponseBodyContent())

if (API.getResponseData(GetProductTypeActualByIcon).Status) {
	KeywordUtil.markPassed("API GetProductTypeActualByIcon Passed")
}  else {
	API.Note((API.getResponseData(GetProductTypeActualByIcon).Status))
	API.Note((API.getResponseData(GetProductTypeActualByIcon).Message))
	API.Note((API.getResponseData(GetProductTypeActualByIcon).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def ProcessRegistrationEligibility = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/ProcessRegistrationEligibility',
	[('MemberNo') : MemberNo, 
		('TreatmentType') : TreatmentType,
		('ClaimNo') : ClaimNo,
		('TreatmentTypeIcon') : TreatmentTypeIcon]))

API.Note(ProcessRegistrationEligibility.getResponseBodyContent())

if (API.getResponseData(ProcessRegistrationEligibility).Status) {
	KeywordUtil.markPassed("API ProcessRegistrationEligibility Passed")
}  else {
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Status))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Message))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

ArrayList CoverageEligibility = API.getResponseData(ProcessRegistrationEligibility).Data.DataMember.BenefitList.BenefitType

API.Note(CoverageEligibility)

//API Page Discharge
def GetRegisteredEligibleMember = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetRegisteredEligibleMember',
	[('MemberNo') : MemberNo]))

API.Note(GetRegisteredEligibleMember.getResponseBodyContent())

if (API.getResponseData(GetRegisteredEligibleMember).Status) {
	KeywordUtil.markPassed("API GetRegisteredEligibleMember Passed")
}  else {
	API.Note((API.getResponseData(GetRegisteredEligibleMember).Status))
	API.Note((API.getResponseData(GetRegisteredEligibleMember).Message))
	API.Note((API.getResponseData(GetRegisteredEligibleMember).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def SummaryDischarge = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/SummaryDischarge',
	[('MemberNo') : MemberNo, 
		('TreatmentType') : TreatmentType,
		('ClaimNo') : ClaimNo,
		('TreatmentTypeIcon') : TreatmentTypeIcon]))

API.Note(SummaryDischarge.getResponseBodyContent())

if (API.getResponseData(SummaryDischarge).Status) {
	KeywordUtil.markPassed("API SummaryDischarge Passed")
}  else {
	API.Note((API.getResponseData(SummaryDischarge).Status))
	API.Note((API.getResponseData(SummaryDischarge).Message))
	API.Note((API.getResponseData(SummaryDischarge).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

ArrayList CoverageDischarge = API.getResponseData(SummaryDischarge).Data.DataMember.BenefitList.BenefitType

//Compare CoverageEligibility = CoverageDischarge

def CheckDischargeTransaction = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/CheckDischargeTransaction',
	[('MemberNo') : MemberNo]))

API.Note(CheckDischargeTransaction.getResponseBodyContent())

if (API.getResponseData(CheckDischargeTransaction).Status) {
	KeywordUtil.markPassed("API CheckDischargeTransaction Passed")
}  else {
	API.Note((API.getResponseData(CheckDischargeTransaction).Status))
	API.Note((API.getResponseData(CheckDischargeTransaction).Message))
	API.Note((API.getResponseData(CheckDischargeTransaction).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def GetClaimDetails = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetClaimDetails',
	[('data') : dataGetClaimDetails]))

API.Note(GetClaimDetails.getResponseBodyContent())

GlobalVariable.POPNO = API.getResponseData(GetClaimDetails).Data.OPNO

////GlobalVariable.dataClaimDetails = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0]
//String Name = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].Name
//String ExcessEmpF = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].ExcessEmpF
//String BenefitID = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].BenefitID
//String BenefitName = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].BenefitName
//String BenefitLimit = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].BenefitLimit
//String BenefitLimitHighPlan = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].BenefitLimitHighPlan
//String InitialBenefitLimit = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].InitialBenefitLimit
//String BenefitType = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].BenefitType
//String SDate = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].SDate
//String EDate = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].EDate
//String Qty = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].Qty
////String Billed hard
////String Verified hard
////String Accepted hard
////String UnPaid = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].UnPaid
////String ExcessTotal = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].ExcessTotal
////String ExcessCompany = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].ExcessCompany
////String ExcessEmployee = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].ExcessEmployee
//String Remarks = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].Remarks
//String BenefitIndo = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].BenefitIndo
//String Appropriate = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].Appropriate
//String IsRoomAndBoard = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].IsRoomAndBoard
//String IsNotAsCharged = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].IsNotAsCharged
//String ClaimFormula = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].ClaimFormula
//String AppropriateRBAI = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].AppropriateRBAI
//String TreatmentRBAI = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].TreatmentRBAI
//String TempAppropriateRBAI = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].TempAppropriateRBAI
//String TempTreatmentRBAI = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].TempTreatmentRBAI
//String IsIncludeRB = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].IsIncludeRB
//String IsPre = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].IsPre
//String IsPost = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].IsPost
//String IsApplyRemainingLimitToAllBenefitMA = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].IsApplyRemainingLimitToAllBenefitMA
//String IsPoolfund = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].IsPoolfund
//String IsIncludeNonMedicalItem = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].IsIncludeNonMedicalItem
//String IsIncludeFPItem = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].IsIncludeFPItem
//String PreDays = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].PreDays
//String PreLimit = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].PreLimit
//String PostDays = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].PostDays
//String PostLimit = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].PostLimit
//String PrePostLimit = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].PrePostLimit
//String SurgeryType = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].SurgeryType
//String ParentID = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].ParentID
//String OccuranceFreqLimit = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].OccuranceFreqLimit
//String ConfinementFreqLimit = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].ConfinementFreqLimit
//String YearlyFreqLimit = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].YearlyFreqLimit
//String OccuranceFreqRemainingLimit = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].OccuranceFreqRemainingLimit
//String ConfinementFreqRemainingLimit = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].ConfinementFreqRemainingLimit
//String YearlyFreqRemainingLimit = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].YearlyFreqRemainingLimit
//String LimitRecoverySameDiagosis = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].LimitRecoverySameDiagosis
//String LimitRecoveryDiffDiagosis = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].LimitRecoveryDiffDiagosis
//ArrayList ClaimDetailItem = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].ClaimDetailItem
//ArrayList ClaimDetailReference = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].ClaimDetailReference
//String BenefitGMA = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].BenefitGMA
//String IsMoveToUnpaid = API.getResponseData(GetClaimDetails).Data.ClaimDetails[0].IsMoveToUnpaid

API.Note(GlobalVariable.dataClaimDetails)

if (API.getResponseData(GetClaimDetails).Status) {
	KeywordUtil.markPassed("API GetClaimDetails Passed")
}  else {
	API.Note((API.getResponseData(GetClaimDetails).Status))
	API.Note((API.getResponseData(GetClaimDetails).Message))
	API.Note((API.getResponseData(GetClaimDetails).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def GetPolicyData = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetPolicyData',
	[('data') : dataGetPolicyData]))

API.Note(GetPolicyData.getResponseBodyContent())

if (API.getResponseData(GetPolicyData).Status) {
	KeywordUtil.markPassed("API GetPolicyData Passed")
}  else {
	API.Note((API.getResponseData(GetPolicyData).Status))
	API.Note((API.getResponseData(GetPolicyData).Message))
	API.Note((API.getResponseData(GetPolicyData).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def GetAnnualAndRemainingLimit = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetAnnualAndRemainingLimit',
	[('data') : dataGetAnnualAndRemainingLimit]))

API.Note(GetAnnualAndRemainingLimit.getResponseBodyContent())

GlobalVariable.dataRemainingLimit = API.getResponseData(GetAnnualAndRemainingLimit).Data.RemainingLimit

if (API.getResponseData(GetAnnualAndRemainingLimit).Status) {
	KeywordUtil.markPassed("API GetAnnualAndRemainingLimit Passed")
}  else {
	API.Note((API.getResponseData(GetAnnualAndRemainingLimit).Status))
	API.Note((API.getResponseData(GetAnnualAndRemainingLimit).Message))
	API.Note((API.getResponseData(GetAnnualAndRemainingLimit).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def GetDiagnosisInfo = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetDiagnosisInfo',
	[('pDiagnosisID') : pDiagnosisID]))

API.Note(GetDiagnosisInfo.getResponseBodyContent())

String DiagnosisCode = API.getResponseData(GetDiagnosisInfo).Data.DiagnosisCode
String DiagnosisDescription = API.getResponseData(GetDiagnosisInfo).Data.DiagnosisDescription
String Coverage = API.getResponseData(GetDiagnosisInfo).Data.Coverage
String ProductType = API.getResponseData(GetDiagnosisInfo).Data.ProductType
String IsNeedConfirmation = API.getResponseData(GetDiagnosisInfo).Data.IsNeedConfirmation
String IsMoveToUnpaidDiagnosis = API.getResponseData(GetDiagnosisInfo).Data.IsMoveToUnpaid
String Payer = API.getResponseData(GetDiagnosisInfo).Data.Payer

if (API.getResponseData(GetDiagnosisInfo).Status) {
	KeywordUtil.markPassed("API GetDiagnosisInfo Passed")
}  else {
	API.Note((API.getResponseData(GetDiagnosisInfo).Status))
	API.Note((API.getResponseData(GetDiagnosisInfo).Message))
	API.Note((API.getResponseData(GetDiagnosisInfo).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def GetMasterDoctor = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetMasterDoctor'))

API.Note(GetMasterDoctor.getResponseBodyContent())

GlobalVariable.DoctorID = API.getResponseData(GetMasterDoctor).Data.Items[0].DoctorID
GlobalVariable.DoctorName = API.getResponseData(GetMasterDoctor).Data.Items[0].DoctorName
GlobalVariable.DoctorSpecialty = API.getResponseData(GetMasterDoctor).Data.Items[0].DoctorSpecialty

if (API.getResponseData(GetMasterDoctor).Status) {
	KeywordUtil.markPassed("API GetMasterDoctor Passed")
}  else {
	API.Note((API.getResponseData(GetMasterDoctor).Status))
	API.Note((API.getResponseData(GetMasterDoctor).Message))
	API.Note((API.getResponseData(GetMasterDoctor).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

//String ListBenefit = '''[{"Name":"'''+Name+'''","ExcessEmpF":'''+ExcessEmpF+''',"BenefitID":"'''+BenefitID+'''","BenefitName":"'''+BenefitName+'''","BenefitLimit":'''+BenefitLimit+''',"BenefitLimitHighPlan":'''+BenefitLimitHighPlan+''',"InitialBenefitLimit":'''+InitialBenefitLimit+''',"BenefitType":"'''+BenefitType+'''","SDate":'''+SDate+''',"EDate":'''+EDate+''',"Qty":'''+Qty+''',"Billed":'''+Billed+''',"Verified":'''+Verified+''',"Accepted":'''+Accepted+''',"UnPaid":'''+Unpaid+''',"ExcessTotal":'''+ExcessTotal+''',"ExcessCompany":'''+ExcessCompany+''',"ExcessEmployee":'''+ExcessEmployee+''',"Remarks":"'''+Remarks+'''","BenefitIndo":"'''+BenefitIndo+'''","Appropriate":'''+Appropriate+''',"IsRoomAndBoard":'''+IsRoomAndBoard+''',"IsNotAsCharged":'''+IsNotAsCharged+''',"ClaimFormula":"'''+ClaimFormula+'''","AppropriateRBAI":"'''+AppropriateRBAI+'''","TreatmentRBAI":"'''+TreatmentRBAI+'''","TempAppropriateRBAI":"'''+TempAppropriateRBAI+'''","TempTreatmentRBAI":"'''+TempTreatmentRBAI+'''","IsIncludeRB":'''+IsIncludeRB+''',"IsPre":'''+IsPre+''',"IsPost":'''+IsPost+''',"IsApplyRemainingLimitToAllBenefitMA":'''+IsApplyRemainingLimitToAllBenefitMA+''',"IsPoolfund":'''+IsPoolfund+''',"IsIncludeNonMedicalItem":'''+IsIncludeNonMedicalItem+''',"IsIncludeFPItem":'''+IsIncludeFPItem+''',"PreDays":'''+PreDays+''',"PreLimit":'''+PreLimit+''',"PostDays":'''+PostDays+''',"PostLimit":'''+PostLimit+''',"PrePostLimit":'''+PrePostLimit+''',"SurgeryType":"'''+SurgeryType+'''","ParentID":"'''+ParentID+'''","OccuranceFreqLimit":'''+OccuranceFreqLimit+''',"ConfinementFreqLimit":'''+ConfinementFreqLimit+''',"YearlyFreqLimit":'''+YearlyFreqLimit+''',"OccuranceFreqRemainingLimit":'''+OccuranceFreqRemainingLimit+''',"ConfinementFreqRemainingLimit":'''+ConfinementFreqRemainingLimit+''',"YearlyFreqRemainingLimit":'''+YearlyFreqRemainingLimit+''',"LimitRecoverySameDiagosis":'''+LimitRecoverySameDiagosis+''',"LimitRecoveryDiffDiagosis":'''+LimitRecoveryDiffDiagosis+''',"ClaimDetailItem":[],"ClaimDetailReference":[],"BenefitGMA":"'''+BenefitGMA+'''","IsMoveToUnpaid":'''+IsMoveToUnpaid+'''}]'''

String ListBenefit = '''[{"Name":"KRISDERRY ARYASAKTI NUGROHO","ExcessEmpF":0,"BenefitID":"GM-OPB-01","BenefitName":"Biaya Konsultasi dan tindakan Dokter Umum","BenefitLimit":-1,"BenefitLimitHighPlan":-1,"InitialBenefitLimit":-1,"BenefitType":"","SDate":null,"EDate":null,"Qty":1,"Billed":100000,"Verified":100000,"Accepted":100000,"UnPaid":0,"ExcessTotal":0,"ExcessCompany":0,"ExcessEmployee":0,"Remarks":"","BenefitIndo":"benefit biaya konsultasi dan tindakan dokter umum","Appropriate":0,"IsRoomAndBoard":false,"IsNotAsCharged":true,"ClaimFormula":"","AppropriateRBAI":"","TreatmentRBAI":"","TempAppropriateRBAI":"","TempTreatmentRBAI":"","IsIncludeRB":false,"IsPre":false,"IsPost":false,"IsApplyRemainingLimitToAllBenefitMA":false,"IsPoolfund":false,"IsIncludeNonMedicalItem":false,"IsIncludeFPItem":false,"PreDays":0,"PreLimit":0,"PostDays":0,"PostLimit":0,"PrePostLimit":999999999,"SurgeryType":"","ParentID":"","OccuranceFreqLimit":999999999,"ConfinementFreqLimit":999999999,"YearlyFreqLimit":999999999,"OccuranceFreqRemainingLimit":999999999,"ConfinementFreqRemainingLimit":999999999,"YearlyFreqRemainingLimit":999999999,"LimitRecoverySameDiagosis":0,"LimitRecoveryDiffDiagosis":0,"ClaimDetailItem":[],"ClaimDetailReference":[],"BenefitGMA":"Biaya Dokter Umum","IsMoveToUnpaid":0}]'''

//String AllDiagnosisInfo = '''[{"DiagnosisCode":"'''+DiagnosisCode+'''","DiagnosisDescription":"'''+DiagnosisDescription+'''","Coverage":"'''+Coverage+'''","ProductType":"'''+ProductType+'''","IsNeedConfirmation":'''+IsNeedConfirmation+''',"IsMoveToUnpaid":'''+IsMoveToUnpaidDiagnosis+''',"Payer":"'''+Payer+'''"}]'''

String AllDiagnosisInfo = '''[{"DiagnosisCode":"A36","DiagnosisDescription":"DIPTHERIA","Coverage":"Covered","ProductType":"","IsNeedConfirmation":0,"IsMoveToUnpaid":0,"Payer":""}]'''

def SubmitDischargeTransaction = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/SubmitDischargeTransaction',
	[('TreatmentType') : TreatmentType
		,('MemberNo') : MemberNo
		,('PrimaryDiagnosis') : PrimaryDiagnosis
		,('SecondDiagnosis') : SecondDiagnosis
		,('ListBenefit') : ListBenefit
		,('AllDiagnosisInfo') : AllDiagnosisInfo]))

API.Note(SubmitDischargeTransaction.getResponseBodyContent())

if (API.getResponseData(SubmitDischargeTransaction).Status) {
	KeywordUtil.markPassed("API SubmitDischargeTransaction Passed")
}  else {
	API.Note((API.getResponseData(SubmitDischargeTransaction).Status))
	API.Note((API.getResponseData(SubmitDischargeTransaction).Message))
	API.Note((API.getResponseData(SubmitDischargeTransaction).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

API.Note(GlobalVariable.dataRemainingLimit)

