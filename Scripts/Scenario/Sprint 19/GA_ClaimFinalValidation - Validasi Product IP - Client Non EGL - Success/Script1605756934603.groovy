import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

//Login
String username = 'DFY'
String password = 'ITG@nt1P455QC'

//CreateTreatmentGL
String adminName = ''
String isGLProvider = 'False'
String memberNo = 'A/00106699' //1207068 - A/00106699 - ANI FATONAH - PT UNITED TRACTORS PANDU ENGINEERING
String previousTrID = ''
String treatmentDate = '2020-11-18'
String productType = 'IP'
String diagnosisCode = 'A36'
String diagnosisAdditionalInfo = ''
String providerID = 'TJKRH00013'
String patientPhone = '0987654321'
String callerName = '-'
String treatmentRoom = 'KELAS I'
String treatmentRoomAmount = '811000'
String doctorName = 'Agus Wiyono'
String roomOption = 'On Plan'
String roomAvailability = 'NONE'
String upgradeClass = 'NONE'
String benefitAmount = '0'
String providerEmail = 'ehu@beyond.asuransi.astra.co.id'
String providerFax = '021-8560601'
String providerPhone = '081806221986'
String providerExt = '121'
String isTiri = '0'
String defaultProviderID = ''
String remarks = ''
String guid = 'c5f9e0c43e4844a6a9b772124c04d5ee9f5e1597ad8ef3fdf7741bade5a950f8'
String ticketNo = ''
String gLType = 'GL Awal'
String accountManager = ''
String totalBilled = '0'
String clientID = 'CJKPU02'
String classNo = '2'
String membershipType = '1. EMP'
String allDiagnosis = '[[1,"A36","DIPTHERIA","Initial Primary","","","","","","","","","Covered","","","","","",0,"1","0"]]'
String allDiagnosisAdditionalInfo = '[[]]'
String allDoctors = 'Agus Wiyono'
String gender = 'F'
String dOB = '1982-02-22'
String newMemberName = ''
String previousGuid = ''
String gLStatus = ''
String empMemberNo = ''
String nMEmpID = ''
String followUpTaskID = 'c5f9e0c43e4844a6a9b772124c04d5ee9f5e1597ad8ef3fdf7741bade5a950f8'
String isClient = '0'
String callStatusID = 'Not Need Follow Up'
String appropriateRBClass = 'KELAS I'
String appropriateRBRate = '811000'
String treatmentEnd = ''
String appropriateRBClassChoosen = 'KELAS I'
String appropriateRBRateChoosen = '811000'
String treatmentRoomChoosen = 'KELAS I'
String treatmentRoomAmountChoosen = '811000'
String isReferral = '0'
String isSpecialCondition = '0'
String referralReasonCode = ''
String callInStart = ''
String secondaryDiagnosisCode = '[]'
String clientClassNo = '0'
String userPosition = ''
String oPNO = '88426'
String maternityMedicalTreatment = ''
String maternityFamilyPlanningItem = ''
String treatmentCode = ''
String isFromProcessButton = '0'
String nonMedicalItem = ''
String isDiagnosisQuestionNotRegistered = '0'
String medicalTreatmentAdditionalQuestion = ''
String isODS = '0'
String isODC = '0'
String isProducttypeChange = '1'
String isTreatmentPeriodChange = '1'
String isMaternityTreatmentChange = '1'
String isRoomOptionChange = '1'
String isTreatmentRBClassChange = '1'
String isODSODCChange = '1'
String isDocValidityChange = '1'
String isDocTypeChange = '1'


WebUI.callTestCase(findTestCase('Pages/Login/Login'),
	[ ('username') : username
	, ('password') : password])

WebUI.callTestCase(findTestCase('Pages/GA/CreateTreatmentGL/GA_ClaimFinalValidation'),
	[ ('adminName') : adminName
	, ('isGLProvider') : isGLProvider
	, ('memberNo') : memberNo
	, ('previousTrID') : previousTrID
	, ('treatmentDate') : treatmentDate
	, ('productType') : productType
	, ('diagnosisCode') : diagnosisCode
	, ('diagnosisAdditionalInfo') : diagnosisAdditionalInfo
	, ('providerID') : providerID
	, ('patientPhone') : patientPhone
	, ('callerName') : callerName
	, ('treatmentRoom') : treatmentRoom
	, ('treatmentRoomAmount') : treatmentRoomAmount
	, ('doctorName') : doctorName
	, ('roomOption') : roomOption
	, ('roomAvailability') : roomAvailability
	, ('upgradeClass') : upgradeClass
	, ('benefitAmount') : benefitAmount
	, ('providerEmail') : providerEmail
	, ('providerFax') : providerFax
	, ('providerPhone') : providerPhone
	, ('providerExt') : providerExt
	, ('isTiri') : isTiri
	, ('defaultProviderID') : defaultProviderID
	, ('remarks') : remarks
	, ('guid') : guid
	, ('ticketNo') : ticketNo
	, ('gLType') : gLType
	, ('accountManager') : accountManager
	, ('totalBilled') : totalBilled
	, ('clientID') : clientID
	, ('classNo') : classNo
	, ('membershipType') : membershipType
	, ('allDiagnosis') : allDiagnosis
	, ('allDiagnosisAdditionalInfo') : allDiagnosisAdditionalInfo
	, ('allDoctors') : allDoctors
	, ('gender') : gender
	, ('dOB') : dOB
	, ('newMemberName') : newMemberName
	, ('previousGuid') : previousGuid
	, ('gLStatus') : gLStatus
	, ('empMemberNo') : empMemberNo
	, ('nMEmpID') : nMEmpID
	, ('followUpTaskID') : followUpTaskID
	, ('isClient') : isClient
	, ('callStatusID') : callStatusID
	, ('appropriateRBClass') : appropriateRBClass
	, ('appropriateRBRate') : appropriateRBRate
	, ('treatmentEnd') : treatmentEnd
	, ('appropriateRBClassChoosen') : appropriateRBClassChoosen
	, ('appropriateRBRateChoosen') : appropriateRBRateChoosen
	, ('treatmentRoomChoosen') : treatmentRoomChoosen
	, ('treatmentRoomAmountChoosen') : treatmentRoomAmountChoosen
	, ('isReferral') : isReferral
	, ('isSpecialCondition') : isSpecialCondition
	, ('referralReasonCode') : referralReasonCode
	, ('callInStart') : callInStart
	, ('secondaryDiagnosisCode') : secondaryDiagnosisCode
	, ('clientClassNo') : clientClassNo
	, ('userPosition') : userPosition
	, ('oPNO') : oPNO
	, ('maternityMedicalTreatment') : maternityMedicalTreatment
	, ('maternityFamilyPlanningItem') : maternityFamilyPlanningItem
	, ('treatmentCode') : treatmentCode
	, ('isFromProcessButton') : isFromProcessButton
	, ('nonMedicalItem') : nonMedicalItem
	, ('isDiagnosisQuestionNotRegistered') : isDiagnosisQuestionNotRegistered
	, ('medicalTreatmentAdditionalQuestion') : medicalTreatmentAdditionalQuestion
	, ('isODS') : isODS
	, ('isODC') : isODC
	, ('isProducttypeChange') : isProducttypeChange
	, ('isTreatmentPeriodChange') : isTreatmentPeriodChange
	, ('isMaternityTreatmentChange') : isMaternityTreatmentChange
	, ('isRoomOptionChange') : isRoomOptionChange
	, ('isTreatmentRBClassChange') : isTreatmentRBClassChange
	, ('isODSODCChange') : isODSODCChange
	, ('isDocValidityChange') : isDocValidityChange
	, ('isDocTypeChange') : isDocTypeChange])
