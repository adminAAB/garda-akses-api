import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

//Login
String username = 'DFY'
String password = 'ITG@nt1P455QC'

//SaveTempGL
String pIsGLProvider = '0'
String guid = 'c5f9e0c43e4844a6a9b772124c04d5ee9f5e1597ad8ef3fdf7741bade5a950f8'
String ticketNo = ''
String memberNo = 'A/00141593'
String memberName = 'ANDI RISPANDI'
String membership = '1. EMP'
String classNo = '2'
String clientID = 'CJKPC0005'
String clientName = 'PT. CIMB NIAGA AUTO FINANCE'
String benefitInfo = 'Benefit boleh diketahui oleh Peserta'
String empID = '00000382'
String gLType = 'GL Awal'
String dOB = '1982-02-22'
String productType = 'IP'
String treatmentStart = '2020-11-18'
String treatmentEnd = ''
String providerID = 'TJKRH00013'
String providerName = 'RSIA Hermina Jatinegara'
String providerEmail = 'ehu@beyond.asuransi.astra.co.id'
String providerFax = '021-8560601'
String providerPhone = '081806221986'
String providerExt = '121'
String isPassedAway = '0'
String diagnosis = '[[1,"A36","DIPTHERIA","Initial Primary","","","","","","","","","Covered","","","","","",0,"1","0"]]'
String accountManager = ''
String doctor = 'Agus Wiyono'
String remarks = ''
String appropriateRBClass = 'KELAS I'
String treatmentRBClass = 'KELAS I'
String appropriateRBRate = '811000'
String treatmentRBRate = '811000'
String roomOption = 'On Plan'
String remainingLimit = '0'
String remainingLimitType = 'INDIVIDUAL'
String rBScheme = 'Benefit Kamar Khusus : Tidak diketahui informasi benefit kelas kamar khusus. Silahkan cek TC sebagai panduan.'
String iCULimit = '0'
String isolationLimit = '0'
String totalBilled = '0'
String additionalDiagnosisInfo = '[[]]'
String familyPhone = '0987654321'
String previousGuid = ''
String previousTrID = ''
String gLStatus = ''
String callerName = '-'
String isPreAdmission = '0'
String isInteruptedCalls = '0'
String isReject = '0'
String nMMemberType = '1. EMP'
String nMMemberName = ''
String nMEmpID = ''
String nMEmployee = ''
String nMClassification = '2'
String nMClientId = 'CJKPC0005'
String nMDOB = '2020-11-18'
String nMGender = 'F'
String nMIsNewBorn = '0'
String nMIsTwin = '0'
String isNeedFollowUpF = '0'
String appropriateRBClassActual = 'KELAS I'
String appropriateRBRateActual = '811000'
String treatmentRBClassActual = 'KELAS I'
String treatmentRBRateActual = '811000'
String isReferral = '0'
String isSpecialCondition = '0'
String referralReasonCode = ''
String specialConditionReason = ''
String actor = 'HMY'
String isFromCreateTicket = 'false'
String inboundCallGuid = ''
String treatmentTT = 'Inpatient'
String appropriateTT = 'Inpatient'
String clientClassNo = '0'
String maternityScheme = ''
String coveredQty = ''
String alwaysGuaranted = 'No'
String maternityType = ''
String coverageType = ''
String maternityPackagePrice = '0'
String maternityFamilyPlanningItem = ''
String maternityMedicalTreatment = ''
String maternityTreatmentCode = ''
String providerAdminName = ''
String treatmentDuration = ''
String passedAwayDate = ''
String doctorOnCall = ''
String lastCondition = ''
String lastTreatment = ''
String resusitasiF = '0'
String nonMedicalItem = ''
String isDiagnosisQuestionNotRegistered = '0'
String isGLProvider = ''
String isODS = '0'
String isODC = '0'
String doctorID = '15'
String schemeType = ''
String gMRBClass = ''
String providerRBClass = ''
String amountLimit = '0'
String rBAmount = '0'
String isCovid = '0'


WebUI.callTestCase(findTestCase('Pages/Login/Login'),
	[ ('username') : username
	, ('password') : password])

WebUI.callTestCase(findTestCase('Pages/GA/CreateTreatmentGL/SaveTempGL'),
	[ ('pIsGLProvider') : pIsGLProvider
	, ('guid') : guid
	, ('ticketNo') : ticketNo
	, ('memberNo') : memberNo
	, ('memberName') : memberName
	, ('membership') : membership
	, ('classNo') : classNo
	, ('clientID') : clientID
	, ('clientName') : clientName
	, ('benefitInfo') : benefitInfo
	, ('empID') : empID
	, ('gLType') : gLType
	, ('dOB') : dOB
	, ('productType') : productType
	, ('treatmentStart') : treatmentStart
	, ('treatmentEnd') : treatmentEnd
	, ('providerID') : providerID
	, ('providerName') : providerName
	, ('providerEmail') : providerEmail
	, ('providerFax') : providerFax
	, ('providerPhone') : providerPhone
	, ('providerExt') : providerExt
	, ('isPassedAway') : isPassedAway
	, ('diagnosis') : diagnosis
	, ('accountManager') : accountManager
	, ('doctor') : doctor
	, ('remarks') : remarks
	, ('appropriateRBClass') : appropriateRBClass
	, ('treatmentRBClass') : treatmentRBClass
	, ('appropriateRBRate') : appropriateRBRate
	, ('treatmentRBRate') : treatmentRBRate
	, ('roomOption') : roomOption
	, ('remainingLimit') : remainingLimit
	, ('remainingLimitType') : remainingLimitType
	, ('rBScheme') : rBScheme
	, ('iCULimit') : iCULimit
	, ('isolationLimit') : isolationLimit
	, ('totalBilled') : totalBilled
	, ('additionalDiagnosisInfo') : additionalDiagnosisInfo
	, ('familyPhone') : familyPhone
	, ('previousGuid') : previousGuid
	, ('previousTrID') : previousTrID
	, ('gLStatus') : gLStatus
	, ('callerName') : callerName
	, ('isPreAdmission') : isPreAdmission
	, ('isInteruptedCalls') : isInteruptedCalls
	, ('isReject') : isReject
	, ('nMMemberType') : nMMemberType
	, ('nMMemberName') : nMMemberName
	, ('nMEmpID') : nMEmpID
	, ('nMEmployee') : nMEmployee
	, ('nMClassification') : nMClassification
	, ('nMClientId') : nMClientId
	, ('nMDOB') : nMDOB
	, ('nMGender') : nMGender
	, ('nMIsNewBorn') : nMIsNewBorn
	, ('nMIsTwin') : nMIsTwin
	, ('isNeedFollowUpF') : isNeedFollowUpF
	, ('appropriateRBClassActual') : appropriateRBClassActual
	, ('appropriateRBRateActual') : appropriateRBRateActual
	, ('treatmentRBClassActual') : treatmentRBClassActual
	, ('treatmentRBRateActual') : treatmentRBRateActual
	, ('isReferral') : isReferral
	, ('isSpecialCondition') : isSpecialCondition
	, ('referralReasonCode') : referralReasonCode
	, ('specialConditionReason') : specialConditionReason
	, ('actor') : actor
	, ('isFromCreateTicket') : isFromCreateTicket
	, ('inboundCallGuid') : inboundCallGuid
	, ('treatmentTT') : treatmentTT
	, ('appropriateTT') : appropriateTT
	, ('clientClassNo') : clientClassNo
	, ('maternityScheme') : maternityScheme
	, ('coveredQty') : coveredQty
	, ('alwaysGuaranted') : alwaysGuaranted
	, ('maternityType') : maternityType
	, ('coverageType') : coverageType
	, ('maternityPackagePrice') : maternityPackagePrice
	, ('maternityFamilyPlanningItem') : maternityFamilyPlanningItem
	, ('maternityMedicalTreatment') : maternityMedicalTreatment
	, ('maternityTreatmentCode') : maternityTreatmentCode
	, ('providerAdminName') : providerAdminName
	, ('treatmentDuration') : treatmentDuration
	, ('passedAwayDate') : passedAwayDate
	, ('doctorOnCall') : doctorOnCall
	, ('lastCondition') : lastCondition
	, ('lastTreatment') : lastTreatment
	, ('resusitasiF') : resusitasiF
	, ('nonMedicalItem') : nonMedicalItem
	, ('isDiagnosisQuestionNotRegistered') : isDiagnosisQuestionNotRegistered
	, ('isGLProvider') : isGLProvider
	, ('isODS') : isODS
	, ('isODC') : isODC
	, ('doctorID') : doctorID
	, ('schemeType') : schemeType
	, ('gMRBClass') : gMRBClass
	, ('providerRBClass') : providerRBClass
	, ('amountLimit') : amountLimit
	, ('rBAmount') : rBAmount
	, ('isCovid') : isCovid])

