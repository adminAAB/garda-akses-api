import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

//Login
String username = 'DFY'
String password = 'ITG@nt1P455QC'

//CreateTreatmentGL
String adminName = ''
String isGLProvider = 'False'
String memberNo = 'A/00099042' //00000742 - A/00141607 - ANNA SOPIYANA - PT. CIMB NIAGA AUTO FINANCE
String previousTrID = ''
String treatmentDate = '2021-02-18'
String productType = 'IP'
String diagnosisCode = 'D39'
String diagnosisAdditionalInfo = ''
String providerID = 'TJKRH00013'
String patientPhone = '0987654321'
String callerName = '-'
String treatmentRoom = 'SVIPI'
String treatmentRoomAmount = '1541000'
String doctorName = 'Dele'
String roomOption = 'On Plan'
String roomAvailability = 'NONE'
String upgradeClass = 'NONE'
String benefitAmount = '1500000'
String providerEmail = 'ehu@beyond.asuransi.astra.co.id'
String providerFax = '021-8560601'
String providerPhone = '081806221986'
String providerExt = '121'
String isTiri = '0'
String defaultProviderID = ''
String remarks = ''
String guid = 'b1bb46d7b0c84328a5110976bba12774fb6fe17c9b6a060e7c1952ab3aca5a7c'
String ticketNo = ''
String gLType = 'GL Awal'
String accountManager = ''
String totalBilled = '0'
String clientID = 'CJKPP0009'
String classNo = '13'
String membershipType = '3. CHI'
String allDiagnosis = '[[2,"D39","NEOPLASM OF UNCERTAIN OR UNKNOWN BEHAVIOUR OF FEMALE GENITAL ORGAN","Initial Primary","","","","","","","","","Guaranteed but Uncovered","","","","","",0,"1","0"]]'
String allDiagnosisAdditionalInfo = '[[]]'
String allDoctors = 'Dele'
String gender = 'F'
String dOB = '2004-03-10'
String newMemberName = ''
String previousGuid = ''
String gLStatus = ''
String empMemberNo = ''
String nMEmpID = ''
String followUpTaskID = 'b1bb46d7b0c84328a5110976bba12774fb6fe17c9b6a060e7c1952ab3aca5a7c'
String isClient = '1'
String callStatusID = 'Not Need Follow Up'
String appropriateRBClass = 'SVIP'
String appropriateRBRate = '1541000'
String treatmentEnd = ''
String appropriateRBClassChoosen = 'SVIP'
String appropriateRBRateChoosen = '1541000'
String treatmentRoomChoosen = 'SVIP'
String treatmentRoomAmountChoosen = '1541000'
String isReferral = '0'
String isSpecialCondition = '0'
String referralReasonCode = ''
String callInStart = ''
String secondaryDiagnosisCode = '[]'
String clientClassNo = '16'
String userPosition = ''
String oPNO = '105323'
String maternityMedicalTreatment = ''
String maternityFamilyPlanningItem = ''
String treatmentCode = ''
String isFromProcessButton = '0'
String nonMedicalItem = ''
String isDiagnosisQuestionNotRegistered = '0'
String medicalTreatmentAdditionalQuestion = ''
String isODS = '0'
String isODC = '0'
String isProducttypeChange = '1'
String isTreatmentPeriodChange = '1'
String isMaternityTreatmentChange = '1'
String isRoomOptionChange = '1'
String isTreatmentRBClassChange = '1'
String isODSODCChange = '1'
String isDocValidityChange = '1'
String isDocTypeChange = '1'
String suspectDouble = '0'


WebUI.callTestCase(findTestCase('Pages/GA/Login/Login'),
	[ ('username') : username
	, ('password') : password])

WebUI.callTestCase(findTestCase('Pages/GA/CreateTreatmentGL/GA_ClaimFinalValidation'),
	[ ('adminName') : adminName
	, ('isGLProvider') : isGLProvider
	, ('memberNo') : memberNo
	, ('previousTrID') : previousTrID
	, ('treatmentDate') : treatmentDate
	, ('productType') : productType
	, ('diagnosisCode') : diagnosisCode
	, ('diagnosisAdditionalInfo') : diagnosisAdditionalInfo
	, ('providerID') : providerID
	, ('patientPhone') : patientPhone
	, ('callerName') : callerName
	, ('treatmentRoom') : treatmentRoom
	, ('treatmentRoomAmount') : treatmentRoomAmount
	, ('doctorName') : doctorName
	, ('roomOption') : roomOption
	, ('roomAvailability') : roomAvailability
	, ('upgradeClass') : upgradeClass
	, ('benefitAmount') : benefitAmount
	, ('providerEmail') : providerEmail
	, ('providerFax') : providerFax
	, ('providerPhone') : providerPhone
	, ('providerExt') : providerExt
	, ('isTiri') : isTiri
	, ('defaultProviderID') : defaultProviderID
	, ('remarks') : remarks
	, ('guid') : guid
	, ('ticketNo') : ticketNo
	, ('gLType') : gLType
	, ('accountManager') : accountManager
	, ('totalBilled') : totalBilled
	, ('clientID') : clientID
	, ('classNo') : classNo
	, ('membershipType') : membershipType
	, ('allDiagnosis') : allDiagnosis
	, ('allDiagnosisAdditionalInfo') : allDiagnosisAdditionalInfo
	, ('allDoctors') : allDoctors
	, ('gender') : gender
	, ('dOB') : dOB
	, ('newMemberName') : newMemberName
	, ('previousGuid') : previousGuid
	, ('gLStatus') : gLStatus
	, ('empMemberNo') : empMemberNo
	, ('nMEmpID') : nMEmpID
	, ('followUpTaskID') : followUpTaskID
	, ('isClient') : isClient
	, ('callStatusID') : callStatusID
	, ('appropriateRBClass') : appropriateRBClass
	, ('appropriateRBRate') : appropriateRBRate
	, ('treatmentEnd') : treatmentEnd
	, ('appropriateRBClassChoosen') : appropriateRBClassChoosen
	, ('appropriateRBRateChoosen') : appropriateRBRateChoosen
	, ('treatmentRoomChoosen') : treatmentRoomChoosen
	, ('treatmentRoomAmountChoosen') : treatmentRoomAmountChoosen
	, ('isReferral') : isReferral
	, ('isSpecialCondition') : isSpecialCondition
	, ('referralReasonCode') : referralReasonCode
	, ('callInStart') : callInStart
	, ('secondaryDiagnosisCode') : secondaryDiagnosisCode
	, ('clientClassNo') : clientClassNo
	, ('userPosition') : userPosition
	, ('oPNO') : oPNO
	, ('maternityMedicalTreatment') : maternityMedicalTreatment
	, ('maternityFamilyPlanningItem') : maternityFamilyPlanningItem
	, ('treatmentCode') : treatmentCode
	, ('isFromProcessButton') : isFromProcessButton
	, ('nonMedicalItem') : nonMedicalItem
	, ('isDiagnosisQuestionNotRegistered') : isDiagnosisQuestionNotRegistered
	, ('medicalTreatmentAdditionalQuestion') : medicalTreatmentAdditionalQuestion
	, ('isODS') : isODS
	, ('isODC') : isODC
	, ('isProducttypeChange') : isProducttypeChange
	, ('isTreatmentPeriodChange') : isTreatmentPeriodChange
	, ('isMaternityTreatmentChange') : isMaternityTreatmentChange
	, ('isRoomOptionChange') : isRoomOptionChange
	, ('isTreatmentRBClassChange') : isTreatmentRBClassChange
	, ('isODSODCChange') : isODSODCChange
	, ('isDocValidityChange') : isDocValidityChange
	, ('isDocTypeChange') : isDocTypeChange
	, ('suspectDouble') : suspectDouble])


