import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

//Variable Page Login
String username = GlobalVariable.usernameGMAOP

String password = GlobalVariable.passwordGMAOP

String biztype = '12'

String application = '12'

String applicationID = '12'

//Variable Page Eligi
String MemberNo = findTestData('MemberGMADT').getValue(1, 1)

String TreatmentType = 'DT'

String dataGetClaimDetails = '''{"CNO":0,"MemberNo":"'''+ MemberNo +'''","ProductType":"'''+ TreatmentType +'''","RoomOption":"","ClaimNo":"","DocumentNo":0,"ReferenceClaimNo":"","ReferenceDocumentNo":0,"RefLimitRecoveryClaimNo":"","RefLimitRecoveryDocumentNo":0}'''

String dataGetPolicyData = '''{"MemberNo":"'''+ MemberNo +'''"}'''

String dataGetAnnualAndRemainingLimit = '''{"MemberNo":"'''+ MemberNo +'''"}'''

String pDiagnosisID = 'A36'

String PrimaryDiagnosis = 'A36'

String SecondDiagnosis = ''

API.Note(MemberNo)

//API Page Login
def signin = WS.sendRequest(findTestObject('Object Repository/GMA/root/signin',
	[('username') : username
		, ('password') : password
		, ('biztype') : biztype]))

API.Note(signin.getResponseBodyContent())

if (signin.getStatusCode() == 200) {
	GlobalVariable.authorization = API.getResponseData(signin).token
	API.Note(GlobalVariable.authorization)
	KeywordUtil.markPassed("API signin Passed")
} else {
	API.Note((API.getResponseData(msgCheck).Status))
	API.Note((API.getResponseData(msgCheck).Message))
	API.Note((API.getResponseData(msgCheck).Data))
	KeywordUtil.markFailedAndStop("API signin Failed")
}

def msgCheck = WS.sendRequest(findTestObject('Object Repository/GMA/root/msgCheck',
	[('application') : application]))

API.Note(msgCheck.getResponseBodyContent())

if (API.getResponseData(msgCheck).status) {
	KeywordUtil.markPassed("API msgCheck Passed")
}  else {
	API.Note((API.getResponseData(msgCheck).status))
	API.Note((API.getResponseData(msgCheck).sessage))
	KeywordUtil.markFailedAndStop("Status False")
}

def checkMessage = WS.sendRequest(findTestObject('Object Repository/GMA/desktopNotification/checkMessage',
	[('application') : application]))

API.Note(checkMessage.getResponseBodyContent())

if (API.getResponseData(checkMessage).status) {
	KeywordUtil.markPassed("API checkMessage Passed")
}  else {
	API.Note((API.getResponseData(checkMessage).Status))
	API.Note((API.getResponseData(checkMessage).Message))
	API.Note((API.getResponseData(checkMessage).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def getConnectionDescription = WS.sendRequest(findTestObject('Object Repository/GMA/root/getConnectionDescription',
	[('applicationID') : applicationID]))

API.Note(getConnectionDescription.getResponseBodyContent())

if (!(API.getResponseData(getConnectionDescription).status)) {
	KeywordUtil.markPassed("API getConnectionDescription Passed")
}  else {
	API.Note((API.getResponseData(getConnectionDescription).status))
	API.Note((API.getResponseData(getConnectionDescription).message))
	KeywordUtil.markFailedAndStop("Status False")
}

//API Page Eligibility
def GetMasterDiagnose = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetMasterDiagnose'))

API.Note(GetMasterDiagnose.getResponseBodyContent())

if (API.getResponseData(GetMasterDiagnose).Status) {
	API.Note((API.getResponseData(GetMasterDiagnose).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetMasterDiagnose).Status))
	API.Note((API.getResponseData(GetMasterDiagnose).Message))
	API.Note((API.getResponseData(GetMasterDiagnose).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def CheckBenefitMember = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/CheckBenefitMember',
	[('MemberNo') : MemberNo]))

API.Note(CheckBenefitMember.getResponseBodyContent())

if (API.getResponseData(CheckBenefitMember).Status) {
	API.Note((API.getResponseData(CheckBenefitMember).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(CheckBenefitMember).Status))
	API.Note((API.getResponseData(CheckBenefitMember).Message))
	API.Note((API.getResponseData(CheckBenefitMember).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def ProcessRegistrationEligibility = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/ProcessRegistrationEligibility',
	[('MemberNo') : MemberNo, ('TreatmentType') : TreatmentType]))

API.Note(ProcessRegistrationEligibility.getResponseBodyContent())

if (API.getResponseData(ProcessRegistrationEligibility).Status) {
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Status))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Message))
	API.Note((API.getResponseData(ProcessRegistrationEligibility).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

//API Page Discharge
def GetRegisteredEligibleMember = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetRegisteredEligibleMember',
	[('MemberNo') : MemberNo]))

API.Note(GetRegisteredEligibleMember.getResponseBodyContent())

if (API.getResponseData(GetRegisteredEligibleMember).Status) {
	API.Note((API.getResponseData(GetRegisteredEligibleMember).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetRegisteredEligibleMember).Status))
	API.Note((API.getResponseData(GetRegisteredEligibleMember).Message))
	API.Note((API.getResponseData(GetRegisteredEligibleMember).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def CheckDischargeTransaction = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/CheckDischargeTransaction',
	[('MemberNo') : MemberNo]))

API.Note(CheckDischargeTransaction.getResponseBodyContent())

if (API.getResponseData(CheckDischargeTransaction).Status) {
	API.Note((API.getResponseData(CheckDischargeTransaction).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(CheckDischargeTransaction).Status))
	API.Note((API.getResponseData(CheckDischargeTransaction).Message))
	API.Note((API.getResponseData(CheckDischargeTransaction).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def GetClaimDetails = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetClaimDetails',
	[('data') : dataGetClaimDetails]))

API.Note(GetClaimDetails.getResponseBodyContent())

GlobalVariable.POPNO = API.getResponseData(GetClaimDetails).Data.OPNO

GlobalVariable.dataClaimDetails = API.getResponseData(GetClaimDetails).Data.ClaimDetails

API.Note(GlobalVariable.dataClaimDetails)

if (API.getResponseData(GetClaimDetails).Status) {
	API.Note((API.getResponseData(GetClaimDetails).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetClaimDetails).Status))
	API.Note((API.getResponseData(GetClaimDetails).Message))
	API.Note((API.getResponseData(GetClaimDetails).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def GetPolicyData = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetPolicyData',
	[('data') : dataGetPolicyData]))

API.Note(GetPolicyData.getResponseBodyContent())

if (API.getResponseData(GetPolicyData).Status) {
	API.Note((API.getResponseData(GetPolicyData).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetPolicyData).Status))
	API.Note((API.getResponseData(GetPolicyData).Message))
	API.Note((API.getResponseData(GetPolicyData).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def GetAnnualAndRemainingLimit = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetAnnualAndRemainingLimit',
	[('data') : dataGetAnnualAndRemainingLimit]))

API.Note(GetAnnualAndRemainingLimit.getResponseBodyContent())

if (API.getResponseData(GetAnnualAndRemainingLimit).Status) {
	API.Note((API.getResponseData(GetAnnualAndRemainingLimit).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetAnnualAndRemainingLimit).Status))
	API.Note((API.getResponseData(GetAnnualAndRemainingLimit).Message))
	API.Note((API.getResponseData(GetAnnualAndRemainingLimit).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def GetDiagnosisInfo = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetDiagnosisInfo',
	[('pDiagnosisID') : pDiagnosisID]))

API.Note(GetDiagnosisInfo.getResponseBodyContent())

GlobalVariable.dataGetDiagnosisInfo = API.getResponseData(GetDiagnosisInfo).Data

if (API.getResponseData(GetDiagnosisInfo).Status) {
	API.Note((API.getResponseData(GetDiagnosisInfo).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetDiagnosisInfo).Status))
	API.Note((API.getResponseData(GetDiagnosisInfo).Message))
	API.Note((API.getResponseData(GetDiagnosisInfo).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

def GetMasterDoctor = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetMasterDoctor'))

API.Note(GetMasterDoctor.getResponseBodyContent())

GlobalVariable.DoctorID = API.getResponseData(GetMasterDoctor).Data.Items[0].DoctorID

GlobalVariable.DoctorName = API.getResponseData(GetMasterDoctor).Data.Items[0].DoctorName

GlobalVariable.DoctorSpecialty = API.getResponseData(GetMasterDoctor).Data.Items[0].DoctorSpecialty

if (API.getResponseData(GetMasterDoctor).Status) {
	API.Note((API.getResponseData(GetMasterDoctor).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetMasterDoctor).Status))
	API.Note((API.getResponseData(GetMasterDoctor).Message))
	API.Note((API.getResponseData(GetMasterDoctor).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

API.Note(TreatmentType)
API.Note(MemberNo)
API.Note(PrimaryDiagnosis)
API.Note(SecondDiagnosis)
API.Note(GlobalVariable.dataClaimDetails)
API.Note(GlobalVariable.DoctorID)
API.Note(GlobalVariable.DoctorName)
API.Note(GlobalVariable.DoctorSpecialty)
API.Note(GlobalVariable.dataGetDiagnosisInfo)

def SubmitDischargeTransaction = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/SubmitDischargeTransaction',
	[('TreatmentType') : TreatmentType
		,('MemberNo') : MemberNo
		,('PrimaryDiagnosis') : PrimaryDiagnosis
		,('SecondDiagnosis') : SecondDiagnosis
		,('ListBenefit') : '['+GlobalVariable.dataClaimDetails+']'
		,('AllDiagnosisInfo') : '['+GlobalVariable.dataGetDiagnosisInfo+']']))

API.Note(SubmitDischargeTransaction.getResponseBodyContent())

if (API.getResponseData(SubmitDischargeTransaction).Status) {
	API.Note((API.getResponseData(SubmitDischargeTransaction).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(SubmitDischargeTransaction).Status))
	API.Note((API.getResponseData(SubmitDischargeTransaction).Message))
	API.Note((API.getResponseData(SubmitDischargeTransaction).Data))
	KeywordUtil.markFailedAndStop("Status False")
}