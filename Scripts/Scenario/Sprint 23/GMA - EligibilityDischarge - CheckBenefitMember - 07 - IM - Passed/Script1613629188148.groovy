import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

String MemberNo = GlobalVariable.MemberGMAOPDTMCUIMFPGL //findTestData('MemberGMAOP').getValue(1, 1)

String TreatmentTypeMember = 'IM'

println MemberNo

WebUI.callTestCase(findTestCase('Pages/GMA/root/Signin'),[:])

def CheckBenefitMember = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/CheckBenefitMember', 
	[('MemberNo') : MemberNo]))

if (API.getResponseData(CheckBenefitMember).Status) {
	KeywordUtil.markPassed("Passed")
}  else {
	KeywordUtil.markFailedAndStop("API Failed")
}

if (API.getResponseData(CheckBenefitMember).Data.Treatment.TreatmentType.contains(TreatmentTypeMember)) {
	KeywordUtil.markPassed("Member memiliki Product " + API.getResponseData(CheckBenefitMember).Data.Treatment.TreatmentType)
}  else {
	KeywordUtil.markFailed("Member tidak memiliki Product [" + TreatmentTypeMember + "]. Member hanya memiliki Product " + API.getResponseData(CheckBenefitMember).Data.Treatment.TreatmentType)
}