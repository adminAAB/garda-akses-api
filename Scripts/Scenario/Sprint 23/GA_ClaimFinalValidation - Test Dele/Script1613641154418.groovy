import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

def GA_ClaimFinalValidation = WS.sendRequest(findTestObject('Object Repository/GA/CreateTreatmentGL/GA_ClaimFinalValidation',
	[ ('adminName') : adminName
	, ('isGLProvider') : isGLProvider
	, ('memberNo') : memberNo
	, ('previousTrID') : previousTrID
	, ('treatmentDate') : treatmentDate
	, ('productType') : productType
	, ('diagnosisCode') : diagnosisCode
	, ('diagnosisAdditionalInfo') : diagnosisAdditionalInfo
	, ('providerID') : providerID
	, ('patientPhone') : patientPhone
	, ('callerName') : callerName
	, ('treatmentRoom') : treatmentRoom
	, ('treatmentRoomAmount') : treatmentRoomAmount
	, ('doctorName') : doctorName
	, ('roomOption') : roomOption
	, ('roomAvailability') : roomAvailability
	, ('upgradeClass') : upgradeClass
	, ('benefitAmount') : benefitAmount
	, ('providerEmail') : providerEmail
	, ('providerFax') : providerFax
	, ('providerPhone') : providerPhone
	, ('providerExt') : providerExt
	, ('isTiri') : isTiri
	, ('defaultProviderID') : defaultProviderID
	, ('remarks') : remarks
	, ('guid') : guid
	, ('ticketNo') : ticketNo
	, ('gLType') : gLType
	, ('accountManager') : accountManager
	, ('totalBilled') : totalBilled
	, ('clientID') : clientID
	, ('classNo') : classNo
	, ('membershipType') : membershipType
	, ('allDiagnosis') : allDiagnosis
	, ('allDiagnosisAdditionalInfo') : allDiagnosisAdditionalInfo
	, ('allDoctors') : allDoctors
	, ('gender') : gender
	, ('dOB') : dOB
	, ('newMemberName') : newMemberName
	, ('previousGuid') : previousGuid
	, ('gLStatus') : gLStatus
	, ('empMemberNo') : empMemberNo
	, ('nMEmpID') : nMEmpID
	, ('followUpTaskID') : followUpTaskID
	, ('isClient') : isClient
	, ('callStatusID') : callStatusID
	, ('appropriateRBClass') : appropriateRBClass
	, ('appropriateRBRate') : appropriateRBRate
	, ('treatmentEnd') : treatmentEnd
	, ('appropriateRBClassChoosen') : appropriateRBClassChoosen
	, ('appropriateRBRateChoosen') : appropriateRBRateChoosen
	, ('treatmentRoomChoosen') : treatmentRoomChoosen
	, ('treatmentRoomAmountChoosen') : treatmentRoomAmountChoosen
	, ('isReferral') : isReferral
	, ('isSpecialCondition') : isSpecialCondition
	, ('referralReasonCode') : referralReasonCode
	, ('callInStart') : callInStart
	, ('secondaryDiagnosisCode') : secondaryDiagnosisCode
	, ('clientClassNo') : clientClassNo
	, ('userPosition') : userPosition
	, ('oPNO') : oPNO
	, ('maternityMedicalTreatment') : maternityMedicalTreatment
	, ('maternityFamilyPlanningItem') : maternityFamilyPlanningItem
	, ('treatmentCode') : treatmentCode
	, ('isFromProcessButton') : isFromProcessButton
	, ('nonMedicalItem') : nonMedicalItem
	, ('isDiagnosisQuestionNotRegistered') : isDiagnosisQuestionNotRegistered
	, ('medicalTreatmentAdditionalQuestion') : medicalTreatmentAdditionalQuestion
	, ('isODS') : isODS
	, ('isODC') : isODC
	, ('isProducttypeChange') : isProducttypeChange
	, ('isTreatmentPeriodChange') : isTreatmentPeriodChange
	, ('isMaternityTreatmentChange') : isMaternityTreatmentChange
	, ('isRoomOptionChange') : isRoomOptionChange
	, ('isTreatmentRBClassChange') : isTreatmentRBClassChange
	, ('isODSODCChange') : isODSODCChange
	, ('isDocValidityChange') : isDocValidityChange
	, ('isDocTypeChange') : isDocTypeChange
	, ('suspectDouble') : suspectDouble])

API.Note (API.getResponseData(GA_ClaimFinalValidation).TransactionalData.ProductType)

if (GA_ClaimFinalValidation.getStatusCode() == 200) {
	if (API.getResponseData(GA_ClaimFinalValidation).Status == true) {
		if (API.getResponseData(GA_ClaimFinalValidation).TransactionalData.ProductType == 'IP') {
			if (API.getResponseData(GA_ClaimFinalValidation).Data.ClaimValidationMessage == 'Perlu pengecekan produk Inpatient (Rawat Inap) peserta oleh tim Garda Medika') {
				API.Note('GA_ClaimFinalValidation Success - Skenario Tidak Punya Produk IP')
			} else {
			API.Note('GA_ClaimFinalValidation Failed - Bukan Skenario Validasi Produk IP')
			}
		} else if (API.getResponseData(GA_ClaimFinalValidation).TransactionalData.ProductType == 'MA') {
			if (API.getResponseData(GA_ClaimFinalValidation).Data.ClaimValidationMessage == 'Member tidak memiliki produk yang terdaftar.') {
				API.Note('GA_ClaimFinalValidation Success - Skenario Tidak Punya Produk MA')
			} else {
				API.Note('GA_ClaimFinalValidation Failed - Bukan Skenario Validasi Produk MA')
			}
		}
	} else {
		API.Note('Terjadi kesalahan pada data!')
	}
} else {
	API.Note('Terjadi kesalahan pada website ! ' + GA_ClaimFinalValidation.getStatusCode())
}


API.Note(API.getResponseData(GA_ClaimFinalValidation).Data)

API.Note(API.getResponseData(GA_ClaimFinalValidation).TransactionalData)

def AllValidation = API.getResponseData(GA_ClaimFinalValidation).Data.AllClaimValidationNonCoverReason

if (AllValidation!=null){​​​​​​​
	if (AllValidation.contains('NONCOVER_DIAGNOSIS')){​​​​​​​
		KeywordUtil.markPassed('NON COVER PASSED') 
	} else {​​​​​​​
		KeywordUtil.markFailed('NON COVER FAILED')}
​​​​} else {​​​​​​​
	KeywordUtil.markFailed('NON COVER IS NULL')
}




​​​​​​​