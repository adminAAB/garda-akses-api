import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.testobject.ResponseObject


String pTrID = '2892485'
String pMemberNo = 'A/00099096'
String pGuid = 'ae758866970c4707b440a96930971a2c9330a0edcc87af9d6f37e64ae77d6271'
String pUserID = 'DOO'
String pIsGLProvider = 'false'
String pProviderName = ''

ResponseObject request = WS.sendRequest(findTestObject('GA/CreateTreatmentGL/GetGLData', [('pTrID') : pTrID , ('pMemberNo') : pMemberNo , ('pGuid') : pGuid , ('pUserID') : pUserID , ('pIsGLProvider') : pIsGLProvider , ('pProviderName') : pProviderName ]))

GlobalVariable.ParsedData = request.getResponseBodyContent()

//RETURN DATA
//AMShowFlag
//AccountManager
//AdditionalDiagnosisInfo
//AdditionalMedicalTreatment
//AnnualLimitValidationF
//AppropriateRBClass
//AppropriateRBClassChoosen
//AppropriateRBRate
//AppropriateRBRateChoosen
//BenefitInfo
//COVID19F
//CallDate
//ClaimOutstandingValidationF
//ClassNo
//ClientClassNo
//ClientID
//ClientName
//DOB
//Diagnosis
//DiagnosisID
//DiagnosisName
//DiagnosisRejectMsgValidationF
//Doctor
//DoctorID
//DoctorOnCall
//Document
//DocumentValidationF
//EmpID
//ExcessValidationF
//FamilyPhone
//GLType
//Guid
//ICULimit
//IsACPProvider
//IsAPTTO
//IsDiagnosisQuestionNotRegistered
//IsNeedChecking
//IsNeedFollowUp
//IsODC
//IsODS
//IsPassedAway
//IsReferral
//IsShowAsProduce
//IsSpecialCondition
//IsSpecialProvider
//IsolationLimit
//LastCondition
//LastTreatment
//MaternityFamilyPlanningItem
//MaternityMedicalTreatment
//MaternityPackagePrice
//MaternityScheme
//MaternityTreatmentCode
//MaxChildValidationF
//MemberName
//MemberNo
//Membership
//MembershipDescription
//NewMemberValidationF
//NonMedicalItems
//NonMember
//PassedAwayDate
//ProductType
//ProductValidationF
//ProviderAdminName
//ProviderEmail
//ProviderExt
//ProviderFax
//ProviderID
//ProviderName
//ProviderPhoneNo
//RBScheme
//ReferralReasonCode
//RemainingLimit
//RemainingLimitType
//Remarks
//ResusitasiF
//RoomOption
//SecondSpouseValidationF
//SpecialConditionReasonCode
//TicketNo
//TotalBilled
//TrID
//TreatmentDuration
//TreatmentDurationSaved
//TreatmentEnd
//TreatmentRBClass
//TreatmentRBClassChoosen
//TreatmentRBRate
//TreatmentRBRateChoosen
//TreatmentStart
//TreatmentTT
