import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.keyword.API as API
import com.kms.katalon.core.testobject.ResponseObject


String pPageNo = '1'
String pSortOption = '-1'
String pSortDirection = '21'
String pTicketNo = ''
String pMemberNo = ''
String pMemberName = ''
String pClientName = ''
String pEmployeeID = ''
String pTreatmentDate = ''
String pProviderID = 'OJKRI00001'
String isGLProvider = '1'
String pStatus = 'Produce'
String pDate = ''

ResponseObject request = WS.sendRequest(findTestObject('GMA/CreateTreatmentGL/InquiryGLData', [('pPageNo') : pPageNo, ('pSortOption') : pSortOption
        , ('pSortDirection') : pSortDirection, ('pTicketNo') : pTicketNo, ('pMemberNo') : pMemberNo, ('pMemberName') : pMemberName
        , ('pClientName') : pClientName, ('pEmployeeID') : pEmployeeID, ('pTreatmentDate') : pTreatmentDate, ('pProviderID') : pProviderID
        , ('isGLProvider') : isGLProvider, ('pStatus') : pStatus, ('pDate') : pDate]))

GlobalVariable.ParsedData = request.getResponseBodyContent()


//RETURN VALUE
//	ClientName
//	ContinueF
//	CreatedDate
//	EmployeeID
//	GAGuid
//	GLType
//	MemberName
//	MemberNo
//	ProductType
//	ProviderID
//	ProviderName
//	Status
//	TicketNo
//	TrID
//	TreatmentDate
//	TreatmentStart