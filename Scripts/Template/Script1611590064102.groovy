import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API

String MemberNo = findTestData('MemberGMAOP').getValue(1, 1)

String dataGetPolicyData = '''{"MemberNo":"'''+ MemberNo +'''"}'''

WebUI.callTestCase(findTestCase('Pages/GMA/root/signin - Sukses'), [:])

def GetPolicyData = WS.sendRequest(findTestObject('Object Repository/GMA/EligibilityDischarge/GetPolicyData',
	[('data') : dataGetPolicyData]))

API.Note(GetPolicyData.getResponseBodyContent())

if (API.getResponseData(GetPolicyData).Status) {
	API.Note((API.getResponseData(GetPolicyData).Data))
	KeywordUtil.markPassed("Passed")
}  else {
	API.Note((API.getResponseData(GetPolicyData).Status))
	API.Note((API.getResponseData(GetPolicyData).Message))
	API.Note((API.getResponseData(GetPolicyData).Data))
	KeywordUtil.markFailedAndStop("Status False")
}

//